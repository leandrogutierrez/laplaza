CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_inf_pedidos_reparto`(pIdReparto INT)
SALIR: BEGIN
	/*
    Informe permite visualizar una planilla de productos y cantidades de los pedidos del dia
    */
    -- Declaración de variables
    DECLARE pZona INT;
    DECLARE pFechaReparto DATE;
    SET @@group_concat_max_len = 5120000;
    
    SET pZona = (SELECT IdZona FROM Zonas INNER JOIN Repartos USING (IdZona) WHERE IdReparto = pIdReparto);
    SET pFechaReparto = (SELECT FechaReparto FROM Repartos WHERE IdReparto = pIdReparto);
    
    SET @pan = NULL;
    SET @pan2 = NULL;
    SET @tortilla = NULL;
    SET @facturas = NULL;
    
		SELECT	CONCAT(
				GROUP_CONCAT(
					DISTINCT  
						CONCAT('SUM(IF(IdProducto = ',pr.IdProducto,', lp.Cantidad, 0)) AS ''',IF(LENGTH(pr.Descripcion) > 12,pr.CodigoProd,pr.Descripcion),'''')
						ORDER BY cp.IdCategoriaProducto ASC, pr.IdProducto ASC) , ', SUM(IF(pr.IdCategoriaProducto = 1, lp.Cantidad, 0))  AS Pan1 '
		) INTO @pan
		FROM	Productos pr 
		INNER JOIN 	CategoriasProducto cp USING(IdCategoriaProducto) 
		WHERE cp.IdCategoriaProducto = 1 ;
        
        SELECT	CONCAT(
				GROUP_CONCAT(
					DISTINCT  
						CONCAT('SUM(IF(IdProducto = ',pr.IdProducto,', lp.Cantidad, 0)) AS ''',IF(LENGTH(pr.Descripcion) > 12,pr.CodigoProd,pr.Descripcion),'''')
						ORDER BY cp.IdCategoriaProducto ASC, pr.IdProducto ASC) , ',','SUM(IF(pr.IdCategoriaProducto = 2, lp.Cantidad, 0))  AS Pan2 '
		) INTO @pan2
		FROM	Productos pr
		INNER JOIN 	CategoriasProducto cp USING(IdCategoriaProducto) 
		WHERE cp.IdCategoriaProducto = 2;
        
        SELECT	CONCAT(
				GROUP_CONCAT(
					DISTINCT  
						CONCAT('SUM(IF(IdProducto = ',pr.IdProducto,', lp.Cantidad, 0)) AS ''',IF(LENGTH(pr.Descripcion) > 12,pr.CodigoProd,pr.Descripcion),'''')
						ORDER BY cp.IdCategoriaProducto ASC, pr.IdProducto ASC), ', SUM(IF(pr.IdCategoriaProducto = 3, lp.Cantidad, 0))  AS Tortillas '
		) INTO @tortillas
		FROM	Productos pr
		INNER JOIN 	CategoriasProducto cp USING(IdCategoriaProducto) 
		WHERE cp.IdCategoriaProducto = 3 ;
        
	   SELECT	CONCAT(
				GROUP_CONCAT(
					DISTINCT  
						CONCAT('SUM(IF(IdProducto = ',pr.IdProducto,', lp.Cantidad, 0)) AS ''',IF(LENGTH(pr.Descripcion) > 12,pr.CodigoProd,pr.Descripcion),'''')
						ORDER BY cp.IdCategoriaProducto ASC, pr.IdProducto ASC) , ', SUM(IF(pr.IdCategoriaProducto = 3, lp.Cantidad, 0))  AS Tortillas '
		) INTO @tortillas
		FROM	Productos pr
		INNER JOIN 	CategoriasProducto cp USING(IdCategoriaProducto) 
		WHERE cp.IdCategoriaProducto = 3 ;
        
        SELECT	CONCAT(
				GROUP_CONCAT(
					DISTINCT  
						CONCAT('SUM(IF(IdProducto = ',pr.IdProducto,', lp.Cantidad, 0)) AS ''',IF(LENGTH(pr.Descripcion) > 12,pr.CodigoProd,pr.Descripcion),'''')
						ORDER BY cp.IdCategoriaProducto ASC, pr.IdProducto ASC), ', SUM(IF(pr.IdCategoriaProducto IN(6,7), lp.Cantidad, 0))  AS Facturas '
		) INTO @facturas
		FROM	Productos pr
		INNER JOIN 	CategoriasProducto cp USING(IdCategoriaProducto) 
		WHERE cp.IdCategoriaProducto IN(6,7) ;
        
        SET @pan = CONCAT(@pan,',',@pan2,',',@tortillas,',',@facturas);
        
    
	SET @pan = CONCAT(	'SELECT  c.Apellidos  Cliente, ', @pan, ' FROM LineasPedido lp 
													INNER JOIN Productos pr USING(IdProducto)
													INNER JOIN Pedidos p USING(IdPedido)
													INNER JOIN Clientes c USING(IdCliente)
                                                    WHERE p.FechaEntrega = ''',pFechaReparto,''' AND c.IdZona = ',pZona,
                                                    ' GROUP BY p.IdCliente 
						UNION
						SELECT  "Total:"  Cliente, ', @pan, ' FROM LineasPedido lp 
						INNER JOIN Productos pr USING(IdProducto)
						INNER JOIN Pedidos p USING(IdPedido)
						INNER JOIN Clientes c USING(IdCliente)
						WHERE p.FechaEntrega = ''',pFechaReparto,''' AND c.IdZona = ',pZona);
               
	PREPARE stmt1 FROM @pan;
	EXECUTE stmt1;
	DEALLOCATE PREPARE stmt1;
END