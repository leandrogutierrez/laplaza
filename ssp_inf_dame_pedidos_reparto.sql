SET @sql = NULL;
SELECT
  GROUP_CONCAT(DISTINCT  
   CONCAT(
      'SUM(IF(IdProducto = ',
      lp.IdProducto,
      ', lp.Cantidad, 0)) AS ''',lp.IdProducto,''''
    )
  ) INTO @sql
FROM	LineasPedido lp
	INNER JOIN	Pedidos p USING(IdPedido)
    INNER JOIN 	Clientes c USING(IdCliente)
	WHERE p.FechaEntrega = '2018-04-26'  AND c.IdZona = 11;

SET @sql = CONCAT('SELECT p.IdCliente, ', @sql, ' FROM LineasPedido lp INNER JOIN Pedidos p USING(IdPedido) GROUP BY p.IdCliente');

SELECT @sql;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;