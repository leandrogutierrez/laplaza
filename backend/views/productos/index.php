<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

                                
$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="box">
        <div class="box-body">
            <div id="errores"></div>
            <div class="row">
                <div class="col-md-8">
                    <?php $form = ActiveForm::begin(['id' => 'busquedaproducto-form', 'layout' => 'inline']) ?>

                    <?= $form->field($busqueda, 'Cadena') ?>

                    <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>

                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarproductos-button']) ?> 

                    <?php ActiveForm::end() ?>
                    <br>

                </div>
                <div class="col-md-4">
                    <div class="col-md-6">
                     <?php if (in_array('AltaProducto', Yii::$app->session->get('Permisos'))) : ?>
                        <button type="button" class="btn btn-primary pull-right"
                                data-modal="<?= Url::to(['productos/alta']) ?>">
                            <i class="fa fa-plus"></i> Nuevo producto
                        </button>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <a href="<?= Url::to(['modo-edicion']) ?>">
                            <button class="btn btn-warning pull-right" type="button">
                                <i class="fa fa-gear"></i>  Modo edición
                            </button>
                        </a>
                    </div>
                </div>  
            </div>
            <div class="row">
                    <?php if (count($models) > 0) : ?>
                    <table class="table table-hover ">
                        <thead>
                            <tr class="tabla-header">
                                <th>Código prod.</th>
                                <th>Descripción</th>
                                <th>Distribuidor</th>
                                <th>Almacen</th>
                                <th>Público</th>
                                <th>Unidad</th> 
                                <th>Categoría</th> 
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($models as $model) : ?>
                            <tr>
                                <td><?= Html::encode($model['CodigoProd']) ?></td>
                                <td><?= Html::encode($model['Descripcion']) ?></td>
                                <td><?= Html::encode($model['PrecioDistribuidor']) ?></td>
                                <td><?= Html::encode($model['PrecioAlmacen']) ?></td>
                                <td><?= Html::encode($model['PrecioPublico']) ?></td>
                                <td><?= Html::encode($model['Unidad']) ?></td>
                                <td><?= Html::encode($model['Categoria']) ?></td>
                                <td><?php switch ($model['Estado']) {
                                    case 'A': echo 'Activo';
                                        break;
                                    case 'B': echo 'Baja';
                                        break;
                                } ?></td>
                                <td>
                                    <div class="btn-group">
                                        <?php if ($model['Estado'] == 'B') : ?>
                                            <?php if (in_array('ActivarProducto', Yii::$app->session->get('Permisos'))) : ?>
                                            <button type="button" class="btn btn-default"
                                                    data-ajax="<?= Url::to(['productos/activar','id' => $model['IdProducto']]) ?>"
                                                    title="Activar">
                                                <i class="fa fa-check-circle" style="color: green"></i>
                                            </button>
                                            <?php endif; ?>
                                        <?php else :?>
                                            <?php if (in_array('DarBajaProducto', Yii::$app->session->get('Permisos'))) : ?>
                                            <button type="button" class="btn btn-default"
                                                    data-ajax="<?= Url::to(['productos/dar-baja','id' => $model['IdProducto']]) ?>"
                                                    title="Desactivar">
                                                <i class="fa fa-minus-circle" style="color: red"></i>
                                            </button>
                                            <?php endif;?>
                                        <?php endif;?> 
                                        <?php if (in_array('ModificarProducto', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-modal="<?= Url::to(['productos/modificar','id' => $model['IdProducto']]) ?>"
                                                title="Modificar">
                                            <i class="fa fa-edit" style="color: dodgerblue"></i>
                                        </button>
                                        <?php endif; ?>
                                        <?php if (in_array('BorrarProducto', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['productos/borrar','id' => $model['IdProducto']]) ?>"
                                                data-mensaje="Desea borrar el producto indicado?">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="pull-right">
                    <?=
                    LinkPager::widget([
                        'pagination' => $paginado,
                        'firstPageLabel' => '<<',
                        'lastPageLabel' => '>> ',
                        'nextPageLabel' => '>',
                        'prevPageLabel' => '<'
                    ]);
                    ?>
                    </div>
                    <?php else :?>
                    <p><strong>No existen productos con el criterio de búsqueda indicado.</strong></p>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</div>