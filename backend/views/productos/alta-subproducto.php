<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;

?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Agregar subproducto</h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'datossubproducto-form',]) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div>
            <?= Html::activeHiddenInput($model, 'IdProducto') ?>
            <div class="col-md-6">
                <p><strong>Descripción: </strong> <?= $model['Descripcion'] ?></p>
            </div>
            <div class="col-md-6">
                <p><strong>Código prod: </strong> <?= $model['CodigoProd'] ?></p>
            </div>
             <?php 
            $url = Url::to(['productos/autocompletar-productos']);

            $initScript = <<<SCRIPT
                function (element, callback) {
                    var id=\$(element).val();
                    if (id && id !== "") {
                        \$.ajax("{$url}?id=" + id , {
                            dataType: "json"
                        }).done(function(data) { callback(data);});
                    }
                    else {
                        callback([]);
                    }
                }
SCRIPT;
            echo $form->field($model, 'IdSubproducto')->widget(Select2::className(), [
                    'options' => [
                        'placeholder' => 'Buscar subproducto...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                            'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                        ],
                        'initSelection' => new JsExpression($initScript),
                    ]
                ]);

            ?>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


