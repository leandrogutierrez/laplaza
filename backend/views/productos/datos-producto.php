<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $model backend\models\Productos */
/* @var $form ActiveForm */
/* @var $this View */

?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'datosproducto-form',]) ?>

        <div class="modal-body">

            <div id="errores-modal"> </div>

            <div class="col-md-6">
                <?= Html::activeHiddenInput($model, 'IdProducto') ?>

                <?= $form->field($model, 'CodigoProd') ?>

                <?= $form->field($model, 'Descripcion') ?>  
                
                <?= $form->field($model, 'IdCategoriaProducto')->dropDownList($categorias) ?>
                
                <div class="col-md-4">
                <?= $form->field($model, 'IdUnidad')->dropDownList($unidades) ?>
                </div>
                <div class="col-md-4">
                <?= $form->field($model, 'UnidadProduccion')->textInput() ?>
                </div>
                <div class="col-md-4">
                <br>
                <?= $form->field($model, 'Equivalencia')->textInput()?>
                </div>
            </div>
            
            <div class="col-md-6">
                <?= $form->field($model, 'PrecioDistribuidor') ?>

                <?= $form->field($model, 'PrecioAlmacen') ?>

                <?= $form->field($model, 'PrecioPublico') ?>
                <?php 
                    
//                $lineas = json_decode($model['Planillas']);
//                echo '<label class="control-label">Planillas</label>';
//                echo Select2::widget([
//                    'name' => 'Planillas',
//                    'id' => 'productos-planillas',
//                    'value' => $lineas,
//                    'data' => ['1'=>'Pedidos de reparto','2'=>'Puntual de reparto','3'=>'Dulces de reparto'],
//                    'options' => [
//                        'placeholder' => 'Buscar planillas...',
//                        'multiple' => true,
//                    ],
//                    'pluginOptions' => [
//
//
//                    ]
//                ]);
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


