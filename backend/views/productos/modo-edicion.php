<?php

use common\assets\ProductosAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
//use function GuzzleHttp\json_encode;

/* @var $this View */
/* @var $form ActiveForm */

$productos = json_encode($productos);
$categorias = json_encode($categorias);
$unidades = json_encode($unidades);

ProductosAsset::register($this);

$this->registerJs("Productos.ModoEdicion.init({$productos},{$categorias},{$unidades})");

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12" v-cloak id="modo-edicion">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('ModificarProducto', Yii::$app->session->get('Permisos'))) : ?>
                    <button type="button" class="btn btn-primary pull-right"
                            @click="guardar()"
                            :disabled="cargando">
                        <i class="fa fa-save"></i> Guardar
                    </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['id' => 'busquedaproducto-form', 'layout' => 'inline']) ?>

                <?= $form->field($busqueda, 'Cadena') ?>

                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>

                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarproductos-button']) ?> 

                <?php ActiveForm::end() ?>
                <br>
                <hr>
                <table class="table table-hover table-condensed table-bordered" style="table-layout: auto" 
                       style="font-size: small" 
                       v-if="arrayProductos.length != 0" >
                    <thead  style="background-color: #d9e9ec">
                        <tr class="tabla-header">
                            <th>Descripción</th>
                            <th>Distribuidor</th>
                            <th>Almacen</th>
                            <th>Público</th>
                            <th>Categoría</th> 
                            <th>Unidad venta</th> 
                            <th>Unidad producc.</th> 
                            <th>Equivaléncia</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(producto,indice) in arrayProductos" v-on="">
                            <td>
                                {{producto.Descripcion}}
                            </td> 
                            <td>
                                <input v-model="producto.PrecioDistribuidor"></input>
                            </td>
                            <td>
                                <input v-model="producto.PrecioAlmacen"></input>
                            </td>
                            <td>
                                <input v-model="producto.PrecioPublico"></input>
                            </td>
                            <td>
                                <select v-model="producto.IdCategoriaProducto">
                                    <option disabled value="">Please select one</option>
                                    <option v-for="(categoria,indica) in arrayCategorias"
                                            v-bind:value="categoria.IdCategoriaProducto">
                                        {{categoria.Categoria}}
                                    </option>
                                </select>
                            </td>
                            <td>
                                <select v-model="producto.IdUnidad">
                                    <option disabled value="">Please select one</option>
                                    <option v-for="(unidad,indica) in arrayUnidades"
                                            v-bind:value="unidad.IdUnidad">
                                        {{unidad.Unidad}}
                                    </option>
                                </select>
                            </td>
                            <td>
                                <input v-model="producto.UnidadProduccion">
                            </td>
                            <td>
                                <input v-model="producto.Equivalencia">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div v-else>
                    <p><strong>No se encontraron productos.</strong></p>
                </div>
            </div>
            <?php if (in_array('ModificarProducto', Yii::$app->session->get('Permisos'))) : ?>
                <button type="button" class="btn btn-primary pull-right"
                        @click="guardar()"
                        :disabled="cargando">
                    <i class="fa fa-save"></i> Guardar
                </button>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>