<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model backend\models\Productos */
/* @var $this yii\web\View */

$this->title = 'Subproductos de "'.$model['Descripcion'].'"';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Productos',
        'url' => ['productos/index']
    ],
    $this->title
];

?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <?php if (in_array('AltaSubproducto', Yii::$app->session->get('Permisos'))) : ?>
                <button type="button" class="btn btn-primary pull-right"
                        data-modal="<?= Url::to(['productos/alta-subproducto','id' => $model['IdProducto']]) ?>">
                    <i class="fa fa-plus"></i> Nuevo subproducto
                </button>
                <?php endif; ?>
                <?php if (count($subproductos) > 0): ?>
                <table class="table table-hover table-condensed" style="font-size: smaller">
                    <thead>
                        <tr>
                            <th>Código prod.</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($subproductos as $subproducto): ?>
                        <tr>
                            <td><?= Html::encode($subproducto['CodigoProd']) ?></td>
                            <td><?= Html::encode($subproducto['Descripcion']) ?></td>
                            <td>
                                <div class="btn-group-sm">
                                    <?php if (in_array('AltaSubproducto', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default"
                                            data-ajax="<?= Url::to(['productos/borrar-subproducto',
                                                'id' => $model['IdProducto'],
                                                'idSubproducto' => $subproducto['IdSubproducto']]) ?>"
                                            data-mensaje="¿Desea eliminar el subproducto?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif ; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <p><strong>No hay subproductos para el producto indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>