<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Conceptos de compra';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaConceptoCompra', Yii::$app->session->get('Permisos'))) : ?>
                <button class="btn btn-primary pull-right" type="button"
                        data-modal="<?= Url::to(['conceptos-compra/alta']) ?>">
                    <i class="fa fa-plus"></i> Nuevo concepto de compra
                </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['layout' => 'inline', 'id' => 'busquedaconceptoscompra-form']) ?>
                
                <?= $form->field($busqueda,'Cadena') ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarconceptoscompra-button']) ?> 
                
                <?php ActiveForm::end() ?>
                
                <?php if(count($models) > 0) : ?>
                <table class="table table-hover">
                    <thead>
                        <tr class="tabla-header">
                            <th>Concepto de compra</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td><?= Html::encode($model['ConceptoCompra']) ?></td>
                            <td>
                                <div class="btn-group">
                                    <?php if (in_array('ModificarConceptoCompra', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-modal="<?= Url::to(['conceptos-compra/modificar','id' => $model['IdConceptoCompra']]) ?>"
                                            title="Modificar">
                                        <i class="fa fa-edit" style="color: dodgerblue"></i>
                                    </button>
                                    <?php endif; ?>
                                    <?php if (in_array('BorrarConceptoCompra', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['conceptos-compra/borrar','id' => $model['IdConceptoCompra']]) ?>"
                                            data-mensaje="¿Desea borrar el concepto de compra indicado?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <p><strong>No existen conceptos de compra que coincidan con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>