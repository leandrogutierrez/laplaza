<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaProveedor', Yii::$app->session->get('Permisos'))) : ?>
                <button class="btn btn-primary pull-right" type="button"
                        data-modal="<?= Url::to(['proveedores/alta']) ?>">
                    <i class="fa fa-plus"></i> Nuevo proveedor
                </button>
                <?php endif; ?>
                
                <?php $form = ActiveForm::begin(['layout' => 'inline', 'id' => 'busquedaproveedores-form']) ?>
                
                <?= $form->field($busqueda,'Cadena') ?>
                
                <?= $form->field($busqueda,'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarproveedores-button']) ?> 
                
                <?php ActiveForm::end() ?>
                
                <?php if(count($models) > 0) : ?>
                <table class="table table-hover">
                    <thead>
                        <tr class="tabla-header">
                            <th>Proveedor</th>
                            <th>CUIT</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td><?= Html::encode($model['Proveedor']) ?></td>
                            <td><?= Html::encode($model['Cuit']) ?></td>
                            <td><?php switch($model['Estado']) {
                                case 'A' : echo 'Activo';
                                    break;
                                case 'B' : echo 'Baja';
                                    break;
                            } ?></td>
                            <td>
                                <div class="btn-group">
                                    
                                    <?php if($model['Estado'] == 'A') : ?>
                                        <?php if (in_array('DarBajaProveedor', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['proveedores/dar-baja', 'id' => $model['IdProveedor']])?>"
                                                title="Desactivar">
                                            <i class="fa fa-minus-circle" style="color: red"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if (in_array('ActivarProveedor', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['proveedores/activar', 'id' => $model['IdProveedor']])?>"
                                                title="Activar">
                                            <i class="fa fa-check-circle" style="color: green"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (in_array('ModificarProveedor', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-modal="<?= Url::to(['proveedores/modificar','id' => $model['IdProveedor']]) ?>"
                                            title="Modificar">
                                        <i class="fa fa-edit" style="color: dodgerblue"></i>
                                    </button>
                                    <?php endif; ?>
                                    <?php if (in_array('BorrarProveedor', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['proveedores/borrar','id' => $model['IdProveedor']]) ?>"
                                            data-mensaje="Desea borrar el proveedor indicado?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <p><strong>No existen proveedores con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>