<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Usuarios;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
                                

$this->title = 'Vendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaVendedor', Yii::$app->session->get('Permisos'))) : ?>
                <button class="btn btn-primary pull-right" type="button"
                        data-modal="<?= Url::to(['vendedores/alta']) ?>">
                    <i class="fa fa-plus"></i> Nuevo vendedor
                </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['layout' => 'inline', 'id' => 'busquedavendedores-form']) ?>
                
                <?= $form->field($busqueda, 'Cadena') ?>
                
                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarvendedores-button']) ?> 
                
                <?php ActiveForm::end() ?>
                
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover ">
                    <thead>
                        <tr class="tabla-header">
                            <th>Apellido/s</th>
                            <th>Nombre/s</th> 
                            <th>Usuario</th> 
                            <th>CUIL</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) :
                        ?>
                        <tr>
                            <td><?= Html::encode($model['Nombres']) ?></td>
                            <td><?= Html::encode($model['Apellidos']) ?></td> 
                            <td><?= Html::encode($model['Usuario']) ?></td> 
                            <td><?= Html::encode($model['Cuil']) ?></td>
                            <td><?php switch ($model['Estado']) {
                                case 'A': echo 'Activo';
                                    break;
                                case 'B': echo 'Baja';
                                    break;
                            } ?></td>
                            <td>
                                <div class="btn-group">
                                    <?php if ($model['Estado'] == 'A') : ?>
                                        <?php if (in_array('DarBajaVendedor', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['vendedores/dar-baja', 'id' => $model['IdVendedor']])?>"
                                                title="Desactivar">
                                            <i class="fa fa-minus-circle" style="color: red"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if (in_array('ActivarVendedor', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['vendedores/activar', 'id' => $model['IdVendedor']])?>"
                                                title="Activar">
                                            <i class="fa fa-check-circle" style="color: green"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (in_array('ModificarVendedor', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-modal="<?= Url::to(['vendedores/modificar','id' => $model['IdVendedor']]) ?>"
                                            title="Modificar">
                                        <i class="fa fa-edit" style="color: dodgerblue"></i>
                                    </button>
                                    <?php endif; ?>
                                    <?php if (in_array('BorrarVendedor', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['vendedores/borrar','id' => $model['IdVendedor']]) ?>"
                                            data-mensaje="¿Desea borrar el vendedor indicado?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <p><strong>No existen clientes que coincidan con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>