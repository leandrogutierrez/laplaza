<?php


use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Html;
use \common\models\Usuarios;

?>
<div class="modal-dialog">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'datosvendedor-form',]) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div>

            <?= Html::activeHiddenInput($model, 'IdVendedor') ?>
            
            <?= $form->field($model, 'Apellidos') ?>
            
            <?= $form->field($model, 'Nombres') ?> 
            
            <?= $form->field($model, 'Telefono') ?> 
            
            <?= $form->field($model, 'Cuil') ?>  
             
            <?php 
                $urlUsuarios = Url::to(['usuarios/autocompletar']);
                echo $form->field($model, 'IdUsuario')->widget(Select2::className(), [
                        'initValueText' => !empty($model->Usuario)? $model->Usuario->Usuario : '', // set the initial display text
                        'options' => [
                            'placeholder' => 'Buscar usuario...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $urlUsuarios,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                            ],
                        ]
                ]);

            ?>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


