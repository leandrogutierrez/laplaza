<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $busqueda common\models\forms\BusquedaForm */

$this->title = 'Roles y Permisos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaRol', Yii::$app->session->get('Permisos'))): ?>
                    <button class="btn btn-primary pull-right" 
                            data-modal="<?= Url::to(['/roles/alta']) ?>">
                        <i class="fa fa-plus"></i> Nuevo Rol
                    </button>
                <?php endif; ?>
                
                <?php $form = ActiveForm::begin(['layout' => 'inline',]); ?>
  
                <?= $form->field($busqueda, 'Cadena') ?> 
                
                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?> 
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarusuario-button']) ?> 

                <?php ActiveForm::end(); ?>
                
                <br>
                
                <?php if (count($models) > 0): ?>
                <table class="table table-hover">
                    <thead>
                        <tr class="tabla-header">
                            <th>Rol</th>
                            <th>Descripción</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model): ?>
                        <tr>
                            <td><?= Html::encode($model['Rol']) ?></td>
                            <td><?= Html::encode($model['Descripcion']) ?></td>
                            <td><?php switch ($model['Estado']) {
                                case 'A': echo 'Activo';
                                    break;
                                case 'B': echo 'Baja';
                                    break;
                                default: echo 'ERROR';
                                    break;
                            }?></td>
                            <td>
                                <div class="btn-group">
                                    <?php if ($model['Estado'] == 'A'): ?>
                                    <button class="btn btn-default" 
                                       data-ajax="<?= Url::to(['/roles/dar-baja' , 'id' => Html::encode($model['IdRol'])])?>"
                                       title="Desactivar">
                                        <i class="fa fa-minus-circle" style="color: red"></i>
                                    </button>
                                    <?php else: ?>
                                    <button class="btn btn-default" 
                                       data-ajax="<?= Url::to(['/roles/activar' , 'id' => Html::encode($model['IdRol'])])?>"
                                       title="Activar">
                                        <i class="fa fa-check-circle" style="color: green"></i>
                                    </button>
                                    <?php endif; ?>
                                    <button class="btn btn-default" 
                                       data-modal="<?= Url::to(['/roles/modificar' , 'id' => Html::encode($model['IdRol'])])?>"
                                       title="Modificar">
                                        <i class="fa fa-pencil" style="color: dodgerblue"></i>
                                    </button>
                                    <button class="btn btn-default" 
                                       data-ajax="<?= Url::to(['/roles/borrar' , 'id' => Html::encode($model['IdRol'])])?>"
                                       data-mensaje="Seguro desea eliminar el rol <?= $model['Rol'] ?>?">
                                       <i class="fa fa-trash"></i>
                                    </button>
                                    <a class="btn btn-default" 
                                       href="<?= Url::to(['/roles/permisos' , 'id' => Html::encode($model['IdRol'])])?>"
                                       title="Ver permisos">
                                        <i class="fa fa-key" style="color: goldenrod"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <strong>No hay roles que coincidan con el criterio de búsqueda</strong>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>