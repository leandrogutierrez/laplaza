<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Permisos de rol: '.$model['Rol'];
$this->params['breadcrumbs'] = [
    [
        'label' => 'Gestión de Usuarios',
        'url'   => ['/usuarios']
    ],
    $this->title,
];
function arbol($permisos, $padre = null)
{
    $edicion = !in_array('ActualizarPermisosRol', Yii::$app->session->get('Permisos'));
    echo '<ul style="list-style-type:none">';
    foreach ($permisos as $permiso) {
        if ($permiso['IdPermisoPadre'] == $padre) {
            echo '<li>';
            if ($permiso['EsHoja'] == 'N') {
                echo '<h4>' . $permiso['Descripcion'] . '</h4>';
                arbol($permisos, $permiso['IdPermiso']);
            } else {
                echo Html::checkbox('Permisos[' . $permiso['IdPermiso'] . ']', $permiso['Estado'] == 'S' ? true : false, ['label' => $permiso['Descripcion'].' ',
                    'class' => 'tree-hoja', 'disabled' => $edicion,]);
                echo '<span class="print-only pull-left"> '. $permiso['Permiso'] .' </span> ';
            }
            echo '</li>';
        }
    }
    echo '</ul>';
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div id="errores"></div>
            
            <div class="box-body">
               
                <?php $form = ActiveForm::begin(['id' => 'permisos-form']) ?>
                <div class='tree-container'>
                    <?php arbol($permisos) ?>
                </div>   
                
                <?= Html::submitButton('Actualizar permisos', [
                    'class' => 'btn btn-primary pull-right',
                    'disabled' => !in_array('ActualizarPermisosRol', Yii::$app->session->get('Permisos'))]) ?>
                
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>