<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
                                
$this->title = 'Zonas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaZona', Yii::$app->session->get('Permisos'))) : ?>
                <button class="btn btn-primary pull-right" type="button"
                        data-modal="<?= Url::to(['zonas/alta']) ?>">
                    <i class="fa fa-plus"></i> Nueva zona
                </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['layout' => 'inline', 'id' => 'busquedazonas-form']) ?>
                
                <?= $form->field($busqueda, 'Cadena') ?>
                
                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarzonas-button']) ?> 
                
                <?php ActiveForm::end() ?>
                
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover ">
                    <thead>
                        <tr class="tabla-header">
                            <th>Zona/s</th>
                            <th>Codigo</th> 
                            <th>Observaciones</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td><?= Html::encode($model['Zona']) ?></td>
                            <td><?= Html::encode($model['Codigo']) ?></td>
                            <td><?= Html::encode($model['Observaciones']) ?></td>   
                            <td><?php switch ($model['Estado']) {
                                case 'A': echo 'Activo';
                                    break;
                                case 'B': echo 'Baja';
                                    break;
                            } ?></td>
                            <td>
                                <div class="btn-group">
                                    <?php if ($model['Estado'] == 'A') : ?>
                                        <?php if (in_array('DarBajaZona', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['zonas/dar-baja', 'id' => $model['IdZona']])?>"
                                                title="Desactivar">
                                            <i class="fa fa-minus-circle" style="color: red"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if (in_array('ActivarZona', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['zonas/activar', 'id' => $model['IdZona']])?>"
                                                title="Activar">
                                            <i class="fa fa-check-circle" style="color: green"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (in_array('ModificarZona', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-modal="<?= Url::to(['zonas/modificar','id' => $model['IdZona']]) ?>"
                                            title="Modificar">
                                        <i class="fa fa-edit" style="color: dodgerblue"></i>
                                    </button>
                                    <?php endif; ?>
                                    <?php if (in_array('BorrarZona', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['Zonas/borrar','id' => $model['IdZona']]) ?>"
                                            data-mensaje="¿Desea borrar la zona?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <p><strong>No existen clientes que coincidan con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>