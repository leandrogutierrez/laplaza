<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$condicionIva = [
    'M' => 'Monotributista',
    'R' => 'Responsable inscripto',
    'E' => 'IVA Excento',
    'F' => 'Consumidor final'
 ];

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
  <!--core first + styles last-->
  <link href="/static/fontawesome/fontawesome-all.css" rel="stylesheet">
</head>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaCliente', Yii::$app->session->get('Permisos'))) : ?>
                <button class="btn btn-primary pull-right" type="button"
                        data-modal="<?= Url::to(['clientes/alta']) ?>">
                    <i class="fa fa-plus"></i> Nuevo cliente
                </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['layout' => 'inline', 'id' => 'busquedaclientes-form']) ?>
                
                <?= $form->field($busqueda, 'Cadena') ?>
                
                <?= $form->field($busqueda, 'Combo')
                           ->dropDownList(ArrayHelper::map($zonas, 'IdZona', 'Zona'), [
                               'prompt' => 'Seleccione Zona...'
                   ]) ?> 
                
                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarclientes-button']) ?> 
                
                <?php ActiveForm::end() ?>
                
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover ">
                    <thead>
                        <tr class="tabla-header">
                            <th>Apellido/s</th>
                            <th>Nombre/s</th>
                            <th>Direccion</th>
                            <th>Teléfono</th> 
                            <th>Zona</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td><?= Html::encode($model['Apellidos']) ?></td>
                            <td><?= Html::encode($model['Nombres']) ?></td>
                            <td><?= Html::encode($model['Direccion']) ?></td>
                            <td><?= Html::encode($model['Telefono']) ?></td> 
                            <td><?= Html::encode($model['Zona'])?></td>
                            <td>
                                <div class="btn-group">
                                    <?php if ($model['Estado'] == 'A') : ?>
                                        <?php if (in_array('DarBajaCliente', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['clientes/dar-baja', 'id' => $model['IdCliente']])?>"
                                                title="Desactivar">
                                            <i class="fa fa-minus-circle" style="color: red"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if (in_array('ActivarCliente', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['clientes/activar', 'id' => $model['IdCliente']])?>"
                                                title="Activar">
                                            <i class="fa  fa-check-circle" style="color: green"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (in_array('ModificarCliente', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-modal="<?= Url::to(['clientes/modificar','id' => $model['IdCliente']]) ?>"
                                            title="Modificar">
                                        <i class="fa fa-edit" style="color: dodgerblue"></i>
                                    </button>
                                    <?php endif; ?>
                                    <?php if (in_array('BorrarCliente', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['clientes/borrar','id' => $model['IdCliente']]) ?>"
                                            data-mensaje="¿Desea borrar el cliente indicado?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                        <?=
                        LinkPager::widget([
                            'pagination' => $paginado,
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>> ',
                            'nextPageLabel' => '>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
                </div>
                <?php else: ?>
                <p><strong>No existen clientes que coincidan con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>