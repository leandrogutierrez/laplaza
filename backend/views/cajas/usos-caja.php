 <?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\LinkPager;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
 
$this->title = 'Cajas';

$this->params['breadcrumbs'][] = $this->title;
$estados = [
    'T' => 'Todas',
    'C' => 'Cerreadas',
    'A' => 'Abiertas',
    'B' => 'Bajas',
];
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body"> 
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover">
                    <thead>
                        <tr class="tabla-header">
                            <th>Estado </th> 
                            <th>Caja</th> 
                            <th>Usuario</th>
                            <th>Apertura</th>
                            <th>Cierre</th>
                            <th>Nro Uso Caja</th>   
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td>
                                <?php if ($model['Estado'] == 'A'): ?> 
                                    <i class="fa fa-circle fa-2x" style="color:MediumSeaGreen"  ></i> 
                                <?php else :?>   
                                    <i class="fa fa-circle fa-2x" style="color:Maroon  "> </i> 
                                <?php endif?> 
                            </td>
                            <td><?= Html::encode($model['Caja']) ?></td> 
                            <td><?= Html::encode($model['Usuario']) ?></td>
                            <td><?= Html::encode($model['FechaInicio']) ?></td> 
                            <td><?= Html::encode($model['FechaFin']) ?></td> 
                            <td><?= Html::encode($model['NroUsoCaja']) ?></td> 
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran ventas en el sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

