 <?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\LinkPager;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Cajas';

$this->params['breadcrumbs'][] = $this->title;
$estados = [
    'T' => 'Todas',
    'C' => 'Cerreadas',
    'A' => 'Abiertas',
    'B' => 'Bajas',
];
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body"> 
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="tabla-header">
                                <th style="text-align:center">Estado </th> 
                                <th>Caja</th> 
                                <th>Usuario</th>
                                <th>Apertura</th> 
                                <th style="text-align:center">Acciones</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($models as $model) : ?>
                            <tr>
                                 <td align="center">
                                    <?php if ($model['Estado'] == 'A'): ?> 
                                        <i class="fa fa-circle fa-2x" style="color:#4cae4c" title="Abierta" ></i> 
                                    <?php else :?>   
                                        <i class="fa fa-circle fa-2x" style="color:FireBrick" title="Cerrada"> </i> 
                                    <?php endif?> 
                                </td>
                                <td><?= Html::encode($model['Caja']) ?></td> 
                                <td><?= Html::encode($model['Usuario']) ?></td>
                                <td><?= Html::encode($model['FechaInicio']) ?></td>  
                                <td align="center">
                                    
                                <div class="btn-group<?php if (\Yii::$app->devicedetect->isMobile()) :
                                            echo "-vertical ";
                                        endif;
                                    ?>">
                                    <?php if ($model['Estado'] == 'A'): ?> 
                                        <button class="btn btn-default pull-center" type="button" title="Rendir caja" >
                                            <i class="fa fa-money "></i>
                                        </button> 
                                    <?php endif;?>
                                    <?php if ($model['Estado'] == 'C'): ?> 
                                        <button class="btn btn-default pull-center" type="button" title="Balance caja">
                                            <i class="fa fa-balance-scale  "></i>
                                        </button> 
                                    <?php endif;?>
                                    <button class="btn btn-default pull-center" type="button" title="Ver uso de caja">
                                        <i class="fa fa-search  "></i>
                                    </button> 
                                </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran ventas en el sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

