<?php

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faenas */
/* @var $form yii\bootstrap\ActiveForm  */
?>
<div class="modal-dialog modal-lg" id="datos-venta">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Reparto <?= $model['Zona'] . ' - ' .$model->Usuario ?></h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'ventasreparto-form',]) ?>
        <div class="modal-body">
            <div id="errores-modal"> </div>
            <?php if ($edicionPrecios == 'S'):?>
                <div class="alert  alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>El reparto posee ventas con precios modificados</strong> 
                </div>
            <?php  endif;?>
            <?php if (count($ventas) > 0): ?>
                <div class="col-md-12"> 
                    <table class="table table-hover table-condensed table-bordered"> 
                        <thead>
                            <tr style="background-color: #C0D8C6">
                                <th>Nº venta</th>  
                                <th>Cliente</th>
                                <th>Importe</th>
                                <th>Fecha Venta</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($ventas as $venta): ?>
                                <?php $lineasVenta = json_decode($venta['LineasVenta']) ?>
                                <tr data-toggle="collapse" 
                                    data-target="<?= '#venta' . $venta['IdVenta'] ?>" 
                                    <?php if ($venta['EdicionPrecios'] == 'S'): ?>
                                        style="background-color:#f2dede"
                                    <?php endif;?>
                                    class="accordion-toggle">
                                    <td><?= Html::encode($venta['NroVenta']) ?></td>  
                                    <td><?= Html::encode($venta['Cliente']) ?></td> 
                                    <td><?= Html::encode($venta['Importe']) ?></td> 
                                    <td><?= Html::encode($venta['FechaAlta']) ?></td> 
                                </tr>
                                <tr>
                                    <td>
                                        <div class="accordion-body collapse" id="<?= 'venta' . $venta['IdVenta'] ?>"> 
                                            <table class="table table-condensed table-bordered" style="font-size: small">
                                                <caption>Detalle del venta</caption>
                                                <thead>
                                                    <tr style="background-color: #C0C2D8">
                                                        <th>Código</th>
                                                        <th>Descripción</th>
                                                        <th>Unidad</th>
                                                        <th>Cantidad</th>
                                                        <th>Precio</th>
                                                        <th>Subtotal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (count($lineasVenta) > 0) : ?>
                                                    <?php foreach ($lineasVenta as $linea): ?>
                                                        <tr>
                                                            <td><?= $linea->CodigoProd ?></td>
                                                            <td><?= $linea->Descripcion ?></td>
                                                            <td><?= $linea->Unidad ?></td>
                                                            <td><?= $linea->Cantidad ?></td>
                                                            <td><?= $linea->Precio?></td>
                                                            <td><?= $linea->Importe?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    <?php endif;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table> 
                </div>
            <?php else: ?>
                <p><strong>El reparto no posee ventas.</strong></p>
            <?php endif; ?>
            <?php if (in_array('AltaVenta', Yii::$app->session->get('Permisos')) && $model['Estado'] == 'A') : ?>
                <?= Html::a('Nueva venta', Url::to(['ventas/alta','IdReparto' => $model['IdReparto']]), ['class'=>'btn btn-primary grid-button']) ?>
            <?php endif; ?>
            <div class="clearfix"></div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
        </div>
        <?php ActiveForm::end(); ?>
    </div>  
</div>s