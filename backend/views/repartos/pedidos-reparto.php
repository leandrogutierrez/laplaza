<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faenas */
/* @var $form yii\bootstrap\ActiveForm  */
 
?>

<div class="modal-dialog modal-lg" id="datos-pedido">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?> - Reparto <?= $reparto['Zona'].$reparto['Estado']  ?></h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'pedidosreparto-form',]) ?>
        <div class="modal-body">
            <div id="errores-modal"> </div>
             
            <?= Html::activeHiddenInput($model, 'IdReparto') ?>    
            <?= Html::activeHiddenInput($model, 'LineasDespacho') ?>    
            <?= Html::activeHiddenInput($model, 'Pedidos') ?>    
            <?php if (count($lineasPedido) > 0): ?>
                <div class="col-md-12"> 
                    <table class="table table-hover table-condensed table-bordered"> 
                        <thead>
                            <tr style="background-color: #C0D8C6">
                                <th>Codigo</th> 
                                <th>Descripcion</th>
                                <th>Unidad</th>
                                <th>Cantidad</th>  
                            </tr>
                        </thead>
                        <tbody> 
                        <?php foreach ($lineasPedido as $linea): ?>
                            <tr>
                                <td><?= $linea['CodigoProd'] ?></td>
                                <td><?= $linea['Descripcion']?></td>
                                <td><?= $linea['Unidad']?></td>
                                <td><?= $linea['Cantidad'] ?></td>
                            </tr>
                        <?php endforeach; ?> 
                        </tbody>
                    </table> 
                </div>
            <?php else: ?>
                <p><strong>El reparto no posee pedidos pendientes de despacho.</strong></p>
            <?php endif; ?> 
                 <?php if (count($lineasPedido) > 0 && in_array('AltaDespacho', Yii::$app->session->get('Permisos'))): ?>
                    <?= Html::submitButton('Despachar todo', ['class' => 'btn btn-primary',]) ?>  
                 <?php endif; ?>  
            <div class="clearfix"></div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
        </div>
        <?php ActiveForm::end(); ?>
    </div>  
</div>