<?php

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faenas */
/* @var $form yii\bootstrap\ActiveForm  */
?>
<div class="modal-dialog modal-lg" id="datos-despacho">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?> - Reparto <?= $model['Zona'] ?></h4>
        </div>
        
        <?php $form = ActiveForm::begin(['id' => 'despachosreparto-form',]) ?>
          
        <div class="modal-body">
            <div id="errores-modal"> </div>
            <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#home">Despachos</a></li>
               <li><a data-toggle="tab" href="#menu1">Pedidos</a></li> 
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <h3> </h3>
                    <?php if (count($despachos) > 0) : ?>
                        <div class="col-md-12"> 
                          <table class="table table-hover table-condensed table-bordered"> 
                              <thead>
                                  <tr style="background-color: #C0D8C6">
                                      <th>Nº Despacho</th> 
                                      <th>Despachante</th>
                                      <th>Fecha Despacho</th>  
                                      <th>Acciones</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($despachos as $despacho): ?>
                                      <?php $lineasDespacho = json_decode($despacho['LineasDespacho']) ?>
                                      <tr>
                                          <td data-toggle="collapse" 
                                          data-target="<?= '#despacho' . $despacho['IdDespacho'] ?>" 
                                          class="accordion-toggle"><?= Html::encode($despacho['NroDespacho']) ?></td> 
                                          <td data-toggle="collapse" 
                                          data-target="<?= '#despacho' . $despacho['IdDespacho'] ?>" 
                                          class="accordion-toggle"><?= Html::encode($despacho['Despachante']) ?></td>
                                          <td data-toggle="collapse" 
                                          data-target="<?= '#despacho' . $despacho['IdDespacho'] ?>" 
                                          class="accordion-toggle"><?= Html::encode($despacho['FechaAlta']) ?></td> 
                                          <td align="center">
                                            <?php if(in_array('AnularDespacho', Yii::$app->session->get('Permisos')) && $despacho['Estado'] != 'B'): ?>
                                                <button class="btn btn-default" type="button"
                                                    data-ajax="<?= Url::to(['anular-despacho','id' => $despacho['IdDespacho']]) ?>"
                                                    title="Anular">
                                                    <i class="fa fa-ban"></i> 
                                                </button>
                                            <?php endif; ?>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <div class="accordion-body collapse" id="<?= 'despacho' . $despacho['IdDespacho'] ?>"> 
                                                  <table class="table table-condensed table-bordered" style="font-size: small">
                                                    <?php if ($despacho['Estado'] != 'B'): ?>
                                                        <caption>Detalle del despacho</caption>
                                                        <thead>
                                                          <tr style="background-color: #C0C2D8">
                                                              <th>Código</th>
                                                              <th>Descripción</th>
                                                              <th>Unidad</th>
                                                              <th>Cantidad</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>  
                                                            <?php if ( count($lineasDespacho) > 0): ?>
                                                                <?php foreach ($lineasDespacho as $linea): ?>
                                                                    <tr>
                                                                        <td><?= $linea->CodigoProd ?></td>
                                                                        <td><?= $linea->Descripcion ?></td>
                                                                        <td><?= $linea->Unidad ?></td>
                                                                        <td><?= $linea->Cantidad ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            <?php endif ?>
                                                            
                                                        </tbody>
                                                    <?php else: ?>
                                                        El despacho fue anulado
                                                    <?php endif ?>
                                                  </table>
                                              </div>
                                          </td>
                                      </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table> 
                      </div> 
                    <?php else: ?>
                        <p><strong>El reparto no posee despachos.</strong></p>
                    <?php endif; ?> 
                    <?php if (in_array('AltaDespacho', Yii::$app->session->get('Permisos')) && $model['Estado'] == 'A') : ?>
                        <?= Html::a('Nuevo despacho', Url::to(['pedidos/index']), ['class'=>'btn btn-primary grid-button']) ?>
                    <?php endif; ?> 
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3> </h3>
                    <?php if (count($pedidos) > 0) : ?>
                        <div class="col-md-12"> 
                          <table class="table table-hover table-condensed table-bordered"> 
                              <thead>
                                  <tr style="background-color: #C0D8C6">
                                      <th>Nº Pedido</th> 
                                      <th>Usuario</th>
                                      <th>Fecha Pedido</th>  
                                      <th  style="text-align:center">Despachar</th>  
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($pedidos as $pedido): ?>
                                      <?php $lineasPedido = json_decode($pedido['LineasPedido']) ?>
                                      <tr data-toggle="collapse" 
                                          data-target="<?= '#pedido' . $pedido['IdPedido'] ?>" 
                                          class="accordion-toggle">
                                          <td><?= Html::encode($pedido['NroPedido']) ?></td> 
                                          <td><?= Html::encode($pedido['Usuario']) ?></td>
                                          <td><?= Html::encode($pedido['FechaAlta']) ?></td> 
                                          <td align="center" >
                                            <?php if ($pedido['Estado'] == 'A'):
                                                if (in_array('AltaDespacho', Yii::$app->session->get('Permisos'))) : ?>
                                                    <button class="btn btn-default" type="button"
                                                            data-modal="<?= Url::to(['pedidos/despachar','id' => $pedido['IdPedido'],'reparto'=>$model['IdReparto']]) ?>"
                                                            title="Despachar">
                                                            <i class="fa fa-truck" ></i> 
                                                    </button>
                                                <?php endif;
                                            endif;
                                            ?> 
                                          </td> 
                                      </tr>
                                      <tr>
                                          <td>
                                              <div class="accordion-body collapse" id="<?= 'pedido' . $pedido['IdPedido'] ?>"> 
                                                  <table class="table table-condensed table-bordered" style="font-size: small">
                                                      <caption>Detalle del pedido</caption>
                                                      <thead>
                                                          <tr style="background-color: #C0C2D8">
                                                              <th>Código</th>
                                                              <th>Descripción</th>
                                                              <th>Unidad</th>
                                                              <th>Cantidad</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <?php if (count($lineasPedido) > 0): ?>
                                                              <?php foreach ($lineasPedido as $linea): ?>
                                                                  <tr>
                                                                      <td><?= $linea->CodigoProd ?></td>
                                                                      <td><?= $linea->Descripcion ?></td>
                                                                      <td><?= $linea->Unidad ?></td>
                                                                      <td><?= $linea->Cantidad ?></td>
                                                                  </tr>
                                                              <?php endforeach; ?>
                                                          <?php endif ?>
                                                      </tbody>
                                                  </table>
                                              </div>
                                          </td>
                                      </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table> 
                      </div>
                    <?php else: ?>
                        <p><strong>El reparto no pedidos despachados.</strong></p>
                    <?php endif; ?> 
                </div>
              
            </div> 
            
            <div class="clearfix"></div> 
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
        </div>
        
        <?php ActiveForm::end(); ?>
        
    </div>  
</div>