<?php

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\Html;

?>
<div class="modal-dialog modal-lg" id="arribo-reparto">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?> - Reparto <?= $reparto['Zona']?></h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'arriboreparto-form',]) ?>
        <div class="modal-body"> 
            <div id="errores-modal"> </div>
<!--        <?php if ($edicionPrecios == 'S'):?>
                <div class="alert  alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>El reparto posee ventas con precios modificados</strong> 
                </div>
            <?php  endif;?> -->
            <div class="row">
                <div class="col-md-6">      
                    <a class="btn btn-danger no-print"
                       target="_blank"
                       href="<?= Url::to(['imprimir-precios','id' => $reparto['IdReparto']]) ?>">
                        <i class="fa fa-exclamation-triangle"></i> Ver variación de precios
                    </a> 
                </div>  
            </div>  
            </br>
            <?php if ($arribo != null): ?>
            <div class="row">
                <div class="col-md-12"> 
                    <table class="table table-hover table-condensed table-bordered"> 
                        <thead>
                            <tr style="background-color: #C0D8C6"> 
                                <th>Recepción</th>
                                <th>Fecha Arribo</th>  
                                <th>Estado</th>  
                                <th>Acciones</th>  
                            </tr> 
                        </thead>
                        <tbody> 
                            <?php $lineasArribo = json_decode($arribo['LineasArribo']) ?>
                            <tr class="accordion-toggle"> 
                                
                                <td data-toggle="collapse" 
                                    data-target="<?= '#arribo' . $arribo['IdArribo'] ?>" >
                                    <?= Html::encode($arribo['Despachante']) ?>
                                </td>
                                <td data-toggle="collapse" 
                                    data-target="<?= '#arribo' . $arribo['IdArribo'] ?>" > 
                                    <?= Html::encode($arribo['FechaArribo']) ?>
                                </td> 
                                <td data-toggle="collapse" 
                                    data-target="<?= '#arribo' . $arribo['IdArribo'] ?>" 
                                    <?= Html::encode($arribo['Estado']) ?>>
                                </td>
                              
                                <td <?= Html::encode($arribo['Despachante']) ?>
                                    align="center">
                                    <?php if(in_array('AnularArribo', Yii::$app->session->get('Permisos')) && $arribo['Estado'] != 'B'): ?>
                                        <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['anular-arribo','id' => $arribo['IdArribo']]) ?>"
                                            title="Anular">
                                            <i class="fa fa-ban"></i> 
                                        </button>
                                    <?php endif; ?> 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="accordion-body collapse" id="<?= 'arribo' . $arribo['IdArribo'] ?>"> 
                                        <table class="table table-condensed table-bordered" style="font-size: small">
                                            <caption>Detalle del arribo</caption>
                                            <thead>
                                                <tr style="background-color: #C0C2D8">
                                                    <th>Código</th>
                                                    <th>Descripción</th>
                                                    <th>Unidad</th>
                                                    <th>Cantidad</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <?php if ($lineasArribo != null):
                                                    foreach ($lineasArribo as $linea): ?>
                                                        <tr>
                                                            <td><?= $linea->CodigoProd ?></td> 
                                                            <td><?= $linea->Descripcion ?></td> 
                                                            <td><?= $linea->Unidad ?></td> 
                                                            <td><?= $linea->Cantidad ?></td>
                                                        </tr>
                                                    <?php endforeach;
                                                endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr> 
                        </tbody>
                    </table> 
                </div>
            </div>
            <?php else: ?>
                <p><strong>El reparto no posee arribo.</strong></p>
            <?php endif; ?> 
            <div class="clearfix"></div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
        </div>
        <?php ActiveForm::end(); ?>
    </div>  
</div>