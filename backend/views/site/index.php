<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'La Plaza';
?>
<html>
    
</html>
<div class="site-index"> 
    <?php if (count($notificaciones)> 0): ?>
        <?php  foreach ($notificaciones as $notificacion): ?>
            <?php $string = '<div class="alert '. $notificacion['Tipo'] .' alert-dismissible">';?>
            <?= $string ?>
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= $notificacion['Titulo'] . ': '?></strong> 
            <?= $notificacion['Notificacion']?>
            <?= '</div>'?>
        <?php endforeach;?>
    <?php endif;?>
    <div class="body-content">
        <div class="row">
            <div class="col-md-6 margin">
                <div class="row"> 
                    <h2>Repartos</h2>    
                    <?php if (in_array('AltaReparto', Yii::$app->session->get('Permisos'))) : ?>
                    <button class="btn btn-primary pull-right" type="button"
                            data-modal="<?= Url::to(['repartos/alta']) ?>">
                        <i class="fa fa-plus"></i> Nuevo reparto
                    </button>
                    <?php endif; ?>
                </div> 
                <p></p>
                <div class="row">
                    <table class="table  table-hover table-bordered texto">
                        <thead>
                            <tr class="tabla-header"> 
                                <th>Zona</th>
                                <th>Usuario</th>  
                                <th>Fecha</th>  
                                <th style="text-align:center">Pendientes</th>
                                <th style="text-align:center">Ver raparto</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($repartos)> 0):
                                foreach ($repartos as $model): ?>
                            <tr> 
                                <td><?= Html::encode($model['Zona']) ?></td> 
                                <td><?= Html::encode($model['Usuario']) ?></td> 
                                <td><?= Html::encode($model['FechaReparto']) ?></td> 
                                <td align="center">
                                    <?php if ($model['Estado'] != 'V'): ?>
                                    <button class="btn pull-center btn-success" type="button"
                                            data-modal="<?= Url::to(['repartos/despacho-masivo', 'id' => $model['IdReparto']]) ?>"
                                                title="Pedidos pendientes">
                                            <i class="fa fa-file-text-o"></i> 
                                    </button>   
                                    <?php endif?>
                                </td>
                                <td align="center">
                                    <div class="dropdown">
                                        <button class="btn btn-default  dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            Acciones
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a 
                                                data-modal="<?= Url::to(['repartos/ver-despachos-reparto', 'id' => $model['IdReparto']]) ?>"
                                                >Ver despachos</a>
                                            </li>
                                            <?php if ($model['Estado'] == 'V'): ?>
                                                <li>
                                                    <a data-modal="<?= Url::to(['repartos/ver-arribo-reparto', 'id' => $model['IdReparto']]) ?>"
                                                    >Ver Arribo</a>
                                                </li>
                                            <?php elseif (in_array('AltaArribo', Yii::$app->session->get('Permisos'))):?>
                                                <li>
                                                    <a data-modal="<?= Url::to(['repartos/arribo-reparto', 'id' => $model['IdReparto']]) ?>"
                                                    >Dar Arribo</a>
                                                </li>
                                            <?php endif?>
                                            <li>
                                                <a data-modal="<?= Url::to(['repartos/ver-ventas-reparto', 'id' => $model['IdReparto']]) ?>"
                                                >Ver Ventas</a>
                                            </li>
                                            <?php if (in_array('BuscarRepartos', Yii::$app->session->get('Permisos'))) : ?>
                                            <li>
                                                <a 
                                                data-modal="<?= Url::to(['repartos/planillas', 'id' => $model['IdReparto']]) ?>"
                                                >Planillas</a>
                                            </li>
                                            <?php endif; ?>  
                                        </ul>
                                    </div>
                                <div class="btn-group<?php if (Yii::$app->devicedetect->isMobile()) :
                                            echo "-vertical ";
                                        endif;
                                ?>">  
                                </div>
                                </td>  
                            </tr> 
                            <?php endforeach;
                            endif;?>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <?=
                        LinkPager::widget([
                            'pagination' => $paginado,
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>> ',
                            'nextPageLabel' => '>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
                    </div>
                <p><a class="btn btn-default" href="<?= Url::to(['repartos/index'])?>">Ir &raquo;</a></p>
                </div>  
            </div>  
            <?php if (in_array('BuscarCajas', Yii::$app->session->get('Permisos'))) : ?>
                <div class="col-md-6 margin"  >
                <div class="row">
                    <h2>Cajas</h2>   
                        <a class="btn btn-primary pull-right" type="button"
                            href="<?= Url::to(['cajas/index']) ?>">
                        <i class="fa fa-search"></i> Ver cajas
                        </a>
                   
                </div> 
                <p></p>
                <div class="row"> 
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr class="tabla-header">
                                <th style="text-align:center">Estado </th> 
                                <th>Caja</th> 
                                <th>Usuario</th>
                                <th>Apertura</th> 
                                <th style="text-align:center">Acciones</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cajas as $model) : ?>
                            <tr>
                                 <td align="center">
                                    <?php if ($model['Estado'] == 'A'): ?> 
                                        <i class="fa fa-circle fa-2x" style="color:#4cae4c" title="Abierta" ></i> 
                                    <?php else :?>   
                                        <i class="fa fa-circle fa-2x" style="color:FireBrick" title="Cerrada"> </i> 
                                    <?php endif?> 
                                </td>
                                <td><?= Html::encode($model['Caja']) ?></td> 
                                <td><?= Html::encode($model['Usuario']) ?></td>
                                <td><?= Html::encode($model['FechaInicio']) ?></td>  
                                <td align="center">
                                    
                                <div class="btn-group
                                    <?php if (Yii::$app->devicedetect->isMobile()) :
                                            echo "-vertical ";
                                        endif;
                                    ?>">
                                    <?php if ($model['Estado'] == 'A'): ?> 
                                        <a class="btn btn-default pull-center" type="button" 
                                                href="<?= Url::to(['cajas/ver-movimientos-caja', 'IdCaja' => $model['IdCaja'],'NroUsoCaja' => $model['NroUsoCaja']]) ?>"
                                                accesskey=""title="Ver uso de caja abierto">
                                            <i class="fa fa-money  "></i>
                                        </a>  
                                        <button class="btn btn-default pull-center" type="button" 
                                            data-ajax="<?= Url::to(['/cajas/cerrar','IdCaja' => $model['IdCaja'],'NroUsoCaja' => $model['NroUsoCaja']]) ?>"
                                            data-mensaje="¿Desea cerrar caja?"> 
                                            <i class="fa fa-sign-out"></i>  
                                        </button>
                                    <?php elseif ($model['Estado']== 'C'): ?>
                                        <a class="btn btn-default pull-center" type="button" 
                                                href="<?= Url::to(['cajas/ver-uso-caja', 'IdCaja' => $model['IdCaja'],'NroUsoCaja' => $model['NroUsoCaja']]) ?>"
                                                accesskey=""title="Balance de ultimo uso">
                                            <i class="fa fa-balance-scale  "></i>
                                        </a> 
                                        <a class="btn btn-default pull-center" type="button" 
                                                href="<?= Url::to(['cajas/rendir-caja', 'IdCaja' => $model['IdCaja'],'NroUsoCaja' => $model['NroUsoCaja']]) ?>"
                                                accesskey=""title="Rendir uso caja">
                                            <i class="fa fa-handshake-o  "></i>
                                        </a>  
                                    <?php endif;?>
                                </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <?=
                        LinkPager::widget([
                            'pagination' => $paginado,
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>> ',
                            'nextPageLabel' => '>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
                    </div>  
                </div>
            </div>
             <?php endif; ?>
        </div>   
    </div>
</div>
 