<?php

use yii\helpers\Html;

use yii\helpers\Url;

$lineasPedido = json_decode($pedido['LineasPedido']);
$lineasPedidoHijo = json_decode($pedidoHijo['LineasPedido']);
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>    
        <div class="modal-body">
            <div id="errores-modal"> </div>
            <?= Html::activeHiddenInput($pedido, 'IdPedido') ?>
            <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">Pedido Nro: <?= Html::encode($pedido['NroPedido']) ?> 
                    <strong class="pull-right"> Pedido original (<?= Html::encode($pedido['Estado'])?>)</strong>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><strong>Usuario:</strong> <?= Html::encode($pedido['Usuario']) ?></div>
                    </div>             
                    <div class="col-md-12">
                        <div><strong>Cliente:</strong> <?= Html::encode($pedido['Cliente']) ?></div>
                    </div>
                    <div class="col-md-12">
                        <div><strong>Fecha alta:</strong><?= Html::encode($pedido['FechaAlta']) ?> </div>
                    </div> 
                </div>
            </div> 
            <?php if (count($lineasPedido) > 0) : ?>
            <table class="table table-hover table-condensed table-bordered ">
                <thead>
                    <tr style="background-color: #C0D8C6"> 
                        <th>Descripción</th>
                        <th>Cantidad</th>  
                        <th>Unidad</th>  
                        <th>Precio</th>  
                        <th>Importe</th>  
                    </tr>
                </thead> 
                <tbody>
                    <?php foreach ($lineasPedido as $linea) : ?>
                    <tr>
                        <td><?= Html::encode($linea->Descripcion) ?></td>
                        <td><?= Html::encode($linea->Unidad) ?></td>
                        <td><?= Html::encode($linea->Cantidad) ?></td> 
                        <td>$ <?= Html::encode($linea->Precio) ?></td> 
                        <td>$ <?= Html::encode($linea->Importe) ?></td>  
                            
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            <p><strong>Error al cargar la venta.</strong></p>
            <?php endif; ?>
            </div> 
            <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">Pedido Nro: <?= Html::encode($pedidoHijo['NroPedido']) ?> 
                    <strong class="pull-right"> Pedido nuevo (<?= Html::encode($pedidoHijo['Estado'])?>)</strong> 
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><strong>Usuario:</strong> <?= Html::encode($pedidoHijo['Usuario']) ?></div> 
                    </div>             
                    <div class="col-md-12">
                        <div><strong>Cliente:</strong> <?= Html::encode($pedidoHijo['Cliente']) ?></div>
                    </div>
                    <div class="col-md-12">
                        <div><strong>Fecha alta:</strong><?= Html::encode($pedidoHijo['FechaAlta']) ?> </div>
                    </div> 
                </div>
            </div> 
            <?php if (count($lineasPedido) > 0) : ?>
            <table class="table table-hover table-condensed table-bordered ">
                <thead>
                    <tr style="background-color: #C0D8C6"> 
                        <th>Descripción</th>
                        <th>Cantidad</th>  
                        <th>Unidad</th>  
                        <th>Precio</th>  
                        <th>Importe</th>  
                    </tr>
                </thead> 
                <tbody>
                    <?php foreach ($lineasPedidoHijo as $linea) : ?>
                    <tr>
                        <td><?= Html::encode($linea->Descripcion) ?></td>
                        <td><?= Html::encode($linea->Unidad) ?></td>
                        <td><?= Html::encode($linea->Cantidad) ?></td> 
                        <td>$ <?= Html::encode($linea->Precio) ?></td> 
                        <td>$ <?= Html::encode($linea->Importe) ?></td>  
                            
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            <p><strong>Error al cargar la venta.</strong></p>
            <?php endif; ?>
            </div> 
            <div class="clearfix"></div>
        </div> 
        <div class="modal-footer">
            <?php if (in_array('ModificarPedido', Yii::$app->session->get('Permisos')) && $pedido['Estado'] == 'A') : ?>
                <?= Html::a('Modificar', Url::to(['pedidos/modificar','id' => $pedido['IdPedido']]), ['class'=>'btn btn-primary grid-button']) ?>
            <?php endif; ?> 
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>