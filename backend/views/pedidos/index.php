<?php

use common\assets\PedidosAsset;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

/* @var $this View */
/* @var $form ActiveForm */

$this->title = 'Gestión de pedidos';
$this->params['breadcrumbs'][] = $this->title;

PedidosAsset::register($this);
$this->registerJs('Pedidos.Alta.init()');

$estados = [
    'T' => 'Todos',
    'A' => 'Activo',
    'D' => 'Despachado',
    'B' => 'Baja',
    'M' => 'Modificado',
];
 
$this->title = 'Pedidos';
?>
<div class="row"  id="pedidos">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body"> 
                    <div class="row">
                        <?php if (in_array('AltaPedido', Yii::$app->session->get('Permisos'))) : ?>
                        <a href="<?= Url::to(['pedidos/alta']) ?>">
                            <button class="btn btn-primary pull-right" type="button" >
                            <i class="fa fa-plus"></i> Nuevo pedido
                            </button>
                        </a>
                        <?php endif; ?>
                    </div>
                    <div class="row">
                    <?php $form = ActiveForm::begin(['id' => 'buscarpedidos-form',
                                                    'method' => 'get',
                                                    'layout' => 'inline']) ?>

                    <div class="col-md-4">
                       <?php 
                        $urlClientes = Url::to(['clientes/autocompletar']);
                        echo $form->field($busqueda, 'Combo3')->widget(Select2::className(), [
                                'data' => $clientes,
                                'options' => [
                                  'placeholder' => 'Buscar cliente...',
                                    'v-select2' => 'idCliente',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                    
                                ]
                            ]);
                    ?> 
                    <p></p>
                    <?php  
                        echo $form->field($busqueda, 'Combo2')->widget(Select2::className(), [
                                'data' => $usuarios,
                                'options' => [
                                  'placeholder' => 'Buscar vendedor...',
                                    'v-select2' => 'idVendedor',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                    
                                ]
                            ]);
                    ?>  
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                            'attributeTo' => 'FechaFin',
                            'labelTo' => 'al',
                            'form' => $form,
                            'language' => 'es',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd/mm/yyyy',
                                'endDate' => 'd',
                            ]
                        ])
                        ?>
                        <p></p>
                        <?= $form->field($busqueda, 'Combo')->dropDownList($estados) ?>
                    </div>   
                    <div class="col-md-4">
                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarpedido-button']) ?> 
                    </div>
                    <?php ActiveForm::end() ?> 
                </div>
                <p></p>
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover">
                    <thead>
                        <tr class="tabla-header">
                            <th>Nº pedido</th> 
                            <th>Usuario</th>
                            <th>Cliente</th>
                            <th>Fecha Engrega</th>
                            <th>Importe</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr v-on:dblclick="clickRow(<?= Html::encode($model['IdPedido']) ?> )">
                            <td><?= Html::encode($model['NroPedido']) ?></td> 
                            <td><?= Html::encode($model['Usuario']) ?></td>
                            <td><?= Html::encode($model['Cliente']) ?></td>
                            <td><?= Html::encode($model['FechaEntrega']) ?></td>
                            <td>$ <?= Html::encode($model['Importe']) ?></td>
                            <td><?= $estados[$model['Estado']] ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default"
                                            data-modal="<?= Url::to(['pedidos/detalles','id' => $model['IdPedido']]) ?>"
                                            title="Detalle del pedido">
                                        <i class="fa fa-search"></i>
                                    </button>
                                    <?php if ($model['Estado'] == 'A'):?>
                                        <?php if (in_array('ModificarPedido', Yii::$app->session->get('Permisos'))) : ?> 
                                        <a  class="btn btn-default" title="Modificar" 
                                            href="<?= Url::to(['pedidos/modificar','id' => $model['IdPedido']]) ?>">
                                            <i class="fa fa-edit"></i>  
                                        </a> 
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                    <?php if ($model['Estado'] == 'A'):?>
                                        <?php if (in_array('AltaDespacho', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-modal="<?= Url::to(['pedidos/despachar','id' => $model['IdPedido']]) ?>"
                                                title="Despachar">
                                                <i class="fa fa-truck" ></i> 
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                    <?php if ($model['Estado'] != 'B'):?>
                                        <?php if (in_array('AnularPedido', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['pedidos/anular', 'id' => $model['IdPedido']]) ?>"
                                                data-mensaje="¿Desea anular la pedido?">
                                            <i class="fa fa-ban"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                    <?php if ($model['Estado'] == 'B'):?>
                                        <?php if (in_array('BorrarPedido', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['pedidos/borrar', 'id' => $model['IdPedido']]) ?>"
                                                data-mensaje="¿Desea borrar el pedido?" >
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                    <?php if ($model['Estado'] == 'M'):?>
                                        <button type="button" class="btn btn-default"
                                                data-modal="<?= Url::to(['pedidos/detalles-modificado', 'id' => $model['IdPedido']]) ?>" 
                                                title="Ver modificaciones">
                                            <i class="fa fa-exchange"></i>
                                        </button> 
                                    <?php endif; ?>
                                    
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran pedidos en el sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

