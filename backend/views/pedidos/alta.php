    <?php

use common\assets\PedidosAsset;
use common\models\Zonas;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

PedidosAsset::register($this);
$this->registerJs('Pedidos.Alta.init()');
$this->title = 'Pedidos';

?>
<div class="row" id="pedidos" v-cloak>
    <div class="col-md-12">
        <div class="box">
            <?php $form = ActiveForm::begin(['id' => 'pedidos-form',
                ]) ?>
            
            <?= Html::activeHiddenInput($model, 'IdPedido') ?>
            
            <div class="box-body">
                <div id="errores"></div> 
                <div class="row">
                    <div class="col-md-4">
                    <?php 
                        $urlZonas = Url::to(['zonas/autocompletar']);
                        $zona = new Zonas();
                        echo '<label class="control-label">Zona</label>';
                        echo Select2::widget([
                            'name' => 'pedidosform-idzona',
                            'id' => 'pedidosform-idzona',
                            'options' => [
                                'placeholder' => 'Buscar zona ...',
                                'v-select2' => 'idZona',
                                'v-focus' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => $urlZonas,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                    'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                ],
                            ]
                        ]);
                    ?> 
                    </div>
                    <div class="col-md-4">
                    <?php 
                        $urlClientes = Url::to(['pedidos/autocompletar-clientes']);
                        $cliente = $model->Cliente;
                           
                        echo $form->field($model, 'IdCliente')->widget(Select2::className(), [
                                'initValueText' => $cliente, // set the initial display text
                                'options' => [
                                    'placeholder' => 'Buscar cliente...',
                                    'v-select2' => 'idCliente',
                                    'v-bind:disabled' => 'idZona == ""',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlClientes,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param){ return { idZona: $("#pedidosform-idzona").val(),  cadena : param.term }}'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                    ],
                                ]
                        ]);
                    ?> 
                    </div>
                    <div class="col-md-4">
                        <br>
                        <button id="bBorrarTodo" v-bind:disabled="arrayLP.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="borrarTodo()">
                            <i class="fa fa-minus"></i> Borrar todo
                        </button>
                        <button id="bGuardarPedido" v-bind:disabled="arrayLP.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="finalizarPedido()">
                            <i class="fa fa-check"></i> Guardar pedido 
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="pedidos-idproducto" class="control-label">Productos</label>
                        <?php 
                            $urlProductos = Url::to(['productos/autocompletar-productos']);
                            echo Select2::widget([
                                'name' => 'pedidos-idproducto',
                                'id' => 'pedidos-idproducto',
                                'options' => [
                                    'placeholder' => 'Buscar producto...',
                                    'v-select2' => 'idProducto',
                                    'v-bind:disabled' => 'idCliente == ""',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlProductos,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                    ],
                                     
                                ]
                            ]);

                        ?>
                    </div>
                    <div class="col-md-4">
                        <?php 
                            $date = date("Y-m-d", strtotime($model->FechaEntrega));
                             echo $form->field($model, 'FechaEntrega')->widget(DateControl::classname(), [
                                'ajaxConversion'=>true,
                                'options' => [
                                    'id'=>'fechaEntrega',
                                    'v-model'=>'fechaEntrega'
                                ],
                                'displayFormat' =>   'dd-MM-yyyy',
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                         'autoclose' => true,
                                        'todayHighlight' => true,
                                        'todayBtn' => 'linked',
                                        'startDate' =>  $date,
                                    ]
                                ]
                            ]);
                        ?>            
                    </div>
                    <div class="col-md-4">
                                <?= $form->field($model, 'Importe', [
                                'inputTemplate' => '<div class="col-md-12 input-group"><span class="input-group-addon">$</span>{input}</div>',
                            ])->textInput([
                                'v-bind:id' => '"importe"',
                                'v-model' => "importe",
                                'v-init' => true,
                                'readonly' => true])->label('Total') ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div v-cloak>
                    <hr>
                    <table class="table table-hover table-condensed " 
                           style="font-size: small" 
                           v-if="arrayLP.length != 0">
                        <thead>
                            <tr style="background-color: #d9e9ec">
                                <th>Producto</th> 
                                <th>Precio</th> 
                                <th>Cantidad</th> 
                                <th>Subtotal</th> 
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-bind:id="'lp[' + indice + ']'" v-for="(lp,indice) in arrayLP" >
                                <td>
                                    {{lp.descripcion}}
                                    <input type="hidden" v-bind:name="'lineas[' + indice + '][IdProducto]'"
                                           v-bind:value="lp.idProducto" >
                                </td> 
                                <td>
                                    <input type="number" step="0.1" v-focus @blur="calcularTotal()"
                                        <?php if (!in_array('EdicionPrecioPedidos', Yii::$app->session->get('Permisos'))) :?>
                                           disabled="true"
                                        <?php endif;?> 
                                        v-model="lp.precio"
                                        v-bind:name="'lineas[' + indice + '][Precio]'"
                                        v-bind:id="'lp[precio][' + indice + ']'"  
                                    />
                                </td> 
                                <td>
                                    <input type="number" step="0.1" v-focus @blur="calcularTotal()"
                                        v-model="lp.cantidad"
                                        v-bind:name="'lineas[' + indice + '][Cantidad]'"
                                        v-bind:id="'lp[cantidad][' + indice + ']'"
                                        v-on:keyup="computarCantidad(indice)" 
                                        v-on:keydown.enter="enter(indice)" 
                                    />
                                </td> 
                                <td>{{lp.subtotal}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" @click="borrarLP(indice)">
                                            <i class="fa fa-eraser"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div v-else>
                    <p><strong>Agregue productos</strong></p>
                </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>