<?php

use common\assets\VentasAsset;
use common\models\Usuarios;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
if (in_array('EdicionPrecioVenta', Yii::$app->session->get('Permisos'))) {
    $permisoEdicion = "true";
} else {
    $permisoEdicion = "false";
}
        
Yii::Info($permisoEdicion);
VentasAsset::register($this);
$this->registerJs("Ventas.Alta.init($permisoEdicion)");
$this->title = 'Ventas';

?>
<div class="row" id="ventas" v-cloak>
    <div class="col-md-12">
        <div class="box">
            <?php $form = ActiveForm::begin(['id' => 'ventas-form']) ?>
            <div class="box-body">
                <div id="errores"></div> 
                <div class="row">
                    <div class="col-md-4">
                    <?php if (Yii::$app->id == 'app-backend'): ?>
                    <?php 
                        $usuario = $reparto->Zona . ' - ' . $reparto->Usuario . ' - ' . $reparto->FechaReparto;
                        $urlUsuarios = Url::to(['ventas/autocompletar-repartos']);
                        echo $form->field($model, 'IdReparto')->widget(Select2::className(), [
                            'initValueText' => $usuario, // set the initial display text
                            'options' => [
                                'placeholder' => 'Buscar reparto...',
                                'v-select2' => 'idReparto',
                               // 'disabled' => Yii::$app->session->get('Permisos')['A']
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => $urlUsuarios,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                    'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                ],

                            ]
                        ]);
                    ?>
                    <?php else: ?>
                        <h3>
                            <strong>
                                   <?php 
                                        $usuario = Usuarios::findIdentity(Yii::$app->user->id);
                                        echo $usuario->Nombres . '' . $usuario->Apellidos;
                                   ?>
                            </strong>
                        </h3>
                    <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        <?php 
                            $urlClientes = Url::to(['autocompletar-clientes',
                                                    'id' => $model['IdVenta'],
                                                    'idReparto'=>$model['IdReparto']]);
                            echo $form->field($model, 'IdCliente')->widget(Select2::className(), [
                                'options' => [
                                    'placeholder' => 'Buscar cliente...',
                                    'v-select2' => 'idCliente',
                                    'v-bind:disabled' => 'idReparto == ""',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlClientes,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param) { return {cadena:param.term, '
                                                . 'idReparto : $("#ventas-idreparto").val(),'
                                                . '}; }'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }'),
                                    ],
                                ]
                            ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <br>
                        <button id="bBorrarTodo" v-bind:disabled="arrayLV.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="borrarTodo()">
                            <i class="fa fa-minus"></i> Borrar todo
                        </button>
                        <button id="bRealizarVenta" v-bind:disabled="arrayLV.length <= 0" 
                                type="button" 
                                data-modal="<?= Url::to(['comprobantes-pago/alta']) ?>"
                                class="btn btn-primary" 
                                @click="finalizarVenta()">
                            <i class="fa fa-check"></i> Guardar venta
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="ventas-idproducto" class="control-label">Productos</label>
                        <?php 
                            $urlProductos = Url::to(['productos/autocompletar-productos']);
                            echo Select2::widget([
                                'name' => 'ventas-idproducto',
                                'id' => 'ventas-idproducto',
                                'options' => [
                                    'placeholder' => 'Buscar producto...',
                                    'v-select2' => 'idProducto',
                                    'v-bind:disabled' => 'idCliente == ""',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlProductos,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                    ],
                                ]
                            ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-6"> 
                            <label class="control-label">Lista de precios</label>
                            <?= 
                                Select2::widget([
                                    'name' => 'IdListaPrecios',
                                    'data' => $listasPrecios,
                                    'options' => [
                                        'placeholder' => 'Seleccione...',
                                        'v-select2' => 'listaPrecios',
                                        ':disabled' => '!( idCliente != "" && edicionPrecios == true)'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                        </div>
                        <div class="col-md-6">
                                    <?= $form->field($model, 'Importe', [
                                            'inputTemplate' => '<div class="col-md-12 input-group"><span class="input-group-addon">$</span>{input}</div>',
                                        ])->textInput([
                                        'v-bind:id' => '"importe"',
                                        'v-model' => "importe",
                                        'v-init' => true,
                                        'readonly' => true])->label('Total') ?>
                        </div>   
                    </div>
                    <div class="col-md-4"> 
                        <?php if (in_array('EdicionPrecioVenta', Yii::$app->session->get('Permisos'))) :?>
                            <b>Editar precios</b><br>
                            <button class="btn btn-danger" id="ventas-btn-edicion" v-bind:disabled="arrayLV.length <= 0" 
                                    type="button" 
                                    class="btn btn-primary"    
                                    @click="activarEdicion()">Pedir autorización

                                <i id="asd" class="glyphicon glyphicon-thumbs-up"></i> 
                            </button>
                        <?php endif;?>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div v-cloak>
                    <hr>
                    <table class="table table-hover table-condensed table-bordered" 
                           style="font-size: small" 
                           v-if="arrayLV.length != 0" >
                        <thead>
                            <tr style="background-color: #d9e9ec">
                                <th>Producto</th> 
                                <th>Unidad</th> 
                                <th>Precio</th>  
                                <th>Cantidad</th> 
                                <th>Subtotal</th> 
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-bind:id="'lv[' + indice + ']'" v-for="(lv,indice) in arrayLV" >
                                <td>
                                    {{lv.descripcion}}
                                    <input type="hidden" v-bind:name="'lineas[' + indice + '][IdProducto]'"
                                           v-bind:value="lv.idProducto" >
                                </td> 
                                <td>{{lv.unidad}}</td>
                                <td>
                                    <input type="number" step="0.1" v-focus @blur="calcularTotal()"
                                        v-model="lv.precio" 
                                        v-bind:disabled="!edicionPrecios" 
                                        v-bind:name="'lineas[' + indice + '][Precio]'"
                                        v-bind:id="'lv[precio][' + indice + ']'"  
                                    />
                                </td> 
                                <td>
                                    <input type="number" step="0.1"  v-focus @blur="calcularTotal()"
                                        v-model="lv.cantidad" 
                                        v-bind:name="'lineas[' + indice + '][Cantidad]'"
                                        v-bind:id="'lv[cantidad][' + indice + ']'"
                                        v-on:keyup="computarCantidad(indice)" 
                                        v-on:keydown.enter="enter(indice)" 
                                    />
                                </td> 
                                <td>{{lv.subtotal}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" @click="borrarLV(indice)">
                                            <i class="fa fa-eraser"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div v-else>
                    <p><strong>Agregue productos a la venta.</strong></p>
                </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>