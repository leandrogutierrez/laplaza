<?php

use yii\helpers\Html;

/* @var $model common\models\Ventas */
/* @var $this yii\web\View */

$lineasPedido = json_decode($model['LineasVenta']);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>
        <div class="modal-body">
            <div id="errores-modal"> </div>
            <?= Html::activeHiddenInput($model, 'IdVenta') ?>
            <div class="panel panel-primary">
                <div class="panel-heading">Información de la venta</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div><strong>Fecha de alta:</strong> <?= Html::encode($model['FechaAlta']) ?></div>
                            <div><strong>Importe: </strong> $<?= Html::encode($model['Importe']) ?></div>
                        </div>
                        <div class="col-md-6">
                            <div><strong>Cliente:</strong> <?= Html::encode($model['Cliente']) ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (count($lineasPedido) > 0) : ?>
            <table class="table table-hover table-condensed table-bordered">
                <thead>
                    <tr style="background-color: #C0D8C6"> 
                        <th>Descripción</th>
                        <th>Unidad</th>
                        <th>Cantidad</th> 
                        <th>Precio</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lineasPedido as $linea) : ?>
                    <tr>
                        <td><?= Html::encode($linea->Descripcion) ?></td>
                        <td><?= Html::encode($linea->Unidad) ?></td> 
                        <td><?= Html::encode($linea->Cantidad) ?></td> 
                        <td>$ <?= Html::encode($linea->Precio) ?></td>
                        <td>$ <?= Html::encode($linea->Importe) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            <p><strong>Error al cargar la venta.</strong></p>
            <?php endif; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>