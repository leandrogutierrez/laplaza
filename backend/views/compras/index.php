<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Gestión de compras';
$estados = [
    'T' => 'Todas',
    'A' => 'Activa',
    'B' => 'Baja',
];
 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php if (in_array('AltaCompra', Yii::$app->session->get('Permisos'))) : ?>
                    <a href="<?= Url::to(['compras/alta']) ?>">
                        <button class="btn btn-primary pull-right" type="button" >
                            <i class="fa fa-plus"></i> Nueva compra
                        </button>
                    </a>
                    <?php endif; ?>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'buscacompras-form', 'layout' => 'inline']) ?>
                
                <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                    'attributeTo' => 'FechaFin',
                    'labelTo' => 'al',
                    'form' => $form,
                    'language' => 'es',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy',
                        'endDate' => 'd',
                    ]
                ])
                ?>
                
                <?= $form->field($busqueda, 'Combo')->dropDownList($estados) ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarcompras-button']) ?> 
                
                <?php ActiveForm::end() ?>
                <div id="mensajes"></div>
                <?php Pjax::begin(['id' => 'content']) ?>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover"  >
                    <thead>
                        <tr>
                            <th>Fecha Alta</th>
                            <th>Proveedores</th> 
                            <th>Importe</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($models as $model) : ?>
                                <tr>
                                    <td><?= Html::encode($model['FechaAlta']) ?></td>
                                    <td><?= Html::encode($model['Proveedor']) ?></td>  
                                    <td>$<?= Html::encode($model['Importe']) ?></td>
                                    <td><?= $estados[$model['Estado']] ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default"
                                                data-modal="<?= Url::to(['compras/detalles','id' => $model['IdCompra']]) ?>"
                                                title="Detalle de la compra">
                                            <i class="fa fa-search"></i>
                                            </button>
                                            <?php if ($model['Estado'] == 'A'):?>
                                                <?php if (in_array('BorrarCompra', Yii::$app->session->get('Permisos'))) : ?>
                                                    <button type="button" class="btn btn-default"
                                                            data-ajax="<?= Url::to(['compras/borrar', 'id' => $model['IdCompra']]) ?>"
                                                            data-mensaje="¿Desea borrar la compra?">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran compras en le sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
                
                <?php Pjax::end(); ?> 
            </div>
        </div>
    </div>
</div>

