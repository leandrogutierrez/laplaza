<?php

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

common\assets\ComprasAsset::register($this);
$this->registerJs('Compras.Alta.init()');
$this->title = 'Alta de compra';
$this->params['breadcrumbs'][] = $this->title;

$model['Tipo'] = 'P';
?>
<div class="row" id="compras" v-cloak>
    <div class="col-md-12">
        <div class="box">
            <?php $form = ActiveForm::begin(['id' => 'compras-form',
                ]) ?>
            <div class="box-body">
                <div id="errores"></div>
                
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'Tipo')->inline()->radioList([
                            'P' => 'De producción',
                            'U' => 'De uso',
                            'O' => 'Otros',
                        ], [
                            'v-bind:readonly' => 'arrayMP.length != 0',
                            'itemOptions' => [
                                'v-model' => 'tipoCompra',
                            ]
                        ]) ?>
                    </div>
                    <div class="col-md-4">
                    <?php 
                        $urlProveedores = Url::to(['proveedores/autocompletar-proveedores']);

                        $initScriptProveedores = <<<SCRIPT
                            function (element, callback) {
                                var id=\$(element).val();
                                if (id && id !== "") {
                                    \$.ajax("{$urlProveedores}?id=" + id , {
                                        dataType: "json"
                                    }).done(function(data) { callback(data);});
                                }
                                else {
                                    callback([]);
                                }
                            }
SCRIPT;
                        echo $form->field($model, 'IdProveedor')->widget(Select2::className(), [
                                'options' => [
                                    'placeholder' => 'Buscar proveedor...',
                                    'v-select2' => 'idProveedor',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlProveedores,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                    ],
                                    'initSelection' => new JsExpression($initScriptProveedores),
                                ]
                            ]);

                        ?>
                    </div>
                    <div class="col-md-4">
                        <br>
                        <button id="bBorrarTodo" v-bind:disabled="arrayMP.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="borrarTodo()">
                            <i class="fa fa-minus"></i> Borrar todo
                        </button>
                        <button id="bRealizarCompra" v-bind:disabled="arrayMP.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="finalizarCompra()">
                            <i class="fa fa-check"></i> Guardar compra
                        </button>
                    </div>
                </div>
                <div class="row">  
                    <div class="col-md-4 "> 
                        <?= $form->field($model, 'IdConceptoCompra')
                            ->dropDownList(
                                    ArrayHelper::map($conceptosCompra, 'IdConceptoCompra', 'ConceptoCompra'),
                                [
                                'v-model' => 'idConceptoCompra'
                            ]
                        ) ?> 
                    </div> 
                    <div class="col-md-4">
                        <div class="col-md-4" v-show="arrayConceptos.length > 0">
                            <table class="table table-condensed" 
                                   style="font-size: smaller">
                                <thead>
                                    <tr>
                                        <th>Concepto</th>
                                        <th>Importe</th>
                                        <th>Quitar</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    <tr class="form-group-sm" v-for="(concepto, indice) in arrayConceptos">
                                        <td style="padding-bottom: -1em; padding-top: -1em">{{concepto.ConceptoCompra}}</td>
                                        <td style="padding-bottom: -1em; padding-top: -1em">
                                            <input type="number" 
                                                   v-bind:id="'Conceptos[' + concepto.IdConceptoCompra + '][Importe]'"
                                                   v-bind:name="'Conceptos[' + concepto.IdConceptoCompra + ']'"
                                                   v-model = "concepto.Importe" />
                                        </td>
                                        <td style="padding-bottom: -1em; padding-top: -1em">
                                            <button type="button" class="btn btn-default btn-xs" 
                                                    @click="quitarConcepto(indice)">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>   
                    <div class="col-md-4">
                        <?= $form->field($model, 'Importe', [
                                'template' => '{label}<div class="col-md-6 input-group" style="float:right"><span class="input-group-addon">$</span>{input}</div>',
                            ])->textInput([
                                'v-bind:id' => 'subtotal',
                                'v-model' => "subtotal",
                                'v-init' => true,
                                'readonly' => true])->label('Subtotal') ?>  
                        <?= $form->field($model, 'Importe', [
                                'template' => '{label}<div class="col-md-6 input-group" style="float:right"><span class="input-group-addon">$</span>{input}</div>',
                            ])->textInput([
                                'v-bind:id' => 'importeConceptos',
                                'v-model' => "importeConceptos",
                                'v-init' => true,
                                'display'=> 'inline-block',
                                'readonly' => true])->label('Total conceptos') ?>  
                        <?= $form->field($model, 'Importe', [
                                'template' => '{label}<div class="col-md-6 input-group" style="float:right"><span class="input-group-addon">$</span>{input}</div>',
                            ])->textInput([
                                'v-bind:id' => 'importe',
                                'v-model' => "importe",
                                'v-init' => true,
                                'readonly' => true])->label('Total') ?>
                    </div>
                        <div class="clearfix"></div>  
                    </div>              
                </div> 

                <div class='row'>
                    <div class="col-md-4">
                    <label for="compras-idmp" class="control-label">Materia prima</label>
                        <?php 
                        $urlMP = Url::to(['materias-prima/autocompletar-mp']);
                        $initScriptMP = <<<SCRIPT
                            function (element, callback) {
                                var id=\$(element).val();
                                if (id && id !== "") {
                                    \$.ajax("{$urlMP}?id=" + id , {
                                        dataType: "json"
                                    }).done(function(data) { callback(data);});
                                }
                                else {
                                    callback([]);
                                }
                            }
SCRIPT;
                        echo Select2::widget([
                                'name' => 'compras-idmp',
                                'id' => 'compras-idmp',
                                'options' => [
                                    'placeholder' => 'Buscar tipo de materia prima...',
                                    'v-select2' => 'idMateriaPrima',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                    'ajax' => [
                                        'url' => $urlMP,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param){ return { cadena : param.term, tipo : Compras.Alta.vue.tipoCompra }}'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                    ],
                                    'initSelection' => new JsExpression($initScriptMP),
                                ]
                            ]);
                        ?>
                    </div> 
                </div> 
                <div v-cloak>
                    <hr>
                    <table class="table table-hover table-condensed table-bordered" 
                           style="font-size: small" 
                           v-if="arrayMP.length != 0">
                        <thead>
                            <tr style="background-color: #d9e9ec">
                                <th>Materia prima</th>
                                <th>Peso</th>
                                <th>Cantidad</th>
                                <th>Importe</th> 
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-bind:id="'lc[' + indice + ']'" v-for="(lc,indice) in arrayMP" >
                                <td>
                                    {{lc.materiaPrima}}
                                    <input type="hidden"
                                           v-bind:name="'lineas[' + indice + '][idMateriaPrima]'"
                                           v-bind:value="lc.idMateriaPrima" />
                                </td> 
                                <td>
                                    <input required type="number" step="0.01"
                                           v-bind:name="'lineas[' + indice + '][peso]'"
                                           v-bind:id="'lc[peso][' + indice + ']'"
                                           v-model="lc.peso"
                                           v-focus />
                                </td>
                                <td>
                                    <input required type="number" 
                                           v-bind:name="'lineas[' + indice + '][cantidad]'"
                                           v-bind:id="'lc[cantidad][' + indice + ']'"
                                           v-model="lc.cantidad" />
                                </td>
                                <td><input required type="number" step="0.01"
                                           v-bind:name="'lineas[' + indice + '][importe]'"
                                           v-bind:id="'lc[importe][' + indice + ']'"
                                           v-model="lc.importe" 
                                           v-on:keydown.tab="calcularImporte()"  />
                                </td> 
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" @click="borrarLC(indice)">
                                            <i class="fa fa-eraser"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div v-else>
                    <p><strong>Agregue productos a la compra.</strong></p>
                </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>