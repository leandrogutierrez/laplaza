<?php

use yii\helpers\Html;

$lineasCompra = json_decode($model['LineasCompra']);

?>
  
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hiden="true">
                    &times; 
                </span>
            </button>
            <h4 class="modal-title">Información de la compra</h4>
         </div>
        <div class="modal-body">
            <div class="errores-modal"></div>
            <?= Html::activeHiddenInput($model, 'IdCompra')?>
            <div class="row col-md-12">
                <div class="col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Compra - <?= $model['NroCompra'] ?></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div><strong>Alta:</strong><?= Html::encode($model['FechaAlta']) ?></div>
                                    <div><strong>Importe:</strong><?= Html::encode($model['Importe']) ?></div>                            
                                </div>
                                <div class="col-md-6">
                                    <div><strong>Proveedor:</strong><?= Html::encode($model['Proveedor']) ?></div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Concenpto</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>IVA</td>
                                <td>xxx</td>
                            </tr>
                            <tr>
                                <td>Transporte</td>
                                <td>yyy</td>
                            </tr>
                            <tr>
                                <td>Subtotal:</td>
                                <td>zzz</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if (count($lineasCompra) > 0) : ?>
            <table class="table table-hover table-condensed table-bordered">
                <thead>
                    <tr style="background-color: #C0D8C6"> 
                        <th>Materia prima</th>
                        <th>Peso</th>
                        <th>Cantidad</th>
                        <th>Importe</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lineasCompra as  $linea) : ?>
                        <tr> 
                            <td><?= $linea->MateriaPrima ?></td>
                            <td><?= $linea->Peso ?></td>
                            <td><?= $linea->Cantidad ?></td>
                            <td><?= $linea->Importe ?></td> 

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
                <p><strong>Error. La compra no tiene líneas de compra.</strong></p>
            <?php endif; ?>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
        </div>
    </div>
    
</div>