<?php

use yii\helpers\Inflector;

$attributes = $model->attributes();

$headers = array();

if ($tabla != null) {
    $primeraFila = $tabla[0];
    foreach ($primeraFila as $key => $elemento) {
        $headers[] = $key;
    }
}
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-5 pull-left">
                            <p><?= $titulo ?></p>
                        </div>
                        <div class="col-xs-5 pull-right">
                            <p align="right" style="font-size: 10px">F. imp.: <?= date('d/m/Y H:i') ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($attributes as $attribute): ?>
                            <?php if ($model->{$attribute} != null): ?>
                                <div class="col-xs-5" style="margin-top: -0.3em; margin-bottom: -0.4em;
                                     font-size: 10px">
                                    <p><strong><?= Inflector::camel2words($attribute) ?>: </strong><?= ($model->{$attribute} == '0') ? '-' :  $model->{$attribute}?></p>                            
                                </div>
                            <?php endif;?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-condensed table-bordered" style="border: 2px solid black; font-size: 10px">
                        <thead>
                            <tr>
                                <?php foreach ($headers as $header): ?>
                                <th  style="<?= $modeloReporte['CssHeaders']?>"><?= $header?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($tabla as $fila): ?>
                            <tr> 
                                <?php foreach ($fila as $columna => $valor): ?>
                                <td style="<?=$modeloReporte['CssCells']?>">
                                    <?php 
                                        if ($modeloReporte['MuestraCeros'] || $valor >0 || !is_numeric($valor)):
                                            echo $valor ;
                                        endif;
                                    ?>
                                </td>    
                                <?php endforeach; ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>