<?php

use common\assets\ProduccionesAsset;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

/* @var $this View */
/* @var $form ActiveForm */
ProduccionesAsset::register($this);
$this->registerJs('Producciones.Alta.init()');

$this->title = 'Gestión de producciones';
$this->params['breadcrumbs'][] = $this->title;
$estados = [
    'T' => 'Todas',
    'B' => 'Anulada',
    'A' => 'Activa',
];
 
$this->title = 'Producciones';
Yii::info($categorias);
?> 

<div class="row" id="producciones">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php if (in_array('AltaProduccion', Yii::$app->session->get('Permisos'))) : ?>
                    <a href="<?= Url::to(['producciones/alta']) ?>">
                        <button class="btn btn-primary pull-right" type="button" >
                            <i class="fa fa-plus"></i> Nueva produccion
                        </button>
                    </a>
                    <?php endif; ?>
                </div>
                <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'buscarproducciones-form',
                                                        'method' => 'get',
                                                        'layout' => 'inline']) ?>
                    <div class="col-md-4">  
                    <?php  
                        echo $form->field($busqueda, 'Combo3')->widget(Select2::className(), [
                                'data' => $categorias,
                                'options' => [
                                  'placeholder' => 'Buscar categoria...',
                                    'v-select2' => 'idCategoria',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                ]
                            ]);
                    ?> 
                    <p></p>
                     <?php  
                        echo $form->field($busqueda, 'Combo2')->widget(Select2::className(), [
                                'data' => $usuarios,
                                'options' => [
                                  'placeholder' => 'Buscar usuario...',
                                    'v-select2' => 'idUsuario',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                    
                                ]
                            ]);
                    ?>  
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                            'attributeTo' => 'FechaFin',
                            'labelTo' => 'al',
                            'form' => $form,
                            'language' => 'es',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd/mm/yyyy',
                                'endDate' => 'd',
                            ]
                        ])?>
                        <p></p>
                        <?= $form->field($busqueda, 'Combo')->dropDownList($estados) ?>
                    </div>
                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarcompras-button']) ?> 

                    <?php ActiveForm::end() ?>
                </div>
                <p></p>
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover" id="fixedheight">
                    <thead>
                        <tr class="tabla-header">
                            <th>Categoria</th>
                            <th>Fecha Alta</th> 
                            <th>Fecha Producción</th> 
                            <th>Nº producción</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr v-on:dblclick="clickRow(<?= Html::encode($model['IdProduccion']) ?> )">
                            <td><?= Html::encode($model['Categoria']) ?></td> 
                            <td><?= Html::encode($model['FechaAlta']) ?></td> 
                            <td><?= Html::encode($model['FechaProduccion']) ?></td>
                            <td><?= Html::encode($model['NroProduccion']) ?></td>
                            <td><?= Html::encode($model['Usuario']) ?></td>
                            <td><?= $estados[$model['Estado']] ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default"
                                            data-modal="<?= Url::to(['producciones/detalles','id' => $model['IdProduccion']]) ?>"
                                            title="Detalle de la produccion">
                                        <i class="fa fa-search"></i>
                                    </button> 
                                    <?php if ($model['Estado'] != 'A'):?>
                                        <?php if (in_array('AnularProduccion', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['producciones/anular', 'id' => $model['IdProduccion']]) ?>"
                                                data-mensaje="¿Desea anular la produccion?">
                                            <i class="fa fa-ban"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if ($model['Estado'] == 'A'):?>
                                        <?php if (in_array('BorrarProduccion', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['producciones/borrar', 'id' => $model['IdProduccion']]) ?>"
                                                data-mensaje="¿Desea borrar la produccion?">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran producciones en el sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

