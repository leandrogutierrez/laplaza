<?php

use yii\helpers\Html;

/* @var $model common\models\Producciones */
/* @var $this yii\web\View */

$lineasProduccion = json_decode($model['LineasProduccion']);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>
        <div class="modal-body">
            <div id="errores-modal"> </div>
            <?= Html::activeHiddenInput($model, 'IdProduccion') ?>
            <div class="panel panel-primary">
                <div class="panel-heading">Información de la produccion</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div><strong>Usuario:</strong> <?= Html::encode($model['Usuario']) ?></div>
                                <div><strong>Categoria:</strong> <?= Html::encode($model['Categoria']) ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div><strong>Producción Nro:</strong> <?= Html::encode($model['NroProduccion']) ?></div>
                            <div><strong>Fecha de alta:</strong> <?= Html::encode($model['FechaAlta']) ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (count($lineasProduccion) > 0) : ?>
            <table class="table table-hover table-condensed table-bordered">
                <thead>
                    <tr style="background-color: #C0D8C6"> 
                        <th>Descripción</th>
                        <th>UnidadProduccion</th>
                        <th>Cantidad</th> 
                        <th>Equivalencia</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lineasProduccion as $linea) : ?>
                    <tr>
                        <td><?= Html::encode($linea->Descripcion) ?></td>
                        <td><?= Html::encode($linea->UnidadProduccion) ?></td> 
                        <td><?= Html::encode($linea->Cantidad) ?></td> 
                        <td><?= Html::encode($linea->Equivalencia) .' '. Html::encode($linea->Unidad) ?></td> 
                        <td><?= Html::encode($linea->Produccion) .' '. Html::encode($linea->Unidad) ?></td> 
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            <p><strong>Error al cargar la produccion.</strong></p>
            <?php endif; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>