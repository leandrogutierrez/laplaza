<?php

use common\assets\ProduccionesAsset;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;


/* @var $this View */
/* @var $form ActiveForm */

ProduccionesAsset::register($this);
$this->registerJs("Producciones.Alta.init()");
$this->title = 'Producciones';

?>
<div class="row" id="producciones" v-cloak>
    <div class="col-md-12">
        <div class="box">
            <?php $form = ActiveForm::begin(['id' => 'producciones-form']) ?>
            <div class="box-body">
                <div id="errores"></div> 
                <div class="row">
                    <div class="col-md-4">
                    <?php 
                        echo $form->field($model, 'IdCategoria')->widget(Select2::className(), [
                            'data' =>$categorias,
                            'options' => [
                                'placeholder' => 'Seleccione categoria...',
                                'v-select2' => 'idCategoria',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ]);
                    ?>
                    </div>
                    <div class="col-md-4">
                        <?php 
                            $date = date("Y-m-d", strtotime($model->FechaProduccion));
                             echo $form->field($model, 'FechaProduccion')->widget(DateControl::classname(), [
                                'ajaxConversion'=>true,
                                'options' => [
                                    'id'=>'fechaProduccion',
                                    'v-model'=>'fechaProduccion'
                                ],
                                'displayFormat' =>   'dd-MM-yyyy',
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                         'autoclose' => true,
                                        'todayHighlight' => true,
                                        'todayBtn' => 'linked',
                                        'startDate' =>  $date,
                                    ]
                                ]
                            ]);
                        ?>            
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-md-4">
                        <label for="producciones-idproducto" class="control-label">Productos</label>
                        <?php 
                            $urlProductos = Url::to(['autocompletar-productos']);
                            echo Select2::widget([
                                'name' => 'producciones-idproducto',
                                'id' => 'producciones-idproducto',
                                'options' => [
                                    'placeholder' => 'Buscar producto...',
                                    'v-select2' => 'idProducto',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlProductos,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(param) { '
                                                                    . 'return {cadena:param.term, '
                                                                    . 'idCategoria : $("#producciones-idcategoria").val(),'
                                                                    . '}; }'),
                                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                    ],
                                ]
                            ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <br>
                        <button id="bBorrarTodo" v-bind:disabled="arrayLP.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="borrarTodo()">
                            <i class="fa fa-minus"></i> Borrar todo
                        </button>
                        <button id="bRealizarProduccion" v-bind:disabled="arrayLP.length <= 0" 
                                type="button" 
                                class="btn btn-primary" 
                                @click="finalizarProduccion()">
                            <i class="fa fa-check"></i> Guardar produccion
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div v-cloak>
                    <hr>
                    <table class="table table-hover table-condensed table-bordered" 
                           style="font-size: small" 
                           v-if="arrayLP.length != 0" >
                        <thead>
                            <tr style="background-color: #d9e9ec">
                                <th>Producto</th> 
                                <th>Unida venta</th> 
                                <th>Unida produccion</th> 
                                <th>Equivalencia</th> 
                                <th>Cantidad</th> 
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-bind:id="'lp[' + indice + ']'" v-for="(lp,indice) in arrayLP" >
                                <td>
                                    {{lp.descripcion}}
                                    <input type="hidden" v-bind:name="'lineas[' + indice + '][IdProducto]'"
                                           v-bind:value="lp.idProducto" >
                                </td> 
                                <td>{{lp.unidad}}</td>
                                <td>{{lp.unidadProduccion}}</td>
                                <td>{{lp.equivalencia}}</td>
                                <td>
                                    <input type="number" step="0.1"  v-focus 
                                        v-model="lp.cantidad" 
                                        v-bind:name="'lineas[' + indice + '][Cantidad]'"
                                        v-bind:id="'lp[cantidad][' + indice + ']'"
                                        v-on:keydown.enter="enter(indice)" 
                                    />
                                </td> 
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" @click="borrarLP(indice)">
                                            <i class="fa fa-eraser"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div v-else>
                    <p><strong>Agregue productos a la produccion.</strong></p>
                </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>