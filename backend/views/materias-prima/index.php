<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$tipo = [
    'P' => 'De producción',
    'U' => 'De uso',
    'O' => 'Otro'
];

$tieneRendimiento = [
    'S' => 'Existen rendimientos',
    'N' => '-'
];

$this->title = 'Materias prima';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaMateriaPrima', Yii::$app->session->get('Permisos'))): ?>
                    <button type="button" class="btn btn-primary pull-right"
                            data-modal="<?= Url::to(['materias-prima/alta']) ?>">
                        <i class="fa fa-plus"></i> Nueva materia prima
                    </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['id' => 'busquedamp-form', 'layout' => 'inline']) ?>

                <?= $form->field($busqueda, 'Cadena') ?>

                <?= $form->field($busqueda, 'Combo')->dropDownList($tipo) ?>

                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>

                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarmp-button']) ?> 

                <?php ActiveForm::end() ?>
                <br>
                <?php if (count($models) > 0) : ?>
                    <table class="table table-hover">
                        <thead>
                            <tr class="tabla-header">
                                <th>Materia prima</th>
                                <th>Tipo</th> 
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($models as $model) : ?>
                                <tr>
                                    <td><?= Html::encode($model['MateriaPrima']) ?></td>
                                    <td><?= Html::encode($tipo[$model['Tipo']]) ?></td>
                                    <td><?php
                                        switch ($model['Estado']) {
                                            case 'A': echo 'Activo';
                                                break;
                                            case 'B': echo 'Baja';
                                                break;
                                        }
                                        ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <?php if ($model['Estado'] == 'B') : ?>
                                                <?php if (in_array('ActivarMateriaPrima', Yii::$app->session->get('Permisos'))): ?>
                                                    <button type="button" class="btn btn-default"
                                                            data-ajax="<?= Url::to(['materias-prima/activar', 'id' => $model['IdMateriaPrima']]) ?>"
                                                            title="Activar">
                                                        <i class="fa fa-check-circle" style="color: green"></i>
                                                    </button>
                                                <?php endif; ?>
                                            <?php else : ?>
                                                <?php if (in_array('DarBajaMateriaPrima', Yii::$app->session->get('Permisos'))): ?>
                                                <button type="button" class="btn btn-default"
                                                        data-ajax="<?= Url::to(['materias-prima/dar-baja', 'id' => $model['IdMateriaPrima']]) ?>"
                                                        title="Desactivar">
                                                    <i class="fa fa-minus-circle" style="color: red"></i>
                                                </button>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php if (in_array('ModificarMateriaPrima', Yii::$app->session->get('Permisos'))): ?>
                                            <button type="button" class="btn btn-default"
                                                    data-modal="<?= Url::to(['materias-prima/modificar', 'id' => $model['IdMateriaPrima']]) ?>"
                                                    title="Modificar">
                                                <i class="fa fa-edit" style="color: dodgerblue"></i>
                                            </button>
                                            <?php endif; ?>
                                            <?php if (in_array('BorrarMateriaPrima', Yii::$app->session->get('Permisos'))): ?>
                                            <button type="button" class="btn btn-default"
                                                    data-mensaje="¿Seguro desea eliminar la materia prima indicada?"
                                                    data-ajax="<?= Url::to(['materias-prima/borrar', 'id' => $model['IdMateriaPrima']]) ?>">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else : ?>
                    <p><strong>No existen materias prima con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>