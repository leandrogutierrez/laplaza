<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model backend\models\MateriasPrima */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

?>
<div class="modal-dialog">
    <div class="modal-content" id="datos-mp">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'datosmp-form',]) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div>
            <div class="col-md-12">
                <?= Html::activeHiddenInput($model, 'IdMateriaPrima') ?>

                <div class="col-md-6">
                    <?= $form->field($model, 'MateriaPrima') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'Tipo')
                            ->radioList([
                                'P' => 'De producción',
                                'U' => 'De uso',
                                'O' => 'Otro'])
                    ?>
                </div>
                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>