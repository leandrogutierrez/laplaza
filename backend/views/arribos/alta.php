<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use common\models\Zonas;
use common\models\Repartos;
use common\models\Usuarios;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;

common\assets\ArribosAsset::register($this);
$this->registerJs('Arribos.Alta.init()');
$this->title = 'Arribos';
?>
<div class="modal-dialog modal-lg" id="arribos">
    <div class="modal-content"> 
        <div class="modal-header">
            <button type="button" class="close" tabindex='-1' data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'arriboreparto-form']) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div>  
            <?php if ($edicionPrecios == 'S'):?>
                <div class="alert  alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>El reparto posee ventas con precios modificados.
                        <a data-modal="<?= Url::to(['repartos/ver-ventas-reparto', 'id' => $model['IdReparto']])?>">Ver Ventas</a>
                    </strong> 
                </div>
            <?php  endif;?>
            <?= Html::activeHiddenInput($model, 'IdArribo') ?>  
            <?= Html::activeHiddenInput($model, 'IdReparto') ?>  
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                    <?= $form->field($reparto, 'Usuario')->label('Vendedor')->textInput(['disabled' =>true])   ?>
                    </div>  
                    <div class="col-md-6">
                    <?= $form->field($reparto, 'Zona')->textInput(['disabled' =>true])  ?>
                    </div> 
                </div> 
                <div class="col-md-12">
                    <div class="col-md-6">
                        <?php 
                            echo $form->field($model, 'FechaArribo')->widget(DateControl::classname(), [
                                'ajaxConversion'=>false,
                                'displayFormat' =>   'dd-MM-yyyy',
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'todayHighlight' => true,
                                        'todayBtn' => 'linked',
                                        'startDate' => date('d-m-Y'),
                                    ]
                                ]
                            ]);
                        ?>
                    </div> 
                    <div class="col-md-6">      
                        <br>
                        <a class="btn btn-danger no-print"
                           target="_blank"
                           href="<?= Url::to(['imprimir-precios','id' => $model['IdReparto']]) ?>">
                            <i class="fa fa-exclamation-triangle"></i> Ver variación de precios
                        </a> 
                    </div>  
                </div> 
            </div> 
            <div class="row">
            <div class="col-md-6">
                <label for="arribos-idproducto" class="control-label">Productos</label>
                <?php 
                    $urlProductos = Url::to(['productos/autocompletar-productos']);
                    echo Select2::widget([
                        'name' => 'arribos-idproducto',
                        'id' => 'arribos-idproducto',
                        'options' => [
                            'placeholder' => 'Buscar producto...',
                            'v-select2' => 'idProducto',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $urlProductos,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                            ],

                        ]
                    ]);

                ?>
            </div> 
            <div class="col-md-12" v-cloak> 
                <table class="table table-hover table-condensed " 
                       style="font-size: small" 
                       v-if="arrayLA.length != 0">
                    <thead>
                        <tr style="background-color: #d9e9ec">
                            <th>Producto</th> 
                            <th>Despachado</th> 
                            <th>Vendido</th> 
                            <th>Arribo</th> 
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-bind:id="'la[' + indice + ']'" v-for="(la,indice) in arrayLA" >
                            <td>
                                {{la.descripcion}}
                                <input type="hidden" 
                                       v-bind:name="'lineas[' + indice + '][IdProducto]'"
                                       v-bind:value="la.idProducto" >
                            </td> 
                            <td>
                                <input type="number"    
                                       v-model="la.despacho"
                                       readonly="true" 
                                />
                            </td>
                            <td>
                                <input type="number" 
                                       v-model="la.venta"
                                       readonly="true" 
                                />
                            </td>                             
                            <td>
                                <input type="number" v-focus step='0.1'  
                                    v-model="la.cantidad" 
                                    v-bind:name="'lineas[' + indice + '][Cantidad]'"
                                    v-bind:id="'la[cantidad][' + indice + ']'"   
                                    v-on:keydown.enter="enter(indice)" 
                                />
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" @click="borrarLP(indice)">
                                        <i class="fa fa-eraser"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div v-else>
                    <p><strong>Agregue productos</strong></p>
                </div> 
            </div> 
            <div class="clearfix"></div> 
        </div>
        </div>
        <div class="modal-footer"> 
            <a class="btn btn-danger no-print"
                target="_blank"
                href="<?= Url::to(['imprimir-precios','id' => $model['IdReparto']]) ?>">
                 <i class="fa fa-exclamation-triangle"></i> Ver variación de precios
            </a> 
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


