<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
                                

$this->title = 'Vehículos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div id="errores"></div>
                <?php if (in_array('AltaVehiculo', Yii::$app->session->get('Permisos'))) : ?>
                <button class="btn btn-primary pull-right" type="button"
                        data-modal="<?= Url::to(['vehiculos/alta']) ?>">
                    <i class="fa fa-plus"></i> Nuevo vehículo
                </button>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(['layout' => 'inline', 'id' => 'busquedavehiculos-form']) ?>
                
                <?= $form->field($busqueda, 'Cadena') ?>
                
                <?= $form->field($busqueda, 'Check')->checkbox(['value' => 'S', 'uncheck' => 'N'])->label('Mostrar desactivados') ?>
                
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default', 'name' => 'buscarvehiculos-button']) ?> 
                
                <?php ActiveForm::end() ?>
                
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover ">
                    <thead>
                        <tr class="tabla-header">
                            <th>Marca</th>
                            <th>Modelo</th> 
                            <th>Patente</th> 
                            <th>Kilometraje</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td><?= Html::encode($model['Marca']) ?></td>
                            <td><?= Html::encode($model['Modelo']) ?></td> 
                            <td><?= Html::encode($model['Patente']) ?></td> 
                            <td><?= Html::encode($model['Kilometraje']) ?></td>
                            <td><?php switch ($model['Estado']) {
                                case 'A': echo 'Activo';
                                    break;
                                case 'B': echo 'Baja';
                                    break;
                            } ?></td>
                            <td>
                                <div class="btn-group">
                                    <?php if ($model['Estado'] == 'A') : ?>
                                        <?php if (in_array('DarBajaVehiculo', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['vehiculos/dar-baja', 'id' => $model['IdVehiculo']])?>"
                                                title="Desactivar">
                                            <i class="fa fa-minus-circle" style="color: red"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if (in_array('ActivarVehiculo', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                data-ajax="<?= Url::to(['vehiculos/activar', 'id' => $model['IdVehiculo']])?>"
                                                title="Activar">
                                            <i class="fa fa-check-circle" style="color: green"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (in_array('ModificarVehiculo', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-modal="<?= Url::to(['vehiculos/modificar','id' => $model['IdVehiculo']]) ?>"
                                            title="Modificar">
                                        <i class="fa fa-edit" style="color: dodgerblue"></i>
                                    </button>
                                    <?php endif; ?>
                                    <?php if (in_array('BorrarVehiculo', Yii::$app->session->get('Permisos'))) : ?>
                                    <button class="btn btn-default" type="button"
                                            data-ajax="<?= Url::to(['vehiculos/borrar','id' => $model['IdVehiculo']]) ?>"
                                            data-mensaje="¿Desea borrar el vehiculo indicado?">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <p><strong>No existen clientes que coincidan con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>