<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html> 
<html lang="<?= Yii::$app->language ?>" class="bg-primary">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <?= Html::csrfMetaTags() ?>
        <title><?= Yii::$app->name ?> | <?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="bg-black login-page">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <?php $this->beginBody() ?>

                    <div class="login-box"> 
                        </h1><!-- /.login-logo -->
                            <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
