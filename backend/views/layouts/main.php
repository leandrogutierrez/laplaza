<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html> 
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <!-- put in head tag -->
    <?= $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::$app->request->baseUrl.'/favicon.ico']);?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'La Plaza',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        [
            'label' => 'Parametrización', 'url' => ['#'],
            'items' => [
                //['label' => 'Materias prima', 'url' => ['/materias-prima/index']],
                //['label' => 'Proveedores', 'url' => ['/proveedores/index']],
                //['label' => 'Conceptos compra', 'url' => ['/conceptos-compra/index']],
                ['label' => 'Productos', 'url' => ['/productos/index']],
                ['label' => 'Clientes', 'url' => ['/clientes/index']],
                ['label' => 'Vendedores', 'url' => ['/vendedores/index']],
                ['label' => 'Vehículos', 'url' => ['/vehiculos/index']],
                ['label' => 'Zonas', 'url' => ['/zonas/index']],
            ]
        ],
        //['label' => 'Compras', 'url' => ['/compras/index'], ],
        ['label' => 'Producciones', 'url' => ['/producciones/index'], ],
        ['label' => 'Pedidos', 'url' => ['/pedidos/index'], ],
        ['label' => 'Repartos', 'url' => ['/repartos/index']],
        ['label' => 'Ventas', 'url' => ['/ventas/index'],],
        ['label' => 'Pagos', 'url' => ['/comprobantes-pago/index']],
        ['label' => 'Cajas', 'url' => ['/cajas/index']],
        ['label' => 'Informes', 'url' => ['/informes/index']],
        [
            'label' => 'Sistema', 'url' => ['#'],
            'items' => [
                ['label' => 'Usuarios', 'url' => ['/usuarios/index']],
                ['label' => 'Roles', 'url' => ['/roles/index']],
            ]
        ],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/usuarios/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/usuarios/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->Usuario . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container"> 
        <p><a href='http://ngserver.sytes.net:8050'>Acceso a Desarrollo</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
