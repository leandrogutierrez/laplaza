<?php

use common\assets\StockAsset;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

/* @var $this View */
/* @var $form ActiveForm */
StockAsset::register($this);
$this->registerJs('Stock.Alta.init()');

$this->title = 'Gestión de stock';
$this->params['breadcrumbs'][] = $this->title;
 
Yii::info($categorias);
?> 

<div class="row" id="producciones">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php if (in_array('AjusteProduccion', Yii::$app->session->get('Permisos'))) : ?>
                    <a href="<?= Url::to(['stock/ajuste']) ?>">
                        <button class="btn btn-primary pull-right" type="button" >
                            <i class="fa fa-plus"></i> Ajuste stock
                        </button>
                    </a>
                    <?php endif; ?>
                </div>
                <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'ver-stock-form',
                                                        'method' => 'get',
                                                        'layout' => 'inline']) ?>
                    <div class="col-md-4">  
                    <?php  
                        echo $form->field($busqueda, 'Combo3')->widget(Select2::className(), [
                                'data' => $categorias,
                                'options' => [
                                  'placeholder' => 'Buscar categoria...',
                                    'v-select2' => 'idCategoria',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                ]
                            ]);
                    ?> 
                    <p></p>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                            'attributeTo' => 'FechaFin',
                            'labelTo' => 'al',
                            'form' => $form,
                            'language' => 'es',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd/mm/yyyy',
                                'endDate' => 'd',
                            ]
                        ])?>
                    </div>
                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarcompras-button']) ?> 

                    <?php ActiveForm::end() ?>
                </div>
                <p></p>
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover" id="fixedheight">
                    <thead>
                        <tr class="tabla-header">
                            <th>Producto</th>
                            <th>Stock</th> 
                            <th>Pedidos</th> 
                            <th>Repartos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr v-on:dblclick="clickRow(<?= Html::encode($model['IdProduccion']) ?> )">
                            <td><?= Html::encode($model['Producto']) ?></td> 
                            <td><?= Html::encode($model['Stock']) ?></td> 
                            <td><?= Html::encode($model['Pedidos']) ?></td>
                            <td><?= Html::encode($model['Repartos']) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registra stock en el sistema.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

