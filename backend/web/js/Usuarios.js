'use strict';

var Usuarios = {};

Usuarios.Login = {
    keyServer: 'http://localhost:8000',
    init: function () {
        $.get(this.keyServer + '/key')
                .done(function (data) {
                    $('#codigo-llave').val(data.key);
                    if (data.version)
                        $('#version').val(data.version);
                });
    }
};