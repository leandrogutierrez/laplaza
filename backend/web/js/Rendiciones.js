'use strict';

var Rendiciones = {};

Rendiciones.Alta = {
    vue : null,
    init : function(){
        this.vue = new Vue({
            el : "#rendiciones-form",
            data : {
                idUsuario : "",
                usuario : {},
                pagos : [], 
                importe : ""
            },
            watch : {
                idUsuario : function(val){
                    var _this = this;
                    
                    if(parseInt(val) > 0){
                        $.get('/usuarios/dame-pagos',{id:val})
                                .done(function(result){
                                    console.log(result); 
                                    _this.pagos = result;
                        }).fail(function(){
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contáctese con el administrador.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores-modal').html(html);
                        });
                        $.get('/usuarios/dame-usuario',{id:val})
                                .done(function(result){
                                    console.log(result); 
                                    _this.usuario = result; 
                        }).fail(function(){
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contáctese con el administrador.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores-modal').html(html);
                        });
                    }
                }
            }
        });
    }
}