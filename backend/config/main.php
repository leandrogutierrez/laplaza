<?php

use kartik\mpdf\Pdf;
use kartik\datecontrol\Module;

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        require(__DIR__ . '/../../common/config/params-local.php'),
        require(__DIR__ . '/params.php'),
        require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name' => 'La Plaza',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],  
   
    'components' => [ 
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\Usuarios',
            'loginUrl' => '/usuarios/login',
            'authTimeout' => 60 * 60,
        ],
        'session' => [
            'name' => 'BACKSESSID',
            'timeout' => 60 * 60,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ], 
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller>/<id:\d+>' => '<controller>',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller>/index' => '<controller>',
            ],
        ],
        'consoleRunner' => [
            'class' => 'toriphes\console\Runner', 
        ],
        'pdf' => [
            'class' => 'kartik\mpdf\Pdf',
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
            ]         // Más opciones de configuración: http://demos.krajee.com/mpdf#settings       
        ],
    ],
    'as access' => [
            'class' => 'yii\filters\AccessControl',
            'rules' => [
                /**
                 *  Usuarios no logueados (rol ? )
                 */
                [
                    'allow' => true,
                    'actions' => ['login', 'error'],
                    'roles' => ['?'],
                ],
                //Mantenimiento
                [
                    'allow' => true,
                    'controllers' => ['mantenimientos']
                    
                ],
                //Debug
                [
                    'allow' => true,
                    'controllers' => ['debug', 'default', 'debug/default'],
                 ],
                /**
                 *  Usuarios logueados que deben cambiar contraseña
                 */
                [
                    'allow' => true,
                    'actions' => ['cambiar-password', 'logout',],
                    'roles' => ['@'],
                ],
                /**
                 *  Usuarios logueados que no deben cambiar contraseña, están activos
                 *  y tienen Token bueno
                 */
                [
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function()
                        {
                            $Usuario = Yii::$app->user->identity;
                            $Token = Yii::$app->session->get('Token');
                            return $Usuario->DebeCambiarPass == 'N' && $Usuario->Estado == 'A' && $Usuario->Token == $Token;
                        },
                ],
            ],
            'denyCallback' => function($rule, $action)
                {
                    if (!Yii::$app->user->isGuest)
                    {
                        if (Yii::$app->user->identity->DebeCambiarPass == 'S')
                        {
                            //Redirect
                            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
                            return $action->controller->redirect('/usuarios/cambiar-password');
                        }
                        else
                        {
                            Yii::$app->user->logout();
                            Yii::$app->session->setFlash('danger', 'Ocurrió un problema con su sesión.');
                            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
                            return $action->controller->redirect(Yii::$app->user->loginUrl);
                        }
                    }
                    
                    return $action->controller->redirect(Yii::$app->user->loginUrl);
                },
        ],
    'params' => $params,
];
