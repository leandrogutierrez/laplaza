<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use common\models\forms\BusquedaForm;
use common\models\GestorProveedores;
use common\models\Proveedores;

class ProveedoresController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarProveedores', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
        }
        
        $gestor = new GestorProveedores();
        $models = $gestor->Buscar($Cadena, $IncluyeBajas);
        
        return $this->render('index', [
                    'models' => $models,
                    'busqueda' => $busqueda,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaProveedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $proveedor = new Proveedores();
        $proveedor->setScenario(Proveedores::_ALTA);
        if ($proveedor->load(Yii::$app->request->post()) && $proveedor->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorProveedores();
            $resultado = $gestor->Alta($proveedor);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            return $this->renderAjax('datos-proveedor', [
                        'model' => $proveedor,
                        'titulo' => 'Alta de proveedor'
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarProveedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $proveedor = new Proveedores();
        $proveedor->setScenario(Proveedores::_MODIFICAR);
        if (intval($id)) {
            $proveedor->IdProveedor = $id;
        } else {
            throw new HttpException('422', 'El proveedor indicado es inválido');
        }
        
        if ($proveedor->load(Yii::$app->request->post()) && $proveedor->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorProveedores();
            $resultado = $gestor->Modificar($proveedor);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $proveedor->Dame();
            return $this->renderAjax('datos-proveedor', [
                        'model' => $proveedor,
                        'titulo' => 'Modificación de proveedor'
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarProveedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $proveedor = new Proveedores();
        if (intval($id)) {
            $proveedor->IdProveedor = $id;
        } else {
            throw new HttpException('422', 'El proveedor indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $proveedor->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaProveedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $proveedor = new Proveedores();
        if (intval($id)) {
            $proveedor->IdProveedor = $id;
        } else {
            throw new HttpException('422', 'El proveedor indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $proveedor->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarProveedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $proveedor = new Proveedores();
        if (intval($id)) {
            $proveedor->IdProveedor = $id;
        } else {
            throw new HttpException('422', 'El proveedor indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorProveedores();
        $resultado = $gestor->Borrar($proveedor);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletarProveedores($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $proveedor = new Proveedores();
            
            $proveedor->IdProveedor = $id;
            
            $proveedor->Dame();
            
            $out = [
                'id' => $proveedor->IdProveedor,
                'text' => $proveedor->Proveedor,
            ];
        } else {
            $gestor = new GestorProveedores();
            
            $proveedores = $gestor->Buscar($cadena);
            
            $out = array();
            
            foreach ($proveedores as $proveedor) {
                $out[] = [
                    'id' => $proveedor['IdProveedor'],
                    'text' => $proveedor['Proveedor']
                ];
            }
        }
        
        return $out;
    }
}
