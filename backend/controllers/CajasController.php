<?php
namespace backend\controllers;

use common\models\Cajas;
use common\models\forms\BusquedaForm;
use common\models\GestorCajas;
use common\models\GestorSucursales;
use common\models\GestorUsuarios;
use common\models\Sucursales;
use common\models\UsosCaja;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;

class CajasController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($IdSucursal = null)
    {
        if (!in_array('BuscarCajas', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $IdSucursal = $busqueda->Combo3;
        } else {
            $busqueda->Combo3 = $IdSucursal;
        }
          
        $gestorUsuarios  = new GestorUsuarios();
        $usuarios = $gestorUsuarios->Buscar('', 'N');
        
        $gestorSucursales = new GestorSucursales();
        $sucursales = $gestorSucursales->Buscar();
        
        $gestorCajas = new GestorCajas();
        $cajas = $gestorCajas->EstadoCajas();
        
        $paginado->totalCount = count($cajas);
        $cajas = array_slice($cajas, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $cajas,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'usuarios' => $usuarios,
                    'sucursales' => $sucursales
        ]);
    }
    
     
    public function actionAbrir($IdCaja)
    {
        $caja = new Cajas();
        $caja->IdCaja = $IdCaja;
        $caja->Dame();
        
        $mensaje = $caja->AbrirCaja();

        if (substr($mensaje, 0, 2) == 'OK') {
            $usoCaja = $caja->DameUsoCajaActual();
            Yii::$app->session->set('UsoCaja', $usoCaja);
            return $this->redirect(Url::to(['/cajas']));
        } else {
            Yii::$app->session->setFlash('danger', $mensaje);
            return $this->redirect(Url::to(['cerrada']));
        }
    }

    public function actionCerrar($IdCaja, $NroUsoCaja)
    {
        $caja = new Cajas();
        $caja->IdCaja = $IdCaja;
        $caja->Dame();
        
        $usoCaja = new UsosCaja();
        $usoCaja->IdCaja = $IdCaja;
        $usoCaja->NroUsoCaja = $NroUsoCaja;
        $usoCaja->Dame();
        
        $caja->UsoCaja = $usoCaja;
            
        if (Yii::$app->request->getIsGet()) {
            Yii::$app->response->format = 'json';
  
            $mensaje = $caja->CerrarCajaAdministracion();

            if ($mensaje == 'OK') {
                Yii::$app->session->set('UsoCaja', $caja->DameUsoCajaActual());
                return ['error' => null];
            } else {
                return ['error' => $mensaje];
            }
        }
    }
    
    public function actionAutocompletarSucursales($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $sucursal = new Sucursales();
            
            $sucursal->IdSucursal = $id;
            
            $sucursal->Dame();
            
            $out = [
                'id' => $sucursal->IdSucursal,
                'text' => $sucursal->Sucursal
            ];
        } else {
            $gestor = new GestorSucursales();
            
            $sucursales = $gestor->Buscar($cadena);
            
            $out = array();
            
            foreach ($sucursales as $sucursal) {
                $out[] = [
                    'id' => $sucursal['IdSucursal'],
                    'text' => $sucursal['Sucursal']
                ];
            }
        }
        return $out;
    }
    
    public function actionVerMovimientosCaja($IdCaja = null, $NroUsoCaja = null, $FechaInicio = null, $FechaFin = null, $TipoMov = 'T', $NroMov = null)
    {
        $usoCaja = new UsosCaja();
        $usoCaja->IdCaja = $IdCaja;
        $usoCaja->NroUsoCaja = $NroUsoCaja;
        $usoCaja->Dame();
        
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        $busqueda = new BusquedaForm();
            
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $TipoMov = $busqueda->Combo;
            $NroMov = $busqueda->Cadena;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $TipoMov = $busqueda->Combo;
            $NroMov = $busqueda->Cadena;
        }
        
        $movimientos = $usoCaja->BuscarMovimientos($FechaInicio, $FechaFin, $TipoMov, $NroMov);
        $paginado->totalCount = count($movimientos);
        $movimientos = array_slice($movimientos, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('movimientos-uso-caja', [
                    'usoCaja' => $usoCaja,
                    'busqueda' => $busqueda,
                    'movimientos' => $movimientos,
                    'paginado' => $paginado
        ]);
    }
}
