<?php
namespace backend\controllers;

use common\components\FechaHelper;
use common\models\Clientes;
use common\models\forms\BusquedaForm;
use common\models\GestorCategoriasProducto;
use common\models\GestorClientes;
use common\models\GestorProducciones;
use common\models\GestorProductos;
use common\models\GestorRepartos;
use common\models\GestorUsuarios;
use common\models\Producciones;
use common\models\Productos;
use common\models\Repartos;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use function GuzzleHttp\json_encode;

class ProduccionesController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($IdUsuario = null, $FechaInicio = null, $FechaFin = null, $Estado = null, $IdCategoria = null)
    {
        if (!in_array('BuscarProducciones', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $IdCategoria = $busqueda->Combo3;
            $IdUsuario = $busqueda->Combo2;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo3 = $IdCategoria;
            $busqueda->Combo2 = $IdUsuario;
        }
        
        $gestorUsuarios  = new GestorUsuarios();
        $usuarios = ArrayHelper::map($gestorUsuarios->Buscar('', 'N'), 'IdUsuario', 'NombreCompleto');
   
        $gestorCategorias = new GestorCategoriasProducto();
        $categorias = ArrayHelper::map($gestorCategorias->ListarCategoriasPadre(), 'IdCategoriaProducto', 'Categoria');

        $gestor = new GestorProducciones();
        $producciones = $gestor->BuscarAvanzado($IdUsuario, $IdCategoria, $FechaInicio, $FechaFin, $Estado);
        
        $paginado->totalCount = count($producciones);
        $producciones = array_slice($producciones, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $producciones,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'categorias' => $categorias,
                    'usuarios' => $usuarios,
        ]);
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarProduccion', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $produccion = new Producciones();
        if (intval($id)) {
            $produccion->IdProduccion = $id;
        } else {
            throw new HttpException('422', 'La produccion indicada es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorProducciones();
        $resultado = $gestor->Borrar($produccion);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaProduccion', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $produccion = new Producciones();
        
        $gestorCategorias = new GestorCategoriasProducto();
        $categorias = ArrayHelper::map($gestorCategorias->ListarCategoriasPadre(), 'IdCategoriaProducto', 'Categoria');

        return $this->render('alta', [
                    'model' => $produccion,
                    'categorias' => $categorias
        ]);
    }
    
    public function actionDetalles($id)
    {
        if (!in_array('BuscarProducciones', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $produccion = new Producciones();
        if (intval($id)) {
            $produccion->IdProduccion = $id;
        } else {
            throw new HttpException('422', 'La produccion indicada es inválida');
        }
        
        $produccion->Dame();
        
        return $this->renderAjax('datos-produccion', [
                'model' => $produccion,
                'titulo' => 'Datos de la produccion'
        ]);
    }
    
    public function actionAnular($id)
    {
        if (!in_array('AnularProduccion', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $produccion = new Producciones();
        if (intval($id)) {
            $produccion->IdProduccion = $id;
        } else {
            throw new HttpException('422', 'La produccion indicada es inválida');
        }
        
        $produccion->Dame();
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorProducciones();
        $resultado = $gestor->Anular($produccion);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionFinalizarProduccion()
    {
        Yii::$app->response->format = 'json';
        
        if (!in_array('AltaProduccion', Yii::$app->session->get('Permisos'))) {
            return ['error' => 'No tiene permisos para realizar esta acción.'];
        }
        
        if (Yii::$app->request->isPost) {
            $produccion = new Producciones();
            $produccion->IdCategoria = Yii::$app->request->post('idCategoria');
            if ($produccion->IdCategoria == 0) {
                $produccion->IdCategoria = null;
            }
            
            $produccion->LineasProduccion = json_encode(Yii::$app->request->post('lineas'));
            $produccion->FechaProduccion = Yii::$app->request->post('fechaProduccion');
            
            $gestor = new GestorProducciones();
            $resultado = $gestor->Alta($produccion);
            
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        }
    }
    
    public function actionAutocompletarRepartos($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $reparto = new Repartos();
            
            $reparto->IdRepearto = $id;
            
            $reparto->Dame();
            
            $out = [
                'id' => $reparto->IdReparto,
                'text' => $reparto->Zona . ' - ' . $reparto->Usuario . ' - ' . $reparto->FechaReparto,
            ];
        } else {
            $gestor = new GestorRepartos();
            
            if (!in_array('AltaReparto', Yii::$app->session->get('Permisos'))) {
                $repartos = $gestor->BuscarAvanzado(Yii::$app->user->identity->IdUsuario, null, null, null, 'A');
            } else {
                $repartos = $gestor->Buscar($cadena, 'N');
            }
            $out = array();
            
            foreach ($repartos as $reparto) {
                $out[] = [
                    'id' => $reparto['IdReparto'],
                    'text' => $reparto['Zona'] .' - '. $reparto['Usuario'].' - '.$reparto['FechaReparto']
                ];
            }
        }
        return $out;
    }
    
    
    public function actionAutocompletarClientes($id = 0, $idReparto = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $cliente = new Clientes();
            
            $cliente->IdCliente = $id;
            
            $cliente->Dame();
            
            $out = [
                'id' => $cliente->IdCliente,
                'text' => $cliente->Nombres . ' ' . $cliente->Apellidos,
            ];
        } else {
            $gestor = new GestorClientes();
            
            $reparto = new Repartos();
            $reparto->IdReparto = $idReparto;
            $reparto->Dame();
            
            $idZona = $reparto->IdZona;
            $clientes = $gestor->Buscar($cadena, $idZona, 'N');
            
            $out = array();
            
            foreach ($clientes as $cliente) {
                $out[] = [
                    'id' => $cliente['IdCliente'],
                    'text' => $cliente['Nombres'] .' '. $cliente['Apellidos']
                ];
            }
        }
        return $out;
    }
    
    public function actionDamePedidoFecha($id, $idReparto)
    {
        Yii::$app->response->format = 'json';
        
        $reparto = new Repartos();
        $reparto->IdReparto = $idReparto;
        $reparto->Dame();
         
        $cliente = new Clientes();
        $cliente->IdCliente = $id;
        
        $pedido = $cliente->DamePedidoFecha(FechaHelper::formatearDateMysql($reparto->FechaReparto));
        
        return $pedido;
    }

    public function actionAutocompletarProductos($id = 0, $idCategoria = '', $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $producto = new Productos();
            
            $producto->IdProducto = $id;
            
            $producto->Dame();
            
            $out = [
                'id' => $producto->IdProducto,
                'text' => $producto->Descripcion
            ];
        } else {
            $gestor = new GestorProductos();
            
            $productos = $gestor->BuscarPorCategoria($cadena,$idCategoria);
            
            $out = array();
            
            foreach ($productos as $producto) {
                $out[] = [
                    'id' => $producto['IdProducto'],
                    'text' => $producto['Descripcion']
                ];
            }
        }
        return $out;
    }
}
