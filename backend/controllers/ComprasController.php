<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use common\models\forms\BusquedaForm;
use common\models\Compras;
use common\models\GestorCompras;
use common\models\GestorMateriasPrima;
use common\models\GestorConceptosCompra;

class ComprasController extends Controller
{
    public function actionIndex()
    {
        $fechaInicio = date('d/m/Y', strtotime('-1 month', strtotime(date('Y-m-d'))));
        return $this->actionListar($fechaInicio, date('d/m/Y'), 'T');
    }
    
    public function actionListar($FechaInicio = null, $FechaFin = null, $Estado = 'T', $Tipo = 'T')
    {
        if (!in_array('BuscarCompras', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $Tipo = $busqueda->Combo2;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo2 = $Tipo;
        }
        $gestor = new GestorCompras();
        $compras = $gestor->BuscarAvanzado($FechaInicio, $FechaFin, $Estado, $Tipo);
        
        $paginado->totalCount = count($compras);
        $compras = array_slice($compras, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $compras,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
        ]);
    }
    
    
    public function actionAlta()
    {
        if (!in_array('AltaCompra', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $compra = new Compras();
        
        $gestorCC = new GestorConceptosCompra();
        $conceptosCompra = $gestorCC->Buscar();
        
        return $this->render('alta', [
                    'model' => $compra,
                    'conceptosCompra' => $conceptosCompra,
        ]);
    }
    
    public function actionFinalizarCompra()
    {
        Yii::$app->response->format = 'json';
        
        if (!in_array('AltaCompra', Yii::$app->session->get('Permisos'))) {
            return ['error' => 'No tiene permisos para realizar esta acción.'];
        }
        
        if (Yii::$app->request->isPost) {
            $compra = new Compras();
            $compra->IdProveedor = Yii::$app->request->post('idProveedor');
            if ($compra->IdProveedor == 0) {
                $compra->IdProveedor = null;
            }
            
            $compra->LineasCompra = json_encode(Yii::$app->request->post('lineas'));
            $compra->Importe = Yii::$app->request->post('importe');
            $compra->Tipo = Yii::$app->request->post('tipo');
            
            $gestor = new GestorCompras();
            $resultado = $gestor->Alta($compra);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        }
    }
    
    public function actionDetalles($id)
    {
        if (!in_array('BuscarCompras', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $compra = new Compras();
        if (intval($id)) {
            $compra->IdCompra = $id;
        } else {
            throw new HttpException('422', 'La compra indicada es inválida');
        }
        
        $compra->Dame();
        
        
        return $this->renderAjax('datos-compra', [
                'model' => $compra,
                'titulo' => 'Datos de la compra'
        ]);
    }
   
    public function actionBorrar($id)
    {
        Yii::$app->response->format = 'json';
        
        if (!in_array('BorrarCompra', Yii::$app->session->get('Permisos'))) {
            return ['error' => 'No tiene permisos para realizar esta acción.'];
        }
        
        $compra = new Compras();
        if (intval($id)) {
            $compra->IdCompra = $id;
        } else {
            return ['error' => 'La compra indicada es inválida.'];
        }
        
        $gestor = new GestorCompras();
        $resultado = $gestor->Borrar($compra);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletarMp($id = 0, $cadena = '', $tipoCompra = 'T')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $materiaPrima = new MateriasPrima();
            
            $materiaPrima->IdMateriaPrima = $id;
            
            $materiaPrima->Dame();
            
            $out = [
                'id' => $materiaPrima->IdMateriaPrima,
                'text' => $materiaPrima->MateriaPrima
            ];
        } else {
            $esAnimalVivo = ($tipoCompra == 'V') ? 'S' : 'N';
            
            $gestor = new GestorMateriasPrima();
            $materiasPrima = $gestor->Buscar($cadena, 'N', $esAnimalVivo);
            
            $out = array();
            foreach ($materiasPrima as $mp) {
                $out[] = [
                    'id' => $mp['IdMateriaPrima'],
                    'text' => $mp['MateriaPrima']
                ];
            }
        }
        return $out;
    }
}
