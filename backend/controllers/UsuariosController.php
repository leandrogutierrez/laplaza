<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Empresa;
use common\models\forms\BusquedaForm;
use common\models\forms\CambiarPasswordForm;
use common\models\Usuarios;
use common\models\GestorUsuarios;
use common\models\GestorRoles;


class UsuariosController extends Controller
{
    
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarUsuarios', Yii::$app->session->get('Permisos')))
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        
        $busqueda = new BusquedaForm();
        if($busqueda->load(Yii::$app->request->post()) && $busqueda->validate())
        {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
        }
        
        $gestor = new GestorUsuarios();
        $usuarios = $gestor->Buscar($Cadena, $IncluyeBajas);
        
        return $this->render('index',[
                    'models' => $usuarios,
                    'busqueda' => $busqueda,
        ]);
    }
    
    public function actionLogin()
    {
        // Si ya estoy logueado redirecciona al home
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $this->layout = 'login';
        $usuario = new Usuarios();
        $usuario->setScenario(Usuarios::_LOGIN);
        
        $empresa = new Empresa();
        Yii::$app->session->set('Parametros', ArrayHelper::map($empresa->DameDatos(), 'Parametro', 'Valor'));
        
        if ($usuario->load(Yii::$app->request->post()) && $usuario->validate())
        {
            $login = $usuario->Login($usuario->Password);
          
            if ($login == 'OK')
            {
                Yii::$app->user->login($usuario);
                Yii::$app->session->set('Token', $usuario->Token);
                Yii::$app->session->set('Permisos', $usuario->DamePermisos());
                // El usuario debe modificar el password
                if ($usuario->DebeCambiarPass == 'S')
                {
                    Yii::$app->getSession()->setFlash('info','Debe modificar su contraseña antes de ingresar.');
                    return $this->redirect('/usuarios/cambiar-password');
                }
                else
                    return $this->redirect(Url::to(['/site/index']));
            }
            else
            {
                $usuario->Password = NULL;
                Yii::$app->getSession()->setFlash('danger', $login);
                
                return $this->render('login', [
                            'model' => $usuario,
                ]);
            }
        }
        else
        {
            return $this->render('login', [
                        'model' => $usuario,
            ]);
        }
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
        
    public function actionAlta()
    {
        if (!in_array('AltaUsuario', Yii::$app->session->get('Permisos')))
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        
        $usuario = new Usuarios();
        $usuario->setScenario(Usuarios::_ALTA);
        if($usuario->load(Yii::$app->request->post()) && $usuario->validate())
        {      
            Yii::$app->response->format = 'json';
            $gestor = new GestorUsuarios();
            $resultado = $gestor->Alta($usuario);
            if(substr($resultado,0,2) == 'OK')
            {
                return ['error' => null];
            }
            else
            {
                return ['error' => $resultado];
            }
        }
        else
        { 
            $gestorRoles = new GestorRoles();
            $listadoRoles = $gestorRoles->Buscar();
            return $this->renderAjax('datos-usuario',[
                'model' => $usuario,
                'titulo' => 'Alta de usuario',
                'listadoRoles' => $listadoRoles,
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarUsuario', Yii::$app->session->get('Permisos')))
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        
        $usuario = new Usuarios();
        $usuario->setScenario(Usuarios::_MODIFICAR);
        if(intval($id))
            $usuario->IdUsuario = $id;
        else
            throw new HttpException('422','El usuario indicado es inválido.');
        
        if($usuario->load(Yii::$app->request->post()) && $usuario->validate())
        {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorUsuarios();
            $resultado = $gestor->Modificar($usuario);
            if($resultado == 'OK')
            {
                return ['error' => null];
            }
            else
            {
                return ['error' => $resultado];
            }
        }
        else
        {
            $usuario->Dame();
            $gestorRoles = new GestorRoles();
            $roles = $gestorRoles->Buscar();
            return $this->renderAjax('datos-usuario',[
                        'model' => $usuario,
                        'listadoRoles' => $roles,
                        'titulo' => 'Modificar usuario'
            ]);
        }
    }
    
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarUsuario', Yii::$app->session->get('Permisos')))
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        
        $usuario = new Usuarios();
        if(intval($id))
            $usuario->IdUsuario = $id;
        else
            throw new HttpException('422','El usuario indicado es inválido.');
        
        Yii::$app->response->format = 'json';
        $resultado = $usuario->Activar();
        if($resultado == 'OK')
        {
            return ['error' => null];
        }
        else
        {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaUsuario', Yii::$app->session->get('Permisos')))
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        
        $usuario = new Usuarios();
        if(intval($id))
            $usuario->IdUsuario = $id;
        else
            throw new HttpException('422','El usuario indicado es inválido.');
        
        Yii::$app->response->format = 'json';
        $resultado = $usuario->DarBaja();
        if($resultado == 'OK')
        {
            return ['error' => null];
        }
        else
        {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarUsuario', Yii::$app->session->get('Permisos')))
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        
        $usuario = new Usuarios();
        if(intval($id))
            $usuario->IdUsuario = $id;
        else
            throw new HttpException('422','El usuario indicado es inválido.');
        
        Yii::$app->response->format = 'json';
        $gestor = new GestorUsuarios();
        $resultado = $gestor->Borrar($usuario);
        if($resultado == 'OK')
        {
            return ['error' => null];
        }
        else
        {
            return ['error' => $resultado];
        }
    }
    
    public function actionCambiarPassword()
    {
        $form = new CambiarPasswordForm();
        $form->setScenario(CambiarPasswordForm::_CAMBIAR);

        $this->layout = 'login';

        if ($form->load(Yii::$app->request->post()) && $form->validate())
        {
            $usuario = Yii::$app->user->identity;

            $mensaje = $usuario->CambiarPassword($usuario->Token,$form->Anterior,$form->Password_repeat);

            if ($mensaje == 'OK')
            {
                Yii::$app->user->logout();
                Yii::$app->session->setFlash('success', 'La contraseña fue modificada.');
                return $this->goHome();
            }
            else
            {
                Yii::$app->session->setFlash('danger', $mensaje);
                return $this->render('password', [
                            'model' => $form,
                ]);
            }
        }
        else
        {
            return $this->render('password', [
                        'model' => $form,
            ]);
        }
    }
    
    public function actionAutocompletar($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if($id != 0)
        {
            $usuario = new Usuarios();
            
            $usuario->IdUsuario = $id;
            
            $usuario->Dame();
            
            $out = [
                'id' => $usuario->IdUsuario, 
                'text' => $usuario->NombreCompleto . ' ('.$usuario->Usuario.')'
            ];
        }
        else
        {
            $gestor = new GestorUsuarios();
            
            $usuarios = $gestor->Buscar($cadena, 'N');
            
            $out = array();
            
            foreach($usuarios as $usuario)
            {
                $out[] = [
                    'id' => $usuario['IdUsuario'],
                    'text' => $usuario['NombreCompleto']. ' ('.$usuario['Usuario'].')'
                ];
            }
        }
        return $out;
    }
}
