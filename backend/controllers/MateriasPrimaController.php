<?php
namespace backend\controllers;

use common\models\GestorMateriasPrima;
use common\models\MateriasPrima;
use common\models\forms\BusquedaForm;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;

class MateriasPrimaController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N', $Tipo = 'T')
    {
        if (!in_array('BuscarMateriasPrima', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
            $Tipo = $busqueda->Combo;
        }
        
        $gestor = new GestorMateriasPrima();
        $models = $gestor->Buscar($Cadena, $IncluyeBajas, $Tipo);
        return $this->render('index', [
                    'models' => $models,
                    'busqueda' => $busqueda,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaMateriaPrima', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $materiaPrima = new MateriasPrima();
        $materiaPrima->setScenario(MateriasPrima::_ALTA);
        if ($materiaPrima->load(Yii::$app->request->post()) && $materiaPrima->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorMateriasPrima();
            $resultado = $gestor->Alta($materiaPrima);
            
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            return $this->renderAjax('datos-materia-prima', [
                        'titulo' => 'Alta de materia prima',
                        'model' => $materiaPrima
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarMateriaPrima', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $materiaPrima = new MateriasPrima();
        if (intval($id)) {
            $materiaPrima->IdMateriaPrima = $id;
        } else {
            throw new HttpException('422', 'La materia prima indicada es inválida.');
        }
        
        $materiaPrima->setScenario(MateriasPrima::_MODIFICAR);
        if ($materiaPrima->load(Yii::$app->request->post()) && $materiaPrima->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorMateriasPrima();
            $resultado = $gestor->Modificar($materiaPrima);
            
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $materiaPrima->Dame();
            return $this->renderAjax('datos-materia-prima', [
                        'titulo' => 'Modificar materia prima',
                        'model' => $materiaPrima
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarMateriaPrima', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $materiaPrima = new MateriasPrima();
        if (intval($id)) {
            $materiaPrima->IdMateriaPrima = $id;
        } else {
            throw new HttpException('422', 'La materia prima indicada es inválida.');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $materiaPrima->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaMateriaPrima', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $materiaPrima = new MateriasPrima();
        if (intval($id)) {
            $materiaPrima->IdMateriaPrima = $id;
        } else {
            throw new HttpException('422', 'La materia prima indicada es inválida.');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $materiaPrima->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarMateriaPrima', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $materiaPrima = new MateriasPrima();
        if (intval($id)) {
            $materiaPrima->IdMateriaPrima = $id;
        } else {
            throw new HttpException('422', 'La materia prima indicada es inválida.');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorMateriasPrima();
        $resultado = $gestor->Borrar($materiaPrima);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletarMp($id = 0, $cadena = '', $tipo = 'T')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $materiaPrima = new MateriasPrima();
            
            $materiaPrima->IdMateriaPrima = $id;
            
            $materiaPrima->Dame();
            
            $out = [
                'id' => $materiaPrima->IdMateriaPrima,
                'text' => $materiaPrima->MateriaPrima
            ];
        } else {
            $gestor = new GestorMateriasPrima();
            $materiasPrima = $gestor->Buscar($cadena, 'N', $tipo);
            
            $out = array();
            foreach ($materiasPrima as $mp) {
                $out[] = [
                    'id' => $mp['IdMateriaPrima'],
                    'text' => $mp['MateriaPrima']
                ];
            }
        }
        return $out;
    }
    public function actionDameMp($id)
    {
        Yii::$app->response->format = 'json';
        
        $materiaPrima = new MateriasPrima();
        if (intval($id)) {
            $materiaPrima->IdMateriaPrima = $id;
        } else {
            return ['error' => 'La materia prima indicada es inválida.'];
        }
        
        $materiaPrima->Dame();
        
        return $materiaPrima;
    }
}
