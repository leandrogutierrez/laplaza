<?php
namespace backend\controllers;

use common\models\GestorReportes;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class InformesController extends Controller
{
    public function actionIndex($id = null, $key = null)
    {
        $gestor = new GestorReportes();

        $menu = $gestor->ListarMenu();
        $model = null;
        $reporte = null;
        $parametros = null;
        $tabla = null;

        if (intval($id)) {
            $reporte = $gestor->DameModeloReporte($id);

            if (!in_array($reporte['Reporte'], Yii::$app->session->get('Permisos'))) {
                throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
            }

            if (!Yii::$app->request->isPost) {
                $parametros = $gestor->DameParametrosModeloReporte($id);

                $model = new DynamicModel(ArrayHelper::map($parametros, 'Parametro', 'ValorDefecto'));

                if ($key != null && Yii::$app->cache->get($key)) {
                    $model = Yii::$app->cache->get($key)['model'];
                    $tabla = Yii::$app->cache->get($key)['resultado'];
                }
            } else {
                $parametros = $gestor->DameParametrosModeloReporte($id, 'P');

                $model = new DynamicModel(ArrayHelper::getColumn($parametros, 'Parametro'));

                $model->addRule($model->attributes(), 'safe');

                // Agrego reglas de validación
                foreach ($parametros as $parametro) {
                    if ($parametro['Tipo'] == 'E') {
                        $model->addRule($parametro['Parametro'], 'integer');
                    } elseif ($parametro['Tipo'] == 'M') {
                        $model->addRule($parametro['Parametro'], 'match', ['pattern' => '/^[0-9]{1,12}((\.|,)[0-9]{0,2})?$/', 'message' => '{attribute} debe ser un número positivo.']);
                    }
                }
                
                if (count($parametros) == 0 || $model->load(Yii::$app->request->post()) && $model->validate()) {
                    $valores = [];
                    
                    foreach ($parametros as $parametro) {
                        if ($parametro['Tipo'] == 'F') {
                            $valor = $model->{$parametro['Parametro']} == null ? null : date("Y-m-d", strtotime(str_replace('/', '-', $model->{$parametro['Parametro']})));
                        } elseif ($parametro['Tipo'] == 'M') {
                            $valor = str_replace(',', '.', $model->{$parametro['Parametro']});
                        } elseif ($parametro['Tipo'] == 'A') {
                            $valor = ($model->{$parametro['Parametro']} == '') ? 0 : $model->{$parametro['Parametro']};
                        } elseif ($parametro['Tipo'] == 'T') {
                            $valor = ($model->{$parametro['Parametro']} == '') ? 0 : $model->{$parametro['Parametro']};
                            /* $cadena = '';
                            if (is_array($valor)) {
                                foreach($valor as $v){
                                    $cadena .= $v;
                                    if(next($valor)){
                                        $cadena .= ',';
                                    }
                                    
                                }
                                $valor = $cadena; 
                            }*/
                        } else {
                            $valor = $model->{$parametro['Parametro']};
                        }
                        Yii::info($valor, 'valor');
                        $valores[] = is_array($valor) ? "'".json_encode($valor, JSON_NUMERIC_CHECK)."'" : "'$valor'";
                    }

                    $output = null;

                    $key = Yii::$app->security->generateRandomString();
                    $idReporte = $reporte['IdModeloReporte'];
                    $cadena = implode(',', $valores);
                    $tabla = Yii::$app->cache->set($key, [
                        'model' => $model,
                        'resultado' => null,
                    ]);
                        

                    $comando = "informes/generar '$key' '$idReporte' \"$cadena\" > /dev/null &";
                    
                    Yii::info($comando);
                    Yii::$app->consoleRunner->run($comando);

                    Yii::$app->response->format = 'json';
                    return [
                        'error' => null,
                        'key' => $key
                    ];
                }
            }
        }
        return $this->render('index', [
                    'menu' => $menu,
                    'model' => $model,
                    'reporte' => $reporte,
                    'parametros' => $parametros,
                    'tabla' => $tabla,
        ]);
    }
    
    public function actionEstado($key)
    {
        Yii::$app->response->format = 'json';

        $resultado = Yii::$app->cache->get($key)['resultado'];

        return ['ready' => $resultado != null || is_array($resultado)];
    }
    
    public function actionAutocompletar($idModeloReporte, $nroParametro, $id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';

        $gestor = new GestorReportes();

        if ($id != 0) {
            $nombres = $gestor->DameParametroListado($idModeloReporte, $nroParametro, $id);
            
            if(sizeof($nombres,0) <= 1)
                $out = ['id' => $id, 'text' => $nombres[0]];
            else
                $out = ['id' => $id, 'text' => $nombres];
        } else {
            $elementos = $gestor->LlenarListadoParametro($idModeloReporte, $nroParametro, $cadena);

            $out = array();

            foreach ($elementos as $elemento) {
                $out[] = ['id' => $elemento['Id'], 'text' => $elemento['Nombre']];
            }
        }

        return $out;
    }
    
    public function actionImprimirInforme($id, $key)
    {
        $gestor = new GestorReportes();
        $modeloReporte = $gestor->DameModeloReporte($id);
        $parametros = $gestor->DameParametrosModeloReporte($id);
        
        if ($key != null && Yii::$app->cache->get($key)) {
            $model = Yii::$app->cache->get($key)['model'];
            $tabla = Yii::$app->cache->get($key)['resultado'];
        }
        
        foreach ($parametros as $parametro) {
            if ($parametro['ProcDame'] != null && $model->{$parametro['Parametro']} != 0) {
                $model->{$parametro['Parametro']} = $gestor->DameParametro($parametro['ProcDame'], $model->{$parametro['Parametro']})['Nombre'];
            }
        }
        
        $html = $this->renderPartial('informe-pdf', [
            'model' => $model,
            'tabla' => $tabla,
            'titulo' => $modeloReporte['NombreMenu'],
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
    public function actionExportarExcel($id, $key)
    {
        if (!intval($id) || !Yii::$app->cache->get($key)) {
            throw new HttpException('422', 'El informe es inválido.');
        }
        
        $gestor = new GestorReportes();
        $reporte = $gestor->DameModeloReporte($id);
        $operador = Yii::$app->user->identity;
        $tabla = Yii::$app->cache->get($key)['resultado'];
        
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCompany('Milenia')
                ->setCreated(time())
                ->setTitle($reporte['NombreMenu']);
        
        $spreadsheet->getActiveSheet()->mergeCells('A1:C1');
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 1, $reporte['NombreMenu']);

        $dateNow = date('d/m/Y H:i ');
        $spreadsheet->getActiveSheet()->getCell('E1')->setValue($dateNow);

        $spreadsheet->getActiveSheet()->getCell('E2')->setValue('Operador: '.$operador->Apellidos.', '.$operador->Nombres);

        $aplicacion = 'Administración';
        $spreadsheet->getActiveSheet()->getCell('E3')->setValue($aplicacion);

        $ip = Yii::$app->request->userIP;
        $spreadsheet->getActiveSheet()->getCell('E4')->setValue('IP: '.$ip);
        
        // Fill worksheet from values in array
        $spreadsheet->getActiveSheet()->fromArray($tabla, null, 'A8');
        // Set AutoSize
        for ($i = 1; $i <= count($tabla[0]); $i++) {
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($i, 7, array_keys($tabla[0])[$i-1]);
            $spreadsheet->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->freezePane('A8');
        
        header('Content-Type: application/vnd.ms-excel');
        $filename = $reporte['NombreMenu'] . ".xls";
        header('Content-Disposition: attachment;filename="' . $filename . '" ');
        header('Cache-Control: max-age=0');
        
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;
    }
}
