<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\models\forms\BusquedaForm;
use common\models\GestorZonas;
use common\models\Zonas;

class ZonasController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarZonas', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
        }
        
        $gestor = new GestorZonas();
        $models = $gestor->Buscar($Cadena, $IncluyeBajas);
        
        return $this->render('index', [
                    'models' => $models,
                    'busqueda' => $busqueda,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaZona', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $zona = new Zonas();
        $zona->setScenario(Zonas::_ALTA);
        if ($zona->load(Yii::$app->request->post()) && $zona->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorZonas();
            $resultado = $gestor->Alta($zona);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            return $this->renderAjax('datos-zona', [
                        'model' => $zona,
                        'titulo' => 'Alta de zona',
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarZona', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $zona = new Zonas();
        $zona->setScenario(Zonas::_MODIFICAR);
        if (intval($id)) {
            $zona->IdZona = $id;
        } else {
            throw new HttpException('422', 'La zona indicada es inválida');
        }
        
        if ($zona->load(Yii::$app->request->post()) && $zona->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorZonas();
            $resultado = $gestor->Modificar($zona);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $zona->Dame();
            return $this->renderAjax('datos-zona', [
                        'model' => $zona,
                        'titulo' => 'Modificar zona',
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarZona', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $zona = new Zonas();
        if (intval($id)) {
            $zona->IdZona = $id;
        } else {
            throw new HttpException('422', 'La zona indicada es inválida');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $zona->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaZona', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $zona = new Zonas();
        if (intval($id)) {
            $zona->IdZona = $id;
        } else {
            throw new HttpException('422', 'La zona indicada es inválida');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $zona->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarZona', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $zona = new Zonas();
        if (intval($id)) {
            $zona->IdZona= $id;
        } else {
            throw new HttpException('422', 'La zona indicada es inválida');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorZonas();
        $resultado = $gestor->Borrar($zona);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletar($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $zona = new Zonas();
            
            $zona->IdZona = $id;
            
            $zona->Dame();
            
            $out = [
                'id' => $zona->IdZona,
                'text' => $zona->Zona
            ];
        } else {
            $gestor = new GestorZonas();
            
            $zonas = $gestor->Buscar($cadena, 'N');
            
            $out = array();
            
            foreach ($zonas as $zona) {
                $out[] = [
                    'id' => $zona['IdZona'],
                    'text' => $zona['Zona']
                ];
            }
        }
        return $out;
    }
}
