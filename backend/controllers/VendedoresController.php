<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\models\forms\BusquedaForm;
use common\models\GestorVendedores;
use common\models\GestorUsuarios;
use common\models\Vendedores;
use common\models\Usuarios;

class VendedoresController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarVendedores', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
        }
        
        $gestor = new GestorVendedores();
        $models = $gestor->Buscar($Cadena, $IncluyeBajas);
        
        return $this->render('index', [
                    'models' => $models,
                    'busqueda' => $busqueda,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaVendedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vendedor = new Vendedores();
        $vendedor->setScenario(Vendedores::_ALTA);
        if ($vendedor->load(Yii::$app->request->post()) && $vendedor->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorVendedores();
            $resultado = $gestor->Alta($vendedor);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $gestorUsuarios = new GestorUsuarios();
            $items = ArrayHelper::map($gestorUsuarios->Buscar('', 'N'), 'IdUsuario', 'Usuario');
            
            return $this->renderAjax('datos-vendedor', [
                        'model' => $vendedor,
                        'items'=>$items,
                        'titulo' => 'Alta de vendedor',
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarVendedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vendedor = new Vendedores();
        $vendedor->setScenario(Vendedores::_MODIFICAR);
        if (intval($id)) {
            $vendedor->IdVendedor = $id;
        } else {
            throw new HttpException('422', 'El vendedor indicado es inválido');
        }
        
        if ($vendedor->load(Yii::$app->request->post()) && $vendedor->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorVendedores();
            $resultado = $gestor->Modificar($vendedor);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $vendedor->Dame();
            $usuario = new Usuarios();
            $usuario->IdUsuario = $vendedor->IdUsuario;
            $usuario->Dame();
            $vendedor->Usuario = $usuario;
            
            return $this->renderAjax('datos-vendedor', [
                        'model' => $vendedor,
                        'titulo' => 'Modificar de vendedor',
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarVendedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vendedor = new Vendedores();
        if (intval($id)) {
            $vendedor->IdVendedor = $id;
        } else {
            throw new HttpException('422', 'El vendedor indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $vendedor->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaVendedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vendedor = new Vendedores();
        if (intval($id)) {
            $vendedor->IdVendedor = $id;
        } else {
            throw new HttpException('422', 'El vendedor indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $vendedor->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarVendedor', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vendedor = new Vendedores();
        if (intval($id)) {
            $vendedor->IdVendedor= $id;
        } else {
            throw new HttpException('422', 'El vendedor indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorVendedores();
        $resultado = $gestor->Borrar($vendedor);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletar($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $vendedor = new Vendedores();
            
            $vendedor->IdVendedor = $id;
            
            $vendedor->Dame();
            
            $out = [
                'id' => $vendedor->IdVendedor,
                'text' => $vendedor->Nombres . ', '. $vendedor->Apellidos
            ];
        } else {
            $gestor = new GestorVendedores();
            
            $vendedores = $gestor->Buscar($cadena, 'N');
            
            $out = array();
            
            foreach ($vendedores as $vendedor) {
                $out[] = [
                    'id' => $vendedor['IdVendedor'],
                    'text' => $vendedor['Apellidos'] . ', ' . $vendedor['Nombres']
                ];
            }
        }
        return $out;
    }
}
