<?php
namespace backend\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\GestorRepartos;
use common\models\GestorCajas;
use common\models\GestorNotificaciones;
use yii\data\Pagination;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $gestorRepartos = new GestorRepartos();
        $repartos = $gestorRepartos->BuscarAvanzado('', '', '', '', 'A');
        
        $paginado = new Pagination();
        $paginado->totalCount = count($repartos);
        $paginado->pageSize = 6;
        
        $repartos = array_slice($repartos, $paginado->page * $paginado->pageSize, $paginado->pageSize);
           
        $gestorCajas = new GestorCajas();
        $cajas = $gestorCajas->EstadoCajas();
        
        $paginado->totalCount = count($cajas);
        $cajas = array_slice($cajas, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        $gestorNotificaiones = new GestorNotificaciones();
        $notifiaciones = $gestorNotificaiones->Buscar();
        
        return $this->render('index', [
                    'repartos' => $repartos,
                    'cajas' => $cajas ,
                    'notificaciones' => $notifiaciones,
                    'paginado' => $paginado,
        ]);
    }
}
