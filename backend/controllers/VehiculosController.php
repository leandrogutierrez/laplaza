<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\models\forms\BusquedaForm;
use common\models\GestorVehiculos;
use common\models\Vehiculos;

class VehiculosController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarVehiculos', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
        }
        
        $gestor = new GestorVehiculos();
        $models = $gestor->Buscar($Cadena, $IncluyeBajas);
        
        return $this->render('index', [
                    'models' => $models,
                    'busqueda' => $busqueda,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaVehiculo', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vehiculo = new Vehiculos();
        $vehiculo->setScenario(Vehiculos::_ALTA);
        if ($vehiculo->load(Yii::$app->request->post()) && $vehiculo->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorVehiculos();
            $resultado = $gestor->Alta($vehiculo);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            return $this->renderAjax('datos-vehiculo', [
                        'model' => $vehiculo,
                        'titulo' => 'Alta de vehiculo',
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarVehiculo', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vehiculo = new Vehiculos();
        $vehiculo->setScenario(Vehiculos::_MODIFICAR);
        if (intval($id)) {
            $vehiculo->IdVehiculo = $id;
        } else {
            throw new HttpException('422', 'El vehiculo indicado es inválido');
        }
        
        if ($vehiculo->load(Yii::$app->request->post()) && $vehiculo->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorVehiculos();
            $resultado = $gestor->Modificar($vehiculo);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $vehiculo->Dame();
            return $this->renderAjax('datos-vehiculo', [
                        'model' => $vehiculo,
                        'titulo' => 'Modificar de vehiculo',
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarVehiculo', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vehiculo = new Vehiculos();
        if (intval($id)) {
            $vehiculo->IdVehiculo = $id;
        } else {
            throw new HttpException('422', 'El vehiculo indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $vehiculo->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaVehiculo', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vehiculo = new Vehiculos();
        if (intval($id)) {
            $vehiculo->IdVehiculo = $id;
        } else {
            throw new HttpException('422', 'El vehiculo indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $vehiculo->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarVehiculo', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $vehiculo = new Vehiculos();
        if (intval($id)) {
            $vehiculo->IdVehiculo= $id;
        } else {
            throw new HttpException('422', 'El vehiculo indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorVehiculos();
        $resultado = $gestor->Borrar($vehiculo);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletarClientes($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $cliente = new Clientes();
            
            $cliente->IdCliente = $id;
            
            $cliente->Dame();
            
            $out = [
                'id' => $cliente->IdCliente,
                'text' => $cliente->Apellidos.', '.$cliente->Nombres
            ];
        } else {
            $gestor = new GestorClientes();
            
            $clientes = $gestor->Buscar($cadena);
            
            $out = array();
            
            foreach ($clientes as $cliente) {
                $out[] =[
                    'id' => $cliente['IdCliente'],
                    'text' => $cliente['Apellidos'].', '.$cliente['Nombres']
                ];
            }
        }
        
        return $out;
    }
}
