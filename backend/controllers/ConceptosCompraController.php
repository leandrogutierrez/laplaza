<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use common\models\forms\BusquedaForm;
use common\models\GestorConceptosCompra;
use common\models\ConceptosCompra;

class ConceptosCompraController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '')
    {
        if (!in_array('BuscarConceptosCompra', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
        }
        
        $gestor = new GestorConceptosCompra();
        $conceptos = $gestor->Buscar($Cadena);
        
        return $this->render('index', [
                    'busqueda' => $busqueda,
                    'models' => $conceptos,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaConceptoCompra', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $concepto = new ConceptosCompra();
        $concepto->setScenario(ConceptosCompra::_ALTA);
        
        if ($concepto->load(Yii::$app->request->post()) && $concepto->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorConceptosCompra();
            $resultado = $gestor->Alta($concepto);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            return $this->renderAjax('datos-concepto-compra', [
                        'model' => $concepto,
                        'titulo' => 'Alta de concepto de compra'
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarConceptoCompra', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $concepto = new ConceptosCompra();
        $concepto->setScenario(ConceptosCompra::_MODIFICAR);
        
        if (intval($id)) {
            $concepto->IdConceptoCompra = $id;
        } else {
            throw new HttpException('422', 'El concepto de compra indicado es inválido.');
        }
        
        if ($concepto->load(Yii::$app->request->post()) && $concepto->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorConceptosCompra();
            $resultado = $gestor->Modificar($concepto);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $concepto->Dame();
            return $this->renderAjax('datos-concepto-compra', [
                        'model' => $concepto,
                        'titulo' => 'Modificar concepto de compra'
            ]);
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarConceptoCompra', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        Yii::$app->response->format = 'json';
        
        $concepto = new ConceptosCompra();
        
        if (intval($id)) {
            $concepto->IdConceptoCompra = $id;
        } else {
            return ['error' => 'El concepto de compra indicado es inválido.'];
        }
        
        $gestor = new GestorConceptosCompra();
        $resultado = $gestor->Borrar($concepto);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
}
