select p.IdProducto, p.Descripcion, MIN(lv.Precio) 'mínimo', MAX(lv.Precio) 'Máximo', SUM(lv.Precio) 'Total', COUNT(v.IdVenta) 'Ventas'
from 	LineasVenta lv 
inner join 	Productos p on p.IdProducto = lv.IdProducto