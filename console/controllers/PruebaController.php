<?php
namespace console\Controllers;

use yii\console\Controller;
use Yii;

/**
 * Mensaje del controlador prueba
 */

class PruebaController extends Controller
{
    
    /**
     * Información sobre el método index
     */
    public function actionIndex()
    {
        $tabla = Yii::$app->cache->set('asd1', 'asd');
        echo "\n hola \n";
    }
}
