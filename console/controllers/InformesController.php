<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\GestorReportes;

class InformesController extends Controller
{
    public function actionGenerar($key, $idReporte, $valores)
    {
        $gestor = new GestorReportes();

        $resultado = Yii::$app->cache->get($key);

        $resultado['resultado'] = $gestor->Ejecutar($idReporte, $valores);
        
        print_r($resultado['resultado']);

        Yii::$app->cache->set($key, $resultado, 3600);
    }
}
