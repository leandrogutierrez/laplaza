CREATE TABLE `aud_MateriasPrima` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `FechaAud` datetime NOT NULL,
  `UsuarioAud` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `IP` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `UserAgent` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Aplicacion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Motivo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoAud` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `IdMateriaPrima` int(11) NOT NULL COMMENT 'Clave primaria de la tabla MateriasPrima (1/1).',
  `MateriaPrima` varchar(45) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre de la materia prima.',
  `Tipo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `Estado` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Estado de la materia prima. A: Activo, B: Baja.',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `aud_RendimientosMateriasPrima` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `FechaAud` datetime NOT NULL,
  `UsuarioAud` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `IP` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `UserAgent` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Aplicacion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Motivo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TipoAud` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `IdRMP` int(11) NOT NULL COMMENT 'Clave primaria de la tabla RendimientosMateriasPrima (1/2).',
  `IdRendimiento` int(11) NOT NULL COMMENT 'Clave primaria de la tabla RendimientosMateriasPrima (2/2).',
  `IdProducto` int(11) NOT NULL COMMENT 'Clave primaria de la tabla Productos (1/1).',
  `Rendimiento` decimal(5,2) NOT NULL COMMENT 'Valor del rendimiento. Puede representar unidades o un valor porcentual.',
  `PrecioMayorista` decimal(10,2) NOT NULL,
  `PrecioMinorista` decimal(10,2) NOT NULL,
  `Peso` decimal(5,2) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=645 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `MateriasPrima` (
  `IdMateriaPrima` int(11) NOT NULL COMMENT 'Clave primaria de la tabla MateriasPrima (1/1).',
  `MateriaPrima` varchar(45) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre de la materia prima.',
  `Tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Indica el tipo de materia prima. V: Animal vivo, M: Media res, O: Otros.',
  `Estado` varchar(45) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Estado de la materia prima. A: Activa, B: Baja.',
  PRIMARY KEY (`IdMateriaPrima`),
  UNIQUE KEY `UI_MateriaPrima` (`MateriaPrima`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `RendimientosMateriasPrima` (
  `IdRMP` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla RendimientosMateriasPrima (1/2).',
  `IdRendimiento` int(11) NOT NULL COMMENT 'Clave primaria de la tabla RendimientosMateriasPrima (2/2).',
  `IdProducto` int(11) NOT NULL COMMENT 'Clave primaria de la tabla Productos (1/1).',
  `Rendimiento` decimal(5,2) NOT NULL COMMENT 'Valor del rendimiento. Puede representar unidades o un valor porcentual.',
  `PrecioMayorista` decimal(10,2) NOT NULL,
  `PrecioMinorista` decimal(10,2) NOT NULL,
  `Peso` decimal(5,2) NOT NULL,
  PRIMARY KEY (`IdRMP`,`IdRendimiento`),
  KEY `fk_RendimientosMateriasPrima_Productos1_idx` (`IdProducto`),
  KEY `fk_RendimientosMateriasPrima_Rendimientos1_idx` (`IdRendimiento`),
  CONSTRAINT `fk_RendimientosMateriasPrima_Productos1` FOREIGN KEY (`IdProducto`) REFERENCES `Productos` (`IdProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_RendimientosMateriasPrima_Rendimientos1` FOREIGN KEY (`IdRendimiento`) REFERENCES `Rendimientos` (`IdRendimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena los rendimientos de las materias prima. Solo se insertan campos cuando se da de alta una producción indicando nuevos valores para los pesos de los productos devenidos.';
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_activar_materiaprima`(pToken CHAR(32), pIdMateriaPrima INT, pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite cambiar el estado de una materia prima a Activo, 
    controlando que no se encuentre activa ya. 
    Devuelve OK o un mensaje de error en Mensaje.
    */
    -- Declaración de variables 
    DECLARE pIdUsuarioGestion INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pIdMateriaPrima IS NULL) THEN
		SELECT 'Debe indicar una materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF NOT EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima) THEN	
		SELECT 'La materia prima indicada no existe en el sistema.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (SELECT Estado FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima) = 'A' THEN
		SELECT 'La materia prima indicada ya se encuentra activa.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		
        -- Auditoría previa
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'ACTIVAR','A',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        UPDATE	MateriasPrima
        SET		Estado = 'A'
        WHERE  	IdMateriaPrima = pIdMateriaPrima;
        
		-- Auditoría posterior
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'ACTIVAR','D',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        SELECT 'OK' Mensaje;
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_alta_materiaprima`(pToken CHAR(32), pMateriaPrima VARCHAR(45),
					pTipo CHAR(1), pIP VARCHAR(40), 
					pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite dar de alta una materia prima controlando que el nombre no exista ya. D
    evuelve OK + el id de la materia prima creada o un mensaje de error en Mensaje.
    */
    -- Declaración de variables 
    DECLARE pIdUsuarioGestion, pIdMateriaPrima INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pMateriaPrima IS NULL OR pMateriaPrima = '') THEN
		SELECT 'Debe indicar el nombre de la materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipo IS NULL OR pTipo = '') THEN
		SELECT 'Debe indicar si la materia prima es o no un animal vivo.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE MateriaPrima = pMateriaPrima) THEN	
		SELECT 'El nombre de la materia prima ya se encuentra en uso.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipo NOT IN ('V','M','O')) THEN
		SELECT 'El tipo de materia prima indicado inválido.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		SET pIdMateriaPrima = (SELECT COALESCE(MAX(IdMateriaPrima),0) + 1 FROM MateriasPrima);
        
        INSERT INTO MateriasPrima VALUES(pIdMateriaPrima, pMateriaPrima, pTipo, 'A' );
		-- Auditoría
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'ALTA','I',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        SELECT CONCAT('OK',pIdMateriaPrima) Mensaje;
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_alta_rendimiento_materiaprima`(pToken CHAR(32), pIdMateriaPrima INT, 
									pIdProducto INT, pRendimiento DECIMAL(5,2), pTipoRendimiento CHAR(1), 
                                    pPrecio DECIMAL(10,2), pPesoReferenciaMP DECIMAL(5,2),
                                    pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite agregar un rendimiento a una materia prima, indicando si
    el valor del rendimiento es kilogramo (K) o por unidad (U) en pTipoRendimiento.
    Devuelve OK o un mensaje de error en Mensaje.
    */
    -- Declaración de variables 
    DECLARE pIdUsuarioGestion, pIdRMP INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pIdMateriaPrima IS NULL) THEN
		SELECT 'Debe indicar una materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pIdProducto IS NULL) THEN
		SELECT 'Debe indicar un producto.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pRendimiento <= 0) THEN 
		SELECT 'El valor del rendimiento debe ser positivo.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipoRendimiento IS NULL OR pTipoRendimiento = '') THEN
		SELECT 'Debe indicar el tipo de rendimiento.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF NOT EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima AND Estado = 'A') THEN
		SELECT 'La materia prima indicada no existe en el sistema o se encuentra dada de baja.' Mensaje;
        LEAVE PROC;
	END IF;
    IF NOT EXISTS (SELECT IdProducto FROM Productos WHERE IdProducto = pIdProducto AND Estado = 'A') THEN
		SELECT 'El producto indicado no existe en el sistema o se encuentra dado de baja.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipoRendimiento NOT IN('K','U')) THEN
		SELECT 'El tipo de rendimiento indicado es inválido.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pPesoReferenciaMP IS NULL OR pPesoReferenciaMP <= 0) THEN
		SELECT 'Debe indicarse el peso de referencia de la materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		SET pIdRMP = (SELECT COALESCE(MAX(IdRMP),0) + 1 FROM RendimientosMateriasPrima);
        
        INSERT INTO RendimientosMateriasPrima VALUES (pIdRMP, pIdMateriaPrima, pIdProducto,pRendimiento, 
													  pTipoRendimiento, NOW(),pPrecio, pPesoReferenciaMP);
        
        -- Auditoría
		INSERT INTO aud_RendimientosMateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'ALTA','I',
        RendimientosMateriasPrima.* FROM RendimientosMateriasPrima WHERE IdRMP = pIdRMP;
        
        SELECT CONCAT('OK',pIdRMP) Mensaje;
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_borrar_materiaprima`(pToken CHAR(32), pIdMateriaPrima INT, 
							pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite borrar una materia prima controlando que no existan rendimientos, 
    lineas de compra ni stock de carniceria asociados.
	Devuelve OK o un mensaje de error en Mensaje.
    */
    -- Declaración de variables 
    DECLARE pIdUsuarioGestion INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pMateriaPrima IS NULL OR pMateriaPrima = '') THEN
		SELECT 'Debe indicar el nombre de la materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF NOT EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima
    AND Estado = 'A') THEN
		SELECT 'La materia prima indicada no existe en el sistema o se encuentra dada de baja.' Mensaje;
        LEAVE PROC;
	END IF;
    IF EXISTS (SELECT IdMateriaPrima FROM RendimientosMateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima) THEN
		SELECT 'No se puede borrar la materia prima. Existen rendimientos asociados.' Mensaje;
        LEAVE PROC;
	END IF;
    IF EXISTS (SELECT IdMateriaPrima FROM LineasCompra WHERE IdMateriaPrima = pIdMateriaPrima) THEN
		SELECT 'No se puede borrar la materia prima. Existen compras asociadas.' Mensaje;
		LEAVE PROC;
	END IF;
    IF EXISTS (SELECT IdMateriaPrima FROM StockCarniceria WHERE IdMateriaPrima = pIdMateriaPrima) THEN
		SELECT 'No se puede borrar la materia prima. Existe stock de carnicería asociado.' Mensaje;
        LEAVE PROC;
	END IF;
	START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		
		-- Auditoría previa
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'BORRAR','B',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        DELETE FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        SELECT 'OK' Mensaje;
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_borrar_rendimiento_materiaprima`(pToken CHAR(32), pIdRMP INT, 
							pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite borrar un rendimiento asociado a una materia prima. 
    Devuelve OK o un mensaje de error en Mensaje.
    */
	-- Declaración de variables 
    DECLARE pIdUsuarioGestion INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pIdRMP IS NULL) THEN
		SELECT 'Debe indicar un rendimiento.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF NOT EXISTS (SELECT IdRMP FROM RendimientosMateriasPrima WHERE IdRMP = pIdRMP) THEN
		SELECT 'El rendimiento indicado no existe en el sistema.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		
        -- Auditoría
		INSERT INTO aud_RendimientosMateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'BORRAR','B',
        RendimientosMateriasPrima.* FROM RendimientosMateriasPrima WHERE IdRMP = pIdRMP;
        
        DELETE FROM RendimientosMateriasPrima WHERE IdRMP = pIdRMP;
        
        SELECT 'OK' Mensaje;
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_buscar_materiasprima`(pCadena VARCHAR(50), 
									pIncluyeBajas CHAR(1), pTipo CHAR(1))
PROC: BEGIN
	/*
    Permite buscar materias prima filtrándolas por una cadena de búsqueda, 
    indicando si se incluyen o no las dadas de baja en pIncluyeBajas [S|N] e 
    indicando si debe filtrarse por tipo de materia prima en 
    pTipo [V : Animal vivo | M: Media res | O: Otros | T: Todos].
	Ordena por MateriaPrima.
    */
    -- Control de parámetros vacíos
    IF (pIncluyeBajas IS NULL OR pIncluyeBajas = '') THEN
		SET pIncluyeBajas = 'N';
	END IF;
    IF (pTipo IS NULL OR pTipo = '') THEN
		SET pTipo = 'T';
	END IF;
     
    SELECT		mp.*, 
				IF(EXISTS(SELECT IdMateriaPrima 
					FROM Rendimientos WHERE IdMateriaPrima = mp.IdMateriaPrima),
                'S','N') TieneRendimientos
    FROM		MateriasPrima mp
    WHERE		(pIncluyeBajas = 'S' OR (pIncluyeBajas = 'N' AND Estado = 'A')) AND
				MateriaPrima LIKE CONCAT('%',pCadena,'%') AND
                (pTipo = 'T' OR Tipo = pTipo)
	ORDER BY 	MateriaPrima;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_dame_materiaprima`(pIdMateriaPrima INT)
PROC: BEGIN
	/*
    Permite instanciar una materia prima desde la base de datos.
    */
    DECLARE pIdRendimiento INT;
    DECLARE pPesoPromedio, pPesoTotalFraccionado DECIMAL(7,3);
    DECLARE pPuntoTeorico DECIMAL(5,2);
    
    IF EXISTS (SELECT IdRendimiento FROM Rendimientos WHERE IdMateriaPrima = pIdMateriaPrima) THEN
		SET pIdRendimiento = (	SELECT 	IdRendimiento 
								FROM 	Rendimientos 
                                WHERE 	IdRendimiento = (SELECT	MAX(IdRendimiento)
														FROM	Rendimientos
                                                        WHERE	IdMateriaPrima = pIdMateriaPrima));
                                                        
		SET pPesoPromedio = (SELECT		SUM(PesoReferenciaMP)/COUNT(IdRendimiento)
							FROM		Rendimientos
							WHERE		IdMateriaPrima = pIdMateriaPrima
							GROUP BY	IdMateriaPrima);
		
		SET pPesoTotalFraccionado =	(SELECT		SUM(rmp.Peso)
									FROM		RendimientosMateriasPrima rmp
									INNER JOIN	Rendimientos r USING (IdRendimiento)
									WHERE		r.IdMateriaPrima = pIdMateriaPrima AND
												r.FechaAlta = (	SELECT 	MAX(FechaAlta) 
																FROM 	Rendimientos
																WHERE	IdMateriaPrima = pIdMateriaPrima));
		
		SELECT		*, pPesoPromedio PesoTotalPromedio, pPesoTotalFraccionado PesoTotalFraccionado
		FROM		MateriasPrima mp
		LEFT JOIN	Rendimientos r USING (IdMateriaPrima)
		WHERE		mp.IdMateriaPrima = pIdMateriaPrima AND
					r.IdRendimiento = (	SELECT 	MAX(IdRendimiento) 
										FROM	Rendimientos
										WHERE	IdMateriaPrima = pIdMateriaPrima);
	ELSE 
    
		SELECT	mp.*, 0 PesoTotalPromedio, 0 PesoTotalFraccionado
        FROM	MateriasPrima mp
        WHERE	mp.IdMateriaPrima = pIdMateriaPrima;
    
    END IF;
    
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_dame_rendimientomateriaprima`(pIdRMP INT)
PROC: BEGIN
	/*
    Permite instanciar un rendimiento de materia prima desde la base de datos.
    */
    SELECT	*
    FROM	RendimientosMateriasPrima
    WHERE	IdRMP = pIdRMP;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_darbaja_materiaprima`(pToken CHAR(32), pIdMateriaPrima INT, pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite cambiar el estado de una materia prima a Baja, 
    controlando que no se encuentre dada de baja ya. 
    Devuelve OK o un mensaje de error en Mensaje.
    */
    -- Declaración de variables 
    DECLARE pIdUsuarioGestion INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pIdMateriaPrima IS NULL) THEN
		SELECT 'Debe indicar una materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF NOT EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima) THEN	
		SELECT 'La materia prima indicada no existe en el sistema.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (SELECT Estado FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima) = 'B' THEN
		SELECT 'La materia prima indicada ya se encuentra dada de baja.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		
        -- Auditoría previa
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'DARBAJA','A',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        UPDATE	MateriasPrima
        SET		Estado = 'B'
        WHERE  	IdMateriaPrima = pIdMateriaPrima;
        
		-- Auditoría posterior
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'DARBAJA','D',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        SELECT 'OK' Mensaje;
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_listar_rendimientos_materiaprima`(pIdMateriaPrima INT)
PROC: BEGIN
	/*
    Permite obtener todos los rendimientos de una materia prima. 
    Ordena por FechaAlta.
    */
    SET @id = 0;
    SELECT		(@id:= @id + 1) RowNum, rmp.IdRMP, rmp.IdProducto, rmp.IdMateriaPrima, rmp.Precio,
				IF(TipoRendimiento = 'K', CONCAT(Rendimiento,' kg'),Rendimiento) Rendimiento, 
                TipoRendimiento, DATE_FORMAT(rmp.FechaAlta,'%d/%m/%Y') FechaAlta, p.Descripcion,
                p.CodigoProd
    FROM		RendimientosMateriasPrima rmp
    INNER JOIN	Productos p USING (IdProducto)
    WHERE		rmp.IdMateriaPrima = pIdMateriaPrima
    ORDER BY	p.CodigoProd;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_modificar_materiaprima`(pToken CHAR(32), pIdMateriaPrima INT, 
						pMateriaPrima VARCHAR(45), pTipo CHAR(1), pIP VARCHAR(40), 
                        pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite modificar una materia prima, 
    controlando que el nombre no se encuentre en uso.
    Devuelve OK o un mensaje de error en Mensaje.
    */
    -- Declaración de variables 
    DECLARE pIdUsuarioGestion INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pIdMateriaPrima IS NULL) THEN	
		SELECT 'Debe indicar una materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pMateriaPrima IS NULL OR pMateriaPrima = '') THEN
		SELECT 'Debe indicar el nombre de la materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipo IS NULL OR pTipo = '') THEN
		SELECT 'Debe indicar el tipo de materia prima.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF NOT EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima
    AND Estado = 'A') THEN
		SELECT 'La materia prima indicada no existe en el sistema o se encuentra dada de baja.' Mensaje;
        LEAVE PROC;
	END IF;
    IF EXISTS (SELECT IdMateriaPrima FROM MateriasPrima WHERE MateriaPrima = pMateriaPrima AND 
    IdMateriaPrima != pIdMateriaPrima) THEN	
		SELECT 'El nombre de la materia prima ya se encuentra en uso.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipo NOT IN ('V','M','O')) THEN
		SELECT 'El tipo de materia prima es inválido.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
		
		-- Auditoría previa
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'MODIFICAR','A',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        UPDATE	MateriasPrima
		SET		MateriaPrima = pMateriaPrima,
				Tipo = pTipo
        WHERE	IdMateriaPrima = pIdMateriaPrima;
        
        -- Auditoría posterior
		INSERT INTO aud_MateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'MODIFICAR','D',
        MateriasPrima.* FROM MateriasPrima WHERE IdMateriaPrima = pIdMateriaPrima;
        
        SELECT 'OK' Mensaje;
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ssp_modificar_rendimiento_materiaprima`(pToken CHAR(32), pIdRMP INT, 
					pRendimiento DECIMAL(5,2), pTipoRendimiento CHAR(1), pPrecio DECIMAL(10,2),
                    pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
PROC: BEGIN
	/*
    Permite modificar los valores del rendimiento de una materia prima. 
    Solo se permite la edición del valor, tipo y precio del rendimiento, si corresponde. 
    Devuelve OK o un mensaje de error en Mensaje.
    */
     -- Declaración de variables 
    DECLARE pIdUsuarioGestion INT;
    DECLARE pUsuarioAud VARCHAR(120);
    -- Manejo de errores
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			
            SELECT 'Error en la transacción.' Mensaje;
            ROLLBACK;
		END;
	-- Validación de sesión
	SET pIdUsuarioGestion = f_valida_cliente(pToken);
    IF pIdUsuarioGestion = 0 THEN
		SELECT 'La sesión expiró. Vuelva a iniciar sesión.' Mensaje;
        LEAVE PROC;
	END IF;
	-- Control de parámetros vacíos
    IF (pIdRMP IS NULL) THEN
		SELECT 'Debe indicar un rendimiento.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pRendimiento <= 0) THEN 
		SELECT 'El valor del rendimiento debe ser positivo.' Mensaje;
        LEAVE PROC;
	END IF;
    IF (pTipoRendimiento IS NULL OR pTipoRendimiento = '') THEN
		SELECT 'Debe indicar el tipo de rendimiento.' Mensaje;
        LEAVE PROC;
	END IF;
    -- Control de parámetros incorrectos
    IF (pTipoRendimiento NOT IN('k','U')) THEN
		SELECT 'El tipo de rendimiento indicado es inválido.' Mensaje;
        LEAVE PROC;
	END IF;
    START TRANSACTION;
		SET pUsuarioAud = (SELECT Usuario FROM Usuarios WHERE IdUsuario = pIdUsuarioGestion);
        
        -- Auditoría previa
		INSERT INTO aud_RendimientosMateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'MODIFICAR','A',
        RendimientosMateriasPrima.* FROM RendimientosMateriasPrima WHERE IdRMP = pIdRMP;
        
        UPDATE	RendimientosMateriasPrima
        SET		Rendimiento = pRendimiento,
				TipoRendimiento = pTipoRendimiento,
                Precio = pPrecio
		WHERE	IdRMP = pIdRMP;
        
        -- Auditoría posterior
		INSERT INTO aud_RendimientosMateriasPrima
        SELECT 0,NOW(),pUsuarioAud,pIP,pUserAgent,pApp,'MODIFICAR','A',
        RendimientosMateriasPrima.* FROM RendimientosMateriasPrima WHERE IdRMP = pIdRMP;
        
        SELECT 'OK' Mensaje;
	COMMIT;
END$$
DELIMITER ;
