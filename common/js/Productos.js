'use strict';

var Productos = {};
Productos.ModoEdicion = {
    vue : null,
    init : function (productos, categorias, unidades) {
        
        var defaultData = function(){
            return {
                arrayProductos : productos,
                arrayCategorias : categorias,
                arrayUnidades : unidades,
                cargando: false,
            };
        };
         
        this.vue = new Vue({
            el : '#modo-edicion',
            data : defaultData(),
            methods : {
                guardar : function(){
                    var _this = this;
                    console.log(_this.arrayProductos);
                    _this.cargando = true; debugger
                    $.ajax({
                        url: "/productos/modificacion-masiva", 
                        dataType: 'json',
                        method: 'POST',
                        data: {
                            lineas : _this.arrayProductos
                        }
                    }).done(function(result){
                            if(!result.error){
                                var tipo = 'success';
                                var mensaje = 'Datos guardados.';
                                var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                '<strong>' + mensaje + '</strong>' + 
                                                '</div>';
                                $('#errores').html(html);
                                _this.cargando = false;
                            }else { 
                                var tipo = 'danger';
                                var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                '<strong>' + result.error + '</strong>' + 
                                                '</div>';
                                $('#errores').html(html);
                                _this.cargando = false;
                            }
                            setTimeout(function(){
                                $('#errores').empty();
                            },2200);
                        })
                        .fail(function(){
                            var tipo = 'danger';
                            var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                            '<strong>Error en la comunicación con el servidor. Contáctese con el administrador.</strong>' + 
                                            '</div>';
                            $('#errores').html(html);
                            _this.cargando = false;
                        });
//
                }                
            }, 
            mounted () { 
                // Disable scroll when focused on a number input.
                $('form').on('focus', 'input[type=number]', function(e) {
                    $(this).on('wheel', function(e) {
                        e.preventDefault();
                    });
                });
                // Restore scroll on number inputs.
                $('form').on('blur', 'input[type=number]', function(e) {
                    $(this).off('wheel');
                });

                // Disable up and down keys.    
                $('form').on('keydown', 'input[type=number]', function(e) {
                    if ( e.which == 38 || e.which == 40 )
                        e.preventDefault();
                });  
            }
        });
    }
};