'use strict';

var Pedidos = {};
Pedidos.Alta = {
    vue : null,
    init : function(){
        
        var defaultData = function(){
            return {
                idPedido : "",
                idCliente : "",
                idUsuario : "",
                idProducto : "",
                arrayLP : [],
                importe : 0.00,
                fechaEntrega : "" ,
                listaPrecios : "",
                idZona : ""
            };
        };
         
        this.vue = new Vue({
            el : '#pedidos',
            data : defaultData(),
            methods : {
                agregarProducto : function(id){
                    var _this = this;
                    if(_this.existeProducto(id) == null){
                        $.get('/productos/dame-producto-cliente',{idProducto:id ,idCliente:_this.idCliente})
                            .done(function(result){      
                                _this.arrayLP.push({
                                    idProducto : result.IdProducto,
                                    idUnidad : result.IdUnidad,
                                    codigoProd : result.CodigoProd,
                                    descripcion : result.Descripcion, 
                                    cantidad : "",
                                    subtotal : "",
                                    precioAlmacen : result.PrecioAlmacen,
                                    precioDistribuidor : result.PrecioDistribuidor,
                                    precioPublico : result.PrecioPublico,
                                    PrecioUltimo : result.PrecioUltimo,
                                    precio : result.PrecioUltimo == 0? result[_this.listaPrecios]: result.PrecioUltimo
                                });
                            }); 
                    }else{  
                        document.getElementById("lp[cantidad]["+_this.existeProducto(id)+"]").focus();
                    } 
                    _this.idProducto = ""; 
                    $("#pedidos-idproducto").val(''); 
                },
                existeProducto : function(val){
                    var _this = this;
                    var existe = null;
                    _this.arrayLP.forEach(function(element, index){
                        if(element.idProducto == val) {                            
                            existe = index; 
                        }
                    }); 
                    return existe;
                },
                computarCantidad : function(index){
                    var _this = this;
                    var cantidad = document.getElementById("lp[cantidad][" + index + "]").value;
                    if (parseFloat(cantidad) <= 0 || cantidad === "") { 
                       // cantidad = 1;
                    }

                    var precio = document.getElementById("lp[precio][" + index + "]").value;  
                    var subtotal = 0;
                    if (cantidad != "") {
                        subtotal = (cantidad * precio * 100) / 100;
                    }  
                    _this.arrayLP[index]["cantidad"] = cantidad;
                    _this.arrayLP[index]["subtotal"] = subtotal.toFixed(2); 
                    _this.arrayLP[index]["precio"] = precio;
 
                    _this.calcularTotal();  
                    
                },
                enter:function(index){
                    if(index == this.arrayLP.length -1)
                    { 
                        $('#pedidos-idproducto').select2("val", "");
                        $('#pedidos-idproducto').select2('open');
                    }else{
                        document.getElementById("lp[cantidad]["+(index +1)+"]").focus();
                    }
                },
                borrarLP : function(val){
                    this.arrayLP.splice(val,1);
                    this.calcularTotal();
                },
                borrarTodo : function(){
                    this.arrayLP = [];
                },  
                clickRow : function(item){ 
                    var html = '<div class="modal fade"></div>';
                    $(html).modal({ backdrop: 'static', keyboard: true})
                        .on('hidden.bs.modal', function () {
                            $(this).remove(); })
                        .load('/pedidos/detalles?id=' + item) 
                        .on('beforeSubmit', 'form', function (e) {
                            _this.submitModal(this);
                            e.preventDefault();
                            e.stopPropagation();
                            e.stopImmediatePropagation();
                            return false;
                        })  ; 
 
                },
                calcularTotal : function(){
                    var _this = this;
                    _this.importe = 0;
                    var length = _this.arrayLP.length;
                    for(var i = 0 ; i < length ; i++){
                        _this.importe += Math.round(_this.arrayLP[i]["subtotal"]*100)/100; 
                    }
                     _this.importe = _this.importe.toFixed(2);
                    
                },
                setPedido : function(id){
                    var _this = this;
                    console.log(id);
                     $.get('/pedidos/dame-ultima-venta',{idCliente:id})
                        .done(function(result){    
                            if(result.LineasVenta != null){
                            _this.arrayLP = [];
                                var jsonObject = JSON.parse(result.LineasVenta) ;
                                jsonObject.forEach(function(linea,index){
                                    _this.arrayLP.push({
                                        idProducto : linea.IdProducto,
                                        idUnidad : linea.IdUnidad,
                                        codigoProd : linea.CodigoProd,
                                        descripcion : linea.Descripcion, 
                                        cantidad : linea.Cantidad,
                                        PrecioUltimo : linea.Precio,
                                        PrecioAlmacen : linea.PrecioAlmacen,
                                        PrecioDistribuidor : linea.PrecioDistribuidor,
                                        PrecioPublico : linea.PrecioPublico,
                                        precio : linea.Precio === 0 ? linea[_this.listaPrecios] : linea.Precio,  
                                        subtotal : (linea.Cantidad * linea.Precio).toFixed(2)
                                    });
                                });
                            }
                        });     
                }, 
                finalizarPedido : function(){
                    var _this = this;
                    _this.fechaEntrega =  $('#fechaEntrega').val();
                    _this.idPedido =  $('#pedidos-idpedido').val();
                    $('#bRealizarVenta').attr('disabled',true); 
                    if(_this.idPedido){ 
                        $.post("/pedidos/modificar/",{ 
                            idCliente : _this.idCliente,
                            idUsuario : _this.idUsuario,
                            lineas : _this.arrayLP,
                            fechaEntrega : _this.fechaEntrega,
                            idPedido : _this.idPedido,
                            importe : _this.importe}) 
                                .done(function(result){
                                    if(!result.error){
                                        var tipo = 'success';
                                        var mensaje = 'Pedido modificado.';
                                        var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                        '<strong>' + mensaje + '</strong>' + 
                                                        '</div>';
                                        $('#errores').html(html);
                                        _this.arrayLP = [];
                                        _this.idProducto = "";
                                        _this.importe = 0;
                                        _this.idCliente = 0;
                                        _this.idPedido = 0;
                                    }else { 
                                        var tipo = 'danger';
                                        var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                        '<strong>' + result.error + '</strong>' + 
                                                        '</div>';
                                        $('#errores').html(html);
                                        $('#bGuardarPedido').attr('disabled',true);
                                    }

                                    setTimeout(function(){
                                        $('#errores').empty();
                                    },2200);
                                })
                                .fail(function(){
                                    var tipo = 'danger';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>Error en la comunicación con el servidor. Contáctese con el administrador.</strong>' + 
                                                    '</div>';
                                    $('#errores').html(html);
                                    $('#bGuardarPedido').attr('disabled',true);
                                });

                                Object.assign(_this.$data,{
                                            idCliente : "",  
                                            idProducto : null,  
                                            arrayLP : [],
                                            importe : 0.00
                                        });
                    }
                    else{
                        $.post("/pedidos/finalizar-pedido",{
                            idVenta : _this.idVenta,
                            idCliente : _this.idCliente,
                            idUsuario : _this.idUsuario,
                            lineas : _this.arrayLP,
                            fechaEntrega : _this.fechaEntrega,
                            idPedido : _this.idPedido,
                            importe : _this.importe})
                                .done(function(result){
                                    if(!result.error){
                                        var tipo = 'success';
                                        var mensaje = 'Pedido guardado.';
                                        var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                        '<strong>' + mensaje + '</strong>' + 
                                                        '</div>';
                                        $('#errores').html(html);
                                        _this.arrayLP = [];
                                        _this.idProducto = "";
                                        _this.importe = 0;
                                        _this.idCliente = 0;
                                    }else { 
                                        var tipo = 'danger';
                                        var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                        '<strong>' + result.error + '</strong>' + 
                                                        '</div>';
                                        $('#errores').html(html);
                                        $('#bGuardarPedido').attr('disabled',true);
                                    }

                                    setTimeout(function(){
                                        $('#errores').empty();
                                    },2200);
                                })
                                .fail(function(){
                                    var tipo = 'danger';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>Error en la comunicación con el servidor. Contáctese con el administrador.</strong>' + 
                                                    '</div>';
                                    $('#errores').html(html);
                                    $('#bGuardarPedido').attr('disabled',true);
                                });

                                Object.assign(_this.$data,{
                                            idCliente : "",  
                                            idProducto : null,  
                                            arrayLP : [],
                                            importe : 0.00
                                        });
                    }
                },
                setListaPrecios : function(id){ 
                    var _this = this; 
                    $.get('/clientes/dame-cliente',{id:id})
                        .done(function(result){   
                            _this.listaPrecios = result.ListaPrecios;
                            _this.arrayLP.forEach(function(element,index){
                                var precio = element['PrecioUltimo'] != 0 ? element['PrecioUltimo'] : element[_this.listaPrecios];  
                                var cantidad = element.cantidad; 
                                var subtotal = 0;
                                if (cantidad != "") {
                                    subtotal = (cantidad * precio * 100) / 100;
                                }   
                                element.cantidad = cantidad;
                                element.subtotal = subtotal.toFixed(2); 
                                element.precio = precio; 
                            });
                            _this.calcularTotal();
                        });                 
                 },
            }, 
            
            watch : {
                idProducto : function(val){
                    if(parseInt(val) > 0){
                        this.agregarProducto(val)}
                }, 
                idCliente : function (val){ 
                    if(parseInt(val) > 0){  
                        console.log("idCliente");
                        this.setListaPrecios(val); 
                        this.setPedido(val); 
                    }   
                },
                importe : function(val){
                    if(isNaN(val)) 
                        Vue.set(this,'importe',0.00);
                }
            },
            mounted () { 
                // Disable scroll when focused on a number input.
                $('form').on('focus', 'input[type=number]', function(e) {
                    $(this).on('wheel', function(e) {
                        e.preventDefault();
                    });
                });

                // Restore scroll on number inputs.
                $('form').on('blur', 'input[type=number]', function(e) {
                    $(this).off('wheel');
                });

                // Disable up and down keys.    
                $('form').on('keydown', 'input[type=number]', function(e) {
                    if ( e.which == 38 || e.which == 40 )
                        e.preventDefault();
                });  
               /* 
                * //agregamos un eventhandler para el change del input del datecontrol
                var _this = this
                $('#fechaEntrega').on(
                    "change", () =>  { 
                    _this.fechaEntrega = $('#fechaEntrega').val();
                    console.log($('#fechaEntrega').val());
                    console.log(_this.fechaEntrega);
                });*/
            }
        });
    }
};