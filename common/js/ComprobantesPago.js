'use strict';

var ComprobantesPago = {};

ComprobantesPago.Alta = {
    vue : null,
    init : function(){
        this.vue = new Vue({
            el : "#comprobantespago-form",
            data : {
                idCliente : "",
                cliente : {},
                importe : "",
                arrayVentas : [],
            }, 
            watch : {
                idCliente : function(val){
                    var _this = this; 
                    if(parseInt(val) > 0){
                        $.get('/clientes/dame-cliente',{id:val})
                                .done(function(result){ 
                                    _this.cliente = result;
                                    var ultimaVenta = parseFloat(_this.cliente["UltimaVenta"]);
                                    var saldo = parseFloat(_this.cliente["Saldo"]);
                                    _this.importe = saldo < ultimaVenta? saldo : ultimaVenta;
                                    if(_this.cliente["UltimaVenta"] < 0)
                                        _this.importe = 0;
                        }).fail(function(){
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contáctese con el administrador.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores-modal').html(html);
                                });
                        $.get('/clientes/dame-ventas-pendientes',{id:val})
                                .done(function(result){  
                                    _this.arrayVentas = result;
                                    document.getElementById("Importe-disp").focus();
                                    document.getElementById("Importe-disp").select();
                        }).fail(function(){
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contáctese con el administrador.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores-modal').html(html);
                        });
                    }else{
                        this.arrayVentas = [];
                    } 
                }, 
            },
             
        });
    }
}