'use strict';

var Repartos = {};
Repartos.Index = {
    vue : null,
    init : function(){        
        var defaultData = function(){
            return {  
                idZona : ""
            };
        };
        this.vue = new Vue({
            el : '#busquedaReparto-form',
            data : defaultData(),
            methods : {
                filtrarZona : function(id){
                    debugger;
                    $("#busquedaReparto-form").submit();
                }
            },
            watch : {
                idZona : function(val){
                    if(parseInt(val) > 0){
                        this.filtrarZona(val)}
                }
            },
            mounted () {
            }
        });
    }
};

