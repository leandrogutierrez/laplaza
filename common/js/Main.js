'use strict';

var Main = {
    config: {
        urlPush: 'http://' + location.hostname + ':3000'
    },
    opciones: {
        pace: {
            ajax: {
                trackMethods: ['GET', 'POST']
            }
        },
        tooltip: {
            container: 'body',
            title: function () {
                return $(this).data('hint');
            }
        },
        confirm: {
            'title': function () {
                return $(this).data('mensaje');
            },
            'btnOkLabel': 'Aceptar',
            'btnCancelLabel': 'Cancelar',
            'singleton': true,
            'placement': 'left',
            'container': 'body',
            'rootSelector' : '[data-mensaje]'
        }
    } ,
    selectores: {
        tooltip: '[data-hint]',
        confirm: '[data-mensaje]', // Confirmar acción
        selectOnFocus: '[data-focus-select]', // Seleccionar el texto al hacer focus
        modal: '[data-modal]',
        ajax: '[data-ajax]' // Request a la url indicada en este selector
    },
    init: function () {
        var _this = Main;
        window.paceOptions = _this.opciones.pace;

        // Inicialización de directivas de Vue
        VueDirectives.init(); 
        _this.initAjax();
        _this.initEventos(); 
    },
    initEventos: function () {
        var _this = Main;

        $('body').on('pjax:complete', function () {
            _this.initAjax();
        });

        $('body').on('click', _this.selectores.selectOnFocus, function (event) {
            $(event.target).select();
        });

        $('body').on('click', _this.selectores.modal, function () {
            _this.modal($(this).data('modal'));
        });

        // Hacer request con ajax en los elementos con data-ajax=url. Evita recarga y muestra mensaje de éxito si hay data-success
        $('body').on('click', _this.selectores.ajax, function (e) {
            _this.ajax(this);
        });
    },
    initAjax: function () {
        var _this = Main;
        $('.tooltip').remove();

        $(_this.selectores.tooltip).tooltip(_this.opciones.tooltip);

        // Mensaje pidiendo confirmación en las acciones con data-mensaje
        $(_this.selectores.confirm).confirmation(_this.opciones.confirm);
 
    }, 
    modal: function (url) {
        var _this = Main;

        // Si hay un modal abierto no abro otro
        /*if ($('.modal').length > 0)
            return;
        
         */
        var html = '<div class="modal fade"></div>';

        $(html).modal({
            backdrop: 'static',
            keyboard: true})
                .on('hidden.bs.modal', function () {
                    $(this).remove();
                })
                .load(url, function () {
                    var $modal = $(this);
                    var $form = $(this).find('form');

                    setTimeout(function () {
                        $modal.trigger('shown.bs.modal');

                        // Obtengo el primer input no oculto
                        var $primerInput = $form.find('input:not([type=hidden]),select').filter(':first'); 
                        // Hago focus si es type=text o select y no es un datepicker
                        if ($primerInput.hasClass('select2-hidden-accessible'))
                           $primerInput.select2('open');
                        else 
                            if (($primerInput.attr('type') == 'text' || $primerInput.is('select')) && 
                                 !$primerInput.hasClass('kranjee-datepicker') && !$primerInput.parent().hasClass('date'))
                             $primerInput.focus();
                         console.log($primerInput.attr('class'));
                        _this.initAjax();

                    }, 500);
                    $modal.on('beforeSubmit', 'form', function (e) {
                        _this.submitModal(this);
                        e.preventDefault();
                        e.stopPropagation();
                        e.stopImmediatePropagation();
                        return false;
                    });
                });
    },
    // Evento ejecutado para hacer submit de formularios con ajax
    submitModal: function (form) {
        var $form = $(form);

        // Se usa FormData porque permite también la subida de archivos
        var datos = new FormData(form);

        //Desactivo el botón de submit, para que el usuario no realice clicks 
        //repetidos hasta que se reciba la respuesta del servidor.
        $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', true);

        // Sólo si no está deshabilitado el botón de submit sigo adelante. Para prevenir doble submit 
        // (pasaba hasta que se deshabilitaba el botón)
        if (!$form.find(':submit').attr('disabled'))
        {
            $form.find(':submit').attr('disabled', true);

            //Se realiza el request con los datos por POST        
            $.ajax({
                url: $form.attr("action"),
                data: datos,
                type: 'POST',
                contentType: false,
                processData: false, })
                    .done(function (data) {
                        if (data.error)
                        {
                            var evento = jQuery.Event("error.modalform");
                            $('.modal').trigger(evento, [data]);

                            if (!evento.isDefaultPrevented())
                            {
                                var mensaje = data.error;
                                var tipo = 'danger';
                                //Agregando mensaje de error al diálogo modal
                                var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                                        + '<i class="fa fa-ban"></i>'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                        + '<b class="texto" >' + mensaje + '</b>'
                                        + '</div>';
                                $('#errores-modal').html(html);
                            }

                            //Se activa nuevamente el botón
                            $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', false);
                            $form.find(':submit').attr('disabled', false);
                        } else
                        {
                            var evento = jQuery.Event("success.modalform");
                            $('.modal').trigger(evento, [data]);

                            if (!evento.isDefaultPrevented())
                            {
                                if ($form.closest(".modal-dialog").data('no-reload') === undefined)
                                    location.reload();
                                else
                                    $('.modal').modal('hide');
                            } else
                            {
                                //Se activa nuevamente el botón
                                $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', false);
                                $form.find(':submit').attr('disabled', false);
                            }
                        }
                    })
                    .fail(function () {
                        var tipo = 'danger';
                        var mensaje = 'Error en la comunicación con el servidor. Contacte con el administrador.';
                        //Agregando mensaje de error al diálogo modal
                        var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                                + '<i class="fa fa-ban"></i>'
                                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                + '<b class="texto" >' + mensaje + '</b>'
                                + '</div>';
                        $('#errores-modal').html(html);
                        //Se activa nuevamente el botón
                        $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', false);
                        $form.find(':submit').attr('disabled', false);
                    });
        }
    },
    // Hacer request con ajax en los elementos con data-ajax=url. Evita recarga y muestra mensaje de éxito si hay data-success
    ajax: function (elemento) {
        var url = $(elemento).data('ajax');
        var success = $(elemento).data('success');

        $.get(url)
                .done(function (data) {
                    if (data.error)
                    {
                        var evento = jQuery.Event("error.ajax");
                        $(elemento).trigger(evento, [data]);

                        if (!evento.isDefaultPrevented())
                        {
                            var tipo = 'danger';
                            var mensaje = data.error;
                            var icono = 'ban';
                            //Agregando mensaje de error al diálogo modal
                            var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                                    + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                    + '<i class="fa fa-' + icono + '"></i>'
                                    + '<b class="texto" >' + mensaje + '</b>'
                                    + '</div>';
                            $('#errores').html(html);
                        }
                    } else
                    {
                        if (success)
                        {
                            var tipo = 'success';
                            var mensaje = success;
                            var icono = 'check';
                            //Agregando mensaje de error al diálogo modal
                            var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                                    + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                    + '<i class="fa fa-' + icono + '"></i>'
                                    + '<b class="texto" >' + mensaje + '</b>'
                                    + '</div>';
                            $('#errores').html(html);
                        } else
                        {
                            var evento = jQuery.Event("success.ajax");
                            $(elemento).trigger(evento, [data]);

                            if (!evento.isDefaultPrevented())
                            {
                                if ($.support.pjax)
                                {
                                    window.location.hash = '';
                                    $.pjax.reload('#pjax-container');
                                } else
                                    location.reload();
                            }
                        }
                    }
                })
                .fail(function () {
                    var tipo = 'danger';
                    var mensaje = 'Error en la comunicación con el servidor. Contacte con el administrador.';
                    var icono = 'ban';
                    //Agregando mensaje de error al diálogo modal
                    var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                            + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                            + '<i class="fa fa-' + icono + '"></i>'
                            + '<b class="texto" >' + mensaje + '</b>'
                            + '</div>';
                    $('#errores').html(html);
                });
    },
    // Sonido de error
    errorBeep: function () {
        var snd = new Audio("/sound/error.ogg");
        snd.play();
    },
    nudgeBeep: function () {
        var snd = new Audio("/sound/nudge.ogg");
        snd.play();
    },
    warningBeep: function () {
        var snd = new Audio("/sound/warning.oga");
        snd.play();
    },
    imprimir: function () {
        var colapsado = $("body").hasClass('sidebar-collapse');

        if (!colapsado)
            $("body").addClass('sidebar-collapse');

        window.print();

        if (!colapsado)
            $("body").removeClass('sidebar-collapse');
    }
};
$(function () {
    Main.init();
});