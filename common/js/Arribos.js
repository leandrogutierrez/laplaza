'use strict';

var Arribos = {};
Arribos.Alta = {
    vue : null,
    init : function(){
        
        var defaultData = function(){
            return {  
                idReparto : "",
                idProducto : "",
                arrayLA : [], 
            };
        };
        
        this.vue = new Vue({
            el : '#arribos',
            data : defaultData(),
            methods : {
                agregarProducto : function(id){
                    var _this = this;
                    if(_this.existeProducto(id) == null){
                        $.get('/productos/dame-producto',{id:id})
                            .done(function(result){      
                                _this.arrayLA.push({
                                    idProducto : result.IdProducto,
                                    idUnidad : result.IdUnidad,
                                    codigoProd : result.CodigoProd,
                                    descripcion : result.Descripcion, 
                                    cantidad : "",
                                    subtotal : "",
                                    precioAlmacen : result.PrecioAlmacen,
                                    precioDistribuidor : result.PrecioDistribuidor,
                                    precioPublico : result.PrecioPublico
                                });
                            }); 
                    }else{  
                        document.getElementById("la[cantidad]["+_this.existeProducto(id)+"]").focus();
                    } 
                    _this.idProducto = ""; 
                    $("#arribos-idproducto").val(''); 
                },
                existeProducto : function(val){
                    var _this = this;
                    var existe = null;
                    _this.arrayLA.forEach(function(element, index){
                        if(element.idProducto == val) {                            
                            existe = index; 
                        }
                    });
                    console.log(existe);
                    return existe;
                },
                computarCantidad : function(index){ 
                    $('#arribos-idproducto').select2("val","");
                    
                    $('#arribos-idproducto').select2('open');
                },
                borrarLP : function(val){
                    this.arrayLA.splice(val,1);
                    this.calcularTotal();
                },
                borrarTodo : function(){
                    this.arrayLA = [];
                },  
                calcularTotal : function(){
                    var _this = this;
                    _this.importe = 0;
                    var length = _this.arrayLA.length;
                    for(var i = 0 ; i < length ; i++)
                        _this.importe += Math.round(_this.arrayLA[i]["subtotal"]*100)/100;
                    
                     _this.importe = _this.importe.toFixed(2);
                    
                },  
                enter:function(index){
                     event.preventDefault();
                    if(index == this.arrayLA.length -1)
                    { 
                        $('#arribos-idproducto').select2("val", "");
                        $('#arribos-idproducto').select2('open');
                    }else{
                        document.getElementById("la[cantidad]["+(index +1)+"]").focus();
                    }
                },
                cargarArriboSugerido : function(){
                    var _this = this;
                    $.get('/repartos/dame-posible-arribo-reparto',{id:_this.idReparto})
                        .done(function(result){    
                            console.log(result);
                            if(result != null){
                                _this.arrayLA = []; 
                                result.forEach(function(linea,index){
                                    _this.arrayLA.push({
                                        idProducto : linea.IdProducto,
                                        idUnidad : linea.IdUnidad,
                                        codigoProd : linea.CodigoProd,
                                        descripcion : linea.Descripcion, 
                                        cantidad : linea.Arribo, 
                                        despacho : linea.Despachados, 
                                        venta : linea.Vendidos, 
                                    });   
                                });
                            }
                        });
                }
            },
            watch : {
                idProducto : function(val){
                    if(parseInt(val) > 0){
                        this.agregarProducto(val)}
                },
                importe : function(val){
                    if(isNaN(val)) 
                        Vue.set(this,'importe',0.00);
                }
            },
            mounted () {
                var _this = this;
                _this.idReparto = $('#arribos-idreparto').val();
                console.log($('#arribos-idreparto').val());
                _this.cargarArriboSugerido();
               /* 
                * //agregamos un eventhandler para el change del input del datecontrol
                var _this = this
                $('#fechaEntrega').on(
                    "change", () =>  { 
                    _this.fechaEntrega = $('#fechaEntrega').val();
                    console.log($('#fechaEntrega').val());
                    console.log(_this.fechaEntrega);
                });*/
           }
        });
    }
};

