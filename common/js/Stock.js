'use strict';

var Producciones = {};
Producciones.Alta = {
    vue : null,
    init : function(){
        
        var defaultData = function(){
            return {
                idProduccion : "",
                idCategoria : "",
                idUsuario : "",
                idProducto : "",
                arrayLP : [],
                fechaProduccion : "" ,
            };
        };
         
        this.vue = new Vue({
            el : '#producciones',
            data : defaultData(),
            methods : {
                agregarProducto : function(id){
                    var _this = this;
                    if(_this.existeProducto(id) == null){
                        $.get('/productos/dame-producto',{id:id})
                            .done(function(result){      
                                console.log(result);
                                _this.arrayLP.push({
                                    idProducto : result.IdProducto,
                                    codigoProd : result.CodigoProd,
                                    descripcion : result.Descripcion, 
                                    cantidad : "",
                                    subtotal : "",
                                    unidad : result.Unidad,
                                    unidadProduccion : result.UnidadProduccion,
                                    equivalencia: result.Equivalencia,
                                    idUnidad : result.IdUnidad,
                                    idUnidadProduccion : result.IdUnidadProduccion,
                                });
                            }); 
                    }else{  
                        document.getElementById("lp[cantidad]["+_this.existeProducto(id)+"]").focus();
                    } 
                    _this.idProducto = "";  
                    $("#producciones-idproducto").val(''); 
                },
                existeProducto : function(val){
                    var _this = this;
                    var existe = null;
                    _this.arrayLP.forEach(function(element, index){
                        if(element.idProducto == val) {                            
                            existe = index; 
                        }
                    }); 
                    return existe;
                },
                enter:function(index){
                    if(index == this.arrayLP.length -1)
                    { 
                        $('#producciones-idproducto').select2("val", "");
                        $('#producciones-idproducto').select2('open');
                    }else{
                        document.getElementById("lp[cantidad]["+(index +1)+"]").focus();
                    }
                },
                borrarLP : function(val){
                    this.arrayLP.splice(val,1);
                    this.calcularTotal();
                },
                borrarTodo : function(){
                    this.arrayLP = [];
                },  
                clickRow : function(item){ 
                    var html = '<div class="modal fade"></div>';
                    $(html).modal({ backdrop: 'static', keyboard: true})
                        .on('hidden.bs.modal', function () {
                            $(this).remove(); })
                        .load('/producciones/detalles?id=' + item) 
                        .on('beforeSubmit', 'form', function (e) {
                            _this.submitModal(this);
                            e.preventDefault();
                            e.stopPropagation();
                            e.stopImmediatePropagation();
                            return false;
                        })  ; 
 
                },
                finalizarProduccion : function(){
                    var _this = this;
                    _this.fechaProduccion =  $('#fechaProduccion').val();
                    _this.idProduccion =  $('#producciones-idproduccion').val();
                    $('#bRealizarVenta').attr('disabled',true); 
                    $.post("/producciones/finalizar-produccion",{
                        idCategoria : _this.idCategoria,
                        idUsuario : _this.idUsuario,
                        lineas : _this.arrayLP,
                        fechaProduccion : _this.fechaProduccion,
                        })
                        .done(function(result){
                            if(!result.error){
                                var tipo = 'success';
                                var mensaje = 'Produccion guardado.';
                                var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                '<strong>' + mensaje + '</strong>' + 
                                                '</div>';
                                $('#errores').html(html);
                                _this.arrayLP = [];
                                _this.idProducto = "";
                                _this.importe = 0;
                                _this.idCliente = 0;
                            }else { 
                                var tipo = 'danger';
                                var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                '<strong>' + result.error + '</strong>' + 
                                                '</div>';
                                $('#errores').html(html);
                                $('#bGuardarProduccion').attr('disabled',true);
                            }

                            setTimeout(function(){
                                $('#errores').empty();
                            },2200);
                        })
                        .fail(function(){
                            var tipo = 'danger';
                            var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                            '<strong>Error en la comunicación con el servidor. Contáctese con el administrador.</strong>' + 
                                            '</div>';
                            $('#errores').html(html);
                            $('#bGuardarProduccion').attr('disabled',true);
                        });
                }                
            }, 
            watch : {
                idProducto : function(val){
                    if(parseInt(val) > 0){
                        this.agregarProducto(val)}
                }, 
            },
            mounted () { 
                // Disable scroll when focused on a number input.
                $('form').on('focus', 'input[type=number]', function(e) {
                    $(this).on('wheel', function(e) {
                        e.preventDefault();
                    });
                });
                // Restore scroll on number inputs.
                $('form').on('blur', 'input[type=number]', function(e) {
                    $(this).off('wheel');
                });

                // Disable up and down keys.    
                $('form').on('keydown', 'input[type=number]', function(e) {
                    if ( e.which == 38 || e.which == 40 )
                        e.preventDefault();
                });  
               /* 
                * //agregamos un eventhandler para el change del input del datecontrol
                var _this = this
                $('#fechaProduccion').on(
                    "change", () =>  { 
                    _this.fechaProduccion = $('#fechaProduccion').val();
                    console.log($('#fechaProduccion').val());
                    console.log(_this.fechaProduccion);
                });*/
            }
        });
    }
};