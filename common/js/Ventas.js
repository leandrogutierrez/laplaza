'use strict';

var Ventas = {};
Ventas.Alta = {
    vue : null,
    init : function(permisoEdicion){
        var defaultData = function(){ 
            return {
                idCliente : "",
                idReparto : "", 
                idUsuario : "",
                idProducto : "", 
                listaPrecios : "",
                fecha :"",
                arrayLV : [],
                importe : 0.00,
                permisoEdicion : permisoEdicion,
                pedidoEdicion : false
            };
        };
        this.vue = new Vue({
            el : '#ventas',
            data : defaultData(),
            methods : {
                agregarProducto : function(id){
                    var _this = this;
                    if(_this.existeProducto(id) == null){
                        $.get('/productos/dame-producto-cliente',{idProducto :id, idCliente:_this.idCliente})
                             .done(function(result){      
                                _this.arrayLV.push({
                                    idProducto : result.IdProducto,
                                    idUnidad : result.IdUnidad,
                                    codigoProd : result.CodigoProd,
                                    descripcion : result.Descripcion, 
                                    cantidad : "",
                                    subtotal : "",
                                    unidad : result.Unidad,
                                    PrecioAlmacen : result.PrecioAlmacen,
                                    PrecioDistribuidor : result.PrecioDistribuidor,
                                    PrecioPublico : result.PrecioPublico,
                                    PrecioUltimo : result.PrecioUltimo,
                                    precio : result.PrecioUltimo == 0? result[_this.listaPrecios]: result.PrecioUltimo
                                });    
                            });             
                    }else{  
                        document.getElementById("lv[cantidad]["+_this.existeProducto(id)+"]").focus();
                    } 
                    _this.idProducto = "";   
                },
                existeProducto : function(val){
                    var _this = this;
                    var existe = null;
                    _this.arrayLV.forEach(function(element, index){
                        if(element.idProducto == val) {                            
                            existe = index; 
                        }
                    }); 
                    return existe;
                },
                computarCantidad : function(index){ 
                    var _this = this;
                    var cantidad = document.getElementById("lv[cantidad][" + index + "]").value;
                    if (parseFloat(cantidad) <= 0 || cantidad === "") { 
                    //    cantidad = 1;
                    }

                    var precio = document.getElementById("lv[precio][" + index + "]").value;  
                    var subtotal = 0;
                    if (cantidad != "") {
                        subtotal = (cantidad * precio * 100) / 100;
                    } 

                    _this.arrayLV[index]["cantidad"] = cantidad;
                    _this.arrayLV[index]["subtotal"] = subtotal.toFixed(2); 
                    _this.arrayLV[index]["precio"] = precio; 
                    _this.calcularTotal();  
                    
                },
                enter:function(index){
                    if(index == this.arrayLV.length -1)
                    { 
                        $('#ventas-idproducto').select2("val", "");
                        $('#ventas-idproducto').select2('open');
                    }else{
                        document.getElementById("lv[cantidad]["+(index +1)+"]").focus();
                    }
                },
                borrarLV : function(val){
                    this.arrayLV.splice(val,1);
                    this.calcularTotal();
                },
                borrarTodo : function(){
                    this.arrayLV = [];
                },  
                calcularTotal : function(){
                    var _this = this;
                    _this.importe = 0;
                    var length = _this.arrayLV.length;
                    for(var i = 0 ; i < length ; i++)
                        _this.importe += Math.round(_this.arrayLV[i]["subtotal"]*100)/100;
                    
                     _this.importe = _this.importe.toFixed(2); 
                },
                setPedido : function(id){
                    var _this = this;
                    return new Promise(function(res,rej){
                         $.get('/clientes/dame-pedido-fecha',{id:id, fecha:_this.fecha})
                            .done(function(result){    
                                if(result.LineasPedido != null){
                                    _this.arrayLV = [];
                                    var jsonObject = JSON.parse(result.LineasPedido) ;
                                    jsonObject.forEach(function(linea,index){
                                        _this.arrayLV.push({
                                            idProducto : linea.IdProducto,
                                            idUnidad : linea.IdUnidad,
                                            codigoProd : linea.CodigoProd,
                                            descripcion : linea.Descripcion, 
                                            cantidad : linea.Cantidad,
                                            unidad : linea.Unidad,
                                            PrecioUltimo : linea.Precio,
                                            PrecioAlmacen : linea.PrecioAlmacen,
                                            PrecioDistribuidor : linea.PrecioDistribuidor,
                                            PrecioPublico : linea.PrecioPublico,
                                            precio : linea.Precio == 0 ? linea[_this.listaPrecios] : linea.Precio,
                                            subtotal : (linea.cantidad * linea.precio * 100) / 100
                                        });   
                                    });
                                    _this.calcularTotal();
                                    res();
                                }
                            })
                            .fail(function(){
                                rej();
                            });
                    });
                },  
                setListaPrecios : function(id){ 
                    var _this = this; 
                    _this.listaPrecios = id;
                    _this.arrayLV.forEach(function(element,index){
                        var precio = element[_this.listaPrecios] !== 0 ? element[_this.listaPrecios] : element['PrecioUltimo'];  
                        var cantidad = element.cantidad; 
                        var subtotal = 0;
                        if (cantidad !== "") {
                            subtotal = (cantidad * precio * 100) / 100;
                        }   
                        element.cantidad = cantidad;
                        element.subtotal = subtotal.toFixed(2); 
                        element.precio = precio; 
                    });
                    _this.calcularTotal();
                },
                clickRow : function(item){
                    console.log(item);
                    var html = '<div class="modal fade"></div>';

                    $(html).modal({ backdrop: 'static', keyboard: true})
                        .on('hidden.bs.modal', function () {
                            $(this).remove(); })
                        .load('/ventas/detalles?id=' + item) 
                        .on('beforeSubmit', 'form', function (e) {
                            _this.submitModal(this);
                            e.preventDefault();
                            e.stopPropagation();
                            e.stopImmediatePropagation();
                            return false;
                        })  ;  
                },
                finalizarVenta : function(){
                    var _this = this;
                    $('#bRealizarVenta').attr('disabled',true);
                    $.post("/ventas/finalizar-venta",{
                        idVenta : _this.idVenta,
                        idCliente : _this.idCliente,
                        idReparto : _this.idReparto,
                        idUsuario : _this.idUsuario,
                        lineas :_this.arrayLV,
                        importe : _this.importe,
                        edicionPrecios : _this.pedidoEdicion})
                            .done(function(result){
                                if(!result.error){ 
                                    var tipo = 'success';
                                    var mensaje = 'Venta exitosa.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores').html(html);  
                                    
                                    var html = '<div class="modal fade"></div>';

                                    $(html).modal({ backdrop: 'static', keyboard: true})
                                        .on('hidden.bs.modal', function () {
                                            $(this).remove(); })
                                        .load('/comprobantes-pago/alta?IdCliente=' + _this.idCliente) 
                                        .on('beforeSubmit', 'form', function (e) {
                                            _this.submitModal(this);
                                            e.preventDefault();
                                            e.stopPropagation();
                                            e.stopImmediatePropagation();
                                            return false;
                                        })  ; 
                                        
                                    Object.assign(_this.$data,{
                                        idCliente : "",  
                                        idProducto : null,  
                                        arrayLV : [],
                                        importe : 0.00
                                    });
                                }else { 
                                    var tipo = 'danger';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + result.error + '</strong>' + 
                                                    '</div>';
                                    $('#errores').html(html); 
                                    $('#bRealizarVenta').attr('disabled',false);
                                     
                                }   
                                setTimeout(function(){
                                    $('#errores').empty();
                                },2200);
                                  
                            })
                            .fail(function(){
                                var tipo = 'danger';
                                var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                '<strong>Error en la comunicación con el servidor. Contáctese con el administrador.</strong>' + 
                                                '</div>';
                                $('#errores').html(html);
                                $('#bRealizarCompra').attr('disabled',true);  
                            });    
                            _this.pedidoEdicion = false;
                },
                setFecha: function(id){
                     var _this = this;
                     $.get('/repartos/dame-reparto',{id:id})
                        .done(function(result){    
                            if(result.FechaReparto != null){
                                _this.fecha = result.FechaReparto;
                            }       
                        });
                },
                submitModal: function (form) {
                    var $form = $(form);

                    // Se usa FormData porque permite también la subida de archivos
                    var datos = new FormData(form);

                    //Desactivo el botón de submit, para que el usuario no realice clicks 
                    //repetidos hasta que se reciba la respuesta del servidor.
                    $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', true);

                    // Sólo si no está deshabilitado el botón de submit sigo adelante. Para prevenir doble submit 
                    // (pasaba hasta que se deshabilitaba el botón)
                    if (!$form.find(':submit').attr('disabled'))
                    {
                        $form.find(':submit').attr('disabled', true);

                        //Se realiza el request con los datos por POST        
                        $.ajax({
                            url: $form.attr("action"),
                            data: datos,
                            type: 'POST',
                            contentType: false,
                            processData: false, })
                                .done(function (data) {
                                    if (data.error)
                                    {
                                        var evento = jQuery.Event("error.modalform");
                                        $('.modal').trigger(evento, [data]);

                                        if (!evento.isDefaultPrevented())
                                        {
                                            var mensaje = data.error;
                                            var tipo = 'danger';
                                            //Agregando mensaje de error al diálogo modal
                                            var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                                                    + '<i class="fa fa-ban"></i>'
                                                    + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                                    + '<b class="texto" >' + mensaje + '</b>'
                                                    + '</div>';
                                            $('#errores-modal').html(html);
                                        }

                                        //Se activa nuevamente el botón
                                        $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', false);
                                        $form.find(':submit').attr('disabled', false);
                                    } else
                                    {
                                        var tipo = 'success';
                                        var mensaje = 'Pago registrado.';
                                        var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                        $('#errores').html(html);  
                                        
                                        var evento = jQuery.Event("success.modalform"); 

                                        if (!evento.isDefaultPrevented())
                                        { 
                                            $('.modal').modal('hide');
                                        } else
                                        {
                                            //Se activa nuevamente el botón
                                            $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', false);
                                            $form.find(':submit').attr('disabled', false);
                                        }
                                    }
                                })
                                .fail(function () {
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contacte con el administrador.';
                                    //Agregando mensaje de error al diálogo modal
                                    var html = '<div id="mensaje-modal" class="alert alert-' + tipo + ' alert-dismissable">'
                                            + '<i class="fa fa-ban"></i>'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                            + '<b class="texto" >' + mensaje + '</b>'
                                            + '</div>';
                                    $('#errores-modal').html(html);  
                                    //Se activa nuevamente el botón
                                    $form.closest('.modal-content').find('[data-dismiss=modal]').attr('disabled', false);
                                    $form.find(':submit').attr('disabled', false);
                                });
                    }
                }, 
                activarEdicion: function(){ 
                    var tipo = 'warning';
                    var mensaje = 'Se habilitó la edición de precios, los mismos serán controlados al momento del arribo.';
                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                    '<strong>' + mensaje + '</strong>' + 
                                    '</div>';
                    $('#errores').html(html);  
//                    $('#ventas-btn-edicion').attr('disabled',true)    
                    this.pedidoEdicion = true;

                }
            },
            watch : {
                idProducto : function(val){
                    if(parseInt(val) > 0)
                        this.agregarProducto(val);
                },
                idReparto : function(val){
                    if(parseInt(val)>0)
                        this.setFecha(val);
                    
                },
                idCliente : function (id){ 
                    var _this = this
                    if(parseInt(id) > 0){
                        $.get('/clientes/dame-cliente',{id:id})
                            .done(function(result){   
                                _this.setPedido(result.IdCliente)
                                .then( function(){
                                    _this.setListaPrecios('PrecioUltimo')
                                    }
                                ); 
                            });
                    }
                },
                importe : function(val){
                    if(isNaN(val)) Vue.set(this,'importe',0.00);
                },
                listaPrecios : function(val){
                    var _this = this;
                    _this.setListaPrecios(val);
                }
            }, 
            computed :{
                edicionPrecios : function(){
                    return (this.permisoEdicion && this.pedidoEdicion)
                }
            },
            mounted () {
                // Disable scroll when focused on a number input.
                $('form').on('focus', 'input[type=number]', function(e) {
                    $(this).on('wheel', function(e) { 
                        e.preventDefault();
                    });   
                });

                // Restore scroll on number inputs.
                $('form').on('blur', 'input[type=number]', function(e) {
                    $(this).off('wheel');
                }); 

                // Disable up and down keys.    
                $('form').on('keydown', 'input[type=number]', function(e) {
                    if ( e.which == 38 || e.which == 40 )
                        e.preventDefault();
                });  
               /* 
                * //agregamos un eventhandler para el change del input del datecontrol
                var _this = this
                $('#fechaEntrega').on(
                    "change", () =>  { 
                    _this.fechaEntrega = $('#fechaEntrega').val();
                    console.log($('#fechaEntrega').val());
                    console.log(_this.fechaEntrega);
                });*/
            }
        });
    }
};