'use strict';

var Compras = {};
Compras.Alta = {
    vue : null,
    init : function(){
        
        var defaultData = function(){
            return {
                idProveedor : "",
                idMateriaPrima : "",
                arrayMP : [],
                importe : 0.00,
                tipoCompra : "P",
                arrayConceptos : [],
                idConceptoCompra : "",
                subtotal : 0.00,
                importeConceptos : 0.00,
                idSucursal : ""
            };
        };
        
        this.vue = new Vue({
            el : '#compras',
            data : defaultData(),
            methods : {
                quitarConcepto : function(index){
                    var _this = this;
                    _this.arrayConceptos.splice(index,1);
                },
                agregarProducto : function(id){
                    var _this = this;
                    $.get('/materias-prima/dame-mp',{id:id})
                        .done(function(result){  
                            _this.arrayMP.push({
                                idMateriaPrima : result.IdMateriaPrima,
                                materiaPrima : result.MateriaPrima,
                                cantidad :"",
                                peso : "",
                                importe : ""
                                
                            });
                        });                    
                    _this.idMateriaPrima = "";
                },
                calcularImporte : function(){
                    var _this = this;
                    _this.calcularTotalConceptos();
                    _this.calcularSubtotal();
                    _this.calcularTotal();
                },  
                borrarLC : function(val){
                    this.arrayMP.splice(val,1);
                    this.calcularTotal();
                },
                borrarTodo : function(){
                    this.arrayMP = [];
                },  
                calcularTotalConceptos(){
                   var _this = this;
                    _this.importeConceptos = 0.00;
                    _this.arrayConceptos.forEach(function(elemento, indice){
                        if(typeof elemento.importe == 'undefined') 
                            elemento.importe = 0;
                        _this.importeConceptos += parseFloat(elemento.Importe);
                    });
                    _this.importeConceptos = _this.importeConceptos.toFixed(2);  
                },
                calcularSubtotal(){
                    var _this = this;
                    _this.subtotal = 0.00;
                    var length = _this.arrayMP.length;
                    for(var i = 0 ; i < length ; i++)
                        _this.subtotal += Math.round(_this.arrayMP[i]["importe"]*100)/100;  
                    _this.subtotal = _this.subtotal.toFixed(2);  
                    
                },
                calcularTotal : function(){
                    var _this = this;
                    _this.importe = 0.00; 
                    _this.importe += Math.round(_this.subtotal*100)/100; 
                    _this.importe += Math.round(_this.importeConceptos*100)/100; 
                    _this.importe = _this.importe.toFixed(2); 
                },
                finalizarCompra : function(){
                    var _this = this;
                    $('#bRealizarCompra').attr('disabled',true);
                    $.post("/compras/finalizar-compra",{
                        idProveedor : _this.idProveedor,
                        idSucursal : _this.idSucursal,
                        lineas:_this.arrayMP,
                        importe : _this.importe,
                        tipo: _this.tipoCompra})
                            .done(function(result){
                                if(!result.error){
                                    var tipo = 'success';
                                    var mensaje = 'Compra exitosa.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores').html(html);
                                    _this.arrayMP = [];
                                    _this.idMateriaPrima = "";
                                    _this.importe = 0;
                                    _this.idProveedor = 0;
                                }else {
                                    
                                    var tipo = 'danger';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + result.error + '</strong>' + 
                                                    '</div>';
                                    $('#errores').html(html);
                                    $('#bRealizarCompra').attr('disabled',true);
                                }
                                
                                setTimeout(function(){
                                    $('#errores').empty();
                                },2200)
                            })
                            .fail(function(){
                                var tipo = 'danger';
                                var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                '<strong>Error en la comunicación con el servidor. Contáctese con el administrador.</strong>' + 
                                                '</div>';
                                $('#errores').html(html);
                                $('#bRealizarCompra').attr('disabled',true);
                            });
                            
                            Object.assign(_this.$data,defaultData())
                }
            },
            watch : {
                idMateriaPrima : function(val){
                    if(parseInt(val) > 0){
                        this.agregarProducto(val)}
                },
                idConceptoCompra : function(val){
                    if(typeof val != 'undefined' && val != ""){
                        var _this = this;
                        var select = document.getElementById('compras-idconceptocompra');
                        var flag;

                        for(var i =0; i<_this.arrayConceptos.length; i++){
                            if(_this.arrayConceptos[i].IdConceptoCompra == val) flag = true;
                        }
                        if(!flag){
                            var value = select.options[select.selectedIndex].value;
                            var text = select.options[select.selectedIndex].text;

                            _this.arrayConceptos.push({
                                IdConceptoCompra : value,
                                ConceptoCompra : text,
                                Importe : ""
                                });
                        }
                    }
                },
                arrayConceptos : {
                        deep : true, 
                        handler: function(){
                            this.calcularImporte();
                        }
                },
                importe : function(val){
                    if(isNaN(val)) Vue.set(this,'importe',0.00);
                },
                importeConceptos : function(val){
                    if(isNaN(val)) Vue.set(this,'importeConceptos',0.00);
                }
            }
        }); 
    }
};