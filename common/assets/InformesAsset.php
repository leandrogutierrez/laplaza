<?php

namespace common\assets;

use yii\web\AssetBundle;

class InformesAsset extends AssetBundle
{
    public $sourcePath = '@common';
    public $css = [
    ];
    public $js = [
        'js/Informes.js',
    ];
    public $depends = [
        'common\assets\CommonAsset'
    ];
}
