<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use yii\web\AssetBundle;

class ProductosAsset extends AssetBundle
{
    public $sourcePath = '@common/';
   
    public $css = [
    ];
    public $js = [
        'js/Productos.js',
    ];
    public $depends = [
        'common\assets\CommonAsset'
    ];
}
