<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
        
    'maskMoneyOptions' => [
        'prefix' => '$',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 2,
        'allowZero' => true,
        'allowNegative' => false,
    ]
];
