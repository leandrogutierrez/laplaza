<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'datecontrol' =>  [
             'class' => '\kartik\datecontrol\Module'
        ]
    ],
    'bootstrap' => ['devicedetect'],
    'components' => [
         'cache' => [
            'class' => 'yii\redis\Cache',
        ],
        'session' => [
            'class' => 'yii\redis\Session',
        ],
        'formatter' => [
            'defaultTimeZone' => 'America/Argentina/Tucuman',
            'dateFormat' => 'dd/MM/yyyy',
            'datetimeFormat' => 'dd/MM/yyyy H:mm',
            'decimalSeparator' => ',',
            'thousandSeparator' => '',
            'locale' => 'es-AR'
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        'consoleRunner' => [
            'class' => 'toriphes\console\Runner',
            'yiiscript' => '@root/yii',
        ],
        'devicedetect' => [
        'class' => 'alikdex\devicedetect\DeviceDetect'
    ],
    ],
];
