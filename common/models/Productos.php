<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Productos extends Model
{
    public $IdProducto;
    public $IdCategoriaProducto;
    public $IdUnidad;
    public $CodigoProd;
    public $Descripcion;
    public $PrecioDistribuidor;
    public $PrecioAlmacen;
    public $PrecioPublico;
    public $Estado;
    
//Derivados
    public $Unidad;
    public $UnidadProduccion;
    public $IdUnidadProduccion;
    public $Equivalencia;
    public $Categoria;
    public $PrecioUltimo;
    public $Planillas;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    
    
    public function attributeLabels()
    {
        return[
            'CodigoProd' => 'Código de producto',
            'Descripcion' => 'Descripción',
            'PrecioDistribuidor' => 'Precio Distribuidor',
            'PrecioAlmacen' => 'Precio Almacen',
            'PrecioPublico' => 'Precio Público',
            'IdUnidad' => 'Unidad de intercambio',
            'IdCategoriaProducto' => 'Categoría',
            'IdUnidadProduccion' => 'Unidad de producción',
        ];
    }
    
    public function rules()
    {
        return[
            //Alta
            [['CodigoProd', 'Descripcion', 'PrecioDistribuidor', 'PrecioAlmacen','PrecioPublico','IdUnidad'],
                    'required', 'on' => self::_ALTA],
            //Modificar
            [['IdProducto', 'CodigoProd', 'Descripcion','PrecioDistribuidor', 'PrecioAlmacen','PrecioPublico',
                'IdUnidad'],'required', 'on' => self::_MODIFICAR],
            //Safe
            [$this->attributes(), 'safe']
        ];
    }


    /**
     * Permite instanciar un producto desde la base de datos.
     * ssp_dame_producto
     */
    public function Dame($idCliente = null)
    {
        if ($idCliente == null) {
            $sql = 'CALL ssp_dame_producto( :idProducto )';

            $query = Yii::$app->db->createCommand($sql);

            $query->bindValues([
                ':idProducto' => $this->IdProducto,
            ]);
        } else {
            $sql = 'CALL ssp_dame_producto_cliente( :idProducto, :idCliente)';
            
            $query = Yii::$app->db->createCommand($sql);
           
            $query->bindValues([
                ':idProducto' => $this->IdProducto,
                ':idCliente' => $idCliente
            ]);
        }
        $this->attributes = $query->queryOne();
    }

    /**
     * Permite cambiar el estado de un producto a Activo, controlando que no se
     * encuentre activo ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_producto
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_producto( :token, :idProducto, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProducto' => $this->IdProducto,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de un producto a Baja, controlando que no se
     * encuentre dado de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_producto
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_producto( :token, :idProducto, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProducto' => $this->IdProducto,
        ]);
        
        return $query->queryScalar();
    }
}
