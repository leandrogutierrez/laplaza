<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 20-Apr-2017 11:13:15
 */
class GestorSucursales extends Model implements iGestor
{
    /**
     * Permite dar de alta una sucursal controlando que el nombre se encuentre en
     * uso ya.
     * Devuelve OK + el id de la sucursal creada o un mensaje de error en Mensaje.
     * ssp_alta_sucursal
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_sucursal( :token, :sucursal, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':sucursal' => $Objeto->Sucursal,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar una sucursal controlando que el nombre no se encuentre en
     * uso ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_sucursal
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_sucursal( :token, :idSucursal, :sucursal,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idSucursal' => $Objeto->IdSucursal,
            ':sucursal' => $Objeto->Sucursal,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar una sucursal controlando que no existan usuarios de sucursal,
     * compras, retiros, stock de materia prima, producciones, ventas, cajas, usos de
     * caja y/o movimientos asociados.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_sucursal
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_sucursal( :token, :idSucursal,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idSucursal' => $Objeto->IdSucursal,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar sucursals filtr�ndolas por una cadena de b�squeda e indicando
     * si se incluyen o no las dadas de baja en pIncluyeBajas = [S|N].
     * Ordena por Sucursal.
     * ssp_buscar_sucursals
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_sucursales( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
}
