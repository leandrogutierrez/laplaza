<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\IdentityInterface;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:57
 */
class Usuarios extends Model implements IdentityInterface
{
    public $IdUsuario;
    public $IdRol;
    public $Usuario;
    public $Password;
    public $Token;
    public $FechaUltIntento;
    public $FechaAlta;
    public $DebeCambiarPass;
    public $Estado;
    public $Nombres;
    public $Apellidos;
    public $IntentosPass;
    
    //Derivados
    public $NombreCompleto;
    public $Saldo;
    
    const _LOGIN = 'login';
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    public function attributeLabels()
    {
        return [
            'Usuario' => 'Nombre de usuario',
            'Password' => 'Contraseña',
            'IdRol' => 'Rol',
        ];
    }
    
    public function rules()
    {
        return [
            ['Usuario', 'filter', 'filter' => 'strtolower'],
            ['Usuario', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/',
            'message' => 'El nombre de Usuario sólo puede contener letras y números.' ],
            //Login
            [['Usuario','Password'],'required','on' => self::_LOGIN],
            //Alta
            [['IdRol', 'Usuario', 'Password', 'Apellidos', 'Nombres'],
            'required','on' => self::_ALTA],
            //Modificar
            [['IdUsuario', 'IdRol', 'Usuario', 'Password', 'Apellidos', 'Nombres'],
            'required','on' => self::_MODIFICAR],
            //Safe
            [['IdUsuario','IdRol','Usuario','Token','FechaAlta','FechaUltIntento',
            'DebeCambiarPass','Nombres','Apellidos','Estado','NombreCompleto','Saldo'],'safe']
        ];
    }
    
    /**
     * Permite instanciar un Usuario desde la base de datos.
     * ssp_dame_usuario
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_usuario ( :idUsuario )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idUsuario' => $this->IdUsuario,
        ]);
        
        $this->attributes = $query->queryOne();
    }
    
    /**
     * Permite instanciar un Usuario desde la base de datos utilizando su token de
     * sesi�n.
     * ssp_dame_usuario_por_token
     */
    public function TokenDame()
    {
        $sql = 'CALL ssp_dame_usuario_por_token( :token )';
       
        $query = Yii::$app->db->createCommand($sql);
       
        $query->bindValues([
           ':token' => $this->Token,
       ]);
       
        $this->attributes = $query->queryOne();
    }
   
    /**
     * Permite obtener el listado de permisos del usuario a partir de su token de
     * sesi�n.
     * ssp_dame_permisos
     *
     * @param Token
     */
    public function DamePermisos()
    {
        $sql = 'CALL ssp_dame_permisos( :token )';
       
        $query = Yii::$app->db->createCommand($sql);
       
        $query->bindValues([
           ':token' => $this->Token,
       ]);
       
        return $query->queryColumn();
    }

    

    /**
     * Permite cambiar el estado de un Usuario a Activo, controlando que el mismo no
     * est� activo ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_usuario
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_usuario ( :token, :idUsuario, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUsuario' => $this->IdUsuario,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de un Usuario a Baja, controlando que el mismo no
     * este dado de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_usuario
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_usuario ( :token, :idUsuario, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUsuario' => $this->IdUsuario,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de un usuario a Suspendido, controlando que el mismo
     * no este suspendido ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_suspender_usuario
     */
    public function Suspender()
    {
        $sql = 'CALL ssp_suspender_usuario ( :token, :idUsuario, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idUsuario' => $this->IdUsuario,
        ]);
        
        return $query->queryScalar();
    }
    
    /**
     * Procedimiento que inicia la sesi�n del usuario validando el nombre de usuario y
     * password en el sistema. Si el usuario es autenticado correctamente, se renueva
     * su Token de sesi�n y se resetea el contador de intentos fallidos. Si el usuario
     * no pasa la autenticaci�n un determinado n�mero de veces indicado por el
     * parametro MAXINTPASS, la cuenta queda en estado Suspendida.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_login_usuarios
     *
     * @param Password
     */
    public function Login($Password)
    {
        $sql = 'CALL ssp_login_usuarios( :usuario, :password, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':usuario' => $this->Usuario,
            ':password' => md5($Password),
        ]);
        $resultado = $query->queryOne();
        $this->attributes = $resultado;
        
        return $resultado['Mensaje'];
    }
    
    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     * The key should be unique for each individual user, and should be persistent so
     * that it can be used to check the validity of the user identity.  The space of
     * such keys should be big enough to defeat potential identity attacks.  This is
     * required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     *
     * @see validateAuthKey()
     */

    public function getAuthKey()
    {
        return $this->Token;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->IdUsuario;
    }

    /**
     * Validates the given auth key.  This is required if [[User::enableAutoLogin]] is
     * enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     *
     * @param authKey
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() == $authKey;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID. Null
     * should be returned if such an identity cannot be found or the identity is not
     * in an active state (disabled, deleted, etc.)
     *
     * @param id
     */
    public static function findIdentity($id)
    {
        $usuario = new Usuarios();
        
        $usuario->IdUsuario = $id;
        
        $usuario->Dame();
        
        return $usuario;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends
     * on the implementation. For example, [[\yii\filters\auth\HttpBearerAuth]] will
     * set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found or the identity is
     * not in an active state (disabled, deleted, etc.)
     *
     * @param token
     * @param type
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $usuario = new Usuarios();
        
        $usuario->Token = $token;
        
        $usuario->TokenDame();
        
        return($usuario->IdUsuario != null ? $usuario : null);
    }


    /**
     * Permite cambiar el password del usuario, validando el password actual.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_cambiar_password_usuario
     *
     * @param Token
     * @param OldPass
     * @param NewPass
     */
    public function CambiarPassword($Token, $OldPass, $NewPass)
    {
        $sql = 'CALL ssp_cambiar_password_usuario( :token, :oldPass, :newPass,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => $Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':oldPass' => md5($OldPass),
            ':newPass' => md5($NewPass),
        ]);
        
        return $query->queryScalar();
    }
}
