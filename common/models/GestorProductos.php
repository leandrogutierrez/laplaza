<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:28:22
 */
class GestorProductos extends Model implements iGestor
{

    /**
     * Permite dar de alta un producto, controlando que el c�digo de producto no se
     * encuentre en uso ya.
     * Devuelve OK + el id del producto creado o un mensaje de error en Mensaje.
     * ssp_alta_producto
     */
    public function Alta($Objeto )
    {
        $sql = 'CALL ssp_alta_producto( :token, :codigoProd, :descripcion,'
                . ' :precioDistribuidor, :precioAlmacen, :precioPublico, :idUnidad,'
                . ' :idCategoriaProducto, :unidadProduccion, :equivalencia, '
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':codigoProd' => $Objeto->CodigoProd,
            ':descripcion' => $Objeto->Descripcion,
            ':precioDistribuidor' => $Objeto->PrecioDistribuidor,
            ':precioAlmacen' => $Objeto->PrecioAlmacen,
            ':precioPublico' => $Objeto->PrecioPublico,
            ':idUnidad' => $Objeto->IdUnidad,
            ':idCategoriaProducto' => $Objeto->IdCategoriaProducto == ''? null : $Objeto->IdCategoriaProducto,
            ':unidadProduccion' => $Objeto->UnidadProduccion,
            ':equivalencia' => $Objeto->Equivalencia
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar un producto, controlando que el codigo de producto no se
     * encuentre en uso ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_producto
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_producto( :token, :idProducto, :codigoProd, :descripcion,'
                . ' :precioDistribuidor, :precioAlmacen, :precioPublico, :idUnidad, :idCategoriaProducto,'
                . ' :unidadProduccion, :equivalencia,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProducto' => $Objeto->IdProducto,
            ':codigoProd' => $Objeto->CodigoProd,
            ':descripcion' => $Objeto->Descripcion,
            ':precioDistribuidor' => $Objeto->PrecioDistribuidor,
            ':precioAlmacen' => $Objeto->PrecioAlmacen,
            ':precioPublico' => $Objeto->PrecioPublico,
            ':idUnidad' => $Objeto->IdUnidad,
            ':idCategoriaProducto' => $Objeto->IdCategoriaProducto == ''? null : $Objeto->IdCategoriaProducto ,
            ':unidadProduccion' => $Objeto->UnidadProduccion,
            ':equivalencia' => $Objeto->Equivalencia
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un producto controlando que el mismo no haya sido parte de una
     * elaboraci�n ni de una venta.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_producto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_producto( :token, :idProducto, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProducto' => $Objeto->IdProducto,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar productos filtr�ndolos por una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas [S|N].
     * Ordena por Descripcion.
     * ssp_buscar_productos
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_productos( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
    
    /**
     * Permite buscar productos filtr�ndolos por categoria, una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas [S|N].
     * Ordena por Descripcion.
     * ssp_buscar_productos_categoria
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function BuscarPorCategoria($Cadena = '', $IdCategoria = null, $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_productos_categoria( :idCategoria, :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':idCategoria' => ($IdCategoria == null? null :$IdCategoria),
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }

    /**
     * Permite realizar una modificacion masiva de productos
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_masivo_productos
     */
    public function ModificacionMasiva($productos = null) {
       $sql = 'CALL ssp_modificar_masivo_productos( :token, :productos, :IP, :userAgent, :app)';
       
       $query = Yii::$app->db->createCommand($sql);
       
       $query->bindValues([
           ':token' => Yii::$app->user->identity->Token,
           ':productos' => $productos,
           ':IP' => Yii::$app->request->userIP,
           ':userAgent' => Yii::$app->request->userAgent,
           ':app' => Yii::$app->id
       ]);
       
       return $query->queryScalar();
    }

}
