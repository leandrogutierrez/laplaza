<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 20-Apr-2017 11:13:08
 */
class Sucursales extends Model
{
    public $IdSucursal;
    public $Sucursal;
    public $Estado;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    
    public function rules()
    {
        return [
            //Alta
            ['Sucursal', 'required', 'on' => self::_ALTA],
            //Modificar
            [['IdSucursal', 'Sucursal'], 'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdSucursal', 'Sucursal', 'Estado'], 'safe']
        ];
    }

    /**
     * Permite instanciar una carnicer�a desde la base de datos.
     * ssp_dame_sucursal
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_sucursal( :idSucursal )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idSucursal' => $this->IdSucursal,
        ]);
        
        $this->attributes = $query->queryOne();
    }

    /**
     * Permite cambiar el estado de una sucursal a Activa, controlando que la misma
     * no se encuentre activa ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_sucursal
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_sucursal( :token, :idSucursal, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idSucursal' => $this->IdSucursal,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de una sucursal a Baja, controlando que la misma no
     * se encuentre dada de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_sucursal
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_sucursal( :token, :idSucursal, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idSucursal' => $this->IdSucursal,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite listar los usuarios asociados a la sucursal.
     * Ordena por Apellidos,Nombres.
     * ssp_listar_usuarios_sucursal
     */
    public function ListarUsuarios()
    {
        $sql = 'CALL ssp_listar_usuarios_sucursal( :idSucursal )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idSucursal' => $this->IdSucursal,
        ]);
        
        return $query->queryAll();
    }

    /**
     * Permite listar las cajas asociadas a una carnicer�a.
     * ssp_listar_cajas_sucursal
     */
    public function ListarCajas()
    {
        $sql = 'CALL ssp_listar_cajas_sucursal( :idSucursal )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idSucursal' => $this->IdSucursal,
        ]);
        
        return $query->queryAll();
    }

    /**
     * Permite dar de alta un usuario en una sucursal, controlando que el mismo no
     * se encuentre dado de alta ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_alta_usuario_sucursal
     *
     * @param UC
     */
    public function AgregarUsuario($UC)
    {
        $sql = 'CALL ssp_alta_usuario_sucursal( :token, :idSucursal, :idUsuario,'
            . ' :IP, :userAgent, :app)';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idSucursal' => $this->IdSucursal,
            ':idUsuario' => $UC->IdUsuario,
        ]);

        return $query->queryScalar();
    }

    /**
     * Permite borrar un usuario de una sucursal, controlando que el mismo exista en
     * la carnicer�a.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_usuario_sucursal
     *
     * @param UC
     */
    public function QuitarUsuario($UC)
    {
        $sql = 'CALL ssp_borrar_usuario_sucursal( :token, :idSucursal, :idUsuario,'
            . ' :IP, :userAgent, :app)';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
        ':token' => Yii::$app->user->identity->Token,
        ':IP' => Yii::$app->request->userIP,
        ':userAgent' => Yii::$app->request->userAgent,
        ':app' => Yii::$app->id,
        ':idSucursal' => $this->IdSucursal,
        ':idUsuario' => $UC->IdUsuario,
    ]);

        return $query->queryScalar();
    }


    /**
     * Permite listar los usuarios que pueden ser asignados a una sucursal. Es decir,
     * aquellos usuarios activos que no se encuentran ya en una sucursal.
     * ssp_listar_usuarios_asignar_sucursal
     */
    public function ListarUsuariosAsignar()
    {
        $sql = 'CALL ssp_listar_usuarios_asignar_sucursal( :idSucursal )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idSucursal' => $this->IdSucursal
        ]);
        
        return $query->queryAll();
    }
}
