<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorRendiciones extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_rendicion( :token, :idUsuario, :importe, :pObservaciones,'
                . ':IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUsuario' => $Objeto->IdUsuario,
            ':importe' => ($Objeto->Importe == '') ? null : $Objeto->Importe,
            ':pObservaciones' =>($Objeto->Observaciones == '') ? null : $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
    }
 
    public function Borrar($Objeto)
    {
    }

    /**
     * NO SE USA
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }
 
    public function BuscarAvanzado($IdUsuario, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_rendiciones( :idVendedor, :fechaInicio,'
            . ' :fechaFin, :estado )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idUsuario' => ($IdUsuario== '') ? null : $IdUsuario,
            ':fechaInicio' => ($FechaInicio == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => ($FechaFin == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
        ]);

        return $query->queryAll();
    }
}
