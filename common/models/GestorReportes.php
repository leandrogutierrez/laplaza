<?php
namespace common\models;

use Yii;
use yii\base\Model;

class GestorReportes extends Model
{

    /**
     * Lista el men� correspondiente a los informes activos, adjuntando un campo que
     * dice si es o no es hoja (EsHoja = [S|N], cuando no es hoja es un men� de
     * distribuci�n). Los �tems de men� est�n listados en orden jer�rquico y arb�reo,
     * con el nodo padre, el nivel del �rbol y una cadena para mostrarlo ordenado.
     * ssp_inf_listar_menu_reportes
     */
    public function ListarMenu()
    {
        $sql = "CALL ssp_inf_listar_menu_reportes()";

        $query = Yii::$app->db->createCommand($sql);

        return $query->queryAll();
    }

    /**
     * Trae todos los campos de la tabla ModelosReporte.
     * ssp_inf_dame_modeloreporte
     *
     * @param IdModeloReporte
     */
    public function DameModeloReporte($IdModeloReporte)
    {
        $sql = "CALL ssp_inf_dame_modeloreporte( :id )";

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':id' => $IdModeloReporte,
        ]);

        return $query->queryOne();
    }

    /**
     * Trae todos los par�metros de un modelo de reporte ordenados por pTipoOrden: P:
     * Par�metro - F: Formulario.
     * ssp_inf_dame_parametros_modeloreporte
     *
     * @param IdModeloReporte
     * @param TipoOrden
     */
    public function DameParametrosModeloReporte($IdModeloReporte, $TipoOrden = 'F')
    {
        $sql = "CALL ssp_inf_dame_parametros_modeloreporte( :id, :tipo)";

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':id' => $IdModeloReporte,
            ':tipo' => $TipoOrden
        ]);

        return $query->queryAll();
    }

    /**
     * Permite traer un resultset de forma {Id,Nombre} para poblar la lista del
     * par�metro que debe ser de tipo L: Listado o A: Autocompletar. En este �ltimo
     * caso, debe pasarse el par�metro pCadena; en otro caso, pasar ''. Lo ordena por
     * nombre.
     * No incluye el TODOS.
     * ssp_inf_llenar_listado_parametro
     *
     * @param IdModeloReporte
     * @param NroParametro
     * @param Cadena
     */
    public function LlenarListadoParametro($IdModeloReporte, $NroParametro, $Cadena)
    {
        $sql = "CALL ssp_inf_llenar_listado_parametro( :id, :nroParam, :cadena )";

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':id' => $IdModeloReporte,
            ':nroParam' => $NroParametro,
            ':cadena' => $Cadena
        ]);

        return $query->queryAll();
    }

    /**
     * Permite traer el nombre del elemento de un listado dado el Id.
     * ssp_inf_dame_parametro_listado
     *
     * @param IdModeloReporte
     * @param NroParametro
     * @param Id
     */
    public function DameParametroListado($IdModeloReporte, $NroParametro, $Id)
    {
        $sql = "CALL ssp_inf_dame_parametro_listado( :idReporte, :nroParam, :id )";

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idReporte' => $IdModeloReporte,
            ':nroParam' => $NroParametro,
            ':id' => $Id
        ]);

        return $query->queryAll();
    }

    /**
     * Permite traer el resultset del reporte. Para ello trae el nombre del SP de la
     * tabla ModelosReporte.
     * ssp_inf_ejecutar_reporte
     *
     * @param IdModeloReporte
     * @param CadenaParam
     */
    public function Ejecutar($IdModeloReporte, $CadenaParam)
    {
        $sql = "CALL ssp_inf_ejecutar_reporte( :id, :cadena )";

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':id' => $IdModeloReporte,
            ':cadena' => $CadenaParam
        ]);
        echo $sql;
        echo $query->getRawSql();

        return $query->queryAll();
    }
    
    public function DameParametro($sp, $id)
    {
        $sql = 'CALL '.$sp.'( :id )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':id' => $id
        ]);

        return $query->queryOne();
    }
}
