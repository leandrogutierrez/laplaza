<?php
namespace common\models;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:41:04
 */
interface iGestor
{
    public function Alta($Objeto);

    public function Modificar($Objeto);

    public function Borrar($Objeto);

    /**
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena, $IncluyeBajas);
}
