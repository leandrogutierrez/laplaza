<?php
namespace common\models;

use Yii;
use yii\base\Model;

class ComprobantesPago extends Model
{
    public $IdComprobantePago;
    public $IdUsuario;
    public $IdCliente;
    public $NroComprobantePago;
    public $FechaAlta;
    public $Importe;
    public $Estado;
    public $IdRendicion;
    public $Observaciones;

    
    //Derivados
    public $LineasPago;
    public $Cliente;
    public $Usuario;
     
    const _ALTA = 'alta';

    public function attributeLabels()
    {
        return [
            'IdCliente' => 'Cliente',
            'IdUsuario' => 'Usuario',
            'IdRencicion' => 'Rendición'
        ];
    }
    
    public function rules()
    {
        return [
            
            [['IdCliente', 'Importe'], 'required', 'on' => self::_ALTA],
            ['Importe', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
            //Safe
            [['IdComprobantePago','IdCliente','IdUsuario','NroComprobante',
                'FechaAlta','Importe','Observaciones','Usuario','Cliente','LineasPago','IdRendicion'], 'safe']
        ];
    }
     
    public function Dame()
    {
        $sql = 'CALL ssp_dame_comprobante_pago( :idComprobante )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idComprobante' => $this->IdComprobantePago
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
