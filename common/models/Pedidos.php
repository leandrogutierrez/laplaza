<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:57
 */
class Pedidos extends Model
{
    public $IdPedido;
    public $IdPedidoHijo;
    public $IdCliente;
    public $IdUsuario;
    public $NroPedido;
    public $Importe;
    public $FechaEntrega;
    public $FechaAlta;
    public $Observaciones;
    public $Estado;
    
    //Derivados
    public $IdPedidoPadre;
    public $LineasPedido;
    public $Cliente;
    public $Usuario;
    
    public function attributeLabels()
    {
        return [
            'IdCliente' => 'Cliente',
            'IdUsuario' => 'Usuario',
            'NroPededo' => 'Nro de pedido',
            'FechaEntrega' => 'Fecha de Entrega',
            'FechaAlta' => 'Fecha de Alta',
            'IdPedidoHijo' => 'Pedido original'
        ];
    }
    
    public function rules()
    {
        return [
            //Safe
            [['IdPedido', 'IdPedidoHijo','IdCliente','IdUsuario', 'NroPedido', 'FechaAlta', 'FechaEntrega', 'Estado',
                'LineasPedido', 'Cliente','Usuario', 'Importe','IdPedidoPadre'], 'safe']
        ];
    }
    
    /**
     * Permite instanciar una pedido desde la base de datos
     * ssp_dame_pedido
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_pedido( :idpedido )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idpedido' => $this->IdPedido
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
