<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 01-Jun-2017 11:51:52
 */
class MateriasPrima extends Model
{
    public $IdMateriaPrima;
    public $MateriaPrima;
    public $Tipo;
    public $Estado;
    

    //Derivados
    public $IdProducto;
    public $LineasRendimiento;
    public $Descripcion;
    public $IndicaRendimiento;
    public $IdCarniceria;
    public $PesoReferenciaMP;
    public $PesoTotalPromedio;
    public $PuntoTeorico;
    public $PrecioTotalFraccionado;

    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    public function attributeLabels()
    {
        return [
            'IdProducto' => 'Producto',
            'Tipo' => 'Tipo de materia prima',
            'IndicaRendimiento' => 'Ingresa rendimiento manual',
            'IdCarniceria' => 'Carnicería',
            'PuntoTeorico' => 'Punto teórico',
            'PrecioTotalFraccionado' => 'Precio total fraccionado',
            'PesoTotalPromedio' => 'Peso total promedio',
            'PesoReferenciaMP' => 'Peso de referencia'
        ];
    }
    
    public function rules()
    {
        return [
            //Alta
            [['MateriaPrima', 'Tipo'], 'required', 'on' => self::_ALTA],
            //Mofificar
            [['IdMateriaPrima', 'MateriaPrima', 'Tipo'], 'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdMateriaPrima', 'MateriaPrima', 'Estado', 'IdProducto',
            'LineasRendimiento', 'Descripcion', 'Tipo', 'IndicaRendimiento',
            'IdCarniceria', 'PesoTotalPromedio', 'PuntoTeorico',
            'PesoReferenciaMP'], 'safe'],
        ];
    }

    /**
     * Permite instanciar una materia prima desde la base de datos.
     * ssp_dame_materiaprima
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_materiaprima( :idMateriaPrima )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idMateriaPrima' => $this->IdMateriaPrima,
        ]);
        
        $this->attributes = $query->queryOne();
    }

    /**
     * Permite cambiar el estado de una materia prima a Activo, controlando que no se
     * encuentre activa ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_materiaprima
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_materiaprima( :token, :idMateriaPrima, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idMateriaPrima' => $this->IdMateriaPrima,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de una materia prima a Baja, controlando que no se
     * encuentre dada de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_materiaprima
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_materiaprima( :token, :idMateriaPrima, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idMateriaPrima' => $this->IdMateriaPrima,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite agregar un rendimiento a una materia prima,  indicando si el valor del
     * rendimiento es porcentual (P) o por unidad (U) en pTipoRendimiento.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_alta_rendimiento_materiaprima
     *
     * @param Rendimiento
     */
    public function AgregarRendimiento($Rendimiento)
    {
        $sql = 'CALL ssp_alta_rendimiento_materiaprima( :token, :idMateriaPrima,'
                . ' :idProducto, :rendimiento, :tipoRendimiento, :precio,'
                . ' :pesoReferenciaMP, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idMateriaPrima' => $this->IdMateriaPrima,
            ':idProducto' => $Rendimiento->IdProducto,
            ':rendimiento' => $Rendimiento->Rendimiento,
            ':tipoRendimiento' => $Rendimiento->TipoRendimiento,
            ':precio' => $Rendimiento->Precio,
            ':pesoReferenciaMP' => $Rendimiento->PesoReferenciaMP
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un rendimiento asociado a una materia prima.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_rendimiento_materiaprima
     *
     * @param Rendimiento
     */
    public function QuitarRendimiento($Rendimiento)
    {
        $sql = 'CALL ssp_borrar_rendimiento_materiaprima( :token, :idRMP,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idRMP' => $Rendimiento->IdRMP,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite obtener todos los rendimientos de una materia prima.
     * Ordena por FechaAlta.
     * ssp_listar_rendimientos_materiaprima
     */
    public function ListarRendimientos()
    {
        $sql = 'CALL ssp_listar_rendimientos_materiaprima(  :idMateriaPrima )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idMateriaPrima' => $this->IdMateriaPrima,
        ]);
        
        return $query->queryAll();
    }

    /**
     * Permite modificar los valores del rendimiento de una materia prima. Solo se
     * permite la edici�n del valor, tipo y precio del rendimiento, si corresponde.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_rendimiento_materiaprima
     *
     * @param Rendimiento
     */
    public function ModificarRendimiento($Rendimiento)
    {
        $sql = 'CALL ssp_modificar_rendimiento_materiaprima( :token, :idRMP,'
                . ' :rendimiento, :tipoRendimiento, :precio,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idRMP' => $Rendimiento->IdRMP,
            ':rendimiento' => $Rendimiento->Rendimiento,
            ':tipoRendimiento' => $Rendimiento->TipoRendimiento,
            ':precio' => $Rendimiento->Precio
        ]);
        
        return $query->queryScalar();
    }
    
    /**
     * Permite obtener el listado de productos que se generan a partir de una materia
     * prima calculando el valor del rendimiento de cada producto.
     * Ordena por Descrpici�n.
     * ssp_dame_rendimiento_mp
     */
    public function DameRendimiento()
    {
        $sql = 'CALL ssp_dame_rendimiento_mp(  :idMateriaPrima )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idMateriaPrima' => $this->IdMateriaPrima,
        ]);
        
        return $query->queryAll();
    }
}
