<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 14-Aug-2017 20:59:27
 */
class GestorVentas extends Model implements iGestor
{
    /**
     * Permite dar de alta una venta junto con sus l�neas de venta. La venta se crea
     * en estado pendiente y se mantiene en el mismo hasta tanto se efect�e el pago.
     * Devuelve OK + el id de la venta creada o un mensaje de error en Mensaje.
     * ssp_alta_ventas
     *
     * @param Objeto
    */
    
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_venta( :token, :idReparto, :idCliente,'
                . ' :lineas, :importe, :edicionPrecios, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        Yii::info($Objeto->EdicionPrecios);
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => ($Objeto->IdReparto == '') ? null : $Objeto->IdReparto,
            ':idCliente' => ($Objeto->IdCliente == '') ? null : $Objeto->IdCliente,
            ':lineas' => $Objeto->LineasVenta,
            ':importe' => ($Objeto->Importe == '') ? null : $Objeto->Importe,
            ':edicionPrecios' => ($Objeto->EdicionPrecios == 'true' ? 'S' : 'N')
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
    }

    /**
     * Permite borrar una venta controlando que la misma se encuentre en estado
     * Pendiente.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_venta
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_venta( :token, :idVenta,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVenta' => $Objeto->IdVenta,
        ]);
        
        return $query->queryScalar();
    }
    
    public function Anular($Objeto)
    {
        $sql = 'CALL ssp_anular_venta( :token, :idVenta,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVenta' => $Objeto->IdVenta,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * NO SE USA
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }
 
    public function BuscarAvanzado($IdReparto, $FechaInicio, $FechaFin, $Estado, $IdCliente)
    {
        $sql = 'CALL ssp_buscar_avanzado_ventas( :idReparto, :fechaInicio,'
            . ' :fechaFin, :estado, :idCliente )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idReparto' => ($IdReparto == '') ? null : $IdReparto,
            ':fechaInicio' => ($FechaInicio == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => ($FechaFin == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
            ':idCliente' => ($IdCliente == '') ? null : $IdCliente,
        ]);

        return $query->queryAll();
    }
    
    public function DameVentasReparto($IdReparto = null)
    {
        $sql = 'CALL ssp_dame_ventas_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($IdReparto == 0) ? null : $IdReparto,
        ]);
        
        return $query->queryAll();
    }
}
