<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 30-Jun-2017 17:31:28
 */
class ConceptosCompra extends Model
{
    public $IdConceptoCompra;
    public $ConceptoCompra;

    //Derivados
    public $Importe;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    public function rules()
    {
        return [
            //Alta
            ['ConceptoCompra', 'required', 'on' => self::_ALTA],
            //Modificar
            [['IdConceptoCompra', 'ConceptoCompra'], 'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdConceptoCompra', 'ConceptoCompra', 'Importe'], 'safe']
        ];
    }
    /**
     * Permite instanciar un concepto de compra desde la base de datos.
     * ssp_dame_conceptocompra
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_conceptocompra( :idConceptoCompra)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idConceptoCompra' => $this->IdConceptoCompra,
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
