<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Vehiculos extends Model
{
    public $IdVehiculo;
    public $Marca;
    public $Modelo;
    public $Patente;
    public $Kilometraje;
    public $Estado;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    public function attributeLabels()
    {
        return[
            'Marca' => 'Marca del Vehículo',
            'Modelo' => 'Modelo del Vehículo',
            'Patente' => 'Numero de patente del Vehículo',
        ];
    }

    public function rules()
    {
        return[
            [['Marca','Modelo','Patente'],'required','on' => self::_ALTA],
            [['IdVehiculo','Marca','Modelo','Patente'],'required', 'on' => self::_MODIFICAR],
            [['IdVehiculo','Marca','Modelo','Patente','Estado','Kilometraje'], 'safe']
        ];
    }
    
    /**
     * Permite instanciar un vehiculo desde la base de datos.
     * ssp_dame_vendedor
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_vehiculo (:IdVehiculo)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':IdVehiculo' => $this->IdVehiculo,
        ]);
        
        $this->attributes = $query->queryOne();
    }
    /**
    * Permite cambiar el estado de un vehiculo a Baja, controlando que el mismo no
    * este dado de baja ya.
    * Devuelve OK o un mensaje de error en Mensaje.
    * ssp_darbaja_vehiculo
    */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_vehiculo( :token, :idVehiculo, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVehiculo' => $this->IdVehiculo,
        ]);
        
        return $query->queryScalar();
    }
    /**
    * Permite cambiar el estado de un vehiculo a Activo, controlando que el mismo no
    * este activo ya.
    * Devuelve OK o un mensaje de error en Mensaje.
    * ssp_activar_vendedor
    */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_vehiculo( :token, :idVehiculo, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVehiculo' => $this->IdVehiculo,
        ]);
        
        return $query->queryScalar();
    }
}
