<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:56
 */
class GestorUsuarios extends Model implements iGestor
{
    /**
     * Permite dar de alta un Usuario, controlando que el nombre de usuario no exista
     * ya. El usuario se crea con la variable DebeCambiarPass = 'S' para que se cambie
     * la contrase�a en el primer inicio de sesi�n.
     * Devuelve OK + el id del usuario creado o un mensaje de error en Mensaje.
     * ssp_alta_usuario
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_usuario( :token, :idRol, :usuario, :password,'
                . ' :nombres, :apellidos, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idRol' => $Objeto->IdRol,
            ':usuario' => $Objeto->Usuario,
            ':password' => md5($Objeto->Password),
            ':nombres' => $Objeto->Nombres,
            ':apellidos' => $Objeto->Apellidos,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar los datos de un usuario controlando que el nombre de usuario
     * no se encuentre en uso ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_usuario
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_usuario( :token, :idUsuario, :idRol, :usuario, :password,'
                . ' :nombres, :apellidos, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUsuario' => $Objeto->IdUsuario,
            ':idRol' => $Objeto->IdRol,
            ':usuario' => $Objeto->Usuario,
            ':password' => md5($Objeto->Password),
            ':nombres' => $Objeto->Nombres,
            ':apellidos' => $Objeto->Apellidos,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un usuario controlando que el mismo no tenga movimientos de caja
     * asociados.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_usuario
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_usuario( :token, :idUsuario, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUsuario' => $Objeto->IdUsuario,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar usuarios filtr�ndolos por una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas = [S: Si | N: No].
     * Ordena por Apellido/s, Nombre/s.
     * ssp_buscar_usuarios
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena, $IncluyeBajas)
    {
        $sql = 'CALL ssp_buscar_usuarios( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
}
