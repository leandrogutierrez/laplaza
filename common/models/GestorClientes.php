<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorClientes extends Model implements iGestor
{

    /**
     * Permite dar de alta un cliente.
     * Devuelve OK + el id del cliente creado o un mensaje de error en Mensaje.
     * ssp_alta_cliente
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_cliente( :token, :nombres, :apellidos, :direccion,'
                . ' :telefono, :condicionIva, :cuit, :idListaPrecios,:idZona, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':nombres' => $Objeto->Nombres,
            ':apellidos' => $Objeto->Apellidos,
            ':direccion' => $Objeto->Direccion,
            ':telefono' => $Objeto->Telefono,
            ':condicionIva' => $Objeto->CondicionIva,
            ':cuit' => ($Objeto->Cuit == '') ? null : $Objeto->Cuit,
            ':idListaPrecios' => $Objeto->IdListaPrecios,
            ':idZona' => $Objeto->IdZona,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar un cliente.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_cliente
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_cliente(:token,:idCliente,:nombres,:apellidos,'
                .':cuit,:telefono,:direccion, :condicionIva,'
                .' :idListaPrecios,:idZona,:IP,:userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCliente' => $Objeto->IdCliente,
            ':nombres' => $Objeto->Nombres,
            ':apellidos' => $Objeto->Apellidos,
            ':direccion' => $Objeto->Direccion,
            ':telefono' => $Objeto->Telefono,
            ':condicionIva' => $Objeto->CondicionIva,
            ':cuit' => $Objeto->Cuit,
            ':idListaPrecios' => $Objeto->IdListaPrecios,
            ':idZona' => $Objeto->IdZona,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un cliente controlando que no existan ventas ni comprobantes de
     * pago asociados.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_cliente
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_cliente( :token, :idCliente, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCliente' => $Objeto->IdCliente,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar clientes filtr�ndolos por una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas : [S: Si | N: No].
     * Ordena por Apellido/s,Nombre/s.
     * ssp_buscar_clientes
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IdZona = null, $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_clientes( :cadena, :idZona, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':idZona' => $IdZona,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }

    public function ListasPrecios($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_listasprecios( :cadena, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
                ':cadena' => $Cadena,
                ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }

    public function BuscarClientesReparto()
    {
        $sql = 'CALL ssp_buscar_clientes_zona( :cadena, :idZona, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':idZona' => $IdZoza,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
}
