<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorUnidades extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_unidad( :token, :unidad, :descripcion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':unidad' => $Objeto->Unidad,
            ':descripcion' => $Objeto->Descripcion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_unidad( :token, :idUnidad, :unidad, :descripcion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUnidad' => $Objeto->IdUnidad,
            ':unidad' => $Objeto->unidad,
            ':descripcion' => $Objeto->Descripcion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_unidad( :token, :idUnidad, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUnidad' => $Objeto->IdUnidad,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_unidades( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
    
    public function ListarUnidadesProduccion()
    {
        $sql = 'CALL ssp_listar_unidades_producion()';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
        ]);
        
        return [['IdUnidad' =>'1','Unidad' => 'unidad1'],['IdUnidad' => '2', 'Unidad' =>'dos']];
    }
}
