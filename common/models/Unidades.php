<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Unidades extends Model
{
    public $IdUnidad;
    public $Unidad;
    public $Descripcion;
    public $Estado;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    public function dame()
    {
        $sql = 'CALL ssp_dame_unidad( :idUnidad) ';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            'idUnidad' => $this->IdUnidad,
        ]);
        
        $this->attributes = $query->queryOne();
    }
    public function rules()
    {
        return [
          [['Unidad','Descripcion'],'required','on' => self::_ALTA],
          [['IdUnidad','Unidad','Descripcion'],'required','on' => self::_MODIFICAR],
          [['IdUnidad','Unidad','Descripcion','Estado'],'safe']
        ];
    }
}
