<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 02-Mar-2017 10:08:03
 */
class Empresa extends Model
{
    public $Parametro;
    public $Descripcion;
    public $Rango;
    public $Valor;
    public $Editable;
    public $EsInicial;
   

    /**
     * Permite traer en formato resultset los par�metros de la empresa que necesitan
     * cargarse al inicio de sesi�n (EsInicial = S).
     * ssp_dame_datos_empresa
     */
    public function DameDatos()
    {
        $sql = 'CALL ssp_dame_datos_empresa()';
        
        $query = Yii::$app->db->createCommand($sql);
        
        return $query->queryAll();
    }
}
