<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:56
 */
class GestorProveedores extends Model implements iGestor
{
    /**
     * Permite dar de alta un Proveedor, controlando que la raz�n social y el CUIT
     * (opcional) no se encuentren en uso.
     * Devuelve OK + el id del proveedor creado o un mensaje de error en Mensaje.
     * ssp_alta_proveedor
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_proveedor( :token, :proveedor, :cuit, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':proveedor' => $Objeto->Proveedor,
            ':cuit' => $Objeto->Cuit,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar un proveedor controlando que la raz�n social y el CUIT no se
     * encuentren en uso.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_proveedor
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_proveedor( :token, :idProveedor, :proveedor,'
                . ' :cuit, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProveedor' => $Objeto->IdProveedor,
            ':proveedor' => $Objeto->Proveedor,
            ':cuit' => $Objeto->Cuit,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un proveedor controlando que el mismo no tenga compras asociadas.
     *
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_proveedor
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_proveedor( :token, :idProveedor, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProveedor' => $Objeto->IdProveedor,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar proveedores filtr�ndolos por una cadena de b�squeda e indicando
     * si se incluyen o no los dados de baja en pIncluyeBajas [S|N].
     * Ordena por RazonSocial.
     * ssp_buscar_proveedores
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_proveedores( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas
            ]);
        
        return $query->queryAll();
    }
}
