<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Vendedores extends Model
{
    public $IdVendedor;
    public $Nombres;
    public $Apellidos;
    public $Telefono;
    public $Cuil;
    public $IdUsuario;
    
    //Derivados
    public $Usuario;
    public $Zona;
    public $UltimoReparto;
    
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    public function attributeLabels()
    {
        return[
            'IdUsuario' => 'Usuario',
        ];
    }
    public function rules()
    {
        return[
            [['Nombres','Apellidos'],'required','on' => self::_ALTA],
            [['IdVendedor','Nombres','Apellidos','Cuil'],'required','on' => self::_MODIFICAR],
            [['IdVendedor','Nombres','Apellidos','Telefono','Cuil','IdUsuario','Zona','UltimoReparto'],'safe']
        ];
    }
    
    /**
     * Permite instanciar un vendedor desde la base de datos.
     * ssp_dame_vendedor
     */
    public function dame()
    {
        $sql = 'CALL ssp_dame_vendedor( :IdVendedor)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':IdVendedor' => $this->IdVendedor,
        ]);
        
        $this->attributes = $query->queryOne();
    }
   
    /**
    * Permite cambiar el estado de un vendedor a Activo, controlando que el mismo no
    * este activo ya.
    * Devuelve OK o un mensaje de error en Mensaje.
    * ssp_activar_vendedor
    */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_vendedor( :token, :idVendedor, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVendedor' => $this->IdVendedor,
        ]);
        
        return $query->queryScalar();
    }
    /**
    * Permite cambiar el estado de un vendedor a Baja, controlando que el mismo no
    * este dado de baja ya.
    * Devuelve OK o un mensaje de error en Mensaje.
    * ssp_darbaja_vendedor
    */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_vendedor( :token, :idVendedor, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVendedor' => $this->IdVendedor,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * Permite instanciar un vendedor desde la base de datos en funcion del idUsuario.
     * ssp_dame_vendedor
     */
    public function DameVendedorPorUsuario()
    {
        $sql = 'CALL ssp_dame_vendedor_por_usuario( :idUsuario)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idUsuario' => $this->IdUsuario,
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
