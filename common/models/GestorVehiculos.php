<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorVehiculos extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_vehiculo( :token, :marca,'
                . ' :modelo, :patente,  '
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':marca' => $Objeto->Marca,
            ':modelo' => $Objeto->Modelo,
            ':patente' => $Objeto->Patente,
        ]);
        
        return $query->queryScalar();
    }

    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_vehiculo( :token, :idVehiculo, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVehiculo' => $Objeto->IdVehiculo,
        ]);
        
        return $query->queryScalar();
    }

    public function Buscar($Cadena, $IncluyeBajas)
    {
        $sql = 'CALL ssp_buscar_vehiculos( :cadena, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }

    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_vehiculo( :token, :idVehiculo, :marca,'
                . ':modelo, :patente, '
                . ':IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVehiculo' => $Objeto->IdVehiculo,
            ':marca' => $Objeto->Marca,
            ':modelo' => $Objeto->Modelo,
            ':patente' => $Objeto->Patente,
        ]);
        
        return $query->queryScalar();
    }
}
