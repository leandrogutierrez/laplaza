<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Clientes extends Model
{
    public $IdCliente;
    public $IdZona;
    public $IdListaPrecios;
    public $Nombres;
    public $Apellidos;
    public $Direccion;
    public $Telefono;
    public $CondicionIva;
    public $Cuit;
    public $FechaAlta;
    public $Estado;
    
    //Derivados
    public $Saldo;
    public $UltimaVenta;
    public $ListaPrecios;
    public $Zona;

    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    public function attributeLabels()
    {
        return[
            'Nombres' => 'Nombres',
            'Apellidos' => 'Apellidos',
            'IdListaPrecios' => 'Lista de precios',
            'IdZona' => 'Zona',
        ];
    }
    
    public function rules()
    {
        return [
            ['Cuit', 'string', 'length' => 11],
            //Alta
            [['Nombres', 'Apellidos', 'Direccion', 'CondicionIva','IdListaPrecios','IdZona'],
            'required', 'on' => self::_ALTA],
            //Modificar
            [['IdCliente', 'Nombres', 'Apellidos', 'Direccion', 'CondicionIva','IdListaPrecios'],
            'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdCliente', 'Nombres', 'Apellidos', 'Direccion', 'Telefono','Saldo', 'UltimaVenta',
            'CondicionIva', 'Cuit', 'FechaAlta', 'Estado','IdListaPrecios','IdZona','Zona','ListaPrecios'], 'safe']
        ];
    }
    

    /**
     * Permite instanciar un Cliente desde la base de datos.
     * ssp_dame_cliente
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_cliente( :idCliente )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => $this->IdCliente,
        ]);
        
        $this->attributes = $query->queryOne();
    }

    /**
     * Permite cambiar el estado de un cliente a Activo, controlando que el mismo no
     * este activo ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_cliente
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_cliente( :token, :idCliente, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCliente' => $this->IdCliente,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de un cliente a Baja, controlando que el mismo no
     * este dado de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_cliente
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_cliente( :token, :idCliente, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCliente' => $this->IdCliente,
        ]);
        
        return $query->queryScalar();
    }
    
    /**
     * Permite obtener el pedido del dia de un Cliente desde la base de datos.
     * ssp_dame_pedido_cliente
     */
    public function DamePedido()
    {
        $sql = 'CALL ssp_dame_pedido_cliente( :idCliente )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => $this->IdCliente,
        ]);
        
        return $query->queryOne();
    }
    
    public function DamePedidoFecha($fecha)
    {
        $sql = 'CALL ssp_dame_pedido_cliente_fecha( :idCliente, :fecha )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => $this->IdCliente,
            ':fecha'=> $fecha,
        ]);
        
        return $query->queryOne();
    }
    /**
     * Permite obtener el ultimo pedido de un Cliente desde la base de datos.
     * ssp_dame_pedido_cliente
     */
    public function DameUltimoPedido()
    {
        $sql = 'CALL ssp_dame_ultimo_pedido_cliente( :idCliente )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => $this->IdCliente,
        ]);
        
        return $query->queryOne();
    }
    /**
     * Permite obtener las ventas pendientes de alplicacion de pagos desde la base de datos.
     * ssp_dame_ventas_pendientes_cliente
    */
    public function DameVentasPendientes()
    {
        $sql = 'CALL ssp_dame_ventas_pendientes_cliente( :idCliente )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => $this->IdCliente,
        ]);
        
        return $query->queryAll();
    }

    public function DameUltimaVenta()
    {
        $sql = 'CALL ssp_dame_ultima_venta_cliente( :idCliente )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => $this->IdCliente,
        ]);
        
        return $query->queryOne();
    }
}
