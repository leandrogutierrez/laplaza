<?php
namespace common\models;

use Yii;
use yii\base\Model;

class UsosCaja extends Model
{
    public $IdSucursal;
    public $IdUsuario;
    public $IdCaja;
    public $NroUsoCaja;
    public $FechaInicio;
    public $FechaFin;
    
    //Derivados
    public $Sucursal;
    public $Caja;
    public $Estado;
    
    public function rules()
    {
        return [
            [['IdSucursal', 'IdUsuario', 'IdCaja', 'NroUsoCaja', 'FechaInicio',
            'FechaFin', 'Sucursal', 'Caja','Estado'], 'safe']
        ];
    }
    
    public function Dame()
    {
        $sql = 'CALL ssp_dame_uso_caja( :idCaja, :nroUsoCaja)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCaja' => $this->IdCaja,
            ':nroUsoCaja' => $this->NroUsoCaja,
        ]);
        
        $this->attributes = $query->queryOne();
    }
    
    /**
     * Permite buscar movimientos de caja filtr�ndolos por sucursal, operador, uso
     * de caja, tipo de movimiento, n�mero de movimiento y fecha entre FechaInicio y
     * FechaFin. Ordena por Fecha.
     * ssp_buscar_movimientos_usocaja
     * ssp_buscar_movimientos_usocaja
     *
     * @param FechaInicio
     * @param FechaFin
     * @param TipoMov
     * @param NroMov
     */
    public function BuscarMovimientos($FechaInicio, $FechaFin, $TipoMov, $NroMov)
    {
        $sql = 'CALL ssp_buscar_movimientos_usocaja( :idCaja,'
                . ' :nroUsoCaja, :fechaInicio, :fechaFin, :tipoMov, :nroMov)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCaja' => $this->IdCaja,
            ':nroUsoCaja' => $this->NroUsoCaja,
            ':fechaInicio' => $FechaInicio == '' ? null : date("Y-m-d H:i", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => $FechaFin == '' ? null : date("Y-m-d H:i", strtotime(str_replace('/', '-', $FechaFin))),
            ':tipoMov' => $TipoMov,
            ':nroMov' => ($NroMov == '') ? null : $NroMov,
        ]);
        
        return $query->queryAll();
    }
}
