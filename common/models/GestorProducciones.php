<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 14-Aug-2017 20:59:27
 */
class GestorProducciones extends Model implements iGestor
{
    /**
     * Permite dar de alta una produccion junto con sus l�neas de produccion. La produccion se crea
     * Devuelve OK + el nro de produccion generado o un mensaje de error en Mensaje.
     * ssp_alta_producciones
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_produccion( :token, :idCategoria,'
                . ' :lineas, :fechaProduccion, :observaciones, :IP, :userAgent, :app )';

        $query = Yii::$app->db->createCommand($sql);
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':idCategoria' => $Objeto->IdCategoria,
            ':lineas' => $Objeto->LineasProduccion,
            ':fechaProduccion' => ($Objeto->FechaProduccion !=''? $Objeto->FechaProduccion : null),
            ':observaciones' => $Objeto->Observaciones,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id
        ]);
        
        return $query->queryScalar();
    }
    
    public function Anular($Objeto)
    {
        $sql = 'CALL ssp_anular_produccion( :token, :idProduccion,'
                . ' :IP, :userAgent, :app )'; 

        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProduccion' => $Objeto->IdProduccion,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * Permite borrar una produccion controlando que la misma se encuentre en estado
     * Pendiente.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_produccion
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_produccion( :token, :idProduccion,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProduccion' => $Objeto->IdProduccion,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }

    /**
     * Permite listar producciones filtr�ndolas por vendedor, cliente  y por  fecha de alta entre
     * fehca inicio y fecha fin.
     * Ordena por FechaAlta.
     * ssp_buscar_avanzado_producciones
     *
     */
    public function BuscarAvanzado($IdUsuario, $IdCategoria, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_producciones(:idUsuario, :idCategoria, :fechaInicio,'
            . ' :fechaFin, :estado )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idUsuario' => ($IdUsuario == '') ? null : $IdUsuario,           
            ':idCategoria' => ($IdCategoria == '') ? null : $IdCategoria,
            ':fechaInicio' => ($FechaInicio == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => ($FechaFin == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
        ]);

        return $query->queryAll();
    }

    public function Modificar($Objeto) {
        
    }

}
