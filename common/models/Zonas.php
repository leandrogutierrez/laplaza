<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Zonas extends Model
{
    public $IdZona;
    public $Zona;
    public $Codigo;
    public $Observaciones;
    public $Estado;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';
    
    public function dame()
    {
        $sql = 'CALL ssp_dame_zona( :IdZona )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':IdZona' => $this->IdZona
        ]);
        
        $this->attributes = $query->queryOne();
    }
    
    public function rules()
    {
        return [
          [['Zona'],'required','on' => self::_ALTA],
          [['IdZona','Zona','Codigo'],'required','on' => self::_MODIFICAR],
          [['IdZona','Zona','Codigo','Observaciones','Estado'],'safe']
        ];
    }
    public function Activar()
    {
        $sql = 'CALL ssp_activar_zona( :token, :idZona, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idZona' => $this->IdZona,
        ]);
        
        return $query->queryScalar();
    }
      
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_zona( :token, :idZona, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idZona' => $this->IdZona,
        ]);
        
        return $query->queryScalar();
    }
}
