<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 14-Aug-2017 20:59:27
 */
class Gestorpedidos extends Model implements iGestor
{
    /**
     * Permite dar de alta una pedido junto con sus l�neas de pedido. La pedido se crea
     * Devuelve OK + el nro de pedido generado o un mensaje de error en Mensaje.
     * ssp_alta_pedidos
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_pedido( :token, :idCliente,'
                . ' :lineas, :importe, :fechaEntrega, :observaciones, :IP, :userAgent, :app )';

        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCliente' => $Objeto->IdCliente,
            ':lineas' => $Objeto->LineasPedido,
            ':fechaEntrega' => $Objeto->FechaEntrega,
            ':observaciones' => $Objeto->Observaciones,
            ':importe' => ($Objeto->Importe == '') ? '0.00' : $Objeto->Importe,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_pedido( :token, :idPedido, :idCliente,'
                . ' :lineas, :fechaEntrega, :observaciones, :IP, :userAgent, :app )';

        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idPedido' => $Objeto->IdPedido,
            ':idCliente' => $Objeto->IdCliente,
            ':lineas' => $Objeto->LineasPedido,
            ':fechaEntrega' => $Objeto->FechaEntrega,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }
    
    public function Anular($Objeto)
    {
        $sql = 'CALL ssp_anular_pedido( :token, :idPedido,'
                . ' :IP, :userAgent, :app )';

        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idPedido' => $Objeto->IdPedido,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * Permite borrar una pedido controlando que la misma se encuentre en estado
     * Pendiente.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_pedido
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_pedido( :token, :idPedido,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idPedido' => $Objeto->IdPedido,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }

    /**
     * Permite listar pedidos filtr�ndolas por vendedor, cliente  y por  fecha de alta entre
     * fehca inicio y fecha fin.
     * Ordena por FechaAlta.
     * ssp_buscar_avanzado_pedidos
     *
     */
    public function BuscarAvanzado($IdReparto, $IdCliente, $IdUsuario, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_pedidos(:idCliente, :idUsuario, :fechaInicio,'
            . ' :fechaFin, :estado )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idUsuario' => ($IdUsuario == '') ? null : $IdUsuario,
            ':fechaInicio' => ($FechaInicio == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => ($FechaFin == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
            ':idCliente' => ($IdCliente == '') ? null : $IdCliente,
        ]);

        return $query->queryAll();
    }

    public function DamePedidosReparto($id)
    {
        $sql = 'CALL ssp_dame_pedidos_reparto(:idReparto)';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idReparto' => $id
        ]);

        return $query->queryAll();
    }
}
