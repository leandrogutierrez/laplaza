<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 16-Aug-2017 18:44:34
 */
class Cajas extends Model
{
    public $IdCaja;
    public $IdSucursal;
    public $Caja;
    public $CodigoLlave;
    public $Observaciones;
    public $Estado;
    
    //Derivados
    public $UsoCaja;
    
    const SCENARIO_ALTA = 'alta';
    const SCENARIO_MODIFICAR = 'modificar';
    
    public function attributeLabels()
    {
        return [
            'IdSucursal' => 'Sucursal'
        ];
    }

    public function rules()
    {
        return [
            //Alta
            [['Caja', 'IdSucursal'],'required','on' => self::SCENARIO_ALTA],
            //Modificar
            [['IdCaja','IdSucursal','Caja'],'required','on' => self::SCENARIO_MODIFICAR],
            //Safe
            [['IdCaja','IdSucursal','Caja','CodigoLlave','Estado','Observaciones'],'safe']
        ];
    }
    /**
     * Permite instanciar una caja desde la base de datos.
     * ssp_dame_caja
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_caja( :idCaja )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCaja' => $this->IdCaja
        ]);
        
        $this->attributes = $query->queryOne();
    }
    
    /**
     * Permite cambiar el estado de una caja a Baja, controlando que no se encuentre
     * dada de baja previamente.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_caja
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_dar_baja_caja( :token, :idCaja, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCaja' => $this->IdCaja,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * Permite cambiar el estado de una caja a Activa, controlando que no se encuentre
     * activa previamente.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_caja
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_caja( :token, :idCaja, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCaja' => $this->IdCaja,
        ]);
        
        return $query->queryScalar();
    }
     
    /**
        * Permite devolver el uso de la caja actual. Previamente debe instanciarse el
        * objeto usocaja. Si NroUsoCaja es NULL, entonces no hay caja abierta. Caso
        * contrario es el uso actual.
        * ssp_dame_uso_caja_actual
    */
    public function DameUsoCajaActual()
    {
        $sql = 'CALL ssp_dame_uso_caja_actual(:codigoLlave,:token )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
           ':codigoLlave' => Yii::$app->session->get('codigoLlave'),
            ':token' => Yii::$app->user->identity->Token,
        ]);

        return $query->queryOne();
    }
    
    /**
     * Permite abrir un uso de caja siempre y cuando el usuario est� asociado a la
     * sucursal, el mismo est� activo, y sin usos de caja abiertos por el usuario.
     * Devuelve OK m�s el NroUsoCaja o el mensaje de error en Mensaje.
     * ssp_abre_caja_carniceria
    */
    public function AbrirCaja()
    {
        $sql = 'CALL ssp_abre_caja_sucursal( :token, :codigoLlave, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
           ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':codigoLlave' => Yii::$app->session->get('codigoLlave'),
        ]);
        
        return $query->queryScalar();
    }
      
    /**
     * Permite cerrar un uso de caja siempre y cuando el usuario est� asociado la
     * sucursal, que debe estar activo. Controla adem�s que el uso de caja est�
     * abierto y que el usuario sea el due�o del uso de caja.
     * Devuelve OK o el mensaje de error en Mensaje.
     * ssp_cierra_caja_sucursal
     */
    public function CerrarCaja()
    {
        $sql = 'CALL ssp_cierra_caja_sucursal( :token, :nroUsoCaja, :codigoLlave, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':nroUsoCaja' => $this->UsoCaja->NroUsoCaja,
            ':codigoLlave' => $this->CodigoLlave,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
        ]);
        
        return $query->queryScalar();
    }
    
    /**
     * Permite cerrar un uso de caja. Controla adem�s que el uso de caja est�
     * abierto.
     * Devuelve OK o el mensaje de error en Mensaje.
     * ssp_cierra_caja_carniceria
     */
    public function CerrarCajaAdministracion()
    {
        $sql = 'CALL ssp_cierra_caja_administracion( :token, :nroUsoCaja, :codigoLlave, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':nroUsoCaja' => $this->UsoCaja->NroUsoCaja,
            ':codigoLlave' => $this->CodigoLlave,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
        ]);
        
        return $query->queryScalar();
    }
}
