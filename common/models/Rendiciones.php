<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Rendiciones extends Model
{
    public $IdRendicion;
    public $IdUsuario;
    public $NroRendicion;
    public $FechaAlta;
    public $Importe;
    public $Estado;
    public $Observaciones;
 
    //Derivados
    public $Pagos;
    public $Usuario;
     
    const _ALTA = 'alta';

    public function attributeLabels()
    {
        return [
            'IdUsuario' => 'Usuario',
            'NroRendicion' => 'Nro rendición',
            'FechaAlta' => 'Fecha alta',
        ];
    }
    
    public function rules()
    {
        return [
            [['Importe'], 'required', 'on' => self::_ALTA],
           
            //Safe
            [['IdRendicion','NroRendicion','FechaAlta','Importe',
                'Observaciones','Estado','IdUsuario','Usuario'], 'safe']
        ];
    }
     
    public function Dame()
    {
        $sql = 'CALL ssp_dame_rendicion( :idRendicion)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idRendicion' => $this->IdRendicion
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
