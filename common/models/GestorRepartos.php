<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorRepartos extends Model implements iGestor
{
    /**
     * Permite dar de alta un reparto.
     * Devuelve OK + el id del cliente creado o un mensaje de error en Mensaje.
     * ssp_alta_cliente
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_reparto( :token, :idUsuario, :idVehiculo, :idZona, :fecha,'
                . ':IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idUsuario' => $Objeto->IdUsuario,
            ':idVehiculo' => $Objeto->IdVehiculo,
            ':idZona' => $Objeto->IdZona,
            ':fecha' => $Objeto->FechaReparto,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar un reparto.
     * Devuelve OK o un mensaje de error en Mensaje.
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_reparto(:token, :idReparto, :idUsuario, :idVehiculo, :idZona, :fecha,'
                    .' :IP,:userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => $Objeto->IdReparto,
            ':idUsuario' => $Objeto->IdUsuario,
            ':idVehiculo' => $Objeto->IdVehiculo,
            ':idZona' => $Objeto->IdZona,
            ':fecha' =>$Objeto->FechaReparto
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un reparto
     * Devuelve OK o un mensaje de error en Mensaje.
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_reparto( :token, :idReparto, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => $Objeto->IdReparto,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar repartos filtr�ndolos por una usuario,zona, fechareparto o estado
     * se incluyen o no las bajas en pIncluyeBajas : [S: Si | N: No].
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function BuscarAvanzado($IdUsuario, $IdZona, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_repartos(:idUsuario, :idZona, :fechaInicio,'
            . ' :fechaFin, :estado )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idUsuario' => ($IdUsuario == '') ? null : $IdUsuario,
            ':fechaInicio' => ($FechaInicio == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => ($FechaFin == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
            ':idZona' => ($IdZona == '') ? null : $IdZona,
        ]);

        return $query->queryAll();
    }

    public function Buscar($Cadena, $IncluyeBajas)
    {
        $sql = 'CALL ssp_buscar_repartos( :cadena, :incluyeBajas);';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
}
