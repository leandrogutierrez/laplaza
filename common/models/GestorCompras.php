<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 20-Feb-2017 15:17:12
 */
class GestorCompras extends Model implements iGestor
{
    /**
     * Permite dar de alta una compra junto con sus lineas de compra. La compra se
     * crea inicialmente en estado A : Activa. Luego de ser retirada por un proveedor,
     * pasa al estado R: Retirada.
     * Devuelve OK + el id de la compra creada o un mensaje de error en Mensaje.
     * ssp_alta_compra
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_compra( :token, :idProveedor, :lineasCompra,'
                . ' :importe, :tipo, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProveedor' => $Objeto->IdProveedor == '' ? null : $Objeto->IdProveedor,
            ':lineasCompra' => $Objeto->LineasCompra,
            ':importe' => $Objeto->Importe,
            ':tipo' => $Objeto->Tipo,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
    }

    /**
     * Permite borrar una compra y sus lineas de compra. Solo puede eliminarse una
     * compra que se encuentre en estado A : Activa.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_compra
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_compra( :token, :idCompra, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCompra' => $Objeto->IdCompra,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }


    /**
     * Permite buscar compras filtr�ndolas por fecha de alta entre pFechaInicio y
     * pFechaFin, por Estado = [A : Activa, F: Finalizada, B: Baja, T: Todas] e
     * indicado el tipo de compra en pTipo = [V: Animales vivos | M: Medias reses | O:
     * Otras | T: Todas | F: Faenables].
     * Ordena por FechaAlta.
     * ssp_buscar_avanzado_compras
     *
     * @param FechaInicio
     * @param FechaFin
     * @param Estado
     * @param Tipo
     */
    public function BuscarAvanzado($FechaInicio = null, $FechaFin = null, $Estado = 'T', $Tipo = 'T')
    {
        $sql = 'CALL ssp_buscar_avanzado_compras( :fechaInicio, :fechaFin,'
                . ' :estado, :tipo)';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':fechaInicio' => $FechaInicio == '' ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => $FechaFin == '' ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
            ':tipo' => $Tipo,
        ]);

        return $query->queryAll();
    }
}
