<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 02-Jun-2017 09:19:02
 */
class Compras extends Model
{
    public $IdCompra;
    public $IdProveedor;
    public $NroCompra;
    public $FechaAlta;
    public $Tipo;
    public $Importe;
    public $Estado;
    public $IdUsuario;
    

    //Derivados
    public $Usuario;
    public $LineasCompra;
    public $Proveedor;
    public $IdConceptoCompra;

    public function attributeLabels()
    {
        return[
            'IdProveedor' => 'Proveedor',
            'FechaAlta' => 'Fecha de alta',
            'Tipo' => 'Tipo de compra',
            'IdConceptoCompra' => 'Concepto de compra',
            'NroCompra' => 'Nº compra',
            'IdUsuario' => 'Usuario'
        ];
    }

    public function rules()
    {
        return [
            //Safe
            [['IdCompra', 'IdProveedor', 'NroCompra', 'FechaAlta',
            'Importe', 'Estado', 'LineasCompra', 'Proveedor', 'Tipo',
            'IdConceptoCompra','IdUsuario','Usuario'], 'safe']
        ];
    }

    /**
     * Permite instanciar una compra y sus lineas de compra desde la base de datos.
     * ssp_dame_compra
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_compra( :idCompra )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCompra' => $this->IdCompra,
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
