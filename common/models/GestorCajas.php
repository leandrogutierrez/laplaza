<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 16-Aug-2017 18:38:17
 */
class GestorCajas extends Model implements iGestor
{
    /**
     * Permite dar de alta una Caja controlando que el nombre no exista ya.
     * Devuelve OK + el id de la caja creada o un mensaje en Mensaje.
     * ssp_alta_caja
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_caja( :token, :idSucursal, :caja, :codigoLlave,'
                . ' :observaciones, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idSucursal' => $Objeto->IdSucursal,
            ':caja' => $Objeto->Caja,
            ':codigoLlave' => $Objeto->CodigoLlave,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar una caja, controlando que el nombre no se encuenre en uso.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_caja
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
        $sql ='CALL ssp_modificar_caja( :token, :idCaja, :caja, :observaciones,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCaja' => $Objeto->IdCaja,
            ':caja' => $Objeto->Caja,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar una Caja controlando que no existan Usos de Caja ni Comprobantes
     * de Caja asociados.
     * Devuelve OK o un mensaje en Mensaje.
     * ssp_borrar_caja
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_caja( :token, :idCaja, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCaja' => $Objeto->IdCaja,
        ]);
        
        return $query->queryScalar();
    }

    /**
    * @param Cadena
    * @param IncluyeBajas
    */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }
    
    /**
     * Permite listar las cajas con estado != 'B' de una sucursal
     * ssp_borrar_caja
     *
     * @param Objeto
     */
    public function EstadoCajas()
    {
        $sql = 'CALL ssp_estado_cajas_sucursal(NULL)';

        $query = Yii::$app->db->createCommand($sql);

        return $query->queryAll();
    }
}
