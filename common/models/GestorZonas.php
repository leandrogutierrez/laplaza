<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:28:22
 */
class GestorZonas extends Model implements iGestor
{
    /*
     * public $IdZona;
        public $Zona;
        public $Observaciones;
        public $Estado;
    
     *
     *  */
    /**
     * Permite dar de alta una Zona
     * Devuelve OK + el id de la zona creado o un mensaje de error en Mensaje.
     * ssp_alta_zona
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_zona( :token, :zona, :codigo, :observaciones,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':zona' => $Objeto->Zona,
            ':codigo' => $Objeto->Codigo,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar una zona
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_zona
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_zona( :token, :idZona, :zona, :codigo, :observaciones,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idZona' => $Objeto->IdZona,
            ':zona' => $Objeto->Zona,
            ':codigo' => $Objeto->Codigo,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar una zona controlando que el mismo no haya sido parte de un reparto
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_zona
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_zona( :token, :idZona, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idZona' => $Objeto->IdZona,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar zonas filtr�ndolas por una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas [S|N].
     * Ordena por Descripcion.
     * ssp_buscar_zonas
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_zonas( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
}
