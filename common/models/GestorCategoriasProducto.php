<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorCategoriasProducto extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_categoria( :token, :categoria, :descripcion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':categoria' => $Objeto->Categoria,
            ':descripcion' => $Objeto->Descripcion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_categoria( :token, :idCategoria, :categoria, :descripcion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCategoria' => $Objeto->IdCategoria,
            ':categoria' => $Objeto->categoria,
            ':descripcion' => $Objeto->Descripcion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_categoria( :token, :idCategoria, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCategoria' => $Objeto->IdCategoria,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_categorias( :cadena, :incluyeBajas )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
        
    public function ListarCategoriasPadre()
    {
        $sql = 'CALL ssp_listar_categorias_padre()';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
        ]);
        
        return $query->queryAll();
    }
    
    public function ListarCategoriasHijo()
    {
        $sql = 'CALL ssp_listar_categorias_hijo()';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
        ]);
        
        return $query->queryAll();
    }
}
