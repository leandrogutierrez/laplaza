<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 03-Jul-2017 14:43:44
 */
class GestorConceptosCompra extends Model implements iGestor
{

    /**
     * Permite dar de alta un concepto de compra, controlando que el nombre no se
     * encuentre en uso.
     * Devuelve OK + el id del concepto de compra creado o un mensaje de error en
     * Mensaje.
     * ssp_alta_conceptocompra
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_conceptocompra( :conceptoCompra )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':conceptoCompra' => $Objeto->ConceptoCompra
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar un concepto de compra controlando que el nombre no se
     * encuentre en uso ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_conceptocompra
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_conceptocompra( :idConceptoCompra, :conceptoCompra )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idConceptoCompra' => $Objeto->IdConceptoCompra,
            ':conceptoCompra' => $Objeto->ConceptoCompra
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar un concepto de compra controlando que no tenga detalle de
     * conceptos de compra asocidos.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_conceptocompra
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_conceptocompra( :idConceptoCompra )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idConceptoCompra' => $Objeto->IdConceptoCompra,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar conceptos de compra filtr�ndolos por una cadena de b�squeda.
     * Ordena por ConceptoCompra.
     * ssp_buscar_conceptoscompra
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_conceptoscompra( :cadena )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
        ]);
        
        return $query->queryAll();
    }
}
