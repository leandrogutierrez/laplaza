<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:57
 */
class Ventas extends Model
{
    public $IdVenta;
    public $IdReparto;
    public $IdUsuario;
    public $NroVenta;
    public $IdCliente;
    public $FechaAlta;
    public $Importe;
    public $Estado;
    public $EdicionPrecios;
    
    //Derivados
    public $LineasVenta;
    public $Cliente;
    public $Usuario;
    public $Reparto;
    
    public function attributeLabels()
    {
        return [
            'IdCliente' => 'Cliente',
            'IdUsuario' => 'Vendedor',
            'IdReparto' => 'Reparto'
        ];
    }
    
    public function rules()
    {
        return [
            //Safe
            [['IdVenta', 'IdCliente', 'IdReparto', 'IdUsuario', 'FechaAlta', 'Importe', 'Estado',
            'NroVenta', 'LineasVenta', 'Cliente','Usuario','EdicionPrecios'], 'safe']
        ];
    }
    
    /**
     * Permite instanciar una venta desde la base de datos
     * ssp_dame_venta
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_venta( :idVenta )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idVenta' => $this->IdVenta
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
