<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Arribos extends Model
{
    public $IdArribo;
    public $IdReparto;
    public $IdUsuario;
    public $FechaArribo;
    public $FechaAlta;
    public $Observaciones;
    public $Estado;
    
    //Derivados
    public $LineasArribo;
    public $Usuario;
    public $Reparto;

    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    public function attributeLabels()
    {
        return[
            'IdReparto' => 'Reparto',
            'IdUsuario' => 'Recepción',
            'FechaArribo' => 'Fecha de arribo'
        ];
    }
    
    public function rules()
    {
        return [
            //Alta
            [['IdReparto','FechaArribo'], 'required', 'on' => self::_ALTA],
            //Safe
            [['IdArribo', 'IdReparto', 'IdUsuario','FechaArribo', 'FechaAlta', 'Observaciones',
            'LineasArribo', 'Usuario', 'Estado', 'Reparto'],'safe']
        ];
    }
    
    public function Dame()
    {
        $sql = 'CALL ssp_dame_despacho( :idDespacho)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idDespacho' => $this->IdDespacho,
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
