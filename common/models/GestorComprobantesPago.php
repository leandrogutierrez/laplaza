<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorComprobantesPago extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_comprobante_pago( :token, :idCliente, :importe, :pObservaciones,'
                . ':IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idCliente' => $Objeto->IdCliente,
            ':importe' => ($Objeto->Importe == '') ? null : $Objeto->Importe,
            ':pObservaciones' =>($Objeto->Observaciones == '') ? null : $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
    }
 
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_pago( :token, :idVenta,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVenta' => $Objeto->IdVenta,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * NO SE USA
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
    }
 
    public function BuscarAvanzado($IdUsuario, $IdCliente, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_comprobantes_pago( :idUsuario, :idCliente, :fechaInicio,'
            . ' :fechaFin, :estado )';

        $query = Yii::$app->db->createCommand($sql);

        $query->bindValues([
            ':idCliente' => ($IdCliente == '') ? null : $IdCliente,
            ':idUsuario' => ($IdUsuario == '') ? null : $IdUsuario,
            ':fechaInicio' => ($FechaInicio == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => ($FechaFin == '') ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':estado' => $Estado,
        ]);

        return $query->queryAll();
    }
    
    public function DamePagosCliente($IdCliente)
    {
        $sql = 'CALL ssp_dame_pagos_reparto(:idCliente)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idCliente' => ($IdCliente == 0) ? null : $IdCliente,
        ]);
        
        return $query->queryAll();
    }
    
    public function DamePagosUsuario($id)
    {
        $sql = 'CALL ssp_dame_pagos_usuario( :id )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':id' => $id,
        ]);
        
        return $query->queryAll();
    }
  
    public function Anular($Objeto)
    {
        $sql = 'CALL ssp_anular_comprobante_pago( :token, :idComprobante,'
                . ' :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idComprobante' => $Objeto->IdComprobantePago,
        ]);
        
        return $query->queryScalar();
    }
}
