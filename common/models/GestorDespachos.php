<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorDespachos extends Model implements iGestor
{
    /**
     * Permite dar de alta un despacho.
     * Devuelve OK + el id del despacho creado o un mensaje de error en Mensaje.
     * ssp_alta_despacho
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_despacho( :token, :idReparto, :idPedido, :lineasDespacho, :observaciones,'
                . ':IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idPedido' => ($Objeto->IdPedido == '' ? null : $Objeto->IdPedido),
            ':idReparto' => $Objeto->IdReparto,
            ':lineasDespacho' => $Objeto->LineasDespacho,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }
    /**
    * Permite dar de alta un despacho masivo.
    * Devuelve OK + el id del despacho creado o un mensaje de error en Mensaje.
    * ssp_alta_despacho
    */
    public function AltaDespachoMasivo($Objeto)
    {
        $sql = 'CALL ssp_alta_despacho_masivo( :token, :idReparto, :pedidos, :lineasDespacho,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => $Objeto->IdReparto,
            ':pedidos' => $Objeto->Pedidos ,
            ':lineasDespacho' => $Objeto->LineasDespacho ,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modi3ficar un despacho.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_despacho
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_borrar_despacho( :token, :idDespacho, :idReparto, '
                . ':idPedido, :lineasDespacho, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idDespacho' => $Objeto->IdDespacho,
            ':idReparto' => $Objeto->IdReparto,
            ':idPedido' => $Objeto->IdPedido,
            ':lineasPedido' => $Objeto->LineasDespacho,
        ]);
        return $query->queryScalar();
    }
    /**
     * Permite borrar un despacho controlando que no existan ventas ni comprobantes de
     * pago asociados.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_despacho
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_despacho( :token, :IdDespacho, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idDespacho' => $Objeto->IdDespacho,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * Permite buscar despachos filtr�ndolos por una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas : [S: Si | N: No].
     * Ordena por Apellido/s,Nombre/s.
     * ssp_buscar_despachos
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_despachos( :cadena, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
    
    public function BuscarAvanzado($IdReparto, $IdDespachante, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_despachos( :idReparto, :idDespachante, :fechaInicio, :fechaFin, :estado )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':fechaInicio' => $FechaInicio == '' ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => $FechaFin == '' ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':idReparto' => ($IdReparto == 0) ? null : $IdReparto,
            ':idDespachante' => ($IdDespachante == 0) ? null : $IdDespachante,
            ':estado' => $Estado,
        ]);
        
        return $query->queryAll();
    }
    
    public function DameDespachosReparto($IdReparto = null)
    {
        $sql = 'CALL ssp_dame_despachos_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($IdReparto == 0) ? null : $IdReparto,
        ]);
        
        return $query->queryAll();
    }
    
    public function Anular ($IdDespacho = null)
    {
//        (pToken CHAR(32), pIdDespacho INT, 
//		pIP VARCHAR(40), pUserAgent VARCHAR(255), pApp VARCHAR(50))
        $sql = 'CALL ssp_anular_despacho ( :token, :idDespacho, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':idDespacho' => $IdDespacho,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id
        ]);
        
        return $query->queryScalar();
    }        
}
