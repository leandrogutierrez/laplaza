<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:57
 */
class Producciones extends Model
{
    public $IdProduccion;
    public $IdUsuario;
    public $NroProduccion;
    public $IdCategoria;
    public $FechaProduccion;
    public $FechaAlta;
    public $Estado;
    public $Observaciones;
    
    //Derivados
    public $LineasProduccion;
    public $Usuario;
    public $Categoria;
    
    const SCENARIO_ALTA = 'alta';
    const SCENARIO_BORRA = 'borra';
    
    public function attributeLabels()
    {
        return [
            'IdUsuario' => 'Vendedor',
            'IdCategoria' => 'Categoria',
            'NroProduccion' => 'Nro producción',
            'FechaProduccion' => 'Fecha de producción',
            'FechaAlta' => 'Fecha de alta'
        ];
    }
    
    public function rules()
    {
        return [
            //Safe
            [$this->attributes(), 'safe'],
            [['IdCategoria','FechaProduccion'], 'required', 'on' => self::SCENARIO_ALTA]
        ];
    }
    
    /**
     * Permite instanciar una venta desde la base de datos
     * ssp_dame_venta
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_produccion( :idProduccion )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idProduccion' => $this->IdProduccion
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
