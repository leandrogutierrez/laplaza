<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorArribos extends Model implements iGestor
{
    /**
     * Permite dar de alta un arribo.
     * Devuelve OK + el id del arribo creado o un mensaje de error en Mensaje.
     * ssp_alta_arribo
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_arribo( :token, :idReparto, :fechaArribo, :lineasArribo, :observaciones,'
                . ':IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => $Objeto->IdReparto,
            ':lineasArribo' => $Objeto->LineasArribo,
            ':fechaArribo' => $Objeto->FechaArribo,
            ':observaciones' => $Objeto->Observaciones,
        ]);
        
        return $query->queryScalar();
    }
    
    /**
     * Permite borrar un arribo controlando que no existan ventas ni comprobantes de
     * pago asociados.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_arribo
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_arribo( :token, :idArribo, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idArribo' => $Objeto->IdArribo,
        ]);
        
        return $query->queryScalar();
    }
    /**
     * Permite buscar arribos filtr�ndolos por una cadena de b�squeda e indicando si
     * se incluyen o no las bajas en pIncluyeBajas : [S: Si | N: No].
     * ssp_buscar_arribos
     *
     * @param Cadena
     * @param IncluyeBajas
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_arribos( :cadena, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
    
    public function BuscarAvanzado($IdReparto, $IdUsuario, $FechaInicio, $FechaFin, $Estado)
    {
        $sql = 'CALL ssp_buscar_avanzado_arribos( :idReparto, :idUsuario, :fechaInicio, :fechaFin, :estado )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':fechaInicio' => $FechaInicio == '' ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaInicio))),
            ':fechaFin' => $FechaFin == '' ? null : date("Y-m-d", strtotime(str_replace('/', '-', $FechaFin))),
            ':idReparto' => ($IdReparto == 0) ? null : $IdReparto,
            ':idUsuario' => ($IdUsuario == 0) ? null : $IdUsuario,
            ':estado' => $Estado,
        ]);
        
        return $query->queryAll();
    }

    public function Modificar($Objeto)
    {
    }
  
    public function DameArriboReparto($IdReparto = null)
    {
        $sql = 'CALL ssp_dame_arribo_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($IdReparto == 0) ? null : $IdReparto,
        ]);
         
        return $query->queryOne();
    }
    
    public function Anular ($IdArribo = null)
    { 
        $sql = 'CALL ssp_anular_arribo ( :token, :idArribo, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':idArribo' => $IdArribo,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id
        ]);
        
        return $query->queryScalar();
    }  
}
