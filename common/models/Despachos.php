<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Despachos extends Model
{
    public $IdDespacho;
    public $IdPedido;
    public $IdReparto;
    public $IdDespachante;
    public $NroDespacho;
    public $FechaAlta;
    public $Observaciones;
    public $Estado;
    
    //Derivados
    public $LineasDespacho;
    public $IdVendedor;
    public $Vendedor;
    public $Despachante;
    public $NroPedido;
    public $FechaReparto;
    public $Reparto;
    public $Pedidos;

    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    public function attributeLabels()
    {
        return[
            'IdReparto' => 'Reparto',
            'IdDespachante' => 'Despachante'
        ];
    }
    
    public function rules()
    {
        return [
            //Alta
            [['IdReparto'], 'required', 'on' => self::_ALTA],
            //Modificar
            [['IdDespacho', 'IdReparto', 'IdPedido'],
            'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdDespacho', 'IdReparto', 'IdPedido', 'IdDespachante','NroDespacho','FechaAlta', 'Observaciones',
            'LineasDespacho',  'IdVendedor', 'Vendedor', 'Despachante', 'NroPedido', 'Estado','FechaReparto', 'Reparto','Pedidos'],
            'safe']
            
        ];
    }
    
    public function Dame()
    {
        $sql = 'CALL ssp_dame_despacho( :idDespacho)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idDespacho' => $this->IdDespacho,
        ]);
        
        $this->attributes = $query->queryOne();
    }
}
