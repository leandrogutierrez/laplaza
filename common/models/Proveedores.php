<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * @version 1.0
 * @created 16-Feb-2017 20:36:56
 */
class Proveedores extends Model
{
    public $IdProveedor;
    public $Proveedor;
    public $Cuit;
    public $Estado;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    public function rules()
    {
        return [
            //Cuit
            ['Cuit', 'string', 'length' => 11],
            //Alta
            ['Proveedor', 'required', 'on' => self::_ALTA],
            //Modificar
            [['IdProveedor', 'Proveedor'], 'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdProveedor', 'Proveedor', 'Cuit', 'Estado'], 'safe']
        ];
    }
    /**
     * Permite instanciar un Proveedor desde la base de datos.
     * ssp_dame_proveedor
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_proveedor( :idProveedor )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idProveedor' => $this->IdProveedor,
        ]);
        
        $this->attributes = $query->queryOne();
    }

    /**
     * Permite cambiar el estado de un proveedor a Activo, controlando que el mismo no
     * se encuentre activo ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_activar_proveedor
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_proveedor( :token, :idProveedor, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProveedor' => $this->IdProveedor
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de un proveedor a Baja, controlando que el mismo no
     * se encuentre dado de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_darbaja_proveedor
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_proveedor( :token, :idProveedor, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idProveedor' => $this->IdProveedor
        ]);
        
        return $query->queryScalar();
    }
}
