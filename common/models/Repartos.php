<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Repartos extends Model
{
    public $IdReparto;
    public $IdUsuario;
    public $IdVehiculo;
    public $IdZona;
    public $FechaReparto;
    public $FechaAlta;
    public $Estado;
            
    //Derivados
    public $Usuario;
    public $Vehiculo;
    public $Zona;
    
    const _ALTA = 'alta';
    const _MODIFICAR = 'modificar';

    public function attributeLabels()
    {
        return[
            'IdUsuario' => 'Vendedor',
            'IdZona' => 'Zona',
            'IdVehiculo' => 'Vehículo',
            'Fecha' => 'Fecha',
            'Usuarii' => 'Vendedor'
        ];
    }
    
    public function rules()
    {
        return [
            //Alta
            [['IdUsuario', 'IdVehiculo', 'IdZona', 'FechaReparto'],'required', 'on' => self::_ALTA],
            //Modificar
            [['IdReparto','IdUsuario', 'IdVehiculo', 'IdZona', 'FechaReparto'], 'required', 'on' => self::_MODIFICAR],
            //Safe
            [['IdReparto','IdUsuario', 'IdVehiculo', 'IdZona', 'FechaReparto', 'FechaAlta','Estado',
                'Usuario', 'Vehiculo', 'Zona'], 'safe']
        ];
    }
    

    /**
     * Permite instanciar un reparto desde la base de datos.
     * ssp_dame_cliente
     */
    public function Dame()
    {
        $sql = 'CALL ssp_dame_reparto( :idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => $this->IdReparto,
        ]);
        
        $this->attributes = $query->queryOne();
    }

    /**
     * Permite cambiar el estado de un reaprto a Activo, controlando que el mismo no
     * este activo ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     */
    public function Activar()
    {
        $sql = 'CALL ssp_activar_reparto( :token, :idReparto, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => $this->IdReparto,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite cambiar el estado de un reparto a Baja, controlando que el mismo no
     * este dado de baja ya.
     * Devuelve OK o un mensaje de error en Mensaje.
     */
    public function DarBaja()
    {
        $sql = 'CALL ssp_darbaja_reparto( :token, :idReparto, :IP, :userAgent, :app )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idReparto' => $this->IdReparto,
        ]);
        
        return $query->queryScalar();
    }
    
    public function DamePosibleArribo()
    {
        $sql = 'CALL ssp_dame_posible_arribo_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
            
        $query->bindValues([
            ':idReparto' => $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DameLineasPedidosReparto()
    {
        $sql = 'CALL ssp_dame_lineas_pedidos_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DamePedidosReparto()
    {
        $sql = 'CALL ssp_dame_pedidos_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DamePlanillaPedido()
    {
        $sql = 'CALL ssp_planilla_reparto(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DamePlanillaPuntual()
    {
        $sql = 'CALL ssp_planilla_puntual(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DamePlanillaSalados()
    {
        $sql = 'CALL ssp_planilla_salados(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }
    
    public function DamePlanillaDulces()
    {
        $sql = 'CALL ssp_planilla_dulces(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }
    
    public function DamePlanillaCobranzas()
    {
        $sql = 'CALL ssp_planilla_cobros(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DamePlanillaVentas()
    {
        $sql = 'CALL ssp_planilla_ventas(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }
    
    public function DamePlanillaVentasPrecios()
    {
        $sql = 'CALL ssp_planilla_ventas_precios(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

    public function DamePlanillaPrecios() {
        $sql = 'CALL ssp_planilla_variacion(:idReparto)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':idReparto' => ($this->IdReparto == 0) ? null : $this->IdReparto,
        ]);
        
        return $query->queryAll();
    }

}
