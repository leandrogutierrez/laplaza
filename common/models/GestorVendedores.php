<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorVendedores extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_vendedor( :token, :nombres,'
                . ' :apellidos, :telefono, :cuil, :idUsuario,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':nombres' => $Objeto->Nombres,
            ':apellidos' => $Objeto->Apellidos,
            ':telefono' => $Objeto->Telefono,
            ':cuil' => $Objeto->Cuil,
            ':idUsuario' => $Objeto->IdUsuario == '' ? null : $Objeto->IdUsuario,
        ]);
        
        return $query->queryScalar();
    }

    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_vendedor( :token, :idVendedor, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVendedor' => $Objeto->IdVendedor,
        ]);
        
        return $query->queryScalar();
    }

    public function Buscar($Cadena, $IncluyeBajas)
    {
        $sql = 'CALL ssp_buscar_vendedores( :cadena, :incluyeBajas)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }

    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_vendedor( :token, :idVendedor, :nombres,'
                . ' :apellidos, :telefono, '
                . ' :cuil, :idUsuario, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idVendedor' => $Objeto->IdVendedor,
            ':nombres' => $Objeto->Nombres,
            ':apellidos' => $Objeto->Apellidos,
            ':telefono' => $Objeto->Telefono,
            ':cuil' => $Objeto->Cuil,
            ':idUsuario' => $Objeto->IdUsuario == '' ? null : $Objeto->IdUsuario,
        ]);
        
        return $query->queryScalar();
    }
}
