<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

/**
 * @version 1.0
 * @created 01-Jun-2017 11:50:51
 */
class GestorMateriasPrima extends Model implements iGestor
{

    /**
     * Permite dar de alta una materia prima controlando que el nombre no exista ya.
     * Devuelve OK + el id de la materia prima creada o un mensaje de error en Mensaje.
     *
     * ssp_alta_materiaprima
     *
     * @param Objeto
     */
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_materiaprima( :token, :materiaPrima, :tipo,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':materiaPrima' => $Objeto->MateriaPrima,
            ':tipo' => $Objeto->Tipo,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite modificar una materia prima, controlando que el nombre no se encuentre
     * en uso.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_modificar_materiaprima
     *
     * @param Objeto
     */
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_materiaprima( :token, :idMateriaPrima,'
                . ' :materiaPrima, :tipo,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idMateriaPrima' => $Objeto->IdMateriaPrima,
            ':materiaPrima' => $Objeto->MateriaPrima,
            ':tipo' => $Objeto->Tipo,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite borrar una materia prima controlando que no existan rendimientos,
     * lineas de compra ni stock de carniceria asociados.
     * Devuelve OK o un mensaje de error en Mensaje.
     * ssp_borrar_materiaprima
     *
     * @param Objeto
     */
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_materiaprima( :token, :idMateriaPrima,'
                . ' :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idMateriaPrima' => $Objeto->IdMateriaPrima,
        ]);
        
        return $query->queryScalar();
    }

    /**
     * Permite buscar materias prima filtr�ndolas por una cadena de b�squeda,
     * indicando si se incluyen o no las dadas de baja en pIncluyeBajas [S|N] e
     * indicando si debe filtrarse por tipo de materia prima en pTipo [V : Animal vivo
     * | M: Media res | O: Otros | T: Todos].
     * Ordena por MateriaPrima.
     * ssp_buscar_materiasprima
     *
     * @param Cadena
     * @param IncluyeBajas
     * @param Tipo
     */
    public function Buscar($Cadena = '', $IncluyeBajas = 'N', $Tipo = 'T')
    {
        $sql = 'CALL ssp_buscar_materiasprima( :cadena, :incluyeBajas, :tipo )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
            ':tipo' => $Tipo
        ]);
        
        return $query->queryAll();
    }
}
