<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\iGestor;

class GestorNotificaciones extends Model implements iGestor
{
    public function Alta($Objeto)
    {
        $sql = 'CALL ssp_alta_notificacion( :token, :notificacion, :descripcion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':notificacion' => $Objeto->Notificacion,
            ':descripcion' => $Objeto->Descripcion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Modificar($Objeto)
    {
        $sql = 'CALL ssp_modificar_notificacion( :token, :idNotificacion, :notificacion, :descripcion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idNotificacion' => $Objeto->IdNotificacion,
            ':notificacion' => $Objeto->Notificacion,
            ':descripcion' => $Objeto->Descripcion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Borrar($Objeto)
    {
        $sql = 'CALL ssp_borrar_notificacion( :token, :idNotificacion, :IP, :userAgent, :app)';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':token' => Yii::$app->user->identity->Token,
            ':IP' => Yii::$app->request->userIP,
            ':userAgent' => Yii::$app->request->userAgent,
            ':app' => Yii::$app->id,
            ':idNotificacion' => $Objeto->IdNotificacion,
        ]);
        
        return $query->queryScalar();
    }
 
    public function Buscar($Cadena = '', $IncluyeBajas = 'N')
    {
        $sql = 'CALL ssp_buscar_notificaciones( )';
        
        $query = Yii::$app->db->createCommand($sql);
        
        $query->bindValues([
            ':cadena' => $Cadena,
            ':incluyeBajas' => $IncluyeBajas,
        ]);
        
        return $query->queryAll();
    }
}
