#!/usr/bin/env node

var http = require('http');
var url = require("url");
var md5 = require("md5");
var nconf = require('nconf');

var pjson = require('./package.json');

var server = http.createServer(function (request, response) {
    var pathname = url.parse(request.url).pathname;
    switch (pathname) {
        case '/key':
            key(request, response);
            break;
        case '/reset-key':
            resetKey(request, response);
            break;
        case '/version':
            version(request, response);
            break;
        default:
            response.writeHead(404, {
                'Access-Control-Allow-Origin': '*'
            });
            response.end();
    }
});

// Retorna el código llave de la caja
function key(request, response) {
    nconf.use('file', {file: './config.json'});
    if (nconf.get('key'))
    {
        console.log('[' + new Date().toLocaleString() + '] Consultada la llave de la caja.');
        response.writeHead(200, {
            "Content-Type": "application/json; charset=utf-8",
            'Access-Control-Allow-Origin': '*'
        });
        response.write(JSON.stringify({
            'key': nconf.get('key'),
            'version': pjson.version
        }));
        response.end();
    }
    else
    {
        console.log('[' + new Date().toLocaleString() + '] El archivo con el código de la caja no existe. Se crea uno nuevo.');
        nconf.set('key', md5(new Date().valueOf()));
        nconf.save(function (err) {
            if (err)
                console.log(err);
            key(request, response);
        });
    }
}

function resetKey(request, response) {
    nconf.use('file', {file: './config.json'});
    console.log('[' + new Date().toLocaleString() + '] Generado nuevo código de caja.');
    nconf.set('key', md5(new Date().valueOf()));
    nconf.save(function (err) {
        if (err)
            console.log(err);

        key(request, response);
    });
}

function version(request, response) {
    console.log('[' + new Date().toLocaleString() + '] Consultada la version del driver.');
    response.write(pjson.version);
    response.end();
}

// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(8000);

// Put a friendly message on the terminal
console.log('[' + new Date().toLocaleString() + '] Server running at http://127.0.0.1:8000/');
