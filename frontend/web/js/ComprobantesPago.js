'use strict';

var ComprobantesPago = {};

ComprobantesPago.AltaPago = {
    vue : null,
    init : function(){
        this.vue = new Vue({
            el : "#comprobantespago-form",
            data : {
                idCliente : "",
                cliente : {},
                importe : "",
                arrayVentas : [],
            },
            watch : {
                idCliente : function(val){
                    var _this = this;
                    
                                    console.log(result);
                    if(parseInt(val) > 0){
                        $.get('/clientes/dame-cliente',{id:val})
                                .done(function(result){
                                    console.log(result); 
                                    _this.cliente = result;
                        }).fail(function(){
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contáctese con el administrador.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores-modal').html(html);
                                });
                        $.get('/clientes/dame-ventas-pendientes',{id:val})
                                .done(function(result){ 
                                    console.log(result);
                                    _this.arrayVentas = result;
                        }).fail(function(){
                                    var tipo = 'danger';
                                    var mensaje = 'Error en la comunicación con el servidor. Contáctese con el administrador.';
                                    var html = '<div class="alert alert-'+ tipo + ' alert-dismissable">' + 
                                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+ 
                                                    '<strong>' + mensaje + '</strong>' + 
                                                    '</div>';
                                    $('#errores-modal').html(html);
                        });
                    }
                    
                }
            }
        });
    }
}