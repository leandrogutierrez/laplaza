<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

$sucursal = Yii::$app->session->get('UsoCaja')['Sucursal'];
$caja = Yii::$app->session->get('UsoCaja')['Caja'];
AppAsset::register($this);
?>
<?php $this->beginPage() ?> 
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?= $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::$app->request->baseUrl.'/favicon.ico']);?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Punto de venta - '.$sucursal.'  ('.$caja.')',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Informes', 'url' => ['/informes/index']],
        ['label' => 'Clientes', 'url' => ['/clientes/index']],
        ['label' => 'Pedidos', 'url' => ['/pedidos/index']],
        ['label' => 'Repartos', 'url' => ['/repartos/index']],
        ['label' => 'Ventas', 'url' => ['/ventas/index']],
        ['label' => 'Pagos', 'url' => ['/comprobantes-pago/index']],
        ['label' => 'Caja', 'url' => ['/cajas/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/usuarios/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/usuarios/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->Usuario . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; North Goal <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
