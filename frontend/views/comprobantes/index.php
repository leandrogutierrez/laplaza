 <?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\LinkPager;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Comprobantes de pago';

$this->params['breadcrumbs'][] = $this->title;
$estados = [
    'T' => 'Todas',
    'P' => 'Pendiente de aplicación',
    'F' => 'Aplicado completamente',
    'A' => 'Anulado',
];
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php if (in_array('AltaComprobantePago', Yii::$app->session->get('Permisos'))) : ?>
                    <button class="btn btn-primary pull-right" type="button"
                            data-modal="<?= Url::to(['comprobantes-pago/alta']) ?>">
                        <i class="fa fa-plus"></i> Nuevo pago
                    </button>
                    <?php endif; ?>
                </div>
                <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'busquedacomprobantes-form',
                                                        'method' => 'get',
                                                        'layout' => 'inline']) ?>
                    <div class="col-md-4">  
                    <?php 
                        $urlClientes = Url::to(['clientes/autocompletar']);
                        echo $form->field($busqueda, 'Combo3')->widget(Select2::className(), [
                                'data' => $clientes,
                                'options' => [
                                  'placeholder' => 'Buscar cliente...',
                                    'v-select2' => 'idCliente',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                    
                                ]
                            ]);
                    ?> 
                    <p></p>
                    <?= $form->field($busqueda, 'Combo2')->dropDownList(
                        ArrayHelper::map($usuarios, 'IdUsuario', 'NombreCompleto'),
                                                        [ 'prompt' => 'Seleccione vendedor...',  'style'=>'width: 300px']
                        )
                    ?>   
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                            'attributeTo' => 'FechaFin',
                            'labelTo' => 'al',
                            'form' => $form,
                            'language' => 'es',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd/mm/yyyy',
                                'endDate' => 'd',
                            ]
                        ])?>
                        <p></p>
                        <?= $form->field($busqueda, 'Combo')->dropDownList($estados) ?>
                    </div>
                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarcompras-button']) ?> 

                    <?php ActiveForm::end() ?>
                </div>
                <p></p>
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover">
                    <thead>
                        <tr class="tabla-header">
                            <th>Fecha Alta</th> 
                            <th>Nº comprobante</th>
                            <th>Cliente</th> 
                            <th>Importe</th>
                            <th>Estado</th> 
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr>
                            <td><?= Html::encode($model['FechaAlta']) ?></td> 
                            <td><?= Html::encode($model['NroComprobantePago']) ?></td>
                            <td><?= Html::encode($model['Cliente']) ?></td>
                            <td>$ <?= Html::encode($model['Importe']) ?></td>
                            <td><?= $estados[$model['Estado']] ?></td>
                            <td>
                                <?php if ($model['Estado'] != 'A'): ?>
                                    <div class="btn-group">  
                                        <?php if (in_array('AnularComprobantePago', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['comprobantes-pago/anular', 'id' => $model['IdComprobantePago']]) ?>"
                                                data-mensaje="¿Desea anular el comprobante?">
                                            <i class="fa fa-ban"></i>
                                        </button>
                                        <?php endif; ?> 
                                    </div>
                                <?php endif; ?> 
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran comprobantes en el sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

