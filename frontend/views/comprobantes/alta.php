<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\money\MaskMoney;

common\assets\ComprobantesPagoAsset::register($this);
$this->registerJs('ComprobantesPago.Alta.init()');

?>
<div class="modal-dialog  ">
     <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Nuevo Comprobante de Pago</h4>
        </div>
        <div id="errores-modal"> </div>
        <?php $form = ActiveForm::begin(['id' => 'comprobantespago-form',]) ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-8">
                <?php 
                    $url = Url::to(['clientes/autocompletar']);
                    echo $form->field($model, 'IdCliente')->widget(Select2::className(), [
                        'initValueText' => $cliente->Nombres . ' ' . $cliente->Apellidos,
                        'options' => [
                            'placeholder' => 'Buscar usuario...',
                            'v-select2' => 'idCliente',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                            ],
                        ]
                    ]);

                    ?>
                </div>
                <div class="col-md-4" v-show="idCliente">
                        
                    <h3><p><strong>Saldo: </strong>{{cliente.Saldo}}</p></h3>
                    <?= $form->field($model, 'Importe')->widget(MaskMoney::classname(), ['options' => [
                            'id'=>'Importe',
                            'v-bind:value'=>'importe',
                                'v-init' => true,
                        ],]);
                    ?>
                </div> 
                <div class="col-md-12" v-cloak> 
                    <table class="table table-hover table-condensed table-bordered " 
                           style="font-size: small" 
                           v-if="arrayVentas.length != 0">
                        <caption>Ventas pendientes de aplicación</caption>
                        <thead>
                            <tr style="background-color: #d9e9ec"> 
                                <th>Nro Venta</th>  
                                <th>Fecha</th> 
                                <th>Importe</th>
                                <th>Adeudado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-bind:id="'la[' + indice + ']'" v-for="(la,indice) in arrayVentas" >


                                <td>
                                    {{la.NroVenta}}
                                </td>
                                <td>
                                    {{la.FechaAlta}}
                                </td> 
                                <td>
                                   {{la.Importe}}
                                </td> 
                                <td>
                                   {{la.Adeudado}}
                                </td>

                            </tr>
                        </tbody>
                    </table> 
                </div> 
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


