<?php

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$lineasDespacho = json_decode($model->LineasDespacho);
?>
<div class="modal-dialog">
    <div class="modal-content"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'datosdespacho-form',]) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div> 
            <?= Html::activeHiddenInput($model, 'IdPedido') ?>            
            <?php 
                $urlDespachante = Url::to(['usuarios/autocompletar']);
                echo $form->field($model, 'IdDespachante')->widget(Select2::className(), [
                    'name' => 'despacho-idusuario',
                    'id' => 'despacho-idusuario',
                    'options' => [
                        'placeholder' => 'Buscar despachante...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => $urlDespachante,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                            'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                        ],

                    ]
                ]);
            ?>   
            <?php 
                $urlDespachante = Url::to(['repartos/autocompletar']);
                echo $form->field($model, 'IdReparto')->widget(Select2::className(), [
                    'name' => 'despacho-idusuario',
                    'id' => 'despacho-idusuario',
                    'options' => [
                        'placeholder' => 'Buscar reparto...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => $urlDespachante,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                            'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                        ],

                    ]
                ]);
            ?>   
            <?= Html::activeHiddenInput($model, 'LineasDespacho') ?>
            <?php if (count($lineasDespacho) > 0) : ?>
                <table class="table talble-haber">
                    <thead>
                        <tr>
                            <th>Codigo</th> 
                            <th>Producto</th> 
                            <th>Cantidad</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($lineasDespacho as  $linea) : ?>
                            <tr>   
                                <td><?= Html::encode($linea->CodigoProd) ?></td>
                                <td><?= Html::encode($linea->Descripcion) ?></td>
                                <td><?= Html::encode($linea->Cantidad) ?></td> 
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table> 
            <?php else: ?>
                <p><strong>Error. El pedido no tiene líneas de pedido.</strong></p>
            <?php endif; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Despachar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


