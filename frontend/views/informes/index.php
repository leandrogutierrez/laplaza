<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

common\assets\InformesAsset::register($this);
$this->registerJs('Informes.init()');

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $menu [] */
/* @var $reporte [] */
/* @var $parametros [] */

$this->title = "Informes";
$this->params['breadcrumbs'][] = $this->title;

function arbol($menu, $padre = null)
{
    $clase = ($padre == 1 ? 'nav navbar-nav' : 'dropdown-menu');
    echo "<ul class='$clase'>";
    foreach ($menu as $elemento)
    {
        if($elemento[Yii::$app->id] == 'S')
        {
            if ($elemento['IdModeloReportePadre'] == $padre)
            {
                if ($elemento['EsHoja'] == 'N')
                {
                    echo '<li class="dropdown">';
                    echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" >'
                    . $elemento['NombreMenu']
                    . '<span class="caret"></span></a>';
                    arbol($menu, $elemento['IdModeloReporte']);
                    echo '</li>';
                }
                elseif (in_array($elemento['Reporte'], Yii::$app->session->get('Permisos')))
                {
                    echo '<li>';
                    echo '<a href="' . Url::to(['/informes', 'id' => $elemento['IdModeloReporte']]) . '"'
                    . 'data-hint="' . $elemento['Ayuda'] . '">'
                    . $elemento['NombreMenu']
                    . '</a>';
                    echo '</li>';
                }
            }
        }
    }
    echo '</ul>';
}
?>

<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="collapse navbar-collapse">

            <?php arbol($menu, 1) ?>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<?php if ($reporte != NULL): ?>

    <div class="box" id="informes">

        <div class="overlay" v-show="cargando" v-cloak>
            <i class="fa fa-refresh fa-spin">
            </i>
        </div>

        <div class="box-header">
            <h3 class="box-title"><?= $reporte['NombreMenu'] ?></h3>
        </div>
        <div class="box-body">

            <p><?= $reporte['Ayuda'] ?></p>

            <div class="top10">
                <?php
                $form = ActiveForm::begin([
                            'options' => ['ref' => 'forminformes'],
                            'layout' => 'horizontal'
                        ])
                ?>

                <?php
                foreach ($parametros as $parametro)
                {
                    switch ($parametro['Tipo'])
                    {
                        case 'E':
                            echo $this->render('inputs/entero', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                            break;
                        case 'L':
                            echo $this->render('inputs/listado', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                            break;
                        case 'F':
                            echo $this->render('inputs/fecha', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                            break;
                        case 'A':
                            echo $this->render('inputs/autocompletado', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                            break;
                        case 'M':
                            echo $this->render('inputs/moneda', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                            break;
                        case 'O':
                            echo $this->render('inputs/opcion', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                            break;
                        default:
                            echo $this->render('inputs/cadena', [
                                'model' => $model,
                                'form' => $form,
                                'parametro' => $parametro
                            ]);
                    }
                }
                ?>
                <?php ActiveForm::end(); ?>

                <button class="btn btn-default pull-right no-print"
                        @click="generarInforme( <?= $reporte['IdModeloReporte'] ?>)">
                    <i class="fa fa-play"></i> Ejecutar
                </button>

            </div>

            <?php if ($tabla != NULL) : ?>
                <div class="clearfix"></div>
                
                <a class="btn btn-default pull-left no-print"
                   target="_blank"
                   href="<?= Url::to(['informes/imprimir-informe', 
                       'id' => $reporte['IdModeloReporte'],
                       'key' =>  Yii::$app->request->get('key')])?>">
                    <i class="fa fa-file-pdf-o"></i> Imprimir PDF
                </a>
            <?php endif; ?>

            <div class="clearfix"></div>

            <div id="doublescroll" class="table-responsive top10" v-show="!cargando">
                <?php if (isset($tabla)) : ?>
                    <?php if (count($tabla) > 0): ?>
                        <table class="table table-bordered table-hover  table-condensed ">
                            <thead>
                                <tr>
                                    <?php foreach ($tabla[0] as $titulo => $valor): ?>
                                        <th><?= Html::encode($titulo) ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $tick = 0;
                                $class = "";
                                foreach ($tabla as $fila):
                                    if ($reporte['IdModeloReporte'] == 48)
                                    { // Detalle Apuestas por Cliente
                                        if ($tick <> $fila['IdGrupoTicket'])
                                        {
                                            $tick = $fila['IdGrupoTicket'];
                                            if ($class == 'style="background-color: #D4D4D4"')
                                                $class = 'style="background-color: #B5B5B5"';
                                            else
                                                $class = 'style="background-color: #D4D4D4"';
                                        }
                                    }
                                    ?>
                                    <tr class="no-break" <?= $class ?>>
                                        <?php foreach ($fila as $celda): ?>
                                            <td> 
                                                <?= $celda ?>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>
                    <?php else: ?>
                        <p><strong>No hay resultados que coincidan con los criterios seleccionados.</strong></p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

    </div>
<?php endif; ?>