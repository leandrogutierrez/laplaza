<?php

use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $parametro [] */

$gestor = new \common\models\GestorReportes();
$opciones = ArrayHelper::map($gestor->LlenarListadoParametro($parametro['IdModeloReporte'], $parametro['NroParametro']), 'Id', 'Nombre');

if ($parametro['ListaTieneTodos'] == 'S')
    $opciones = ArrayHelper::merge($opciones, ['T' => 'Todos']);
?>

<?= $form->field($model, $parametro['Parametro'])->dropDownList($opciones)->hint($parametro['ToolTipText'])->label($parametro['Etiqueta']) ?>