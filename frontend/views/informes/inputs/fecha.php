<?php
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $parametro [] */
?>

<?=

$form->field($model, $parametro['Parametro'])->widget(
        DatePicker::className(),
    [
    'language' => 'es',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'dd/mm/yyyy',
    ]
]
)->hint($parametro['ToolTipText'])->label($parametro['Etiqueta']);
?>