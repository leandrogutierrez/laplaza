<?php

use yii\helpers\Html;

use yii\helpers\Url;

$lineasPedido = json_decode($model['LineasPedido']);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>    
        <div class="modal-body">
            <div id="errores-modal"> </div>
            <?= Html::activeHiddenInput($model, 'IdPedido') ?>
            <div class="panel panel-primary">
                <div class="panel-heading">Pedido Nro: <?= Html::encode($model['NroPedido']) ?> </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <div><strong>Cliente:</strong> <?= Html::encode($model['Cliente']) ?></div>
                    </div>
                    <div class="col-md-6">
                        <div><strong>Fecha de entrega:</strong> <?= Html::encode($model['FechaEntrega']) ?></div> 
                    </div>                        
                    <div class="col-md-6">
                        <div><strong>Usuario:</strong> <?= Html::encode($model['Usuario']) ?></div>
                    </div>
                    <div class="col-md-6">
                        <div><strong>Fecha alta:</strong><?= Html::encode($model['FechaAlta']) ?> </div>
                    </div> 
                </div>
            </div>
            <?php if (count($lineasPedido) > 0) : ?>
            <table class="table table-hover table-condensed table-bordered">
                <thead>
                    <tr style="background-color: #C0D8C6"> 
                        <th>Descripción</th>
                        <th>Cantidad</th>  
                        <th>Unidad</th>  
                        <th>Precio</th>  
                        <th>Importe</th>  
                    </tr>
                </thead> 
                <tbody>
                    <?php foreach ($lineasPedido as $linea) : ?>
                    <tr>
                        <td><?= Html::encode($linea->Descripcion) ?></td>
                        <td><?= Html::encode($linea->Unidad) ?></td>
                        <td><?= Html::encode($linea->Cantidad) ?></td> 
                        <td>$ <?= Html::encode($linea->Precio) ?></td> 
                        <td>$ <?= Html::encode($linea->Importe) ?></td>  
                            
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                <?php if ($model['IdPedidoPadre' ]!= null): ?>
                    
                    <button type="button" class="btn btn-primary grid-button"
                                                        data-modal="<?= Url::to(['pedidos/detalles-modificado', 'id' => $model['IdPedidoPadre']]) ?>" 
                                                        title="Ver modificaciones">
                        Ver pedido original
                        <i class="fa fa-reply-all"></i>
                    </button> 
                <?php endif; ?>                                 
            <?php else: ?>
            <p><strong>Error al cargar el pedido.</strong></p>
            <?php endif; ?>
        </div>
        <div class="modal-footer">
            <?php if (in_array('ModificarPedido', Yii::$app->session->get('Permisos')) && $model['Estado'] == 'A') : ?>
                <?= Html::a('Modificar', Url::to(['pedidos/modificar','id' => $model['IdPedido']]), ['class'=>'btn btn-primary grid-button']) ?>
            <?php endif; ?> 
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>