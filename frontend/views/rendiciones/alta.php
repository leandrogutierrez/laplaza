<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\money\MaskMoney;

frontend\assets\RendicionesAsset::register($this);
$this->registerJs('Rendiciones.Alta.init()');

?>
<div class="modal-dialog  ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Nueva rendición</h4>
        </div>
        <div id="errores-modal"> </div>
        <?php $form = ActiveForm::begin(['id' => 'rendiciones-form',]) ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-8">
                <?php 
                    $url = Url::to(['usuarios/autocompletar']);
                    echo $form->field($model, 'IdUsuario')->widget(Select2::className(), [
                            'options' => [
                                'placeholder' => 'Buscar usuario...',
                                'v-select2' => 'idUsuario',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                    'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                                ],
                            ]
                        ]);

                    ?>
                </div>
                <div class="col-md-4" v-show="idUsuario">
                     <?= $form->field($model, 'Importe', [
                                'inputTemplate' => '<div class="col-md-12 input-group"><span class="input-group-addon">$</span>{input}</div>',
                            ])->textInput([
                                'v-model' => "usuario.Saldo",
                                'v-init' => true,
                                'readonly' => true])->label('Importe') ?>
                </div>
                <div class="col-md-12" v-show="idUsuario"> 
                    <table class="table table-condensed table-hover table-bordered"
                           style="font-size: small"
                           v-if="pagos.length != 0">
                        <caption>Pagos pendientes de rendición</caption>
                        <thead>
                            <tr style="background-color: #C0C2D8"> 
                                <th>Nº comprobante</th> 
                                <th>Cliente</th> 
                                <th>Fecha</th> 
                                <th>Importe</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-bind:id="'pago[' + indice + ']'" v-for="(pago,indice) in pagos" >
                                <td>
                                    {{pago.NroComprobantePago}}
                                </td>
                                <td>
                                    {{pago.Cliente}}
                                </td>
                                <td>
                                    {{pago.FechaAlta}}
                                </td> 
                                <td align="right">
                                    {{pago.Importe}}
                                </td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


