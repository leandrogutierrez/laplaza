<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
 
$estados = [
    'T' => 'Todos',
    'A' => 'Activo',
    'V' => 'Vuelta',
    'B' => 'Baja',
];
$this->title = 'Repartos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php if (in_array('AltaReparto', Yii::$app->session->get('Permisos'))) : ?>
                    <button class="btn btn-primary pull-right" type="button"
                            data-modal="<?= Url::to(['repartos/alta']) ?>">
                        <i class="fa fa-plus"></i> Nuevo reparto
                    </button>
                    <?php endif; ?>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'busquedaReparto-form',
                                                    'method' => 'get',
                                                    'layout' => 'inline']) ?>    
                                                    
                    <?= $form->field($busqueda, 'Combo2')
                            ->dropDownList(ArrayHelper::map($usuarios, 'IdUsuario', 'Usuario'), [
                                'prompt' => 'Seleccione usuario...'
                    ]) ?>  

                    <?= $form->field($busqueda, 'Combo3')
                            ->dropDownList(ArrayHelper::map($zonas, 'IdZona', 'Zona'), [
                                'prompt' => 'Seleccione zona...'
                    ]) ?>      

                    <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                        'attributeTo' => 'FechaFin',
                        'labelTo' => 'al',
                        'form' => $form,
                        'language' => 'es',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd/mm/yyyy',
                            'endDate' => 'd',
                        ]
                    ])
                    ?>

                    <?= $form->field($busqueda, 'Combo')->dropDownList($estados) ?>

                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarpedido-button']) ?> 
                 
                <?php ActiveForm::end() ?> 
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover ">
                    <thead>
                        <tr class="tabla-header"> 
                            <th>Zona</th>
                            <th>Usuario</th>
                            <th>Fecha</th> 
                            <th>Estado</th> 
                            <th style="text-align:center">Pedidos pendientes</th> 
                            <th style="text-align:center">Ver raparto</th> 
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr> 
                            <td><?= Html::encode($model['Zona']) ?></td> 
                            <td><?= Html::encode($model['Usuario']) ?></td>
                            <td><?= Html::encode($model['FechaReparto']) ?></td> 
                            <td><?= Html::encode($estados[$model['Estado']]) ?></td> 
                            <td align="center">
                                <?php if ($model['Estado'] != 'V'): ?>
                                <button class="btn pull-center btn-success" type="button"
                                        data-modal="<?= Url::to(['repartos/despacho-masivo', 'id' => $model['IdReparto']]) ?>">
                                        Pedidos        <i class="fa fa-file-text-o"></i> 
                                </button>   
                                <?php endif?>
                            </td>
                            <td align="center">
                                    <button class="btn btn-default pull-center" type="button"
                                            data-modal="<?= Url::to(['repartos/ver-despachos-reparto', 'id' => $model['IdReparto']]) ?>">
                                            Despachos        <i class="fa fa-truck"></i> 
                                    </button>    
                                <?php if ($model['Estado'] == 'V'): ?>
                                    <button class="btn btn-warning pull-center" type="button"
                                        data-modal="<?= Url::to(['repartos/ver-arribo-reparto', 'id' => $model['IdReparto']]) ?>">
                                        Arribo        <i class="fa fa-arrow-circle-down"></i> 
                                    </button>   
                                <?php elseif (in_array('AltaArribo', Yii::$app->session->get('Permisos'))) :?>
                                    <button class="btn btn-warning pull-center" type="button"
                                            data-modal="<?= Url::to(['repartos/arribo-reparto', 'id' => $model['IdReparto']]) ?>">
                                            Arribo        <i class="fa fa-arrow-circle-down"></i> 
                                    </button> 
                                <?php endif?>
                                <button class="btn btn-info pull-center" type="button"
                                        data-modal="<?= Url::to(['repartos/ver-ventas-reparto', 'id' => $model['IdReparto']]) ?>">
                                        Ventas        <i class="fa fa-dollar"></i> 
                                </button> 
                            </td>  
                            <td> 
                                <div class="btn-group">  
                                    <?php if (in_array('AltaReparto', Yii::$app->session->get('Permisos'))
                                            and $model['Estado'] == 'V') : ?>
                                        <button class="btn btn-default pull-center" type="button"
                                                data-modal="<?= Url::to(['repartos/repetir', 'id' => $model['IdReparto']]) ?>">
                                            <i class="fa fa-retweet"></i> 
                                        </button>  
                                    <?php endif; ?>  
                                    <?php if (in_array('BuscarRepartos', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default pull-center" type="button"
                                                data-modal="<?= Url::to(['repartos/planillas', 'id' => $model['IdReparto']]) ?>"
                                                title="Planillas">
                                            <i class="fa fa-print"></i> 
                                        </button>  
                                    <?php endif; ?>  
                                    <?php if (in_array('BorrarReparto', Yii::$app->session->get('Permisos'))) : ?>
                                        <button class="btn btn-default" type="button"
                                                 target="_blank"
                                                data-ajax="<?= Url::to(['repartos/borrar','id' => $model['IdReparto']]) ?>"
                                                data-mensaje="¿Desea borrar el reparto indicado?"
                                                title="Eliminar">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                        <?=
                        LinkPager::widget([
                            'pagination' => $paginado,
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>> ',
                            'nextPageLabel' => '>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
                </div> 
                <?php else: ?>
                <p><strong>No existen clientes que coincidan con el criterio de búsqueda indicado.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>