<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use common\models\Zonas;
use common\models\Repartos;
use common\models\Usuarios;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;

?>
<div class="modal-dialog">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'datosrepartos-form',]) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div>

            <?= Html::activeHiddenInput($model, 'IdReparto') ?> 
            
            <?php 
                $urlUsuarios = Url::to(['usuarios/autocompletar']);
                echo $form->field($model, 'IdUsuario')->widget(Select2::className(), [
                        'initValueText' => $model->Usuario, // set the initial display text
                        'options' => [
                            'placeholder' => 'Buscar usuario...',
                            'v-select2' => 'IdUsuario',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $urlUsuarios,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                            ],
                        ]
                ]);

            ?> 
            
            <?= $form->field($model, 'IdVehiculo')->dropDownList($vehiculos) ?>
            
            <?php 
                $urlZonas = Url::to(['zonas/autocompletar']);
                $zona = new Zonas();
                $zona->IdZona = $model->IdZona;
                $zona->Dame();
                echo $form->field($model, 'IdZona')->widget(Select2::className(), [
                        'initValueText' => $zona->Zona, // set the initial display text
                        'options' => [
                            'placeholder' => 'Buscar zona...',
                            'v-select2' => 'IdZona',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $urlZonas,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                            ],
                        ]
                ]);
            ?>
            
            <?php 
                echo $form->field($model, 'FechaReparto')->widget(DateControl::classname(), [
                                'ajaxConversion'=>false,
                                'displayFormat' =>   'dd-MM-yyyy',
                                 'widgetOptions' => [
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'todayHighlight' => true,
                                        'todayBtn' => 'linked',
                                    ]
                                ]
                ]);
           ?>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


