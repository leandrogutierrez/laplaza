<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use common\models\Zonas;
use common\models\Repartos;
use common\models\Usuarios;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;

?>
<div class="modal-dialog">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo .' - '. $model['Zona'] .' '. $model['FechaReparto'] ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'datosrepartos-form',]) ?>

        <div class="modal-body"
            style="text-align: left; position:relative; width: 100%; border-width: 1px 0 0 0; ">
            <div id="errores-modal"> </div> 
            
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-pedido','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Reparto
            </a>  
            
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-puntual','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Puntual
            </a> 
            
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-salados','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Salados
            </a> 
            
            
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-dulces','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Dulces
            </a> 
            
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-cobranzas','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Cobranzas
            </a> 
            <p></p>
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-ventas','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Ventas (Cantidad)
            </a> 
            <a class="btn btn-primary  no-print"
                   target="_blank"
                   href="<?= Url::to(['imprimir-ventas-precios','id' => $model['IdReparto']]) ?>">
                    <i class="fa fa-file-pdf-o"></i> Ventas (Precio)
            </a>
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
        </div>
        
        <?php ActiveForm::end(); ?>
    </div>
</div>


