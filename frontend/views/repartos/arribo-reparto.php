<?php

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\Html;

?>
<div class="modal-dialog modal-lg" id="arribo-reparto">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?> - Reparto <?= $reparto['Zona']?></h4>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'arriboreparto-form',]) ?>
        <div class="modal-body"> 
            <div id="errores-modal"> </div> 
            <?php if ($arribo != null): ?>
                <div class="col-md-12"> 
                    <table class="table table-hover table-condensed table-bordered"> 
                        <thead>
                            <tr style="background-color: #C0D8C6"> 
                                <th>Recepción</th>
                                <th>Fecha Arribo</th>  
                            </tr> 
                        </thead>
                        <tbody> 
                            <?php $lineasArribo = json_decode($arribo['LineasArribo']) ?>
                            <tr data-toggle="collapse" 
                                data-target="<?= '#arribo' . $arribo['IdArribo'] ?>" 
                                class="accordion-toggle"> 
                                <td><?= Html::encode($arribo['Despachante']) ?></td>
                                <td><?= Html::encode($arribo['FechaArribo']) ?></td> 
                            </tr>
                            <tr>
                                <td>
                                    <div class="accordion-body collapse" id="<?= 'arribo' . $arribo['IdArribo'] ?>"> 
                                        <table class="table table-condensed table-bordered" style="font-size: small">
                                            <caption>Detalle del arribo</caption>
                                            <thead>
                                                <tr style="background-color: #C0C2D8">
                                                    <th>Código</th>
                                                    <th>Descripción</th>
                                                    <th>Unidad</th>
                                                    <th>Cantidad</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <?php if ($lineasArribo != null):
                                                    foreach ($lineasArribo as $linea): ?>
                                                        <tr>
                                                            <td><?= $linea->CodigoProd ?></td> 
                                                            <td><?= $linea->Descripcion ?></td> 
                                                            <td><?= $linea->Unidad ?></td> 
                                                            <td><?= $linea->Cantidad ?></td>
                                                        </tr>
                                                    <?php endforeach;
                                                endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr> 
                        </tbody>
                    </table> 
                </div>
            <?php else: ?>
                <p><strong>El reparto no posee arribo.</strong></p>
            <?php endif; ?> 
            <div class="clearfix"></div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
        </div>
        <?php ActiveForm::end(); ?>
    </div>  
</div>