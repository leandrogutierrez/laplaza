<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Caja cerrada';
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Caja cerrada</h3>
            </div>

            <div class="box-body">

                <p><strong>La caja se encuentra cerrada. Es necesario abrirla para poder realizar movimientos.</strong></p>

                <div class="text-center">
                    <a class="btn btn-primary" href="<?= Url::to('/cajas/abrir') ?>" >Abrir caja</a>
                </div>
            </div>
        </div>
    </div>
</div>