<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
$this->title = 'Movimientos de caja';

$tiposMov = [
    'P' => 'Rendición',
    'A' => 'Anulación'
];
?>
<div class="row">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Movimientos</h3>
            </div> 
            <div class="box-body"> 
                <?php $form = ActiveForm::begin(['layout' => 'inline']); ?>

                <?=
                $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                    'attributeTo' => 'FechaFin',
                    'labelTo' => 'al',
                    'form' => $form,
                    'language' => 'es',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy'
                    ]
                ])->label('')
                ?>                    

                <?=
                $form->field($busqueda, 'Combo')->dropDownList([
                    'T' => 'Todos',
                    'V' => 'Ventas',
                    'P' => 'Pagos',
                    'A' => 'Anulaciones']);
                ?>
                
                <?= $form->field($busqueda, 'Cadena')->textInput([
                    'placeholder' => 'Ingrese nº movimiento...'
                ]) ?>

                <?= Html::submitButton('Buscar', ['class' => 'btn btn-default',]) ?> 

                <?php ActiveForm::end(); ?>
                <div id="errores"> </div>
                <?php if (count($models) > 0): ?>

                    <table class="table table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th>Nro.</th>
                                <th>Fecha</th>
                                <th>Tipo</th>
                                <th>Monto</th>
                                <th>Observaciones</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($models as $model): ?>
                            <tr>
                                <td><?= Html::encode($model['NroMov']) ?></td>
                                <td><?= Html::encode($model['Fecha']) ?></td>
                                <td><?= ($model['TipoMov'] != 'TOTAL') ?
                                        $tiposMov[$model['TipoMov']] : '<strong>TOTAL</strong>' ?></td>
                                <td align="right"><?= Html::encode($model['Monto']) ?></td>
                                <td><?= Html::encode($model['Observaciones']) ?></td>
                                <td></td>
                            </tr>
                            
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                <?php else: ?>
                    <p><strong>El uso caja actual no tiene movimientos que coincidan con los criterios de búsqueda.</strong></p>
                <?php endif; ?> 
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">Caja</div>
            <div class="panel-body">
                <p><strong>Nro:</strong> <?= $usoCaja->NroUsoCaja ?></p>
                <p><strong>Fecha de apertura:</strong> <?= Yii::$app->formatter->asDatetime($usoCaja->FechaInicio) ?></p>
                <div class="text-center">
                    <button class="btn btn-danger" data-ajax="<?= Url::to('/cajas/cerrar') ?>" >Cerrar caja</button>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Rendiciones</div>
            <div class="panel-body">
                <div class="text-center"> 
                    <button class="btn btn-primary" data-modal="<?= Url::to('/rendiciones/alta') ?>" >Nueva rendición</button>
                </div>
            </div>
        </div>
    </div>
</div>