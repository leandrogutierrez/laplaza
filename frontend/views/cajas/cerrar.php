<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


?>
<div class="modal-dialog" data-no-reload>
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Cerrar caja</h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'modal-form']) ?>

        <div class="modal-body">

            <div id="errores-modal"> </div>

            <label for="Observaciones">Observaciones</label>
            <?= Html::textarea('Observaciones', null, ['class' => 'full',]) ?>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>