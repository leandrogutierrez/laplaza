<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

frontend\assets\UsuariosAsset::register($this);

$this->registerJs('Usuarios.Login.init()');
$this->context->layout = 'login';

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\Usuarios */
/* @var $caja frontend\models\Cajas */

$this->title = 'Agregar este dispositivo a una sucursal';
?>

<h3 class="login-box-msg"><?= Html::encode($this->title) ?></h3>

<div class="login-box-body">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <div>

        <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . ' alert-dismissable">'
            . '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
            . $message . '</div>';
        }
        ?>

        <?php if (Yii::$app->user->isGuest): ?>

            <?= $form->field($usuario, 'Usuario')->textInput(['autofocus' => true]) ?>

            <?= $form->field($usuario, 'Password')->passwordInput() ?>
        
        <?php else: ?>

            <?= $form->field($caja, 'Caja') ?>

            <?php

            $url = Url::to(['/cajas/autocompletar-sucursales']);

            $initScript = <<<SCRIPT
                function (element, callback) {
                    var id=\$(element).val();
                    if (id !== "" && id) {
                        \$.ajax("{$url}?id=" + id, {
                            dataType: "json"
                        }).done(function(data) { callback(data);});
                    } else callback([]);
                }
SCRIPT;
            echo $form->field($caja, 'IdSucursal')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Seleccione sucursal...'],
                'pluginOptions' => [
                    'minimumInputLength' => 1,
                    'allowClear' => true,
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(param) { return {cadena:param.term}; }'),
                        'processResults' => new JsExpression('function(data,page) { return {results:data}; }'),
                    ],
                    'initSelection' => new JsExpression($initScript)
                ],
            ]);
            ?>
 
            <?= $form->field($caja, 'Observaciones')->textarea() ?>

            <?= Html::activeHiddenInput($caja, 'CodigoLlave', ['id' => 'codigo-llave']) ?>

        <?php endif; ?>

    </div>
    <div class="footer">                                                               
        <?= Html::submitButton('Guardar', ['class' => 'btn bg-olive btn-block', 'name' => 'login-button']) ?>  

    </div>
    <?php ActiveForm::end(); ?>
</div>