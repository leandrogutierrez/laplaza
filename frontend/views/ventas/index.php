<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\LinkPager;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Gestión de ventas';
$this->params['breadcrumbs'][] = $this->title;
$estados = [
    'T' => 'Todas',
    'P' => 'Pendientes',
    'F' => 'Finalizadas',
    'A' => 'Anuladas',
];
 
$this->title = 'Ventas';
?> 
<div class="row" id="ventas">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php if (in_array('AltaVenta', Yii::$app->session->get('Permisos'))) : ?>
                    <a href="<?= Url::to(['ventas/alta']) ?>">
                        <button class="btn btn-primary pull-right" type="button" >
                            <i class="fa fa-plus"></i> Nueva venta
                        </button>
                    </a>
                    <?php endif; ?>
                </div>
                <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'buscarventas-form',
                                                        'method' => 'get',
                                                        'layout' => 'inline']) ?>
                    <div class="col-md-4">  
                    <?php 
                        $urlClientes = Url::to(['clientes/autocompletar']);
                        echo $form->field($busqueda, 'Combo3')->widget(Select2::className(), [
                                'data' => $clientes,
                                'options' => [
                                  'placeholder' => 'Buscar cliente...',
                                    'v-select2' => 'idCliente',
                                ],
                                'pluginOptions' => [
                                    'width' => '300px',
                                    'allowClear' => true,
                                    
                                ]
                            ]);
                    ?> 
                    <p></p>
                    <?= $form->field($busqueda, 'Combo2')->dropDownList(
                        ArrayHelper::map($usuarios, 'IdUsuario', 'NombreCompleto'),
                                                        [ 'prompt' => 'Seleccione vendedor...',  'style'=>'width: 300px']
                        )
                    ?>   
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($busqueda, 'FechaInicio')->widget(DateRangePicker::className(), [
                            'attributeTo' => 'FechaFin',
                            'labelTo' => 'al',
                            'form' => $form,
                            'language' => 'es',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd/mm/yyyy',
                                'endDate' => 'd',
                            ]
                        ])?>
                        <p></p>
                        <?= $form->field($busqueda, 'Combo')->dropDownList($estados) ?>
                    </div>
                    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'buscarcompras-button']) ?> 

                    <?php ActiveForm::end() ?>
                </div>
                <p></p>
                <div id="errores"></div>
                <?php if (count($models) > 0) : ?>
                <table class="table table-hover" id="fixedheight">
                    <thead>
                        <tr class="tabla-header">
                            <th>Fecha Alta</th> 
                            <th>Nº venta</th>
                            <th>Cliente</th>
                            <th>Importe</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($models as $model) : ?>
                        <tr v-on:dblclick="clickRow(<?= Html::encode($model['IdVenta']) ?> )">
                            <td><?= Html::encode($model['FechaAlta']) ?></td> 
                            <td><?= Html::encode($model['NroVenta']) ?></td>
                            <td><?= Html::encode($model['Cliente']) ?></td>
                            <td>$ <?= Html::encode($model['Importe']) ?></td>
                            <td><?= $estados[$model['Estado']] ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default"
                                            data-modal="<?= Url::to(['ventas/detalles','id' => $model['IdVenta']]) ?>"
                                            title="Detalle de la venta">
                                        <i class="fa fa-search"></i>
                                    </button> 
                                    <?php if ($model['Estado'] != 'A'):?>
                                        <?php if (in_array('AnularVenta', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['ventas/anular', 'id' => $model['IdVenta']]) ?>"
                                                data-mensaje="¿Desea anular la venta?">
                                            <i class="fa fa-ban"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if ($model['Estado'] == 'A'):?>
                                        <?php if (in_array('BorrarVenta', Yii::$app->session->get('Permisos'))) : ?>
                                        <button type="button" class="btn btn-default"
                                                data-ajax="<?= Url::to(['ventas/borrar', 'id' => $model['IdVenta']]) ?>"
                                                data-mensaje="¿Desea borrar la venta?">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pull-right">
                <?=
                LinkPager::widget([
                    'pagination' => $paginado,
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>> ',
                    'nextPageLabel' => '>',
                    'prevPageLabel' => '<'
                ]);
                ?>
                </div>
                <?php else: ?>
                <p><strong>No se registran ventas en el sistema con los criterios de búsqueda utilizados.</strong></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

