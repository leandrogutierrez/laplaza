<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use common\models\Zonas;
use common\models\Repartos;
use common\models\Vendedores;
use common\models\Usuarios;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;

common\assets\ArribosAsset::register($this);
$this->registerJs('Arribos.Alta.init()');
$this->title = 'Arribos';
?>
<div class="modal-dialog" id="arribos">
    <div class="modal-content"> 
        <div class="modal-header">
            <button type="button" class="close" tabindex='-1' data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= $titulo ?></h4>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'arriboreparto-form']) ?>

        <div class="modal-body">
            <div id="errores-modal"> </div> 
            <?= Html::activeHiddenInput($model, 'IdArribo') ?>  
            <?= Html::activeHiddenInput($model, 'IdReparto') ?>   
            <div class="row">
                <div class="col-md-6">
                <?= $form->field($reparto, 'Usuario')->textInput(['disabled' =>true])   ?>
                </div>  
                <div class="col-md-6">
                <?= $form->field($reparto, 'Zona')->textInput(['disabled' =>true])  ?>
                </div> 
                <div class="col-md-6"> 
                    <?php 
                        $usuario = Usuarios::findIdentity(Yii::$app->user->id);
                        $model->IdUsuario = Yii::$app->user->id;
                        echo Html::activeHiddenInput($model, 'IdUsuario');
                        echo $form->field($usuario, 'NombreCompleto')->textInput(['disabled' =>true])->label('Recepción');
                    ?>   
                </div>
                <div class="col-md-6">
                    <?php 
                        echo $form->field($model, 'FechaArribo')->widget(DateControl::classname(), [
                            'ajaxConversion'=>false,
                            'displayFormat' =>   'dd-MM-yyyy',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'todayHighlight' => true,
                                    'todayBtn' => 'linked',
                                    'startDate' => date('d-m-Y'),
                                ]
                            ]
                        ]);
                    ?>
                </div> 
            </div> 
            <div class="row">
            <div class="col-md-6">
                <label for="arribos-idproducto" class="control-label">Productos</label>
                <?php 
                    $urlProductos = Url::to(['productos/autocompletar-productos']);
                    echo Select2::widget([
                        'name' => 'arribos-idproducto',
                        'id' => 'arribos-idproducto',
                        'options' => [
                            'placeholder' => 'Buscar producto...',
                            'v-select2' => 'idProducto',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $urlProductos,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(param){ return { cadena : param.term }}'),
                                'processResults' => new JsExpression('function(data,page) { return {results:data}; }')
                            ],

                        ]
                    ]);

                ?>
            </div> 
            <div class="col-md-12" v-cloak> 
                <table class="table table-hover table-condensed " 
                       style="font-size: small" 
                       v-if="arrayLA.length != 0">
                    <thead>
                        <tr style="background-color: #d9e9ec">
                            <th>Producto</th> 
                            <th>Cantidad</th> 
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-bind:id="'la[' + indice + ']'" v-for="(la,indice) in arrayLA" >
                            <td>
                                {{la.descripcion}}
                                <input type="hidden" 
                                       v-bind:name="'lineas[' + indice + '][IdProducto]'"
                                       v-bind:value="la.idProducto" >
                            </td> 
                            <td>
                                <input type="number" v-focus  
                                       v-model="la.cantidad"
                                       v-bind:name="'lineas[' + indice + '][Cantidad]'"
                                       v-bind:id="'la[cantidad][' + indice + ']'" 
                                />
                            </td> 
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" @click="borrarLP(indice)">
                                        <i class="fa fa-eraser"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div v-else>
                    <p><strong>Agregue productos</strong></p>
                </div> 
            </div> 
            <div class="clearfix"></div> 
        </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary',]) ?>  
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


