<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'La Plaza',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    
    'modules' => [
        'datecontrol' =>  [
             'class' => '\kartik\datecontrol\Module'
        ]
    ],
    
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\Usuarios',
            'loginUrl' => '/usuarios/login',
            'authTimeout' => 60 * 60,
        ],
        'session' => [
            'name' => 'FRONTSESSID',
            'timeout' => 60 * 60,
        ],
         'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller>/<id:\d+>' => '<controller>',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller>/index' => '<controller>',
            ],
        ],
    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            /**
             *  Usuarios no logueados (rol ? )
             */
            [
                'allow' => true,
                'actions' => ['login', 'error'],
                'roles' => ['?'],
            ],
            /**
             *  Alta de cajas. Sólo para ver el formulario de login. El resto está oculto para los guest
             */
            [
                'allow' => true,
                'controllers' => ['cajas'],
                'actions' => ['alta'],
                'roles' => ['?'],
            ],
            /**
             *  Alta de cajas. Usuario logueado con permisos de técnico
             */
            [
                'allow' => true,
                'controllers' => ['cajas'],
                'actions' => ['autocompletar-carnicerias', 'alta'],
                'roles' => ['@'],
                'matchCallback' => function () {
                    $esTecnico = in_array('UsoTecnico', Yii::$app->session->get('Permisos'));
                    $usuario = Yii::$app->user->identity;
                    $token = Yii::$app->session->get('Token');
                    return $usuario->DebeCambiarPass == 'N' &&
                            $usuario->Estado == 'A' &&
                            $usuario->Token == $token &&
                            $esTecnico;
                }
            ],
            /**
             *  Debug
             */
            [
                'allow' => true,
                'controllers' => ['debug', 'default', 'debug/default'],
            ],
            [
                'allow' => true,
                'controllers' => ['cajas', 'site'],
                'actions' => ['cerrada', 'abrir', 'index', 'set-flash'],
                'roles' => ['@'],
                'matchCallback' => function () {
                    $usuario = Yii::$app->user->identity;
                    $token = Yii::$app->session->get('Token');
                    $esCajero = in_array('VentaCaja', Yii::$app->session->get('Permisos'));
                    $esSupervisor = in_array('SupervisorCaja', Yii::$app->session->get('Permisos'));
                    $caja = Yii::$app->session->get('UsoCaja');
                    var_dump($esCajero);
                    var_dump($esSupervisor);
                    
                    return $caja['NroUsoCaja'] == null && ($esCajero || $esSupervisor) && $usuario->Token == $token;
                }
            ],
            /**
             *  Usuarios logueados que deben cambiar contraseña
             */
            [
                'allow' => true,
                'actions' => ['cambiar-password', 'logout',],
                'roles' => ['@'],
            ],
            /**
             *  Usuarios logueados que no deben cambiar contraseña, están activos
             *  y tienen Token bueno
             */
            [
                'allow' => true,
                'roles' => ['@'],
                'matchCallback' => function () {
                    $usuario = Yii::$app->user->identity;
                    $token = Yii::$app->session->get('Token');
                    return $usuario->DebeCambiarPass == 'N' && $usuario->Estado == 'A' && $usuario->Token == $token;
                },
            ],
        ],
        // Función que se ejecuta cuando el request es denegado.
        'denyCallback' => function ($rule, $action) {
            if (!Yii::$app->user->isGuest) {
                $esCajero = in_array('VentaCaja', Yii::$app->session->get('Permisos'));
                $esSupervisor = in_array('SupervisorCaja', Yii::$app->session->get('Permisos'));
                if (Yii::$app->user->identity->DebeCambiarPass == 'S') {
                    //Redirect
                    Yii::$app->user->returnUrl = Yii::$app->request->referrer;
                    return $action->controller->redirect('/usuarios/cambiar-password');
                } elseif (!$esCajero && !$esSupervisor) {
                    Yii::$app->session->destroy();
                    Yii::$app->user->logout();
                    Yii::$app->user->returnUrl = Yii::$app->request->referrer;
                    Yii::$app->session->setFlash('danger', 'Ocurrió un problema con su sesión.');
                    return $action->controller->redirect(Yii::$app->user->loginUrl);
                } elseif (Yii::$app->session->get('UsoCaja')['FechaInicio'] == null) {
                    return $action->controller->redirect('/cajas/cerrada');
                } else {
                    Yii::$app->session->destroy();
                    Yii::$app->user->logout();
                    Yii::$app->session->setFlash('danger', 'Ocurrió un problema con su sesión.');
                    Yii::$app->user->returnUrl = Yii::$app->request->referrer;
                    return $action->controller->redirect(Yii::$app->user->loginUrl);
                }
            }
            
            return $action->controller->redirect(Yii::$app->user->loginUrl);
        },
    ],
    'params' => $params,
];
