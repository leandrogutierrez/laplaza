<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use common\models\Usuarios;
use common\models\forms\BusquedaForm;
use common\models\Cajas;
use common\models\GestorCajas;
use common\models\Sucursales;
use common\models\GestorSucursales;
use common\models\ComprobantesPago;
use common\models\UsosCaja;

class CajasController extends Controller
{
    public function actionIndex()
    {
        $usoCaja = new UsosCaja();
        $caja = new Cajas();
        $usoCaja->attributes = $caja->DameUsoCajaActual();
        
        // La caja está cerrada
        if ($usoCaja->FechaInicio == null) {
            $this->redirect('/cajas/cerrada');
        }
        // La caja está abierta
        else {
            $FechaInicio = date('d/m/Y', strtotime('-1 day', strtotime($usoCaja->FechaInicio)));
            $FechaFin = date('d/m/Y');
            return $this->abierta($FechaInicio, $FechaFin);
        }
    }
    
    /**
    *  La caja está cerrada. Tiene que mostrar la opción para abrirla
    */
    public function actionCerrada()
    {
        $usoCaja = new UsosCaja();
        $usoCaja->attributes = Yii::$app->session->get('UsoCaja');

        return $this->render('cerrada');
    }
    
    private function abierta($FechaInicio, $FechaFin, $TipoMov = 'T', $NroMov = null)
    {
        $usoCaja = new UsosCaja();
        $usoCaja->attributes = Yii::$app->session->get('UsoCaja');

        $busqueda = new BusquedaForm();

        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $TipoMov = $busqueda->Combo;
            $NroMov = $busqueda->Cadena;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $TipoMov = $busqueda->Combo;
            $NroMov = $busqueda->Cadena;
        }

        $movimientos = $usoCaja->BuscarMovimientos($FechaInicio, $FechaFin, $TipoMov, $NroMov);

        return $this->render('abierta', [
                    'usoCaja' => $usoCaja,
                    'busqueda' => $busqueda,
                    'models' => $movimientos,
        ]);
    }
    
    public function actionAbrir()
    {
        $caja = new Cajas();

        $mensaje = $caja->AbrirCaja();

        if (substr($mensaje, 0, 2) == 'OK') {
            $caja->CodigoLlave =  Yii::$app->session->get('codigoLlave');
            $usoCaja = $caja->DameUsoCajaActual();
            Yii::$app->session->set('UsoCaja', $usoCaja);
            return $this->redirect(Url::to(['/cajas']));
        } else {
            Yii::$app->session->setFlash('danger', $mensaje);
            return $this->redirect(Url::to(['cerrada']));
        }
    }

    public function actionCerrar()
    {
        $caja = new Cajas();
        $caja->CodigoLlave = Yii::$app->session->get('codigoLlave');
        $caja->UsoCaja = new UsosCaja();
        $caja->UsoCaja->attributes = Yii::$app->session->get('UsoCaja');
            
        if (Yii::$app->request->getIsGet()) {
            Yii::$app->response->format = 'json';
  
            $mensaje = $caja->CerrarCaja();

            if (substr($mensaje, 0, 2) == 'OK') {
                Yii::$app->session->set('UsoCaja', $caja->DameUsoCajaActual());
                return ['error' => null];
            } else {
                Yii::$app->session->set('UsoCaja', $caja->DameUsoCajaActual());
                return ['error' => $mensaje];
            }
        }
    }
    public function actionAlta()
    {
        if (!Yii::$app->user->isGuest && !in_array('UsoTecnico', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }

        $usuario = new Usuarios();
        $usuario->setScenario(Usuarios::_LOGIN);

        $caja = new Cajas();
        $gestor = new GestorCajas();

        if ($usuario->load(Yii::$app->request->post()) && $usuario->validate()) {
            $login = $usuario->Login($usuario->Password);

            if ($login == 'OK') {
                Yii::$app->session->set('Permisos', $usuario->DamePermisos());

                if (!in_array('UsoTecnico', Yii::$app->session->get('Permisos'))) {
                    Yii::$app->session->setFlash('danger', 'No se tienen los permisos necesarios para ver la página solicitada.');
                } else {
                    Yii::$app->user->login($usuario);
                    Yii::$app->session->set('Token', $usuario->Token);
                }
            } else {
                Yii::$app->session->setFlash('danger', $login);
            }
            $usuario->Password = null;
        }

        if ($caja->load(Yii::$app->request->post()) && $caja->validate()) {
            $mensaje = $gestor->Alta($caja);

            if (substr($mensaje, 0, 2) == 'OK') {
                Yii::$app->session->destroy();
                Yii::$app->user->logout();
                Yii::$app->session->setFlash('success', 'Caja registrada correctamente.');
                return $this->redirect(Yii::$app->user->loginUrl);
            } else {
                Yii::$app->session->setFlash('danger', $mensaje);
            }
        }
        
        return $this->render('alta', [
                    'usuario' => $usuario,
                    'caja' => $caja,
        ]);
    }
    
    public function actionAutocompletarSucursales($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $sucursal = new Sucursales();
            
            $carniceria->IdSucursal = $id;
            
            $carniceria->Dame();
            
            $out = [
                'id' => $carniceria->IdSucursal,
                'text' => $carniceria->Sucursal
            ];
        } else {
            $gestor = new GestorSucursales();
            
            $carnicerias = $gestor->Buscar($cadena);
            
            $out = array();
            
            foreach ($carnicerias as $carniceria) {
                $out[] = [
                    'id' => $carniceria['IdSucursal'],
                    'text' => $carniceria['Sucursal']
                ];
            }
        }
        return $out;
    }
}
