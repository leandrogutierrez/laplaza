<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use common\models\forms\BusquedaForm;
use common\models\GestorPedidos;
use common\models\Pedidos;
use common\models\Despachos;
use common\models\Usuarios;
use common\models\Clientes;
use common\models\GestorDespachos;
use common\models\GestorClientes;
use common\models\GestorUsuarios;
use yii\helpers\ArrayHelper;

class PedidosController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($IdUsuario = null, $FechaInicio = null, $FechaFin = null, $Estado = 'A', $IdCliente = null, $IdReparto = null)
    {
        if (!in_array('BuscarPedidos', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $IdCliente = $busqueda->Combo3;
            $IdUsuario = $busqueda->Combo2;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo3 = $IdCliente;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo2 = $IdUsuario;
        }
          
        $gestorUsuarios  = new GestorUsuarios();
        $usuarios = $gestorUsuarios->Buscar('', 'N');
        
        $gestorClientes = new GestorClientes();
        $clientes = ArrayHelper::map($gestorClientes->Buscar(), 'IdCliente', 'ClienteZona');
        
        $gestorPedidos = new GestorPedidos();
        $pedidos = $gestorPedidos->BuscarAvanzado($IdReparto, $IdCliente, $IdUsuario, $FechaInicio, $FechaFin, $Estado);
        
        $paginado->totalCount = count($pedidos);
        $pedidos = array_slice($pedidos, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $pedidos,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'usuarios' => $usuarios,
                    'clientes' => $clientes,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaVenta', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $pedido = new Pedidos();
        $pedido->FechaEntrega = date('Y-m-d', strtotime('tomorrow'));
        
        $usuario = new Usuarios();
        $usuario->IdUsuario = Yii::$app->user->id;
        $usuario->Dame();
        $pedido->IdUsuario = $usuario->IdUsuario;
        return $this->render('alta', [
                    'model' => $pedido,
                    'usuario' => $usuario,
        ]);
    }
    
    public function actionDetalles($id)
    {
        if (!in_array('BuscarPedidos', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $pedido = new Pedidos();
        if (intval($id)) {
            $pedido->IdPedido = $id;
        } else {
            throw new HttpException('422', 'El pedido indicado es inválido');
        }
        
        $pedido->Dame();
        
        return $this->renderAjax('datos-pedido', [
                'model' => $pedido,
                'titulo' => 'Datos del pedido'
        ]);
    }
    
    public function actionDetallesModificado($id)
    {
        if (!in_array('BuscarPedidos', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $pedido = new Pedidos();
        if (intval($id)) {
            $pedido->IdPedido = $id;
        } else {
            throw new HttpException('422', 'El pedido indicado es inválido');
        }
        
        $pedido->Dame();
        
        $pedidoHijo = new Pedidos();
        $pedidoHijo->IdPedido = $pedido->IdPedidoHijo;
        $pedidoHijo->Dame();
        
        
        return $this->renderAjax('datos-pedido-modificado', [
                'pedido' => $pedido,
                'pedidoHijo'=> $pedidoHijo,
                'titulo' => 'Datos del pedido modificado'
        ]);
    }
    
    public function actionDespachar($id)
    {
        if (!in_array('AltaDespacho', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
         
        $despacho = new Despachos();
        $pedido = new Pedidos();
        
        if (intval($id)) {
            $pedido->IdPedido = $id;
        } else {
            throw new HttpException('422', 'El pedido indicado es inválida');
        }
        
        $despacho->setScenario(Despachos::_ALTA);
        if ($despacho->load(Yii::$app->request->post()) && $despacho->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestorDespachos = new GestorDespachos();
            $resultado = $gestorDespachos->Alta($despacho);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $pedido->Dame();

            $despacho->IdPedido = $pedido->IdPedido;
            $despacho->LineasDespacho = $pedido->LineasPedido;

            return $this->renderAjax('/despachos/alta', [
                    'pedido' => $pedido,
                    'model' => $despacho,
                    'titulo' => 'Despachar'
            ]);
        }
    }
    
    public function actionModificar($id = null)
    {
        if (!in_array('AltaPedido', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
          
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = 'json';
            
            $pedido = new Pedidos();
            $pedido->IdPedido = Yii::$app->request->post('idPedido');
            $pedido->IdCliente = Yii::$app->request->post('idCliente');
            $pedido->IdUsuario = Yii::$app->request->post('idUsuario');
            $pedido->FechaEntrega = Yii::$app->request->post('fechaEntrega');
            
            if ($pedido->IdCliente == 0) {
                $pedido->IdCliente = null;
            }
            if ($pedido->IdUsuario == 0) {
                $pedido->IdUsuario = null;
            }
            
            $pedido->LineasPedido = json_encode(Yii::$app->request->post('lineas'));
            
            $gestor = new GestorPedidos();
            $resultado = $gestor->Modificar($pedido);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => null];
            }
        } else {
            if (!in_array('AltaPedido', Yii::$app->session->get('Permisos'))) {
                return ['error' => 'No tiene permisos para realizar esta acción.'];
            }
         
            $pedido = new Pedidos();
            $pedido->IdPedido = $id;
            $pedido->Dame();
            $pedido->FechaEntrega = date('Y-m-d', strtotime($pedido->FechaEntrega));
            
            $usuario = new Usuarios();
            $usuario->IdUsuario = Yii::$app->user->id;
            $usuario->Dame();
            $pedido->IdUsuario = $usuario->IdUsuario;
            
            return $this->render('alta', [
                        'model' => $pedido,
                        'usuario' => $usuario,
            ]);
        }
    }
    
    public function actionAnular($id = null)
    {
        if (!in_array('AnularPedido', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
          
        $pedido = new Pedidos();
        if (intval($id)) {
            $pedido->IdPedido = $id;
        } else {
            throw new HttpException('422', 'El pedido indicado es inválido');
        }
        
        $pedido->Dame();
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorPedidos();
        $resultado = $gestor->Anular($pedido);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionFinalizarPedido()
    {
        Yii::$app->response->format = 'json';
        
        if (!in_array('AltaPedido', Yii::$app->session->get('Permisos'))) {
            return ['error' => 'No tiene permisos para realizar esta acción.'];
        }
        
        if (Yii::$app->request->isPost) {
            $pedido = new Pedidos();
            $pedido->IdCliente = Yii::$app->request->post('idCliente');
            $pedido->IdUsuario = Yii::$app->request->post('idUsuario');
            $pedido->FechaEntrega = Yii::$app->request->post('fechaEntrega');
            
            if ($pedido->IdCliente == 0) {
                $pedido->IdCliente = null;
            }
            if ($pedido->IdUsuario == 0) {
                $pedido->IdUsuario = null;
            }
            
            $pedido->LineasPedido = json_encode(Yii::$app->request->post('lineas'));
            
            $gestor = new GestorPedidos();
            $resultado = $gestor->Alta($pedido);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        }
    }
    public function actionBorrar($id)
    {
        if (!in_array('BorrarPedido', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $pedido = new Pedidos();
        if (intval($id)) {
            $pedido->IdPedido = $id;
        } else {
            throw new HttpException('422', 'El pedido indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorPedidos();
        $resultado = $gestor->Borrar($pedido);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    public function actionAutocompletarClientes($id = 0, $idZona = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $cliente = new Clientes();
            
            $cliente->IdCliente = $id;
            
            $cliente->Dame();
            
            $out = [
                'id' => $cliente->IdCliente,
                'text' => $cliente->Nombres.' '.$cliente->Apellidos
            ];
        } else {
            $gestor = new GestorClientes();
              
            $clientes = $gestor->Buscar($cadena, $idZona, 'N');
            
            $out = array();
            
            foreach ($clientes as $cliente) {
                $out[] =[
                    'id' => $cliente['IdCliente'],
                    'text' => $cliente['Nombres'] .' '. $cliente['Apellidos']
                ];
            }
        }
        
        return $out;
    }
}
