<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use common\models\forms\BusquedaForm;
use common\models\GestorRepartos;
use common\models\GestorDespachos;
use common\models\GestorPedidos;
use common\models\GestorVentas;
use common\models\GestorVehiculos;
use common\models\GestorArribos;
use common\models\GestorUsuarios;
use common\models\GestorZonas;
use common\models\GestorReportes;
use common\models\Repartos;
use common\models\Arribos;
use common\models\Despachos;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

class RepartosController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar(
    
        $IdUsuario = null,
    
        $IdZona = null,
    
        $FechaInicio = null,
                        $FechaFin = null,
    
        $Estado = 'A'
    
    ) {
        if (!in_array('BuscarRepartos', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $IdUsuario = $busqueda->Combo2;
            $IdZona = $busqueda->Combo3;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo2 = $IdUsuario;
            $busqueda->Combo3 = $IdZona;
        }
          
        $gestorUsuarios  = new GestorUsuarios();
        $usuarios = $gestorUsuarios->Buscar('', 'N');
        
        $gestorZonas = new GestorZonas();
        $zonas = $gestorZonas->Buscar();
        
        $gestorRepartos = new GestorRepartos();
        $repartos = $gestorRepartos->BuscarAvanzado($IdUsuario, $IdZona, $FechaInicio, $FechaFin, $Estado);
        
        $paginado->totalCount = count($repartos);
        $repartos = array_slice($repartos, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $repartos,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'usuarios' => $usuarios,
                    'zonas' => $zonas
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaReparto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $reparto = new Repartos();
        $reparto->setScenario(Repartos::_ALTA);
        if ($reparto->load(Yii::$app->request->post()) && $reparto->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorRepartos();
            $resultado = $gestor->Alta($reparto);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $vehiculos = Arrayhelper::map(GestorVehiculos::Buscar('', 'N'), 'IdVehiculo', 'Vehiculo');
            $reparto->FechaReparto = date('Y-m-d');
            return $this->renderAjax('alta', [
                        'model' => $reparto,
                        'vehiculos' => $vehiculos,
                        'titulo' => 'Alta de reparto',
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarReparto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $reparto = new Repartos();
        $reparto->setScenario(Repartos::_MODIFICAR);
        
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
        
        if ($reparto->load(Yii::$app->request->post()) && $reparto->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorRepartos();
            $resultado = $gestor->Modificar($reparto);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $reparto->Dame();
            $vehiculos = Arrayhelper::map(GestorVehiculos::Buscar('', 'N'), 'IdVehiculo', 'Vehiculo');
            return $this->renderAjax('alta', [
                        'model' => $reparto,
                        'vehiculos' => $vehiculos,
                        'titulo' => 'Alta de reparto',
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarReparto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $reparto = new Repartos();
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El repartp indicado es inválido.');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $reparto->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaReparto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $reparto = new Repartos();
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido.');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $reparto->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionRepetir($id)
    {
        $reparto = new Repartos();
        $reparto->setScenario(Repartos::_ALTA);
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
        
        if ($reparto->load(Yii::$app->request->post()) && $reparto->validate()) {
            Yii::$app->response->format = 'json';
            $gestor = new GestorRepartos();
            $resultado = $gestor->Alta($reparto);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        }
        {
            $reparto->Dame();
            $vehiculos = Arrayhelper::map(GestorVehiculos::Buscar('', 'N'), 'IdVehiculo', 'Vehiculo');
            $reparto->FechaReparto = date('Y-m-d');
            return $this->renderAjax('alta', [
                        'model' => $reparto,
                        'vehiculos' => $vehiculos,
                        'titulo' => 'Repetir reparto',
            ]);
        }
    }
    
    public function actionPlanillas($id)
    {
        $reparto = new Repartos();
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
         
        $reparto->Dame();
        
        return $this->renderAjax('planillas', [
                    'model' => $reparto,
                    'titulo' => 'Planillas',
        ]);
    }
    
    public function actionImprimirPedido($id)
    {
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
        
        $tabla = $reparto->DamePlanillaPedido();
        
        $modeloReporte['CssHeaders'] = ' text-rotate: 90;font-size: 8px;text-align:center; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 8px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = false;
        $modeloReporte['Orientation'] = 'L';
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Pedido Reparto",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Pedidos",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
    
    public function actionImprimirPuntual($id)
    {
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
        
        $tabla = $reparto->DamePlanillaPuntual();
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
         
        $modeloReporte['CssHeaders'] = 'font-size: 8px;text-align:center; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 8px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = false;
        $modeloReporte['Orientation'] = 'P';
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Planilla puntual",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Puntual",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
    
    public function actionImprimirVentas($id)
    {
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
        
        $tabla = $reparto->DamePlanillaVentas();
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
         
        $modeloReporte['CssHeaders'] = 'font-size: 8px;text-align:center; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 8px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = false;
        $modeloReporte['Orientation'] = 'L';
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Planilla ventas",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Puntual",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
    
    public function actionImprimirVentasPrecios($id)
    {
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
        
        $tabla = $reparto->DamePlanillaVentasPrecios();
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
         
        $modeloReporte['CssHeaders'] = 'font-size: 8px;text-align:center; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 8px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = false;
        $modeloReporte['Orientation'] = 'L';
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Planilla ventas",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Puntual",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
    
    public function actionImprimirSalados($id)
    {
        //Salados
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
          
        $tabla = $reparto->DamePlanillaSalados();
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
        
        $modeloReporte['CssHeaders'] = 'font-size: 8px;text-align:center; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 8px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = false;
        $modeloReporte['Orientation'] = 'L';
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Planilla salados",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Salados",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Salados.pdf";
        return $pdf->render();
    }
    
    public function actionImprimirDulces($id)
    {
        //Dulces
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
        
        $tabla = $reparto->DamePlanillaDulces();
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
        
        
        $modeloReporte['CssHeaders'] = 'font-size: 8px;text-align:center; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 8px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = false;
        $modeloReporte['Orientation'] = 'L';
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Planilla Dulces",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Dulces",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Dulces.pdf";
        return $pdf->render();
    }
    
    
    public function actionImprimirCobranzas($id)
    {
        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();
        
        $tabla = $reparto->DamePlanillaCobranzas();
        
        //para que no salgan en la planilla
        $reparto->IdReparto= null;
        $reparto->IdVehiculo = null;
        $reparto->Estado = null;
        $reparto->IdUsuario = null;
        $reparto->IdZona= null;
        $reparto->FechaAlta = null;
        $reparto->Vehiculo = null;
        $reparto->FechaReparto = null;
        
        
        $modeloReporte['CssHeaders'] = 'font-size: 9px; border: 1px solid black;font-weight: bold;';
        $modeloReporte['CssCells'] = 'font-size: 9px;text-align:center;border: 1px solid black;font-weight: bold;';
        $modeloReporte['MuestraCeros'] = 'S';
        $modeloReporte['Orientation'] = 'P';
        
        $html = $this->renderPartial('/informes/informe-pdf', [
            'model' => $reparto,
            'tabla' => $tabla,
            'titulo' => "Planilla cobros",
            'modeloReporte' => $modeloReporte,
            'title' => 'Informe'
        ]);
            
        $orientation = [
            'L' => Pdf::ORIENT_LANDSCAPE,
            'P' => Pdf::ORIENT_PORTRAIT,
        ];
        
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => $orientation[$modeloReporte['Orientation']],
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'showImageErrors' => true,
                'title'=>"Informe Cobranzas",
            ]
        ]);
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
    
    public function actionDameReparto($id)
    {
        Yii::$app->response->format = 'json';
        
        $reparto = new Repartos();
        
        $reparto->IdReparto= $id;
        
        $reparto->Dame();
        
        return $reparto;
    }
    
    public function actionVerDespachosReparto($id)
    {
        $reparto = new Repartos();
        $reparto->setScenario(Repartos::_ALTA);
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
         
        $reparto->Dame();
        $gestorDespachos = new GestorDespachos();
        $despachos = $gestorDespachos->DameDespachosReparto($reparto->IdReparto);
        
        $gestorPedidos = new GestorPedidos();
        $pedidos = $gestorPedidos->DamePedidosReparto($reparto->IdReparto);
        
        return $this->renderAjax('despachos-reparto', [
                    'model' => $reparto,
                    'despachos' => $despachos,
                    'pedidos' => $pedidos,
                    'titulo' => 'Depachos',
        ]);
    }
    
    public function actionVerVentasReparto($id)
    {
        $reparto = new Repartos();
        $reparto->setScenario(Repartos::_ALTA);
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
            
        $reparto->Dame();
        $gestorVentas = new GestorVentas();
        $ventas = $gestorVentas->DameVentasReparto($reparto->IdReparto);
        return $this->renderAjax('ventas-reparto', [
                    'model' => $reparto,
                    'ventas' => $ventas,
                    'titulo' => 'Ventas',
        ]);
    }
    
    public function actionVerArriboReparto($id)
    {
        $arribo = new Arribos();
        $arribo->setScenario(Arribos::_ALTA);
        if (intval($id)) {
            $arribo->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
         
        $arribo = new Arribos();
        $arribo->IdReparto = $id;

        $reparto = new Repartos();
        $reparto->IdReparto = $id;
        $reparto->Dame();

        $gestorArribo = new GestorArribos();
        $arribo = $gestorArribo->DameArriboReparto($reparto->IdReparto);
        return $this->renderAjax('arribo-reparto', [
                    'reparto' => $reparto,
                    'arribo' => $arribo,
                    'titulo' => 'Depachos',
        ]);
    }
    
    public function actionDespachoMasivo($id)
    {
        $despacho = new Despachos();
        $reparto = new Repartos();
         
        if (intval($id)) {
            $reparto->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El pedido indicado es inválida');
        }
        
        $despacho->setScenario(Despachos::_ALTA);
        if ($despacho->load(Yii::$app->request->post()) && $despacho->validate()) {
            if (!in_array('AltaDespacho', Yii::$app->session->get('Permisos'))) {
                throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
            }
         
            Yii::$app->response->format = 'json';
            
            $gestorDespachos = new GestorDespachos();
            $resultado = $gestorDespachos->AltaDespachoMasivo($despacho);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $reparto->Dame();
  
            $despacho->IdReparto = $reparto->IdReparto;
            $lineasPedido = $reparto->DameLineasPedidosReparto();
            $pedidos = $reparto->DamePedidosReparto();
            
            $despacho->LineasDespacho = json_encode($lineasPedido);
            $despacho->Pedidos = json_encode($pedidos);

            $reparto->Dame();
            
            return $this->renderAjax('pedidos-reparto', [
                        'model' => $despacho,
                        'reparto' => $reparto,
                        'lineasPedido' => $lineasPedido,
                        'titulo' => 'Pedidos',
            ]);
        }
    }
     
    public function actionDamePosibleArriboReparto($id)
    {
        Yii::$app->response->format = 'json';
          
        $reparto = new Repartos();
        
        $reparto->IdReparto = $id;
         
        $lineasArribo = $reparto->DamePosibleArribo();
        
        return $lineasArribo;
    }
    
    public function actionArriboReparto($id)
    {
        if (!in_array('AltaArribo', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $arribo = new Arribos();
        $arribo->setScenario(Arribos::_ALTA);
        if (intval($id)) {
            $arribo->IdReparto = $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido');
        }
        
        if ($arribo->load(Yii::$app->request->post()) && $arribo->validate()) {
            Yii::$app->response->format = 'json';
            
            $lineasArribo = Yii::$app->request->post('lineas');

            $arribo->LineasArribo  = json_encode($lineasArribo);
            if (count($lineasArribo)== 0) {
                $arribo->LineasArribo  = null;
            }
            
            $gestor = new GestorArribos();
            $resultado = $gestor->Alta($arribo);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $reparto = new Repartos();
            $reparto->IdReparto = $id;
            $reparto->Dame();
            $arribo->FechaArribo = date('Y-m-d');
            return $this->renderAjax('/arribos/alta', [
                        'model' => $arribo,
                        'reparto' => $reparto,
                        'titulo' => 'Arribo',
            ]);
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarReparto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $reparto = new Repartos();
        if (intval($id)) {
            $reparto->IdReparto= $id;
        } else {
            throw new HttpException('422', 'El reparto indicado es inválido.');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorRepartos();
        $resultado = $gestor->Borrar($reparto);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletar($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $reparto = new Repartos();
            
            $reparto->IdRepearto = $id;
            
            $reparto->Dame();
            
            $out = [
                'id' => $reparto->IdReparto,
                'text' => $reparto->Zona . ' - ' . $reparto->Usuario . ' - ' . $reparto->FechaReparto,
            ];
        } else {
            $gestor = new GestorRepartos();
            
            $repartos = $gestor->Buscar($cadena, 'N');
            
            $out = array();
            
            foreach ($repartos as $reparto) {
                $out[] = [
                    'id' => $reparto['IdReparto'],
                    'text' => $reparto['Zona'] .' - '. $reparto['Usuario'].' - '.$reparto['FechaReparto']
                ];
            }
        }
        return $out;
    }
}
