<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use common\models\forms\BusquedaForm;
use common\models\GestorRendiciones;
use common\models\Rendiciones;
use frontend\models\UsosCaja;

class RendicionesController extends Controller
{
    public function actionIndex()
    { 
    }
    
    public function actionListar(  
        $IdVendedor = null, 
    
        $FechaInicio = null,
                        $FechaFin = null,
 
    
        $Estado = null
 
    
    ) {
        if (!in_array('BuscarRendiciones', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $IdVendedor = $busqueda->Combo3;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo3 = $IdVendedor;
        }
          
        $gestorVendedores = new GestorVendedores();
        $vendedores = $gestorVendedores->Buscar('', 'N');
          
        $gestorRendiciones = new GestorRendiciones();
        $rendiciones = $gestorRendiciones->BuscarAvanzado($IdVendedor, $FechaInicio, $FechaFin, $Estado);
        
        $paginado->totalCount = count($rendiciones);
        $rendiciones = array_slice($rendiciones, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('/rendiciones/index', [
                    'models' => $rendiciones,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'vendedores' => $vendedores,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaRendicion', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $rendicion = new Rendiciones();
        $rendicion->setScenario(Rendiciones::_ALTA);
        if ($rendicion->load(Yii::$app->request->post()) && $rendicion->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorRendiciones();
            $resultado = $gestor->Alta($rendicion);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            return $this->renderAjax('alta', [
                        'model' => $rendicion,
                        'titulo' => 'Alta de comprobante pago',
            ]);
        }
    }
}
