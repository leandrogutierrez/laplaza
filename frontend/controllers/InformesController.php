<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use common\models\GestorReportes;

class InformesController extends Controller
{
    public function actionIndex($id = null, $key = null)
    {
        $gestor = new GestorReportes();

        $menu = $gestor->ListarMenu();
        $model = null;
        $reporte = null;
        $parametros = null;
        $tabla = null;

        if (intval($id)) {
            $reporte = $gestor->DameModeloReporte($id);

            if (!in_array($reporte['Reporte'], Yii::$app->session->get('Permisos'))) {
                throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
            }

            if (!Yii::$app->request->isPost) {
                $parametros = $gestor->DameParametrosModeloReporte($id);

                $model = new DynamicModel(ArrayHelper::map($parametros, 'Parametro', 'ValorDefecto'));

                if ($key != null && Yii::$app->cache->get($key)) {
                    $model = Yii::$app->cache->get($key)['model'];
                    $tabla = Yii::$app->cache->get($key)['resultado'];
                }
            } else {
                $parametros = $gestor->DameParametrosModeloReporte($id, 'P');

                $model = new DynamicModel(ArrayHelper::getColumn($parametros, 'Parametro'));

                $model->addRule($model->attributes(), 'safe');

                // Agrego reglas de validación
                foreach ($parametros as $parametro) {
                    if ($parametro['Tipo'] == 'E') {
                        $model->addRule($parametro['Parametro'], 'integer');
                    } elseif ($parametro['Tipo'] == 'M') {
                        $model->addRule($parametro['Parametro'], 'match', ['pattern' => '/^[0-9]{1,12}((\.|,)[0-9]{0,2})?$/', 'message' => '{attribute} debe ser un número positivo.']);
                    }
                }
                
                if (count($parametros) == 0 || $model->load(Yii::$app->request->post()) && $model->validate()) {
                    $valores = [];
                    
                    foreach ($parametros as $parametro) {
                        if ($parametro['Tipo'] == 'F') {
                            $valor = $model->{$parametro['Parametro']} == null ? null : date("Y-m-d", strtotime(str_replace('/', '-', $model->{$parametro['Parametro']})));
                        } elseif ($parametro['Tipo'] == 'M') {
                            $valor = str_replace(',', '.', $model->{$parametro['Parametro']});
                        } elseif ($parametro['Tipo'] == 'A') {
                            $valor = ($model->{$parametro['Parametro']} == '') ? 0 : $model->{$parametro['Parametro']};
                        } else {
                            $valor = $model->{$parametro['Parametro']};
                        }

                        $valores[] = "'$valor'";
                    }

                    $output = null;

                    $key = Yii::$app->security->generateRandomString();
                    $idReporte = $reporte['IdModeloReporte'];
                    $cadena = implode(',', $valores);

                    $tabla = Yii::$app->cache->set($key, [
                        'model' => $model,
                        'resultado' => null,
                    ]);

                    $comando = "informes/generar '$key' '$idReporte' \"$cadena\" > /dev/null &";
                    
                    Yii::info($comando);
                    Yii::$app->consoleRunner->run($comando, $output);

                    Yii::$app->response->format = 'json';
                    return [
                        'error' => null,
                        'key' => $key
                    ];
                }
            }
        }
        return $this->render('index', [
                    'menu' => $menu,
                    'model' => $model,
                    'reporte' => $reporte,
                    'parametros' => $parametros,
                    'tabla' => $tabla,
        ]);
    }
    
    public function actionEstado($key)
    {
        Yii::$app->response->format = 'json';

        $resultado = Yii::$app->cache->get($key)['resultado'];

        return ['ready' => $resultado != null || is_array($resultado)];
    }
    
    public function actionAutocompletar($idModeloReporte, $nroParametro, $id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';

        $gestor = new GestorReportes();

        if ($id != 0) {
            $nombre = $gestor->DameParametroListado($idModeloReporte, $nroParametro, $id)['Nombre'];

            $out = ['id' => $id, 'text' => $nombre];
        } else {
            $elementos = $gestor->LlenarListadoParametro($idModeloReporte, $nroParametro, $cadena);

            $out = array();

            foreach ($elementos as $elemento) {
                $out[] = ['id' => $elemento['Id'], 'text' => $elemento['Nombre']];
            }
        }

        return $out;
    }
    
    public function actionImprimirInforme($id, $key)
    {
        $gestor = new GestorReportes();
        $modeloReporte = $gestor->DameModeloReporte($id);
        $parametros = $gestor->DameParametrosModeloReporte($id);
        
        if ($key != null && Yii::$app->cache->get($key)) {
            $model = Yii::$app->cache->get($key)['model'];
            $tabla = Yii::$app->cache->get($key)['resultado'];
        }
        
        foreach ($parametros as $parametro) {
            if ($parametro['ProcDame'] != null && $model->{$parametro['Parametro']} != 0) {
                $model->{$parametro['Parametro']} = $gestor->DameParametro($parametro['ProcDame'], $model->{$parametro['Parametro']})['Nombre'];
            }
        }
        
        $html = $this->renderPartial('@common/views/informes/informe-pdf', [
            'model' => $model,
            'tabla' => $tabla,
            'titulo' => $modeloReporte['NombreMenu'],
            'title' => 'Informe'
        ]);

        $pdf = Yii::$app->pdf;
        
        $pdf->marginLeft = 2;
        $pdf->marginTop = 4;
        $pdf->marginRight = 2;
        $pdf->marginBottom = 15;
        $pdf->configure([
            'title' => 'Informe ',
        ]);
        $pdf->content = $html;
        $pdf->execute('SetFooter', ['{PAGENO} de {nbpg}']);
        $pdf->filename = "Informe.pdf";
        return $pdf->render();
    }
}
