<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\models\forms\BusquedaForm;
use common\models\ComprobantesPago;
use common\models\GestorComprobantesPago;
use common\models\GestorClientes;
use common\models\GestorUsuarios;
use common\models\Clientes;

class ComprobantesPagoController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar(
    
        $IdCliente = null,
    
        $FechaInicio = null,
        
        $FechaFin = null,
    
        $Estado = null,
                       $IdUsuario = null
    
    ) {
        if (!in_array('BuscarComprobantesPago', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $IdCliente = $busqueda->Combo3;
            $IdUsuario = $busqueda->Combo2;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo3 = $IdCliente;
            $busqueda->Combo2 = $IdUsuario;
        }
          
        $gestorUsuarios  = new GestorUsuarios();
        $usuarios = $gestorUsuarios->Buscar('', 'N');
    
        $gestorClientes = new GestorClientes();
        $clientes = ArrayHelper::map($gestorClientes->Buscar(), 'IdCliente', 'ClienteZona');
  
        $gestorComprobantes = new GestorComprobantesPago();
        $comprobantes = $gestorComprobantes->BuscarAvanzado($IdUsuario, $IdCliente, $FechaInicio, $FechaFin, $Estado);
        
        $paginado->totalCount = count($comprobantes);
        $comprobantes = array_slice($comprobantes, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('/comprobantes/index', [
                    'models' => $comprobantes,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'clientes' => $clientes,
                    'usuarios' => $usuarios
        ]);
    }
    
    public function actionAlta($IdCliente = null)
    {
        if (!in_array('AltaVenta', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $pago = new ComprobantesPago();
        $pago->setScenario(ComprobantesPago::_ALTA);
        if ($pago->load(Yii::$app->request->post()) && $pago->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorComprobantesPago();
            $resultado = $gestor->Alta($pago);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $cliente = new Clientes();
            if (intval($IdCliente)) {
                $cliente->IdCliente = $IdCliente;
                $cliente->Dame();
                $pago->IdCliente = $IdCliente;
            }
            return $this->renderAjax('/comprobantes/alta', [
                        'model' => $pago,
                        'cliente' => $cliente,
                        'titulo' => 'Alta de comprobante pago',
            ]);
        }
    }
    
    public function actionAnular($id)
    {
        if (!in_array('AnularComprobantePago', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $comprobante = new ComprobantesPago();
        if (intval($id)) {
            $comprobante->IdComprobantePago = $id;
        } else {
            throw new HttpException('422', 'El comprobante indicado es inválido');
        }
        
        $comprobante->Dame();
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorComprobantesPago();
        $resultado = $gestor->Anular($comprobante);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
}
