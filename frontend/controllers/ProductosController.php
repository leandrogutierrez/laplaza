<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\models\forms\BusquedaForm;
use common\models\Productos;
use common\models\GestorProductos;
use common\models\GestorUnidades;
use common\models\GestorCategoriasProducto;

class ProductosController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarProductos', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IncluyeBajas = $busqueda->Check;
        }
        $gestor = new GestorProductos();
        $productos = $gestor->Buscar($Cadena, $IncluyeBajas);
        
        $paginado->totalCount = count($productos);
        $productos = array_slice($productos, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $productos,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaProducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $producto = new Productos();
        $producto->setScenario(Productos::_ALTA);
        
        if ($producto->load(Yii::$app->request->post()) && $producto->validate()) {
            $producto->Planillas = json_encode(Yii::$app->request->post('Planillas'));
            Yii::$app->response->format = 'json';
            $gestor = new GestorProductos();
            $resultado = $gestor->Alta($producto);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $gestorUnidades = new GestorUnidades();
            $unidades = ArrayHelper::map($gestorUnidades->Buscar('', 'N'), 'IdUnidad', 'Unidad');
            
            $gestorCategorias = new GestorCategoriasProducto();
            $categorias = ArrayHelper::map($gestorCategorias->Buscar('', 'N'), 'IdCategoriaProducto', 'Categoria');
            
            return $this->renderAjax('datos-producto', [
                        'model' => $producto,
                        'unidades'=>$unidades,
                        'categorias'=>$categorias,
                        'titulo' => 'Alta de productos',
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarProducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $producto = new Productos();
        $producto->setScenario(Productos::_MODIFICAR);
        if (intval($id)) {
            $producto->IdProducto = $id;
        } else {
            throw new HttpException('422', 'El producto indicado es inválido');
        }
        
        if ($producto->load(Yii::$app->request->post()) && $producto->validate()) {
            Yii::$app->response->format = 'json';
            $gestor = new GestorProductos();
            $resultado = $gestor->Modificar($producto);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $producto->Dame();
            $gestorUnidades = new GestorUnidades();
            $unidades = ArrayHelper::map($gestorUnidades->Buscar('', 'N'), 'IdUnidad', 'Unidad');
            
            $gestorCategorias = new GestorCategoriasProducto();
            $categorias = ArrayHelper::map($gestorCategorias->Buscar('', 'N'), 'IdCategoriaProducto', 'Categoria');
            
            return $this->renderAjax('datos-producto', [
                        'model' => $producto,
                        'unidades'=>$unidades,
                        'categorias'=>$categorias,
                        'titulo' => 'Modificación de productos',
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarProducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $producto = new Productos();
        if (intval($id)) {
            $producto->IdProducto = $id;
        } else {
            throw new HttpException('422', 'El producto indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $producto->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaProducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $producto = new Productos();
        if (intval($id)) {
            $producto->IdProducto = $id;
        } else {
            throw new HttpException('422', 'El producto indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $producto->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarProducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $producto = new Productos();
        if (intval($id)) {
            $producto->IdProducto = $id;
        } else {
            throw new HttpException('422', 'El producto indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorProductos();
        $resultado = $gestor->Borrar($producto);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAutocompletarProductos($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $producto = new Productos();
            
            $producto->IdProducto = $id;
            
            $producto->Dame();
            
            $out = [
                'id' => $producto->IdProducto,
                'text' => $producto->Descripcion
            ];
        } else {
            $gestor = new GestorProductos();
            
            $productos = $gestor->Buscar($cadena);
            
            $out = array();
            
            foreach ($productos as $producto) {
                $out[] = [
                    'id' => $producto['IdProducto'],
                    'text' => $producto['Descripcion']
                ];
            }
        }
        return $out;
    }
    
    public function actionDameProducto($id)
    {
        Yii::$app->response->format = 'json';
        
        $producto = new Productos();
        
        $producto->IdProducto = $id;
        
        $producto->Dame();
        
        return $producto;
    }

    public function actionDameProductoCliente($idProducto = null, $idCliente = null)
    {
        Yii::$app->response->format = 'json';
        
        $producto = new Productos();
        
        $producto->IdProducto = $idProducto;
        
        $producto->Dame($idCliente);
        
        return $producto;
    }

    
    public function actionListarSubproductos($id)
    {
        $producto = new Productos();
        if (intval($id)) {
            $producto->IdProducto = $id;
        } else {
            throw new HttpException('422', 'El producto indicado es inválido');
        }
        
        $producto->Dame();
        $subproductos = $producto->ListarSubproductos();
        
        return $this->render('subproductos', [
                    'model' => $producto,
                    'subproductos' => $subproductos
        ]);
    }
    
    public function actionAltaSubproducto($id)
    {
        if (!in_array('AltaSubproducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $producto = new Productos();
        if (intval($id)) {
            $producto->IdProducto = $id;
        } else {
            throw new HttpException('422', 'El producto indicado es inválido.');
        }
        
        if ($producto->load(Yii::$app->request->post()) && $producto->validate()) {
            Yii::$app->response->format = 'json';
            
            $resultado = $producto->AltaSubproducto($producto->IdSubproducto);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $producto->Dame();
            return $this->renderAjax('alta-subproducto', [
                        'model' => $producto
            ]) ;
        }
    }
    
    public function actionBorrarSubproducto($id, $idSubproducto)
    {
        if (!in_array('BorrarSubproducto', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        Yii::$app->response->format = 'json';
        $producto = new Productos();
        if (intval($id) && intval($idSubproducto)) {
            $producto->IdProducto = $id;
        } else {
            return ['error' => 'El producto o subproducto indicado es inválido'];
        }
        
        $resultado = $producto->BorrarSubproducto($idSubproducto);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
}
