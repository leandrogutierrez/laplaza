<?php
namespace frontend\controllers;

use common\components\FechaHelper;
use common\models\Clientes;
use common\models\forms\BusquedaForm;
use common\models\GestorClientes;
use common\models\GestorRepartos;
use common\models\GestorUsuarios;
use common\models\GestorVentas;
use common\models\Repartos;
use common\models\Ventas;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;

class VentasController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($IdUsuario = null, $FechaInicio = null, $FechaFin = null, $Estado = null, $IdCliente = null)
    {
        if (!in_array('BuscarVentas', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        $paginado = new Pagination();
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        if ($busqueda->load(Yii::$app->request->get()) && $busqueda->validate()) {
            $FechaInicio = $busqueda->FechaInicio;
            $FechaFin = $busqueda->FechaFin;
            $Estado = $busqueda->Combo;
            $IdCliente = $busqueda->Combo3;
            $IdUsuario = $busqueda->Combo2;
        } else {
            $busqueda->FechaInicio = $FechaInicio;
            $busqueda->FechaFin = $FechaFin;
            $busqueda->Combo = $Estado ;
            $busqueda->Combo3 = $IdCliente;
            $busqueda->Combo2 = $IdUsuario;
        }
        
        $gestorUsuarios  = new GestorUsuarios();
        $usuarios = $gestorUsuarios->Buscar('', 'N');
   
        $gestorClientes = new GestorClientes();
        $clientes = ArrayHelper::map($gestorClientes->Buscar(), 'IdCliente', 'ClienteZona');

        $gestor = new GestorVentas();
        $ventas = $gestor->BuscarAvanzado($IdUsuario, $FechaInicio, $FechaFin, $Estado, $IdCliente);
        
        $paginado->totalCount = count($ventas);
        $ventas = array_slice($ventas, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $ventas,
                    'busqueda' => $busqueda,
                    'paginado' => $paginado,
                    'clientes' => $clientes,
                    'usuarios' => $usuarios,
        ]);
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarVenta', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $venta = new Ventas();
        if (intval($id)) {
            $venta->IdVenta = $id;
        } else {
            throw new HttpException('422', 'La venta indicada es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorVentas();
        $resultado = $gestor->Borrar($venta);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionAlta($IdReparto = null)
    {
        if (!in_array('AltaVenta', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $venta = new Ventas();
        $reparto = new Repartos();
        $reparto->IdReparto = $IdReparto;
        $reparto->Dame();
        $venta->IdReparto = $IdReparto;
        return $this->render('alta', [
                    'model' => $venta,
                    'reparto' => $reparto,
        ]);
    }
    
    public function actionDetalles($id)
    {
        if (!in_array('BuscarVentas', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $venta = new Ventas();
        if (intval($id)) {
            $venta->IdVenta = $id;
        } else {
            throw new HttpException('422', 'La venta indicada es inválida');
        }
        
        $venta->Dame();
        
        return $this->renderAjax('datos-venta', [
                'model' => $venta,
                'titulo' => 'Datos de la venta'
        ]);
    }
    
    public function actionAnular($id)
    {
        if (!in_array('AnularVenta', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $venta = new Ventas();
        if (intval($id)) {
            $venta->IdVenta = $id;
        } else {
            throw new HttpException('422', 'La venta indicada es inválida');
        }
        
        $venta->Dame();
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorVentas();
        $resultado = $gestor->Anular($venta);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionFinalizarVenta()
    {
        Yii::$app->response->format = 'json';
        
        if (!in_array('AltaVenta', Yii::$app->session->get('Permisos'))) {
            return ['error' => 'No tiene permisos para realizar esta acción.'];
        }
        
        if (Yii::$app->request->isPost) {
            $venta = new Ventas();
            $venta->IdReparto = Yii::$app->request->post('idReparto');
            $venta->IdCliente = Yii::$app->request->post('idCliente');
            if ($venta->IdCliente == 0) {
                $venta->IdCliente = null;
            }
            if ($venta->IdReparto == 0) {
                $venta->IdReparto = null;
            }
            
            $venta->LineasVenta = json_encode(Yii::$app->request->post('lineas'));
            $venta->Importe = Yii::$app->request->post('importe');
            
            $gestor = new GestorVentas();
            $resultado = $gestor->Alta($venta);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        }
    }
    
    public function actionAutocompletarRepartos($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $reparto = new Repartos();
            
            $reparto->IdRepearto = $id;
            
            $reparto->Dame();
            
            $out = [
                'id' => $reparto->IdReparto,
                'text' => $reparto->Zona . ' - ' . $reparto->Usuario . ' - ' . $reparto->FechaReparto,
            ];
        } else {
            $gestor = new GestorRepartos();
            
            if (!in_array('PermisosAdministrador', Yii::$app->session->get('Permisos'))) {
                $repartos = $gestor->BuscarAvanzado(Yii::$app->user->identity->IdUsuario, null, null, null, 'A');
            } else {
                $repartos = $gestor->Buscar($cadena, 'N');
            }
            $out = array();
            
            foreach ($repartos as $reparto) {
                $out[] = [
                    'id' => $reparto['IdReparto'],
                    'text' => $reparto['Zona'] .' - '. $reparto['Usuario'].' - '.$reparto['FechaReparto']
                ];
            }
        }
        return $out;
    }
    
    
    public function actionAutocompletarClientes($id = 0, $idReparto = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $cliente = new Clientes();
            
            $cliente->IdCliente = $id;
            
            $cliente->Dame();
            
            $out = [
                'id' => $cliente->IdCliente,
                'text' => $cliente->Nombres . ' ' . $cliente->Apellidos . ' - ' . $cliente->Zona
            ];
        } else {
            $gestor = new GestorClientes();
            
            $reparto = new Repartos();
            $reparto->IdReparto = $idReparto;
            $reparto->Dame();
            
            $idZona = $reparto->IdZona;
            $clientes = $gestor->Buscar($cadena, $idZona, 'N');
            
            $out = array();
            
            foreach ($clientes as $cliente) {
                $out[] = [
                    'id' => $cliente['IdCliente'],
                    'text' => $cliente['Nombres'] .' '. $cliente['Apellidos'] .' - ' . $cliente['Zona']
                ];
            }
        }
        return $out;
    }
    
    public function actionDamePedidoFecha($id, $idReparto)
    {
        Yii::$app->response->format = 'json';
        
        $reparto = new Repartos();
        $reparto->IdReparto = $idReparto;
        $reparto->Dame();
         
        $cliente = new Clientes();
        $cliente->IdCliente = $id;
        
        $pedido = $cliente->DamePedidoFecha(FechaHelper::formatearDateMysql($reparto->FechaReparto));
        
        return $pedido;
    }
}
