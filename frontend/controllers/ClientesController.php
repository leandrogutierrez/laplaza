<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use common\models\forms\BusquedaForm;
use common\models\GestorClientes;
use common\models\GestorZonas;
use common\models\Clientes;
use common\models\Pedidos;

class ClientesController extends Controller
{
    public function actionIndex()
    {
        return $this->actionListar();
    }
    
    public function actionListar($Cadena = '', $IdReparto = null, $IncluyeBajas = 'N')
    {
        if (!in_array('BuscarClientes', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $busqueda = new BusquedaForm();
        
        if ($busqueda->load(Yii::$app->request->post()) && $busqueda->validate()) {
            $Cadena = $busqueda->Cadena;
            $IdReparto = $busqueda->Combo;
            $IncluyeBajas = $busqueda->Check;
        }
        
        $gestor = new GestorClientes();
        $models = $gestor->Buscar($Cadena, $IdReparto, $IncluyeBajas);
        
        $gestor = new GestorZonas();
        $zonas = $gestor->Buscar('', 'N');
        
                
        $paginado = new Pagination();
        $paginado->totalCount = count($models);
        $paginado->pageSize = Yii::$app->session->get('Parametros')['CANTFILASPAGINADO'];
        
        $models = array_slice($models, $paginado->page * $paginado->pageSize, $paginado->pageSize);
        
        return $this->render('index', [
                    'models' => $models,
                    'paginado' => $paginado,
                    'busqueda' => $busqueda,
                    'zonas' => $zonas,
        ]);
    }
    
    public function actionAlta()
    {
        if (!in_array('AltaCliente', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $cliente = new Clientes();
        $cliente->setScenario(Clientes::_ALTA);
        if ($cliente->load(Yii::$app->request->post()) && $cliente->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorClientes();
            $resultado = $gestor->Alta($cliente);
            if (substr($resultado, 0, 2) == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $gestor = new GestorClientes();
            $precios = ArrayHelper::map($gestor->ListasPrecios('', 'N'), 'IdListaPrecios', 'Descripcion');
            $gestor = new GestorZonas();
            $zonas = ArrayHelper::map($gestor->Buscar('', 'N'), 'IdZona', 'Zona');
            return $this->renderAjax('datos-cliente', [
                        'model' => $cliente,
                        'zonas' => $zonas,
                        'precios' => $precios,
                        'titulo' => 'Alta de cliente'
            ]);
        }
    }
    
    public function actionModificar($id)
    {
        if (!in_array('ModificarCliente', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $cliente = new Clientes();
        $cliente->setScenario(Clientes::_MODIFICAR);
        if (intval($id)) {
            $cliente->IdCliente = $id;
        } else {
            throw new HttpException('422', 'El cliente indicado es inválido');
        }
        
        if ($cliente->load(Yii::$app->request->post()) && $cliente->validate()) {
            Yii::$app->response->format = 'json';
            
            $gestor = new GestorClientes();
            $resultado = $gestor->Modificar($cliente);
            if ($resultado == 'OK') {
                return ['error' => null];
            } else {
                return ['error' => $resultado];
            }
        } else {
            $cliente->Dame();
            $gestor = new GestorClientes();
            $precios = ArrayHelper::map($gestor->ListasPrecios('', 'N'), 'IdListaPrecios', 'Descripcion');
             
            return $this->renderAjax('datos-cliente', [
                        'model' => $cliente,
                        'precios' => $precios,
                        'titulo' => 'Modificar de cliente'
            ]);
        }
    }
    
    public function actionActivar($id)
    {
        if (!in_array('ActivarCliente', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $cliente = new Clientes();
        if (intval($id)) {
            $cliente->IdCliente = $id;
        } else {
            throw new HttpException('422', 'El cliente indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $cliente->Activar();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDarBaja($id)
    {
        if (!in_array('DarBajaCliente', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $cliente = new Clientes();
        if (intval($id)) {
            $cliente->IdCliente = $id;
        } else {
            throw new HttpException('422', 'El cliente indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $resultado = $cliente->DarBaja();
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionBorrar($id)
    {
        if (!in_array('BorrarCliente', Yii::$app->session->get('Permisos'))) {
            throw new HttpException('403', 'No se tienen los permisos necesarios para ver la página solicitada.');
        }
        
        $cliente = new Clientes();
        if (intval($id)) {
            $cliente->IdCliente = $id;
        } else {
            throw new HttpException('422', 'El cliente indicado es inválido');
        }
        
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorClientes();
        $resultado = $gestor->Borrar($cliente);
        if ($resultado == 'OK') {
            return ['error' => null];
        } else {
            return ['error' => $resultado];
        }
    }
    
    public function actionDameCliente($id)
    {
        Yii::$app->response->format = 'json';
        
        $cliente = new Clientes();
        
        $cliente->IdCliente = $id;
        
        $cliente->Dame();
        
        return $cliente;
    }
    
    public function actionDamePedido($id)
    {
        Yii::$app->response->format = 'json';
         
        $cliente = new Clientes();
        
        $cliente->IdCliente = $id;
        
        $pedido = $cliente->DamePedido();
        
        return $pedido;
    }
    
      
    public function actionDamePedidoFecha($id, $fecha)
    {
        Yii::$app->response->format = 'json';
         
        $cliente = new Clientes();
        
        $cliente->IdCliente = $id;
        
        $pedido = $cliente->DamePedidoFecha($fecha);
        
        return $pedido;
    }
     
    public function actionDameUltimoPedido($id)
    {
        Yii::$app->response->format = 'json';
         
        $cliente = new Clientes();
        
        $cliente->IdCliente = $id;
        
        $pedido = $cliente->DameUltimoPedido();
        
        return $pedido;
    }
    
    public function actionDameVentasPendientes($id)
    {
        Yii::$app->response->format = 'json';
         
        $cliente = new Clientes();
        
        $cliente->IdCliente = $id;
        
        $ventas = $cliente->DameVentasPendientes();
        
        return $ventas;
    }
    
    public function actionAutocompletar($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $cliente = new Clientes();
            
            $cliente->IdCliente = $id;
            
            $cliente->Dame();
            
            $out = [
                'id' => $cliente->IdCliente,
                'text' => $cliente->Nombres.' '.$cliente->Apellidos  . ' - '. $cliente->Zona
            ];
        } else {
            $gestor = new GestorClientes();
            
            $clientes = $gestor->Buscar($cadena);
            
            $out = array();
            
            foreach ($clientes as $cliente) {
                $out[] =[
                    'id' => $cliente['IdCliente'],
                    'text' => $cliente['Nombres'].' '.$cliente['Apellidos'] . ' - '. $cliente['Zona']
                ];
            }
        }
        
        return $out;
    }
}
