<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Empresa;
use common\models\forms\CambiarPasswordForm;
use common\models\Usuarios;
use common\models\GestorUsuarios;
use common\models\GestorComprobantesPago;
use common\models\UsuariosSucursal;
use common\models\Cajas;

class UsuariosController extends Controller
{
    public function actionLogin()
    {
        // Si ya estoy logueado redirecciona al home
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $usuario = new Usuarios();
        $usuario->setScenario(Usuarios::_LOGIN);
        
        $empresa = new Empresa();
        Yii::$app->session->open();
        Yii::$app->session->set('Parametros', ArrayHelper::map($empresa->DameDatos(), 'Parametro', 'Valor'));
        
        if ($usuario->load(Yii::$app->request->post()) && $usuario->validate()) {
            $codigoLlave = Yii::$app->request->post('CodigoLlave');
            $login = $usuario->Login($usuario->Password);
          
            if ($login == 'OK') {
                Yii::$app->user->login($usuario);
                Yii::$app->session->set('Token', $usuario->Token);
                Yii::$app->session->set('Permisos', $usuario->DamePermisos());
                Yii::$app->session->set('codigoLlave', $codigoLlave);

                $caja = new Cajas();
                Yii::$app->session->set('UsoCaja', $caja->DameUsoCajaActual());
                
                // El usuario debe modificar el password
                if ($usuario->DebeCambiarPass == 'S') {
                    Yii::$app->getSession()->setFlash('info', 'Debe modificar su contraseña antes de ingresar.');
                    return $this->redirect('/usuarios/cambiar-password');
                } else {
                    return $this->redirect(Url::to(['/cajas/index']));
                }
            } else {
                $usuario->Password = null;
                Yii::$app->getSession()->setFlash('danger', $login);
                
                return $this->render('login', [
                            'model' => $usuario,
                ]);
            }
        } else {
            return $this->render('login', [
                        'model' => $usuario,
            ]);
        }
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
      
    public function actionCambiarPassword()
    {
        $form = new CambiarPasswordForm();
        $form->setScenario(CambiarPasswordForm::_CAMBIAR);

        $this->layout = 'login';

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $usuario = Yii::$app->user->identity;

            $mensaje = $usuario->CambiarPassword($usuario->Token, $form->Anterior, $form->Password_repeat);

            if ($mensaje == 'OK') {
                Yii::$app->user->logout();
                Yii::$app->session->setFlash('success', 'La contraseña fue modificada.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('danger', $mensaje);
                return $this->render('password', [
                            'model' => $form,
                ]);
            }
        } else {
            return $this->render('password', [
                        'model' => $form,
            ]);
        }
    }
    
    public function actionAutocompletar($id = 0, $cadena = '')
    {
        Yii::$app->response->format = 'json';
        
        if ($id != 0) {
            $usuario = new Usuarios();
            
            $usuario->IdUsuario = $id;
            
            $usuario->Dame();
            
            $out = [
                'id' => $usuario->IdUsuario,
                'text' => $usuario->NombreCompleto . ' ('.$usuario->Usuario.')'
            ];
        } else {
            $gestor = new GestorUsuarios();
            
            $usuarios = $gestor->Buscar($cadena, 'N');
            
            $out = array();
            
            foreach ($usuarios as $usuario) {
                $out[] = [
                    'id' => $usuario['IdUsuario'],
                    'text' => $usuario['NombreCompleto']. ' ('.$usuario['Usuario'].')'
                ];
            }
        }
        return $out;
    }
    
    public function actionDameUsuario($id)
    {
        Yii::$app->response->format = 'json';
        
        $usuario = new Usuarios();
        
        $usuario->IdUsuario = $id;
        
        $usuario->Dame();
        
        return $usuario;
    }
    
    public function actionDamePagos($id)
    {
        Yii::$app->response->format = 'json';
        
        $gestor = new GestorComprobantesPago();

        $pagos = $gestor->DamePagosUsuario($id);

         
        return $pagos;
    }
}
